<?php


return [
 	'settings' => [

 	    // ** IMPORTS
 	    'seo' => require PATH_ROOT . 'config/seo.php',
        'timezone' => 3, // 3 - server 2 - local

        // ** END IMPORTS

		'determineRouteBeforeAppMiddleware' => false,
		'displayErrorDetails' => in_array(env('APP_ENV'), ['dev', 'dev2', 'test']),

        'is_tasks_invoke_logging' => evalBool(getenv('TASKS_INVOKE_LOGGING')),
        'env' => getenv('APP_ENV'),
        'host' => getenv('APP_HOST'),
        'key' => getenv('APP_KEY'),
        'name' => getenv('APP_NAME'),
        'app_debug_data' => getenv('APP_DEBUG_DATA'),

        'upload_media_temp_folder' => getenv('UPLOAD_MEDIA_TEMP_FOLDER'),
        'temp_uploads_lifetime' => getenv('TEMP_UPLOADS_LIFETIME'),

        // used for validation
        'instruments' => 'eur_usd,gbp_usd,gbp_jpy,usd_chf,usd_jpy,usd_cad,aud_usd,nzd_usd,eur_jpy,aud_jpy,gbp_cad,gbp_chf,nzd_jpy,eur_aud,eur_gbp,eur_chf,cad_jpy,aud_nzd,aud_cad,aud_chf,chf_jpy,eur_nzd,eur_cad,cad_chf,gbp_nzd,nzd_chf,nzd_cad,gbp_aud',
        'iqoptions_instruments' => 'gbp_usd,eur_usd,gbp_jpy,eur_jpy,aud_jpy,usd_jpy,nzd_usd,aud_usd,eur_nzd',

		'oanda' => [
		    'api_key' => getenv('OANDA_API_KEY'),
            'account_id' => getenv('OANDA_ACCOUNT_ID')
        ],

		'mails' => [

            'sendgrid' => [
                'api_key' => getenv('SENDGRID_API_KEY')
            ],

            'custom' => [
                'name' => getenv('MAIL_NAME'),


                'host' => getenv('MAIL_HOST'),
                'username' => getenv('MAIL_USERNAME'),
                'password' => getenv('MAIL_PASSWORD'),
                'port' => getenv('MAIL_PORT'),
                'encryption' => getenv('MAIL_ENCRYPTION', 'tls'),
            ],

		],


        'week_days' => [
            1 => 'monday',
            2 => 'tuesday',
            3 => 'wednesday',
            4 => 'thursday',
            5 => 'friday',
            6 => 'saturday',
            7 => 'sunday',
        ],

        'db' => [
        	'driver'    => getenv('DB_DRIVER'),
        	'host'      => getenv('DB_HOST'),
        	'database'  => getenv('DB_NAME'),
        	'username'  => getenv('DB_USER'),
        	'password'  => getenv('DB_PASS'),
        	'charset'   => 'utf8',
        	'collation' => 'utf8_unicode_ci',
        	'prefix'    => '',
        ],


        'redis' => [
            'client' => getenv('REDIS_CLIENT', 'phpredis'),
            'default' => [
                'host' => getenv('REDIS_HOST', 'redis'),
                'password' => getenv('REDIS_PASSWORD'),
                'port' => getenv('REDIS_PORT', 6379),
                'database' => getenv('REDIS_DB', 0),
            ],
        ],

        'pinterest' => [
            'client_id' => getenv('PINTEREST_CLIENT_ID'),
            'client_secret' => getenv('PINTEREST_CLIENT_SECRET'),
        ],

        'twitter' => [
            'consumer_key' => getenv('TWITTER_CONSUMER_KEY'),
            'consumer_secret' => getenv('TWITTER_CONSUMER_SECRET'),
        ],

        'amazon' => [
            's3' => [
                'media_bucket' => getenv('AMAZON_MEDIA_UPLOAD_BUCKET'),
                'region' => getenv('AMAZON_S3_REGION'),
                'key'    => getenv('AMAZON_S3_KEY'),
                'secret' => getenv('AMAZON_S3_SECRET')
            ]
        ],


//        '$pair$ лонг',
//        '$pair$ шорт',
//        'флэш шорт',
//        'флэш лонг',
//
//

        'trademate' => [
            'name' => "Найтири",


            'contexts' => [

                'forex' => [
                    'name' => "форекс",

                    'commands' => [

                    ],

                    'flows' => [
                        'лонг' => [
                            'params' => ['pair'],
                        ]

                    ],
                    ''

                ],

                'planner' => [

                ]
            ],



            'commands' => [
                'тест' => 'test__command__task',
                'открыть лонг' => "",
                'открыть шорт' => "",
                'зыкрыть все' => 'close_all_command_task'

            ]
        ],

        'forex' => [
            'commands' => [
                'STATUS' => 0,
                "GET_HISTORY_TIMESERIA" => 1,
                "GET_OPEN_ORDERS" => 2,
                "OPEN_MARKET_ORDER" => 3,

                "CLOSE_ORDER" => 4,
                "CLOSE_OPEN_ORDERS" => 5,

                "SET_ORDER_BREAK_EVEN" => 6,
                "DEL_ORDER_BREAK_EVEN" => 7,

                "CHANGE_TAKE_PROFIT" => 8, // 8
                "GET_SCREENSHOTS" => 9, // 9
                "GET_SWINGS_STATE"=> 10, // 10
                "GET_CLOSED_ORDERS" => 11,
                "CALC_TLINE_VALUES" => 12,
                "OPEN_SYMBOL" => 13,
            ],

        ],


        'periods' => [1,5,15,30,60,240,1440],
        'screener_periods' => [1,5,15,60,240,1440],
        'ema_swing_periods' => [5,15,240],

        'be' => [
            'XAUUSD' => 50,
            'default' => 4
        ],

        // max 40
        'screener_points' => [
            'ema_swing' => 10,
            'swing20' => 5,
            'hzone' => 10,
            'tlb' => 5,
            'rsi_heiken' => 5,
            'force_histo' => 5,

//fg
//            'tlb144' => 8,
//            'tlb34' => 6,
//            'tlb' => 1,
//            'ema_swing' => 7,
//            'swing144' => 10,
//
//            'nl' => 2,
//            'nh' => 2,
//            'ema144_push' => 5,
//            'ema34_push' => 5,
//            'base_break' => 10,
//            'boogie' => 5,
//            'zero_shot' => 8,
//            'rsi_zero_shot' => 9,
//            'mirror_lev_push' => 6,
//            'rsi_mirror_lev_push' => 6,
//            'alligator' => 10,
        ],

        'attack_panel' => [
            'sl' => [
                'max' => 50,
                'min' => 5,
                'val' => 10,
                'step' => 1
            ],

            'risk' => [
                'max' => 5,
                'min' => 1,
                'val' => 1,
                'step' => 1
            ],
            // not used in game page
            'force_risk' => 0 // 1 percent
         ],


        'healthcheck' => [

        ],

        'indicator' => [
            'tag' => 't3rsi',
            'tag2' => 'taotra',
            'tag_slow' => 'qqe_slow',

            'tag3' => 'tsi_heiken',
            // 'tag' => 'force_histo',
            // 'min_max' => [0, 0]
            'min_max' => [-0,0]
        ],

        'pic_names' => [
            'pic1',
            //'pic2',
            //'pic3'
            //'pic3', 'pic4', 'pic5'
        ],
        'accounts' => [
            '50105751',
            //'365525'
        ],

        // #periods #history
        'charts' => [

            'storing_timeseries' => 0,

            'history' => [
                1 => 130,
                5 => 220,
                15 => 150,
                30 => 50,
                60 => 70,
                240 => 80,
                1440 => 50,
                10080 => 50
            ],

            'empty_bars' => [
                1 => 20,
                5 => 10,
                15 => 10,
                30 => 10,
                60 => 10,
                240 => 5,
                1440 => 5,
                10080 => 10,
            ],


//            'big_periods' => [1, 15, 1440],
//            'big_periods2' => [1, 15, 240],
//            'small_periods'    => [1, 5, 60],
//            'tiny_periods' => [1, 5, 15],



            //#periods-view
            'all_periods' => [10080, 1440, 240, 60, 30, 15, 5, 1],
            'big_periods' => [1440, 240, 15, 1],
            'mt4_periods' => [1440, 240, 60, 15, 5, 1],

            'big_periods2' => [60, 15, 5, 1],
            'small_periods' => [240, 15, 60],
            'tiny_periods' => [15, 5, 1],
            'pivot_periods' => [1440, 240, 15],

            'markup_periods' => [1440, 240, 15, 1]
        ],

        'points' => [
            'swing34' => 5,
            'swing144' => 10,
            'tlb' => 1,
            'tlb34' => 2,
            'tlb144' => 3,
            'nl' => 2,
            'nh' => 2,
            'ema34_push' => 5,
            'ema144_push' => 5,
            'base_break' => 10,
            'ema_swing34' => 10,
            'zero_shot' => 15,
            'rsi_zero_shot' => 15,
            'mirror_lev_push' => 7,
            'rsi_mirror_lev_push' => 7,
        ],

        'pairs_list' => [

            //"US_DX",
            "XAUUSD",
            "XAUEUR",
            "GBPJPY",
            "EURJPY",
            "BTCUSD",
            "GBPUSD",

//            "USDJPY",
//            "CADJPY",
//            "AUDJPY",
//            "EURUSD",
//            "USDCHF",
//            "USDCAD",
//            "GBPCHF",
//            "NZDUSD",
//            "AUDUSD",
//            "NZDJPY",
//            "AUDJPY",
//            'CHFJPY',
//            'CADJPY',
        ],

        // #symbols
        'screenshots' => [
            'periods' => [1,5,15,60,240,1440],
            'tags' => ['main'],

            'crop_percent' => [
                1440 => 30,
                240 => 30,
                60 => 30,
                30 => 30,
                15 => 30,
                5 => 170,
                1 => 60,
            ],
        ]
	]
];
