<?php

//
// Add this line to your crontab file:
//
// * * * * * cd /path/to/project && php jobby.php 1>> /dev/null 2>&1
//

use App\Tasks\ForexCore\clean__trash__task;

require_once __DIR__ . '/vendor/autoload.php';

$jobby = new \Jobby\Jobby();

define('LOGS', 'logs/' );
$commandsLogPath = LOGS. 'jobby.log';

//
//$jobby->add('CommandExample', array(
//    'command' => 'php partisan run:test',
//    'schedule' => '* * * * *',
//    'output' => $commandsLogPath,
//    'enabled' => false,
//));
//
//
//$jobby->add('it-runs-php-stories', array(
//    'command' => 'php partisan run:stories',
//    'schedule' => '* * * * *',
//    'output' => $commandsLogPath,
//    'enabled' => false,
//));

$jobby->add('clean trash task', array(
    'command' => function() {
        echo "Jobby started: " . now(). "\n";

        $res = task(new clean__trash__task, [

        ]);
        return true;
    },
    'schedule' => '* * * * 1',
    'output' => $commandsLogPath,
    'enabled' => true,
));


$jobby->add('it-downloads-future-candles', array(
    'command' => 'php partisan download:future:candles:all:instruments',
    'schedule' => '* * * * *',
//    'output' => LOGS . 'DownloadFutureCandlesForAllInsturumentsCommand.log',
    'enabled' => true,
    'debug' => false
));


//
//$jobby->add('ProcessQueuedPosts', array(
//    'command' => 'php partisan process:posts:publishing:queue',
//    'schedule' => '* * * * *',
//    'output' => LOGS . 'ProcessQueuedPosts.log',
//    'enabled' => true,
//));


//
//$jobby->add('ProcessPostsScheduledTimeCommand', array(
//    'command' => 'php partisan download:candels:all:instruments',
//    'schedule' => '* * * * *',
//    'output' => LOGS . 'ProcessPostsScheduledTimeCommand.log',
//    'enabled' => true,
//));





return $jobby->run();
