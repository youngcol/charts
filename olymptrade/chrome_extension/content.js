// Listen for messages
chrome.runtime.onMessage.addListener(function (msg, sender, sendResponse) {
    // If the received message has the expected format...
    if (msg.text === 'report_back') {
        // Call the specified callback, passing
        // the web-page's DOM content as argument
        // console.log(document.documentElement.innerHTML);

        var $dom = $(document.documentElement.innerHTML);

        // console.log('dom body');
        // console.log($('body', $dom));

        // console.log(document.documentElement.innerHTML);


        var $html = $(document.documentElement.innerHTML);
        var assets = [];

        console.log($('#pair-dashboard-container .pair-assets-item__inner', $html));

        //*[@id="pair-dashboard-container"]/div[2]/div/div/div[3]/div/div[1]/div/div/div[1]/div/div[2]/div[1]/button/div
        $('#pair-dashboard-container .pair-assets-item__inner', $html).each(function (key, item) {
            var $item = $(item);

            var title = $('.pair-assets-list-item__title', $item).text();
            var percent = parseInt($('.pair-assets-item__percent', $item).text());


            if (percent > 60)
            {
                assets.push(title + ',' + percent);
            }
        });

        console.log(assets);

        const req = new XMLHttpRequest();
        const baseUrl = "https://charts.loc/olymp?params="+assets.join(';');
        const urlParams = ``;


        req.open("GET", baseUrl, true);
        req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        req.send(urlParams);


        // sendResponse(document.documentElement.innerHTML);
    }
});
