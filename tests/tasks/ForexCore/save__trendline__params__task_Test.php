<?php
namespace tasks\ForexCore;

use App\Common\GateException;
use App\Common\Res;
use App\Models\ChartTrendline;

use App\Tasks\ForexCore\save__trendline__params__task;
use PHPUnit\Framework\TestCase;

/**
 * Tests for stand-alone functions.
 */
class save__trendline__params__task_Test extends TestCase {

    public function test_job_ok() {

        $data = ['type' => 'alert'];

        $res = task(new save__trendline__params__task, [
            2,
            $data
        ]);

        $r = repo(ChartTrendline::class);
        $model = $r->find(2);

        $this->assertSame($data, $model->getParams());
        $this->assertSame($res->res, 1);

//        $this->assertSame([2, 3, 4], $collection);
//        $this->assertInternalType('int', $carry);
//        $this->assertInternalType('int', $value);
    }

    public function test_gates()
    {
        $this->expectException(GateException::class);
        task(new save__trendline__params__task, [
            2222,
            []
        ]);
    }

}
