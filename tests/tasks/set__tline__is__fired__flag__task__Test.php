<?php
declare(strict_types=1);
namespace tasks;

use PHPUnit\Framework\TestCase;
use App\Common\GateException;


class set__tline__is__fired__flag__task__Test extends TestCase
{
    public function setUp():void
    {
        $this->markTestSkipped();
    }

    public function test_run_ok()
    {

        $res = task(new set__tline__is__fired__flag__task, [

        ]);

        // $this->assertSame($res, new Res));
        // $this->assertSame(1, 0);
        // $this->assertSame([2, 3, 4], $collection);
    //  $this->assertInternalType('int', $carry);
    //  $this->assertInternalType('int', $value);
    }

    public function test_fired_gates()
    {
        $this->expectException(GateException::class);
        task(new set__tline__is__fired__flag__task, [

        ]);

        // $this->assertSame(1, 0);
        // $this->assertSame([2, 3, 4], $collection);
        // $this->assertInternalType('int', $carry);
        // $this->assertInternalType('int', $value);
    }
}
