<?php

namespace models;
use Carbon\Carbon;
use PHPUnit\Framework\TestCase;


class ChartTrendlineTest extends TestCase
{
    protected function setUp():void
    {

    }

    public function test_normolize_time()
    {
        $trendline = new \App\Models\ChartTrendline();
        $trendline->period = 15;

        $this->assertSame("2023-04-14 08:45:00", $trendline->normolize_time(Carbon::parse("2023-04-14 08:57:00")));
        $this->assertSame("2023-04-14 08:45:00", $trendline->normolize_time(Carbon::parse("2023-04-14 08:48:00")));


        $trendline->period = 5;
        $this->assertSame("2023-04-14 08:45:00", $trendline->normolize_time(Carbon::parse("2023-04-14 08:48:00")));
        $this->assertSame("2023-04-14 08:50:00", $trendline->normolize_time(Carbon::parse("2023-04-14 08:54:00")));


        $trendline->period = 60;
        $this->assertSame("2023-04-14 08:00:00", $trendline->normolize_time(Carbon::parse("2023-04-14 08:48:00")));
        $this->assertSame("2023-04-14 09:00:00", $trendline->normolize_time(Carbon::parse("2023-04-14 09:15:00")));


        $trendline->period = 240;
        $this->assertSame("2023-04-14 04:00:00", $trendline->normolize_time(Carbon::parse("2023-04-14 05:48:00")));
        $this->assertSame("2023-04-14 08:00:00", $trendline->normolize_time(Carbon::parse("2023-04-14 09:15:00")));

        $this->assertSame("2023-04-14 00:00:00", $trendline->normolize_time(Carbon::parse("2023-04-14 00:48:00")));
        $this->assertSame("2023-04-14 04:00:00", $trendline->normolize_time(Carbon::parse("2023-04-14 07:15:00")));

        $this->assertSame("2023-04-14 12:00:00", $trendline->normolize_time(Carbon::parse("2023-04-14 13:15:00")));
        $this->assertSame("2023-04-14 16:00:00", $trendline->normolize_time(Carbon::parse("2023-04-14 17:15:00")));
        $this->assertSame("2023-04-14 20:00:00", $trendline->normolize_time(Carbon::parse("2023-04-14 22:15:00")));
        $this->assertSame("2023-04-15 00:00:00", $trendline->normolize_time(Carbon::parse("2023-04-15 01:15:00")));

        $trendline->period = 1440;
        $this->assertSame("2023-04-14 00:00:00", $trendline->normolize_time(Carbon::parse("2023-04-14 13:15:00")));
        $this->assertSame("2023-04-13 00:00:00", $trendline->normolize_time(Carbon::parse("2023-04-13 13:15:00")));

    }

    public function test_precise_sell()
    {

        $trendline = new \App\Models\ChartTrendline();
        $trendline->start_value = 166.1776;
        $trendline->end_value = 165.4080;
        $trendline->start_date = "2023-04-14 08:45:00" ;
        $trendline->end_date = '2023-04-14 11:00:00';
        $trendline->period = 15;

        $vals = [
                $trendline->getPointOnLine(Carbon::parse("2023-04-14 08:45:00")),
                $trendline->getPointOnLine(Carbon::parse("2023-04-14 09:00:00")),
                $trendline->getPointOnLine(Carbon::parse("2023-04-14 09:15:00")),
                $trendline->getPointOnLine(Carbon::parse("2023-04-14 09:30:00")),
                $trendline->getPointOnLine(Carbon::parse("2023-04-14 09:45:00")),
                $trendline->getPointOnLine(Carbon::parse("2023-04-14 10:00:00")),
                $trendline->getPointOnLine(Carbon::parse("2023-04-14 10:15:00")),
                $trendline->getPointOnLine(Carbon::parse("2023-04-14 10:30:00")),
                $trendline->getPointOnLine(Carbon::parse("2023-04-14 10:45:00")),
                $trendline->getPointOnLine(Carbon::parse("2023-04-14 11:00:00"))
        ];

        $val2 = [
                166.1776,
                166.0921,
                166.0066,
                165.9211,
                165.8356,
                165.75,
                165.6645,
                165.579,
                165.4935,
                165.408,
        ];

        $this->assertSame($vals, $val2);

    }


    public function test_precise_buy()
    {
        $start_time = Carbon::parse("2023-04-14 15:45:00");

        $trendline = new \App\Models\ChartTrendline();
        $trendline->start_value = 1.2462;
        $trendline->end_value = 1.2522;
        $trendline->start_date = $start_time->format('Y-m-d H:i:s') ;
        $trendline->end_date = '2023-04-14 17:15:00';
        $trendline->period = 15;

        $val2 = [
                1.2462,
                1.2472,
                1.2482,
                1.2492,
                1.2502,
                1.2512,
                1.2522,
        ];

        $vals = [
                $trendline->getPointOnLine(Carbon::parse("2023-04-14 15:45:00")),
                $trendline->getPointOnLine(Carbon::parse("2023-04-14 16:00:00")),
                $trendline->getPointOnLine(Carbon::parse("2023-04-14 16:15:00")),
                $trendline->getPointOnLine(Carbon::parse("2023-04-14 16:30:00")),
                $trendline->getPointOnLine(Carbon::parse("2023-04-14 16:45:00")),
                $trendline->getPointOnLine(Carbon::parse("2023-04-14 17:00:00")),
                $trendline->getPointOnLine(Carbon::parse("2023-04-14 17:15:00"))
        ];

        $this->assertSame($vals, $val2);
    }

//
//    public function test_breakout_big_periods()
//    {
//        $trendline = new \App\Models\ChartTrendline();
//
//        $trendline->start_value = 20.2884;
//        $trendline->start_date = '2022-09-09 15:00:00' ;
//
//        $trendline->end_value = 51.4929;
//        $trendline->end_date = '2022-09-09 18:30:00';
//
//        $this->assertSame($trendline->isSupportLine(), true);
//        $this->assertSame($trendline->isResistanceLine(), false);
//
//        $this->assertSame(NULL, $trendline->isBreakOut(52, Carbon::parse("2022-09-09 18:30:00")));
//        $this->assertSame(NULL, $trendline->isBreakOut(51.5, Carbon::parse("2022-09-09 18:30:00")));
//
//        $this->assertSame(0, $trendline->isBreakOut(51.3, Carbon::parse("2022-09-09 18:30:00")));
//        $this->assertSame(0, $trendline->isBreakOut(51.48, Carbon::parse("2022-09-09 18:30:00")));
//        $this->assertSame(0, $trendline->isBreakOut(50, Carbon::parse("2022-09-09 18:30:00")));
//    }
//
//    public function test_breakout_big_periods2()
//    {
//        $trendline = new \App\Models\ChartTrendline();
//
//        $trendline->start_value = 13.5646;
//        $trendline->start_date = '2022-09-06 00:00:00' ;
//
//        $trendline->end_value = 68.8268;
//        $trendline->end_date = '2022-09-11 00:00:00';
//
//        $this->assertSame($trendline->isSupportLine(), true);
//        $this->assertSame($trendline->isResistanceLine(), false);
//
//        $this->assertSame(NULL, $trendline->isBreakOut(68.83, Carbon::parse($trendline->end_date)));
//        $this->assertSame(NULL, $trendline->isBreakOut(69.82, Carbon::parse($trendline->end_date)));
//
//        $this->assertSame(0, $trendline->isBreakOut(68.2, Carbon::parse($trendline->end_date)));
//        $this->assertSame(0, $trendline->isBreakOut(68.81, Carbon::parse($trendline->end_date)));
//        $this->assertSame(0, $trendline->isBreakOut(67.82, Carbon::parse($trendline->end_date)));
//    }

    public function test_breakout_buy_15min()
    {
        $trendline = new \App\Models\ChartTrendline();
        $trendline->start_value = 166.16;
        $trendline->end_value = 165.40;
        $trendline->period = 15;

        $trendline->start_date = '2023-04-14 08:45:00' ;
        $trendline->end_date = '2023-04-14 11:00:00';


        $this->assertSame($trendline->isSupportLine(), false);
        $this->assertSame($trendline->isResistanceLine(), true);

        $this->assertSame(165.9067, $trendline->getPointOnLine(Carbon::parse("2023-04-14 09:30:00")));
        $this->assertSame(null, $trendline->isBreakOut(165.89, Carbon::parse("2023-04-14 09:30:00")));
        $this->assertSame(1, $trendline->isBreakOut(165.91, Carbon::parse("2023-04-14 09:30:00")));

    }

    public function test_breakout_sell_15min()
    {
        // support line
        $trendline = new \App\Models\ChartTrendline();
        $trendline->start_value = 1.2189;
        $trendline->end_value = 1.2222;


        $trendline->start_date = '2023-03-24 13:45:00' ;
        $trendline->end_date = '2023-03-24 16:45:00';
        $trendline->period = 15;

        $this->assertSame($trendline->isSupportLine(), true);
        $this->assertSame($trendline->isResistanceLine(), false);

        $this->assertSame([
                1.2192,
                1.2197,
                1.2208,
        ], [
                $trendline->getPointOnLine(Carbon::parse("2023-03-24 14:10:00")),
                $trendline->getPointOnLine(Carbon::parse("2023-03-24 14:30:00")),
                $trendline->getPointOnLine(Carbon::parse("2023-03-24 15:30:00")),
        ]);

        $this->assertSame(3, $trendline->getPriceIndexFromTime(Carbon::parse("2023-03-24 14:30:00")));
        $this->assertSame(1.2197, $trendline->getPointOnLine(Carbon::parse("2023-03-24 14:30:00")));

        $this->assertSame(NULL, $trendline->isBreakOut(1.2200, Carbon::parse("2023-03-24 14:30:00")));
        $this->assertSame(0, $trendline->isBreakOut(1.2195, Carbon::parse("2023-03-24 14:30:00")));
    }
}
