
// https://laravel.com/docs/5.8/mix

let mix = require('laravel-mix');


var sassFiles = [
    './app_vue/scss/components.scss',
    './app_vue/buefy.scss',
    // './htdocs/app_/assets/scss/style.scss'
];

var appSassFolder = './htdocs/app_/assets/styles/';

sassFiles.map(function (fileName) {
    mix.sass(fileName, appSassFolder);
});


// compile css into single file for app dashboard
mix.styles([
    './htdocs/app_/assets/styles/buefy.css',
    './htdocs/app_/assets/styles/components.css',
    
], './htdocs/app_/build/all.css');


mix.js('./app_vue/app.js', './htdocs/app_/build');

mix.scripts([

    './app_vue/vendor/jquery-3.2.1.min.js',
    './app_vue/vendor/underscore.js',
    './app_vue/vendor/d3.v4.min.js',
    './app_vue/vendor/jquery-ui.js',

    // './app_vue/techan.js/dist/techan.js',
    //'./app_vue/techan.js/build/techan-bundle.js',

    // './app_vue/vendor/techan.min.js',
    './app_vue/global_helpers.js',

], './htdocs/app_/build/global_all.js');

mix.sourceMaps();



// Public app
mix
    .js('./app_vue/public_app.js', './htdocs/public/build')

    .sourceMaps();
