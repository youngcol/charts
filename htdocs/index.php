<?php
declare(strict_types=1);
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;


require '../app/server.php';

global $container;

AppFactory::setContainer($container);
$app = AppFactory::create();

//$route = $app->getRouteResolver();
//dump($route);echo "index.php:18"; exit;
//$app = new \Slim\App($container);
//var_dump($app->getContainer());echo "index.php:14"; exit;
//$app->add($afterMiddleware);
//$app->addErrorMiddleware(true, false, false);

require PATH_ROOT . 'app/middlewares.php';
require PATH_ROOT . 'app/routes.php';
require PATH_ROOT . 'app/routes_api.php';
//require PATH_ROOT . 'app/tasks_api_requests.php';

if (settings('env') === 'dev')
{
    require PATH_ROOT . 'app/routes_prototype.php';
}

// Run the app
$app->run();
