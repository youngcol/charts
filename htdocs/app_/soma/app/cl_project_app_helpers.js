/**
* Created with JetBrains PhpStorm.
* User: 379
* Date: 11.05.18
* Time: 10:36
* To change this template use File | Settings | File Templates.
*/

function initHeader(somaData, api, injector, $$, bbExplorer, bookmarks)
{
var contextsVue = new Vue({
    el: ".header .contexts",

    data: {
        newTabTitle: '',
        contexts: somaData.projectIde.contexts
    },
    created: function() {

        displayContextWindows(this, somaData, injector, $$, bbExplorer);

    },

    methods: {

        watchNewTabTitle: function(e) {

            var contexts = this.contexts;
            if (!this.newTabTitle.length) return;
            //-=-=-=-=-=-

            if (e.keyCode === 13) {
                api.call('POST', '/cl/project/context', {
                    projectId:   somaData.projectIde.id,
                    title:       this.newTabTitle
                }, {
                    done: function(resp)
                    {

                        contexts.push(resp.context);

                    },
                    fail: function()
                    {

                    }
                });

                this.newTabTitle = '';
            }
        },

        onClickClose: function(c) {

            if (!confirm('Delete context ?')) return;
            //-=-=-=-=-=-


            var contexts = this.contexts;

            this.contexts = _.reject(contexts, function(con) {
                return con.id == c.id;
            });

            api.call('DELETE', '/cl/project/context', {
                projectId:      somaData.projectIde.id,
                contextId:      c.id
            }, {
                done: function(resp)
                {

                },
                fail: function()
                {

                }
            });
        },

        onClickTab: function(c) {
            _.map(this.contexts, function(c) {
                c.active = 0;
            });
            c.active = 1;

            api.call('POST', '/cl/project/contexts', {
                projectId:   somaData.projectIde.id,
                contexts:    this.contexts
            }, {
                done: function(resp)
                {


                },
                fail: function()
                {

                }
            });

            displayContextWindows(this, somaData, injector, $$, bbExplorer, bookmarks);

        },

        editTabsMode: function() {

            $('.header').toggleClass('edit-tabs');

        }
    }
});

$(window).resize(function () {

    _.map($$.contexts, function(con) {
        con.ide.Resize();
        con.filetree.Resize();
    });
});


$('#header').click(function (e) {

    if($(e.target).parents('#header').length == 0)
    {
        $(this).toggleClass('exta-info');
    }


});

$('#header .src-link').click(function () {
    copyTextToClipboard($(this).data('src'));
    infoNotify('Copied');
})



_.map($$.contexts, function(con) {
    con.ide.Resize();
    con.filetree.Resize();
});

return {
    GetActiveIde: function()
    {
        var ret = null;

        _.map(contextsVue.contexts, function(c) {

            if (c.active == 1) {
                ret = $$.contexts[c.id].ide;
                return false;
            }
        });

        return ret;
    },

    GetLastFocusedEditor: function() {
        var ret = '';


        _.map(contextsVue.contexts, function(c) {

            if (c.active == 1) {
                ret = $$.contexts[c.id].ide.GetLastFocusedEditor();
                return false;
            }
        });

        return ret;
    }
};
}

function hideContextWindows(v, $$)
{

}



function onFiletreeMouseOver(context) {

    context.ide.FlipRight();
}

function onFiletreeMouseOut(context) {

    context.ide.FlipNormal();
}


function displayContextWindows(v, somaData, injector, $$, bbExplorer, bookmarks)
{
    var activeContext = null;

    _.map(v.contexts, function(c) {

        if (c.active == 1) {
            activeContext = c;
        }
    });

    if (!activeContext) return;
    //-=-=-=-=-=-

    _.map($$.contexts, function(con) {
        con.ide.Hide();
        con.filetree.Hide();
    });

    if (!$$.contexts[activeContext.id]) {

        $$.contexts[activeContext.id] = {
            filetree: injector.createInstance(
                app.services.filetree, {
                    contextId: activeContext.id,
                    projectId: somaData.project.id,
                    box:        '#center-content',
                    basepath: somaData.project.paths.src,
                    onSelectFile: function(filepath, e) {

                        var ide = $$.contexts[activeContext.id].ide;
                        var side = bbExplorer.GetOpenedSide();

                        if(side)
                        {
                            side = side === 'right' ? 'first' : 'second';
                        }

                        ide.OpenFile(filepath, side);
                        ide.Show();

                        // if(!ide.isOpenedFile(filepath))
                        // {
                        //
                        // }
                    },

                    onMouseOut: function () {
                        onFiletreeMouseOut($$.contexts[activeContext.id]);
                    },

                    onMouseOver: function () {
                        onFiletreeMouseOver($$.contexts[activeContext.id]);
                    },

                }),
            ide:  injector.createInstance(
                app.services.ide, {
                    box:        '#center-content',
                    project:    somaData.project,
                    contextId: activeContext.id,

                    onGutterClick: function(sel, fileName, editorPos) {

                        var pos = editorPos === 'left' ? 'right' : 'left';

                        if(sel.length)
                        {
                            bbExplorer.AddNewBbCode(sel, fileName, pos, {code: 1});
                        }
                    }

                }),
        };

    }

    $$.contexts[activeContext.id].ide.Show();
    $$.contexts[activeContext.id].filetree.Show();

}