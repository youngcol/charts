/**
 * Создание bb из папки проекта
 */
(function(app, soma_data) {

    'use strict';

    var $$ = {
        contexts: {},
    };

    app.e = {
        'START':                        'start',
        'FILE_CONTENT__CHANGED':'FILE_CONTENT__CHANGED'
//        'DOCUMENT__ON_CHANGE_POSITION': 'DOCUMENT__ON_CHANGE_POSITION',
    };

    var uniqId = 0;
    app.uniqId = function()
    {
        return uniqId++;
    };


    var APP = new soma.Application.extend({

        constructor: function()
        {
            soma.Application.call(this);

        },

        init: function()
        {
            this.injector.mapClass('api', app.services.api, true);
            this.injector.mapClass('route_variants', app.services.route_variants, true);
            this.injector.mapClass('file_navigation', app.services.file_navigation, true);

            var api = this.injector.createInstance(app.services.api);

            $$.bbExplorer = this.injector.createInstance(
                app.services.bbExplorer, {
                    box:        '#bbExplorer',
                    projectIde: soma_data.projectIde,

                    onSelectFile: function(filepath, e) {
//                        var ide = $$.contexts[activeContext.id].ide;
//                        ide.OpenFile(filepath);
//                        ide.Show();
                    }
                });

            $$.bookmarks = this.injector.createInstance(
                app.services.cl_bookmarks, {
                    box:        '#bookmarks',
                    projectIde: soma_data.projectIde,

                    onSelectBookmark: function(line) {

                        var ide = $$.header.GetActiveIde();
                        var editor = ide.GetLastFocusedEditorObject();



                        editor.ScrollToLine(line - 5 < 0 ? line : line - 5);
                    }
                });


            $$.file_navigation = this.injector.createInstance(
                app.services.file_navigation, {
                    box:        '#file-navigation-wrapper',
                    project: soma_data.project
                });


            $$.opened_files = this.injector.createInstance(
                app.services.ide_opened_files, {
                    box:        '#ide_opened_files',
                    project: soma_data.project,
                    projectIde: soma_data.projectIde,

                    onSelectFile: function (filepath) {

                        var ide = $$.header.GetActiveIde();
                        ide.OpenFile(filepath, 'left', {
                            focus_new_file: 1
                        });

                    }
                });


            $$.header = initHeader(soma_data, api, this.injector, $$, $$.bbExplorer, $$.bookmarks);
            $$.bbExplorer.Resize();

            $(window).resize(function () {
                $$.bbExplorer.Resize();
            });


            window.header = $$.header;
            window.api = api;
            window.bookmarks = $$.bookmarks;

        },
        start: function()
        {
            bindEvents();

            // есть ид bb
            if (soma_data.bb) {

                keydownEvent('F3');

            }
        }
    });


    var ideObj = new APP(soma_data);

    function keydownEvent(key, ctrlKey, altKey, e)
    {
        var ret = false;

        if (altKey) {
            switch(key)
            {
                case "1":
                {


                    ret = true;
                    break;
                }
                case "2":
                {

                    ret = true;
                    break;
                }
            }
        }

        if (key == 'w' && ctrlKey)
        {
            ret = true;
        }
        else if (key == 'q' && ctrlKey)
        {

            ret = true;
        }
        else if(key == 'Tab')
        {
            ret = true;

        }
        else if (key == 'F1')
        {
            ret = true;

            if(ctrlKey)
            {

            } else {

                var editor = $$.header.GetLastFocusedEditor();
                var side = editor == 'left' ? 'right' : 'left';
                $$.bbExplorer.Toggle(side);
            }

        }
        else if(key == 'F3')
        {
            ret = true;

            var editor = $$.header.GetLastFocusedEditor();
            var side = editor == 'left' ? 'right' : 'left';

            var ide = $$.header.GetActiveIde();
            var editor = ide.GetLastFocusedEditorObject();

            var fileData = editor.GetCurrentFileData();

            $$.bookmarks.Toggle(side, fileData);

        }
        else if(key == 'e' && ctrlKey)
        {
            e.preventDefault();

            var ide = $$.header.GetActiveIde();
            $$.opened_files.Toggle(ide.GetAllOpenedFiles());

        }
        else if(key == 'F4')
        {
            ret = true;
            $('#readme-win').toggleClass('hidden');
            $('body').toggleClass('no-overflow');


        }
        else if(key == 'F2')
        {

            var ide = $$.header.GetActiveIde();
            var editor = ide.GetLastFocusedEditorObject();

            if(!editor)
            {
                errorNotify('Cant get editor from ide');
                return false;
            }

            var fileData = editor.GetCurrentFileData();

            if(fileData)
            {

                $('#center-content').toggleClass('hidden');
                $('#file-navigation-wrapper').toggleClass('hidden');

                $$.bbExplorer.Hide();

                $$.file_navigation
                    .Toggle(fileData.filename, fileData.content, fileData.mode);

            }


            ret = true;
        }

        return ret;
    }

    function bindEvents()
    {
        $(window).keypress(function(e)
        {
            if (e.key == 'w' && e.ctrlKey)
            {
                e.preventDefault();
            }
        });

        $(window).keydown(function(e)
        {
            var process = keydownEvent(e.key, e.ctrlKey, e.altKey, e);

            if (process) {
                e.preventDefault();
            }
        });

    }

})(window.app = window.app || {}, soma_data);

app.s = {};
