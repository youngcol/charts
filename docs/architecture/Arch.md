
# Models

MediaFile - медиа файл загруженный на сервер
UserSocialAccount - аккаунт из соц. сетей юзера


Post - пост который публикуется
PostSheduledTime - публикация поста забронирована на конкретное время
PostQueuedPlaces - пост добавлен в общую очередь публикаций
PostPublishLog - лог публикации поста в отдельном соц. аккаунте


QueueTime - слот времени из очереди публикаций


### Создание поста 

При создании поста, может быть несколько случаев:
- отправить сразу в соц. сети
- добавить в глобальную очередь
- забронировать конкретное время публикации в календаре

__vo__:
post.creation.type => in:scheduled,queued,instantly,draft


__PublishParams__ - дополнительные параметры при публикации поста, такие как ид доски в pinterest
{
    'accounts' => [
        [
            'account_id' => '1',
            'board_ids' => ['851884154454087762'],
        ]
    ],
}


__CreatePostTask__ - создание поста и так же моделей определяющих его тип

code:
    create Post model
    if scheduled create PostScheduledTime
    if queued create PostQueuedPlace    
endcode:


__PublishToSocialAccountsTask__ - публикация поста в соц. сетях
Params: 
    Post $post - 
    $socialAccounts - массив моделей 
    PublishParams $publishParams

code:

foreach $socialAccounts
    publish to concrete social network 
    create PostPublishLog about this action succeded or not
    
endcode:

#### Очередб сообщений


__QueueTime__ - временной слот для отправления сообщений в очереди
    scheduled_time - in gmt timezone



### Отправление постов
 
**Commands:**
- ProcessPostsPublishingQueueCommand - обработать\отправить очередь постов
- ProcessPostsScheduledTimeCommand - обработать те посты которые забронированы на конкретное время
- TempUploadsTrashCollectCommand - удалить файлы которым больше суток 


__ProcessPostsPublishingQueueCommand__
- Получить за сегодня все неотправленные QueueTime (в queue_time_published_log f)
- Получить PostQueuedPlace c сортировкой по полю order для найденных 
- Отправить посты с post_id из PostQueuedPlace 


PublishToSocialAccountsTask:
    get QueueTime where scheduled_time <= now() grouped by user
    get PostQueuedPlace limit for this user
    

__ProcessPostsScheduledTimeCommand__
1)Получить все PostScheduledTime у которых время отправления меньше чем user_local_now()
2)Отправить посты из PostScheduledTime


### Отправление постов

