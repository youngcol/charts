# Slim Starter Pack

# run migrations
export PATH=/usr/local/php74/bin:$PATH
./partisan migrate:up


# test
phpunit --filter test_precise_buy

__A starter pack based on Slim Framework and some other stuff__

## Installation

* clone the repository (via git or client)
* `composer install`
* create the database
* create `.env` file from `.env.example`
* create the database
* `composer install`
* run the migrations via `php partisan migrate:up`
* run db seeds `php partisan run:seed`
* compile css and js`npm i && npm run dev`
* generate api documentation `npm install apidoc -g && php partisan generate:docs`
* generate phpdoc `php phpdox.phar`
* watch js, css on app and public `npm run watch`
* watch for admin pages `gulp watch`


### Useful links
* Slim Framework <https://www.slimframework.com/>
* Eloquent <https://laravel.com/docs/5.5/eloquent>
* https://cs.symfony.com/
* https://github.com/badoo/phpcf
* https://phpprofi.ru/blogs/post/52
* Api best practices https://code-maze.com/top-rest-api-best-practices/
* ApiDocJS http://apidocjs.com/
* Match array struct https://github.com/ptrofimov/matchmaker
* https://dev.to/biros/my-php-toolkit-to-build-a-quite-frameworkless-app-5f8g

### Frontend components links
* Models https://objectmodel.js.org/

### Partisan commands
Please use command `php partisan list` to see all available commands

## Job scripts


### Development
Please go to `/admin/dashboard` page to see `development` and `workspace` pages

### Packages
https://github.com/rakit/validation
https://github.com/dirkgroenen/pinterest-api-php
https://github.com/seregazhuk/php-pinterest-bot

### App structure
Some app structure points:

app/dependencies.php - dependencies container here
app/server.php - bootstrap point of the app  
app/validation.php - validation rules for inputs here
app/middlewares.php - all middlewares
app/routes.php - all web routes 
app/routes_api.php - all web routes 


# App structure rules
- services know nothing about models
- services, controllers cannot touch models
- services dont validate params, they like kids - trust anything
- tasks input only from value objects 
- only tasks can touch models
- model cannot modify another model
- model can modify only itself 
- business logic stores in tasks
- 

## Backend php stories

# Env variable values
dev, live_closed, live

## Webhooks dumper
To dump webhooks use next url `/webhook-sandbox/{hookName}`
and you can see results in Logs tab in `tag__webhook__{hookName}` files.


## Services
Ничего не знают о домене и моделях, не валидируют входные параметры,
не несут бизнес логики, только выполняют клнкетноедействие

Ничего не знают о приложении, просто набор дейтсвий, можно переносить между приложениями

## Camel case
Везде по максимуму используется при передаче ключей из массивов


# Laravel resource controller
GET	/photos	index	photos.index
GET	/photos/create	create	photos.create
POST	/photos	store	photos.store
GET	/photos/{photo}	show	photos.show
GET	/photos/{photo}/edit	edit	photos.edit
PUT/PATCH	/photos/{photo}	update	photos.update
DELETE	/photos/{photo}	destroy	photos.destroy


# Error and exceptions handling
Variable `env` can be `dev`, `live`.
In `live` mode tasks dont throw exceptions and only return resFail

# Redirect back with errors

# Backend helpers

Сохранить дамп переменной в файл
`
    store_dump_var($name)
    load_dump_var($name)
`
# helpers
Laravel helpers available:
- https://laravel.com/docs/5.1/helpers
- https://laravel.com/docs/5.8/collections#available-methods
- https://lodash.com/docs/4.17.11

# todo
- public theme https://www.brightlocal.com
- https://github.com/FriendsOfPHP/PHP-CS-Fixer


# Make docs for project
Backend:
how to install project and ready to dev
how to create migration
how to create model
how to create console command
how to create task
how to create api controller
how to create app page
how to create admin page
how to create new service
how to use workshop space to develop code

backend helpers + best practicies


Frontend:
how to setup project
how to create new app vue component + api calls


helpers + best practicies

# Frontend

## Run task
`
    async mounted()
    {
        var resSend = await this.runTask('Crm/Pull.create__contact__list__task', {
            subject: this.subject,
            to_emails: [this.toEmail],
            message: this.message,
        });
    }
`

# Fill with candle data
- `php partisan download:history:candles:all:instruments --instrument=eur_usd`
- `php partisan download:future:candles:all:instruments --instrument=gbp_usd`
- `php partisan download:future:candles:all:instruments --instrument=eur_usd`

- Run jobby every 40 seconds
watch -n40 php jobby.php

# Remove old history candles
- `php partisan delete:history:candles:all:instruments`


# RIPPER
https://github.com/vivazzi/JAson


https://www.quora.com/Where-can-I-get-FX-tick-data-feed
https://github.com/giuse88/duka
https://tradermade.com/signup
https://tickstory.com/
https://github.com/ismailfer/dukascopy-api-websocket
https://www.dukascopy.com/trading-tools/api/documentation/quotes
https://github.com/Leo4815162342/dukascopy-node
https://www.multicharts.com/features/data-feeds/
https://github.com/ismailfer/dukascopy-api-websocket
https://datafeed.dukascopy.com/datafeed/{PAIR}/{YEAR}/{MONTH}/{DAY}/{HOUR}h_ticks.bi5
https://github.com/after-the-sunrise/dukas-proxy
https://github.com/drui9/tickterial
https://stackoverflow.com/questions/41176164/decompress-and-read-dukascopy-bi5-tick-files

