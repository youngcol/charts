@extends("app.layout")

@section('content')
    <div class="container-flow">
        <section class="section-start1">
            <screenshots-tape
                :tape = "{{ json_encode($tape) }}"
                :original = "{{ json_encode($original) }}"
            >
            </screenshots-tape>

        </section>
    </div>
@endsection

@section('scripts')
    <script src="https://d3js.org/d3.v4.min.js"></script>
    <script src="http://techanjs.org/techan.min.js"></script>
@endsection

