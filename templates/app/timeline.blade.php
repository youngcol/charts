@extends("app.layout")

@section('content')
    <div class="container-flow">
        <section class="section-start1">
            <timeline
                :res-chart-groups = "{{ json_encode($chartGroups) }}"
                :timeline="{{json_encode($timeline)}}"
                min-latest-loaded-timeseria-date="{{ $minLatestLoadedTimeseriaDate }}"
            >
            </timeline>
        </section>
    </div>
@endsection

