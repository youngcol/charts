
@extends("app.layout")


@section('content')

    <!-- START NAV -->
{{--    <nav class="navbar is-white">--}}
{{--        <div class="container">--}}
{{--            <div class="navbar-brand">--}}
{{--                <a class="navbar-item brand-text" href="../">--}}
{{--                    Bulma Admin--}}
{{--                </a>--}}
{{--                <div class="navbar-burger burger" data-target="navMenu">--}}
{{--                    <span></span>--}}
{{--                    <span></span>--}}
{{--                    <span></span>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div id="navMenu" class="navbar-menu">--}}
{{--                <div class="navbar-start">--}}
{{--                    <a class="navbar-item" href="admin.html">--}}
{{--                        Home--}}
{{--                    </a>--}}
{{--                    <a class="navbar-item" href="admin.html">--}}
{{--                        Orders--}}
{{--                    </a>--}}
{{--                    <a class="navbar-item" href="admin.html">--}}
{{--                        Payments--}}
{{--                    </a>--}}
{{--                    <a class="navbar-item" href="admin.html">--}}
{{--                        Exceptions--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--            </div>--}}
{{--        </div>--}}
{{--    </nav>--}}
    <!-- END NAV -->
    <div class="container-flow">

        <section class="section-start">

            <div class="columns">

                <div class="column is-12">

{{--                    <section class="info-tiles">--}}
{{--                        <div class="tile is-ancestor has-text-centered">--}}
{{--                            <div class="tile is-parent">--}}
{{--                                <article class="tile is-child box">--}}
{{--                                    <p class="title">439k</p>--}}
{{--                                    <p class="subtitle">Users</p>--}}
{{--                                </article>--}}
{{--                            </div>--}}
{{--                            <div class="tile is-parent">--}}
{{--                                <article class="tile is-child box">--}}
{{--                                    <p class="title">59k</p>--}}
{{--                                    <p class="subtitle">Products</p>--}}
{{--                                </article>--}}
{{--                            </div>--}}
{{--                            <div class="tile is-parent">--}}
{{--                                <article class="tile is-child box">--}}
{{--                                    <p class="title">3.4k</p>--}}
{{--                                    <p class="subtitle">Open Orders</p>--}}
{{--                                </article>--}}
{{--                            </div>--}}
{{--                            <div class="tile is-parent">--}}
{{--                                <article class="tile is-child box">--}}
{{--                                    <p class="title">19</p>--}}
{{--                                    <p class="subtitle">Exceptions</p>--}}
{{--                                </article>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </section>--}}

                    {{--<section>--}}
                        {{--<div class="card">--}}
                            {{--<header class="card-header">--}}
                                {{--<p class="card-header-title"> Component </p> <a class="card-header-icon"><span class="icon"><i class="fa fa-angle-down"></i></span></a></header>--}}
                            {{--<div class="card-content">--}}
                                {{--<div class="content"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nec iaculis mauris.--}}
                                    {{--<a>@bulmaio</a>.--}}
                                    {{--<a>#css</a> <a>#responsive</a>--}}
                                    {{--<br> <small>11:09 PM - 1 Jan 2016</small></div>--}}
                            {{--</div>--}}
                            {{--<footer class="card-footer"><a class="card-footer-item">Save</a> <a class="card-footer-item">Edit</a> <a class="card-footer-item">Delete</a></footer>--}}
                        {{--</div>--}}
                    {{--</section>--}}

                    <section>
                        <div class="columns">
                            <div class="column is-6">

                                <div class="card events-card">
                                    <header class="card-header">
                                        <p class="card-header-title">
                                            Chart groups
                                        </p>

                                        {{--<a href="#" class="card-header-icon" aria-label="more options">--}}
                                          {{--<span class="icon">--}}
                                            {{--<i class="fa fa-angle-down" aria-hidden="true"></i>--}}
                                          {{--</span>--}}
                                        {{--</a>--}}
                                    </header>
                                    <div class="card-table">

                                        <div class="content">

                                            <table class="table is-fullwidth is-striped">
                                                <tbody>

                                                    @foreach($timelines as $timeline)
                                                        <tr>
                                                            <td width="5%">
                                                                <i class="fa fa-bell-o"></i>
                                                            </td>
                                                            <td>{{$timeline['id']}}</td>
                                                            <td>{{$timeline['name']}}</td>

                                                            <td>
                                                                <a target="_blank" class="button is-small is-primary"
                                                                   href="{{route('app.timeline', ['timeline' => $timeline['id']])}}">Open
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    @endforeach

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <footer class="card-footer">
                                        <a href="#" class="card-footer-item">View All</a>
                                    </footer>
                                </div>

                                <div class="card">
                                    <header class="card-header">
                                        <p class="card-header-title">
                                            Create chart group
                                        </p>
                                        {{--<a href="#" class="card-header-icon" aria-label="more options">--}}
                                          {{--<span class="icon">--}}
                                            {{--<i class="fa fa-angle-down" aria-hidden="true"></i>--}}
                                          {{--</span>--}}
                                        {{--</a>--}}
                                    </header>

                                    <div class="card-content">
                                        <div class="content">

                                            <div class="field">
                                                <div class="control has-icons-right">
                                                    <p class="control">
                                                    <span class="select">
                                                        <select v-model="newChartGroup.instrument" name="">
                                                            <option value="eur_usd">EUR/USD</option>
                                                            <option value="gbp_usd">GBP/USD</option>
                                                            <option value="gbp_jpy">gbp_jpy</option>
                                                            <option value="usd_chf">usd_chf</option>
                                                            <option value="usd_jpy">usd_jpy</option>
                                                            <option value="aud_usd">aud_usd</option>
                                                            <option value="eur_jpy">eur_jpy</option>
                                                            <option value="nzd_jpy">nzd_jpy</option>
                                                        </select>
                                                    </span>
                                                    </p>
                                                </div>
                                            </div>

                                            <div class="field">
                                                <div class="control has-icons-right">
                                                    <input v-model="newChartGroup.name" class="input"
                                                           type="text" placeholder="Chart group name">
                                                </div>
                                            </div>


                                            <a class="button is-small is-primary" href="#" @click="clickCreateChartGroup">Create</a>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            {{--<div class="column is-6">--}}

                                {{--<div class="card">--}}
                                    {{--<header class="card-header">--}}
                                        {{--<p class="card-header-title">--}}
                                            {{--User Search--}}
                                        {{--</p>--}}
                                        {{--<a href="#" class="card-header-icon" aria-label="more options">--}}
                                            {{--<span class="icon">--}}
                                            {{--<i class="fa fa-angle-down" aria-hidden="true"></i>--}}
                                            {{--</span>--}}
                                        {{--</a>--}}
                                    {{--</header>--}}
                                    {{--<div class="card-content">--}}
                                        {{--<div class="content">--}}
                                            {{--<div class="control has-icons-left has-icons-right">--}}
                                                {{--<input class="input is-large" type="text" placeholder="">--}}
                                                {{--<span class="icon is-medium is-left">--}}
                                                  {{--<i class="fa fa-search"></i>--}}
                                                {{--</span>--}}
                                                {{--<span class="icon is-medium is-right">--}}
                                                  {{--<i class="fa fa-check"></i>--}}
                                                {{--</span>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        </div>

                    </section>


                </div>

            </div>

        </section>
    </div>

@endsection
