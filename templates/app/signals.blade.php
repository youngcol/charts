@extends("app.layout")

@section('content')
    <div class="container-flow">
        <section class="section-start1">
            <signals-feed></signals-feed>
        </section>
    </div>
@endsection

@section('scripts')
    <script src="https://d3js.org/d3.v4.min.js"></script>
    <script src="http://techanjs.org/techan.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">
@endsection

