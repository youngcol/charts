@extends("app.layout")

@section('content')
    <div class="container">

        <section class="section-start">
            <h2 class="title has-text-centered">Contacts</h2>

            <crm-contacts>

            </crm-contacts>
        </section>
    </div>

@endsection
