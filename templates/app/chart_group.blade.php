@extends("app.layout")

@section('content')
    <div class="container-flow">
        <section class="section-start">
            <charts-group
                    :res-chart-group='{!! json_encode($resChartGroup) !!}'
                    :is-compact-view="false"
            ></charts-group>
        </section>
    </div>
@endsection

