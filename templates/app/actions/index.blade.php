
@extends("app.layout")

@section('content')

    <div class="container-fluid actions-page">

        <div class="columns">
            <div class="column is-3 p-left-20">
                <aside class="menu is-hidden-mobile">

                    @foreach($menu as $folder => $actions)
                        <p class="menu-label">
                            {{$folder}}
                        </p>

                        @foreach($actions as $action)
                            <ul class="menu-list">
                                <li class="{{isActive($currentAction, "$folder@$action")}}">
                                    <a href="{{route('app.actions.run', ['action' => "$folder@$action"])}}" class="{{isActive($page, 'account-settings')}}">{{$action}}</a>
                                </li>
                            </ul>
                        @endforeach
                    @endforeach
                </aside>
            </div>

            <div class="column is-9 p-left-50 p-right-25">
                @if(isset($vueComponentName))
                    <{{$vueComponentName}}

                    ></{{$vueComponentName}}>
                @endif
            </div>
        </div>
    </div>

@endsection
