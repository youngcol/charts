@extends("app.layout")

@section('content')
    <div class="container-flow">
        <section class="section-start1">
            <screenshots-overview
                :list = "{{ json_encode($list) }}"
                :list_flat = "{{ json_encode($list_flat) }}"
                :watchlist_up="{{ json_encode($watchlistUp) }}"
                :watchlist_down="{{ json_encode($watchlistDown) }}"
            >
            </screenshots-overview>
        </section>
    </div>
@endsection

