@extends("app.layout")

@section('content')
    <div class="container-flow">
        <section class="section-start1">
            <screenshots
                :pairs = "{{ json_encode($pairs) }}"
                :overview = "{{ json_encode($overview) }}"
                :tags="{{json_encode($tags)}}"
            >
            </screenshots>
        </section>
    </div>
@endsection

