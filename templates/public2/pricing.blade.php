
@extends("public2.layout2")

@section('content')
    <div id="page-container">
        <section id="page-content">

            <div class="panel double_cta " role="main">
                <div class="panel-content">

                    <div class="panel-grid">
                        <div class="grid">
                            <div class="split_icons flex">
                                <div>
                                    <img src="https://corp-brightlocal.imgix.net/2019/07/local-seo-tools.svg?auto=compress%2Cformat&amp;ixlib=php-1.2.1&amp;q=70&amp;s=5fb2324f2207d90cddde91c61836223d" alt="local seo tools">
                                </div>
                                <div>
                                    <img src="https://brightlocal-dev-corp-assets.s3.amazonaws.com/2019/02/pricing_Reputation.svg" alt="">
                                </div>
                                <div>
                                    <img src="https://brightlocal-dev-corp-assets.s3.amazonaws.com/2019/02/pricing_Leads.svg" alt="">
                                </div>
                                <div>
                                    <img src="https://brightlocal-dev-corp-assets.s3.amazonaws.com/2019/02/pricing_Cb.svg" alt="">
                                </div>
                            </div>
                            <h1>Try all our tools FREE for 14 days</h1>
                            <div class="dcta left"><a class="button green" href="https://tools.brightlocal.com/seo-tools/admin/sign-up/2003" title "14=" " day=" " free=" " trial"="">14 Day Free Trial<sub>No Card Needed</sub></a></div>
                        </div>
                        <div class="grid">
                            <div class="split_icons flex">
                                <div>
                                    <img src="https://brightlocal-dev-corp-assets.s3.amazonaws.com/2019/02/pricing_Cb.svg" alt="">
                                </div>
                            </div>
                            <h1>Start building Citations today</h1>
                            <div class="dcta right"><a class="button pink" href="https://tools.brightlocal.com/seo-tools/admin/sign-up" title "get=" " started=" " now"="">Get Started Now<sub>From $2 per citation</sub></a></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel pricing_panel white top-small bottom-small" role="main" id="">
                <div class="panel-content">

                    <div class="panel-row">
                        <h2>How much is BrightLocal after my free trial?</h2>
                        <div class="pay-toggle"><span class="bill-label">Billed Monthly</span>
                            <label class="switch">
                                <input class="switchbox" type="checkbox"><i onclick="togglePriceRates(event);"></i></label><span class="bill-label faded">Billed Annually (Get 2 months FREE)</span></div>
                    </div>

                    <div class="panel-grid span-4">
                        <div class="grid">
                            <div class="priceplan green">
                                <div class="heading">
                                    <h3>Single Business</h3>
                                    <h4 class="monthly">$<span>29</span>/mo</h4>
                                    <h4 class="yearly toggled-off">$<span>24.17</span>/mo</h4><a href="#" onclick="togglePriceRates(event); return false;" class="monthly">(Go Annual - get 2 months free!)</a><a href="#" onclick="togglePriceRates(event); return false;" class="yearly toggled-off">($290/year - you save $58)</a></div>
                                <div class="body">
                                    <p><strong>Perfect for small businesses with up to 3 locations.<br><br><a class="button green" style="font-size:16px;" href="https://tools.brightlocal.com/seo-tools/admin/sign-up/2001">Try for free</a><br></strong></p>
                                    <div class="footer">
                                        <table class="features">
                                            <tbody>
                                            <tr>
                                                <td><b>Local SEO Tools</b></td>
                                                <td><img class="tick lazyloaded" data-src="https://cdn.brightlocal.com/wp-content/themes/brightlocal/assets/images/check-green.svg" alt="yes" src="https://cdn.brightlocal.com/wp-content/themes/brightlocal/assets/images/check-green.svg"></td>
                                            </tr>
                                            <tr>
                                                <td>- Local Search Ranking reports</td>
                                                <td>3</td>
                                            </tr>
                                            <tr>
                                                <td>- Citation Tracker Reports</td>
                                                <td>3</td>
                                            </tr>
                                            <tr>
                                                <td>- Google My Business Audits</td>
                                                <td>10</td>
                                            </tr>
                                            <tr>
                                                <td>- Local SEO Audit Reports</td>
                                                <td>1</td>
                                            </tr>
                                            <tr>
                                                <td><b>Reputation Manager</b></td>
                                                <td><img class="tick lazyloaded" data-src="https://cdn.brightlocal.com/wp-content/themes/brightlocal/assets/images/check-green.svg" alt="yes" src="https://cdn.brightlocal.com/wp-content/themes/brightlocal/assets/images/check-green.svg"></td>
                                            </tr>
                                            <tr>
                                                <td>- Monitor Reviews</td>
                                                <td>3</td>
                                            </tr>
                                            <tr>
                                                <td>- Get Reviews</td>
                                                <td>1</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid">
                            <div class="priceplan green">
                                <div class="heading">
                                    <h3>Multi Business</h3>
                                    <h4 class="monthly">$<span>49</span>/mo</h4>
                                    <h4 class="yearly toggled-off">$<span>40.83</span>/mo</h4><a href="#" onclick="togglePriceRates(event); return false;" class="monthly">(Go Annual - get 2 months free!)</a><a href="#" onclick="togglePriceRates(event); return false;" class="yearly toggled-off">($490/year - you save $98)</a></div>
                                <div class="body">
                                    <p><strong>For mid-sized businesses and growing agencies managing up to 6 locations.<br><br><a class="button green" style="font-size:16px;" href="https://tools.brightlocal.com/seo-tools/admin/sign-up/2002">Try for free</a><br></strong></p>
                                    <div class="footer">
                                        <table class="features">
                                            <tbody>
                                            <tr>
                                                <td><b>Local SEO Tools</b></td>
                                                <td><img class="tick lazyloaded" data-src="https://cdn.brightlocal.com/wp-content/themes/brightlocal/assets/images/check-green.svg" alt="yes" src="https://cdn.brightlocal.com/wp-content/themes/brightlocal/assets/images/check-green.svg"></td>
                                            </tr>
                                            <tr>
                                                <td>- Local Search Ranking Reports</td>
                                                <td>6</td>
                                            </tr>
                                            <tr>
                                                <td>- Citation Tracker Reports</td>
                                                <td>6</td>
                                            </tr>
                                            <tr>
                                                <td>- Google My Business Audits</td>
                                                <td>30</td>
                                            </tr>
                                            <tr>
                                                <td>- Local SEO Audit Reports</td>
                                                <td>6</td>
                                            </tr>
                                            <tr>
                                                <td><b>Reputation Manager</b></td>
                                                <td><img class="tick lazyloaded" data-src="https://cdn.brightlocal.com/wp-content/themes/brightlocal/assets/images/check-green.svg" alt="yes" src="https://cdn.brightlocal.com/wp-content/themes/brightlocal/assets/images/check-green.svg"></td>
                                            </tr>
                                            <tr>
                                                <td>- Monitor Reviews</td>
                                                <td>6</td>
                                            </tr>
                                            <tr>
                                                <td>- Get Reviews</td>
                                                <td>3</td>
                                            </tr>
                                            <tr>
                                                <td><b>Agency Lead Generator</b></td>
                                                <td><img class="tick lazyloaded" data-src="https://cdn.brightlocal.com/wp-content/themes/brightlocal/assets/images/check-green.svg" alt="yes" src="https://cdn.brightlocal.com/wp-content/themes/brightlocal/assets/images/check-green.svg"></td>
                                            </tr>
                                            <tr>
                                                <td><b>Additional Features</b></td>
                                                <td></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <table class="hilite">
                                        <tbody>
                                        <tr>
                                            <td>- Google Analytics Integration</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>- Facebook &amp; Twitter Integration</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>- White-label Reports &amp; Emails</td>
                                            <td></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="grid">
                            <div class="priceplan green">
                                <div class="heading">
                                    <h3>SEO Pro</h3>
                                    <h4 class="monthly">$<span>79</span>/mo</h4>
                                    <h4 class="yearly toggled-off">$<span>65.83</span>/mo</h4>
                                    <h5 class="promo-banner">BEST VALUE</h5><a href="#" onclick="togglePriceRates(event); return false;" class="monthly">(Go Annual - get 2 months free!)</a><a href="#" onclick="togglePriceRates(event); return false;" class="yearly toggled-off">($790/year - you save $158)</a></div>
                                <div class="body">
                                    <p><strong>Our most complete plan for businesses and agencies managing up to 100 locations.<br><br><a class="button green" style="font-size:16px;" href="https://tools.brightlocal.com/seo-tools/admin/sign-up/2003">Try for free</a><br></strong></p>
                                    <div class="footer">
                                        <table class="features">
                                            <tbody>
                                            <tr>
                                                <td><b>Local SEO Tools</b></td>
                                                <td><img class="tick lazyloaded" data-src="https://cdn.brightlocal.com/wp-content/themes/brightlocal/assets/images/check-green.svg" alt="yes" src="https://cdn.brightlocal.com/wp-content/themes/brightlocal/assets/images/check-green.svg"></td>
                                            </tr>
                                            <tr>
                                                <td>- Local Search Ranking Reports</td>
                                                <td>100</td>
                                            </tr>
                                            <tr>
                                                <td>- Citation Tracker Reports</td>
                                                <td>50</td>
                                            </tr>
                                            <tr>
                                                <td>- Google My Business Audits</td>
                                                <td>100</td>
                                            </tr>
                                            <tr>
                                                <td>- Local SEO Audit Reports</td>
                                                <td>20</td>
                                            </tr>
                                            <tr>
                                                <td><b>Reputation Manager</b></td>
                                                <td><img class="tick lazyloaded" data-src="https://cdn.brightlocal.com/wp-content/themes/brightlocal/assets/images/check-green.svg" alt="yes" src="https://cdn.brightlocal.com/wp-content/themes/brightlocal/assets/images/check-green.svg"></td>
                                            </tr>
                                            <tr>
                                                <td>- Monitor Reviews</td>
                                                <td>50</td>
                                            </tr>
                                            <tr>
                                                <td>- Get Reviews</td>
                                                <td>5</td>
                                            </tr>
                                            <tr>
                                                <td><b>Agency Lead Generator</b></td>
                                                <td><img class="tick lazyloaded" data-src="https://cdn.brightlocal.com/wp-content/themes/brightlocal/assets/images/check-green.svg" alt="yes" src="https://cdn.brightlocal.com/wp-content/themes/brightlocal/assets/images/check-green.svg"></td>
                                            </tr>
                                            <tr>
                                                <td><b>Additional Features</b></td>
                                                <td></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <table class="hilite">
                                        <tbody>
                                        <tr>
                                            <td>- Google Analytics Integration</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>- Facebook &amp; Twitter Integration</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>- White-label Reports &amp; Emails</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>- Google Mobile Rank Tracking</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>- Competitor Tracking</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>- Full API Access</td>
                                            <td></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="grid">
                            <div class="priceplan blue">
                                <div class="heading">
                                    <h3>Enterprise</h3>
                                    <a href="#" onclick="togglePriceRates(event); return false;" class="monthly"></a>
                                    <a href="#" onclick="togglePriceRates(event); return false;" class="yearly toggled-off"></a>
                                </div>
                                <div class="body">
                                    <p><strong>Manage SEO, Listings and Reputation for more than 100 locations?<br><br>We can tailor a custom plan that delivers exactly what you need at a price that makes you smile!<br><br><a class="button blue" style="font-size:16px;" href="https://www.brightlocal.com/enterprise-packages/">LEARN MORE</a></strong></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel-row textcenter three-high">
                        <a class="button" href="#plan-comparison">See Full Plan Comparison<img class="down-arrow lazyload" data-src="https://cdn.brightlocal.com/wp-content/themes/brightlocal/assets/images/arrow-down-dark.svg"></a> </div>

                    <div class="panel-row textcenter">
                        <a class="button green" href="https://tools.brightlocal.com/seo-tools/admin/sign-up/2003" title "14=" " day=" " free=" " trial"="">14 DAY FREE TRIAL<sub>No Card Needed</sub></a> </div>

                    <div class="panel-row textcenter">
                        <p>*During your free trial you get access to the full SEO Pro plan; at the end of your trial you can select the best plan for you.</p>
                    </div>

                </div>
            </div>
            <div class="panel text_blocks green-to-blue top-regular bottom-regular" role="main" id="">
                <div class="panel-content">

                    <div class="panel-grid span-1 vertical-top">

                        <div class="grid shaded">
                            <div class="block">
                                <h2 style="text-align: center;">Looking for Citation Builder pricing?</h2>
                                <p style="text-align: center;">Citation Builder is a pay-as-you-go service.
                                    <br> Hand-pick the most relevant sites. Only pay for what you need.</p>
                                <p style="text-align: center;">Direct submissions start from just $2 per site.
                                    <br> Submit to individual aggregators for $5-25 per year or all 4 for just $60 per year.</p>
                                <p style="text-align: center;"><a class="button pink" href="https://tools.brightlocal.com/seo-tools/admin/sign-up">Get started</a><a class="button" href="/citation-builder/#cb-pricing">Compare pricing</a></p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="panel faq none top-none bottom-none" role="main" id="">
                <div class="panel-content">

                    <div class="panel-row">

                        <h2>FAQs</h2>
                        <div class="qna">
                            <div class="question">
                                <h6 onclick="toggleQandA(event);">What countries does BrightLocal work in?</h6><i class="minus" onclick="toggleQandA(event);">–</i><i class="plus" onclick="toggleQandA(event);">+</i></div>
                            <div class="answer">
                                <p>Our tools are configured to work for USA, UK, Canada, and Australia.</p>
                                <p>We plan to add additional countries over the coming months, so please contact us if you want to know when we're launching in your country.</p>
                            </div>
                        </div>
                        <div class="qna">
                            <div class="question">
                                <h6 onclick="toggleQandA(event);">How and when do I pay?</h6><i class="minus" onclick="toggleQandA(event);">–</i><i class="plus" onclick="toggleQandA(event);">+</i></div>
                            <div class="answer">
                                <p>You don't have to pay until the end of your 14 day free trial. We don't take payment details in advance so you're not tied in.</p>
                                <p>We do it this way because we don't believe in tricking people into paying to use our service. It should be good enough for people to want to pay for—and many businesses do!</p>
                            </div>
                        </div>
                        <div class="qna">
                            <div class="question">
                                <h6 onclick="toggleQandA(event);">What happens after my free trial ends?</h6><i class="minus" onclick="toggleQandA(event);">–</i><i class="plus" onclick="toggleQandA(event);">+</i></div>
                            <div class="answer">
                                <p>You will be able to access your account but you can't run new reports or update existing reports.</p>
                                <p>To run or update reports you will need to upgrade to a paid subscription.</p>
                                <p>From time to time we delete old, unused accounts so we may do this if your account is not accessed or reactivated.</p>
                            </div>
                        </div>
                        <div class="qna">
                            <div class="question">
                                <h6 onclick="toggleQandA(event);">What happens if I cancel?</h6><i class="minus" onclick="toggleQandA(event);">–</i><i class="plus" onclick="toggleQandA(event);">+</i></div>
                            <div class="answer">
                                <p>You can cancel at any time. We won't delete your account or your reports so you can come back and access them at any time.</p>
                            </div>
                        </div>
                        <div class="qna">
                            <div class="question">
                                <h6 onclick="toggleQandA(event);">Can I buy just part of the BrightLocal service?</h6><i class="minus" onclick="toggleQandA(event);">–</i><i class="plus" onclick="toggleQandA(event);">+</i></div>
                            <div class="answer">
                                <p>This isn't possible on our standard subscription levels. However, if you have a very high volume of locations and need just one or two types of reports then we can create a customer plan just for you.</p>
                            </div>
                        </div>
                        <div class="qna">
                            <div class="question">
                                <h6 onclick="toggleQandA(event);">What benefit do 'White-label' options give me?</h6><i class="minus" onclick="toggleQandA(event);">–</i><i class="plus" onclick="toggleQandA(event);">+</i></div>
                            <div class="answer">
                                <p>White-label options allow you to remove all mention of BrightLocal from your reports and emails, and customize them with your own logo and brand colors.</p>
                            </div>
                        </div>
                        <div class="qna">
                            <div class="question">
                                <h6 onclick="toggleQandA(event);">I just want to build citations. Do I need to sign up?</h6><i class="minus" onclick="toggleQandA(event);">–</i><i class="plus" onclick="toggleQandA(event);">+</i></div>
                            <div class="answer">
                                <p>Signing up creates your account on BrightLocal. So, yes, you do need to do this. But there is no obligation to use our other tools and you do NOT need to pay a monthly subscription to use Citation Builder.</p>
                            </div>
                        </div>
                        <div class="qna">
                            <div class="question">
                                <h6 onclick="toggleQandA(event);">Is Citation Builder included in each plan?</h6><i class="minus" onclick="toggleQandA(event);">–</i><i class="plus" onclick="toggleQandA(event);">+</i></div>
                            <div class="answer">
                                <p>Citation Builder is a pay-as-you-go service and each citation submission or update we do costs $3 (or $2 when you bulk buy credits).</p>
                                <p>You can access Citation Builder from your account but you don't get any credits included in your monthly subscription. Submission campaigns or submission credits are purchased separately.</p>
                                <p>On the flip side, if you just want to use Citation Builder you don't need to pay for a monthly subscription to our other tools.</p>
                            </div>
                        </div>
                        <div class="qna">
                            <div class="question">
                                <h6 onclick="toggleQandA(event);">How do I get BrightLocal to submit to Data Aggregators?</h6><i class="minus" onclick="toggleQandA(event);">–</i><i class="plus" onclick="toggleQandA(event);">+</i></div>
                            <div class="answer">
                                <p>We do submit to the 'big 4' US data aggregators, and we submit to Factual for all other countries.</p>
                                <p>To submit to data aggregators you need to create a Citation Builder campaign where you can select the aggregators you want and/or manual submissions if you want those as well.</p>
                                <p>Just sign-up for our Citation Builder only plan above.</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="panel text_blocks none top-none bottom-none" role="main" id="">
                <div class="panel-content">

                    <div class="panel-grid span-1 vertical-top">

                        <div class="grid none">
                            <div class="block">
                                <div id="plan-comparison"></div>
                                <h2 style="text-align: center;">Full Comparison of Features</h2>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="panel table_block none top-none bottom-regular" role="main" id="">
                <div class="panel-content">

                    <div class="panel-row">

                        <div class="table-container narrow shaded">
                            <table id="tablepress-397" class="tablepress tablepress-id-397">
                                <thead>
                                <tr class="row-1 odd">
                                    <th class="column-1">&nbsp;</th>
                                    <th class="column-2">
                                        <center>Single Business Plan</center>
                                    </th>
                                    <th class="column-3">
                                        <center>Multi Business Plan</center>
                                    </th>
                                    <th class="column-4">
                                        <center>SEO Pro Plan</center>
                                    </th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr class="row-26 even">
                                    <th class="column-1"><b>Price Per Month</b>
                                        <br>
                                        <br>
                                        <br>
                                        <b>Price Per Year</b></th>
                                    <th class="column-2">
                                        <center><b>$29</b></center>
                                        <br>
                                        <br>
                                        <br>
                                        <center><b>$290</b></center>
                                    </th>
                                    <th class="column-3">
                                        <center><b>$49</b></center>
                                        <br>
                                        <br>
                                        <br>
                                        <center><b>$490</b></center>
                                    </th>
                                    <th class="column-4">
                                        <center><b>$79</b></center>
                                        <br>
                                        <br>
                                        <br>
                                        <center><b>$790</b></center>
                                    </th>
                                </tr>
                                </tfoot>
                                <tbody class="row-hover">
                                <tr class="row-2 even">
                                    <td colspan="4" class="column-1"><b>Local Search Rank Checker</b></td>
                                </tr>
                                <tr class="row-3 odd">
                                    <td class="column-1">- Scheduled reports</td>
                                    <td class="column-2">
                                        <center>3</center>
                                    </td>
                                    <td class="column-3">
                                        <center>6</center>
                                    </td>
                                    <td class="column-4">
                                        <center>100</center>
                                    </td>
                                </tr>
                                <tr class="row-4 even">
                                    <td class="column-1">- Ad-hoc searches/month</td>
                                    <td class="column-2">
                                        <center>10</center>
                                    </td>
                                    <td class="column-3">
                                        <center>50</center>
                                    </td>
                                    <td class="column-4">
                                        <center>100</center>
                                    </td>
                                </tr>
                                <tr class="row-5 odd">
                                    <td class="column-1">- Keywords per report</td>
                                    <td class="column-2">
                                        <center>25</center>
                                    </td>
                                    <td class="column-3">
                                        <center>50</center>
                                    </td>
                                    <td class="column-4">
                                        <center>100</center>
                                    </td>
                                </tr>
                                <tr class="row-6 even">
                                    <td class="column-1">- Google Mobile Tracking</td>
                                    <td class="column-2">
                                        <center>No</center>
                                    </td>
                                    <td class="column-3">
                                        <center>No</center>
                                    </td>
                                    <td class="column-4">
                                        <center>Yes</center>
                                    </td>
                                </tr>
                                <tr class="row-7 odd">
                                    <td class="column-1">- Competitor tracking</td>
                                    <td class="column-2">
                                        <center>No</center>
                                    </td>
                                    <td class="column-3">
                                        <center>No</center>
                                    </td>
                                    <td class="column-4">
                                        <center>Yes</center>
                                    </td>
                                </tr>
                                <tr class="row-8 even">
                                    <td class="column-1">- Keyword query count</td>
                                    <td class="column-2">
                                        <center>No</center>
                                    </td>
                                    <td class="column-3">
                                        <center>No</center>
                                    </td>
                                    <td class="column-4">
                                        <center>Yes</center>
                                    </td>
                                </tr>
                                <tr class="row-9 odd">
                                    <td colspan="4" class="column-1"><b>Local SEO Audit</b></td>
                                </tr>
                                <tr class="row-10 even">
                                    <td class="column-1">- Reports/month</td>
                                    <td class="column-2">
                                        <center>1</center>
                                    </td>
                                    <td class="column-3">
                                        <center>6</center>
                                    </td>
                                    <td class="column-4">
                                        <center>20</center>
                                    </td>
                                </tr>
                                <tr class="row-11 odd">
                                    <td colspan="4" class="column-1"><b>Citation Tracker</b></td>
                                </tr>
                                <tr class="row-12 even">
                                    <td class="column-1">- Scheduled reports</td>
                                    <td class="column-2">
                                        <center>3</center>
                                    </td>
                                    <td class="column-3">
                                        <center>6</center>
                                    </td>
                                    <td class="column-4">
                                        <center>50</center>
                                    </td>
                                </tr>
                                <tr class="row-13 odd">
                                    <td class="column-1">- Ad-hoc searches/month</td>
                                    <td class="column-2">
                                        <center>10</center>
                                    </td>
                                    <td class="column-3">
                                        <center>30</center>
                                    </td>
                                    <td class="column-4">
                                        <center>100</center>
                                    </td>
                                </tr>
                                <tr class="row-14 even">
                                    <td colspan="4" class="column-1"><b>Google My Business Audit</b></td>
                                </tr>
                                <tr class="row-15 odd">
                                    <td class="column-1">- Searches/month</td>
                                    <td class="column-2">
                                        <center>10</center>
                                    </td>
                                    <td class="column-3">
                                        <center>30</center>
                                    </td>
                                    <td class="column-4">
                                        <center>100</center>
                                    </td>
                                </tr>
                                <tr class="row-16 even">
                                    <td colspan="4" class="column-1"><b>Reputation Manager</b></td>
                                </tr>
                                <tr class="row-17 odd">
                                    <td class="column-1">- Monitor reviews</td>
                                    <td class="column-2">
                                        <center>3</center>
                                    </td>
                                    <td class="column-3">
                                        <center>6</center>
                                    </td>
                                    <td class="column-4">
                                        <center>50</center>
                                    </td>
                                </tr>
                                <tr class="row-18 even">
                                    <td class="column-1">- Review sites monitored</td>
                                    <td class="column-2">
                                        <center>18</center>
                                    </td>
                                    <td class="column-3">
                                        <center>18</center>
                                    </td>
                                    <td class="column-4">
                                        <center>18</center>
                                    </td>
                                </tr>
                                <tr class="row-19 odd">
                                    <td class="column-1">- Get reviews</td>
                                    <td class="column-2">
                                        <center>1</center>
                                    </td>
                                    <td class="column-3">
                                        <center>3</center>
                                    </td>
                                    <td class="column-4">
                                        <center>5</center>
                                    </td>
                                </tr>
                                <tr class="row-20 even">
                                    <td class="column-1"></td>
                                    <td class="column-2"></td>
                                    <td class="column-3"></td>
                                    <td class="column-4"></td>
                                </tr>
                                <tr class="row-21 odd">
                                    <td class="column-1"><b>Google Analytics Integration</b></td>
                                    <td class="column-2">
                                        <center>Yes</center>
                                    </td>
                                    <td class="column-3">
                                        <center>Yes</center>
                                    </td>
                                    <td class="column-4">
                                        <center>Yes</center>
                                    </td>
                                </tr>
                                <tr class="row-22 even">
                                    <td class="column-1"><b>Facebook/Twitter Integration</b></td>
                                    <td class="column-2">
                                        <center>Yes</center>
                                    </td>
                                    <td class="column-3">
                                        <center>Yes</center>
                                    </td>
                                    <td class="column-4">
                                        <center>Yes</center>
                                    </td>
                                </tr>
                                <tr class="row-23 odd">
                                    <td class="column-1"><b>Agency Lead Generator</b></td>
                                    <td class="column-2">
                                        <center>No</center>
                                    </td>
                                    <td class="column-3">
                                        <center>Yes</center>
                                    </td>
                                    <td class="column-4">
                                        <center>Yes</center>
                                    </td>
                                </tr>
                                <tr class="row-24 even">
                                    <td class="column-1"><b>White-label reports &amp; emails</b></td>
                                    <td class="column-2">
                                        <center>No</center>
                                    </td>
                                    <td class="column-3">
                                        <center>Yes</center>
                                    </td>
                                    <td class="column-4">
                                        <center>Yes</center>
                                    </td>
                                </tr>
                                <tr class="row-25 odd">
                                    <td class="column-1"><b>Full API Access</b></td>
                                    <td class="column-2">
                                        <center>No</center>
                                    </td>
                                    <td class="column-3">
                                        <center>No</center>
                                    </td>
                                    <td class="column-4">
                                        <center>Yes</center>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <!-- #tablepress-397 from cache -->
                        </div>
                    </div>

                </div>
            </div>
            <div class="panel cta_banner   " role="main" id="">
                <div class="panel-content">

                    <div class="panel-row">
                        <div class="cta-banner blue">
                            <div class="banner-wrapper">
                                <div class="cta-banner-image">
                                    <img class="lazyload" data-src="https://corp-brightlocal.imgix.net/2019/06/icon-cta.svg?auto=compress%2Cformat&amp;ixlib=php-1.2.1&amp;q=70&amp;s=337d02a8feea1d79b538601e01eb75b0" alt=""> </div>
                                <div class="cta-banner-text">
                                    <h4>Try BrightLocal free for 14 days</h4>
                                    <p>All features · Unlimited access · No card required</p>
                                </div>
                                <div class="cta-banner-cta">
                                    <div class="banner_cta"><a class="button green" href="https://tools.brightlocal.com/seo-tools/admin/sign-up/2003" title "14=" " day=" " free=" " trial"="">14 DAY FREE TRIAL<sub>NO CARD NEEDED</sub></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel banner none top-small bottom-regular" role="main" id="">
                <div class="panel-content">

                    <div class="panel-banner blue">
                        <blockquote><img class="quotation" src="https://cdn.brightlocal.com/wp-content/themes/brightlocal/assets/images/quote-icon.svg" alt="star">
                            <p><strong>We have used all their services from citation acquisition to rank tracking. We could not be more satisfied with their service and the tools they provide.</strong></p>
                        </blockquote><cite><div class="flex"><div class="person"><span><strong>David Faltz</strong></span><span>Fort Lauderdale, FL</span></div></div></cite>
                        <div class="ph_cta"><a class="button green" href="https://tools.brightlocal.com/seo-tools/admin/sign-up/2003" title "14=" " day=" " free=" " trial"="">14 Day Free Trial<sub>No Card Needed</sub></a></div>
                        <div class="grid right flex textcenter">
                            <div class="trustpilot">
                                <div class="rating-stars"><img src="https://cdn.brightlocal.com/wp-content/themes/brightlocal/assets/images/trustpilot-star-green.svg" alt="star"><img src="https://cdn.brightlocal.com/wp-content/themes/brightlocal/assets/images/trustpilot-star-green.svg" alt="star"><img src="https://cdn.brightlocal.com/wp-content/themes/brightlocal/assets/images/trustpilot-star-green.svg" alt="star"><img src="https://cdn.brightlocal.com/wp-content/themes/brightlocal/assets/images/trustpilot-star-green.svg" alt="star"><img src="https://cdn.brightlocal.com/wp-content/themes/brightlocal/assets/images/trustpilot-star-green.svg" alt="star"></div>
                                <h3>9.5/10</h3>
                                <p>from 300+ reviews on Trustpilot</p><img class="bl_icon" src="https://cdn.brightlocal.com/wp-content/uploads/2018/10/BrightLocal-Logo.svg"></div>
                        </div>
                    </div>

                </div>
            </div>

        </section>
        <!-- #page-content -->

        <!-- <aside id="page-sidebar">
                        </aside> -->
    </div>
@endsection
