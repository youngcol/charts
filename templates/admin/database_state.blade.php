@extends("admin.layout")

@section('content')

    @include('admin.breadcrumbs', [
        'title' => 'Database state builder',
        'links' => [
            ['route' => 'admin.dashboard', 'title' => 'Dashboard'],
        ]
    ])


    <div class="row">

        @if($isRun)
            <div class="col-12">
                <h2>Build state report</h2>
                <hr>

                <h3>File: {{$file}}</h3>
                <br>

                <h3>Report</h3>
                <hr>
                @php(dump($report))
            </div>


        @else
            <div class="col-4">
                <table class="table table-striped">
                    @foreach ($list as $file)
                        <tr>
                            <td>{{$file}}</td>
                            <td>
                                <a href="{{route('admin.database.state.run', ['file' => $file])}}">Run</a>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        @endif

    </div>

@endsection
