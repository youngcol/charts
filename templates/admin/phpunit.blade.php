@extends("admin.layout")

@section('content')

    <h1>`{{$phpunit_exec}}`</h1>
    <hr>
    <form class="command-form" action="{{route('admin.phpunit')}}" method="get">
        <h4>Filter options: (--filter)</h4>
        {{csrf($csrf)}}
{{--        <input type="hidden" name="command" value="generate:task-api-request">--}}
        <input name="filter_file" type="text" class="form-control mb-1" placeholder="test file">
        <input name="filter_method" type="text" class="form-control mb-1" placeholder="method:test_breakout_buy">

        <input type="submit" class="form-control col-3 btn btn-primary">
        <div class="console"></div>
    </form>


    <hr>


    <h2>
        {{$exec_result}}
    </h2>
    <hr>

    <pre>
        {{{$output}}}
    </pre>

@endsection

@section('scripts')
    <script>

        jQuery(document).ready(function($) {

        });

    </script>
@endsection
