
@extends("admin.layout")

@section('content')

    @include('admin.breadcrumbs', [
        'title' => 'Mysql viewer',
        'links' => [
            ['route' => 'admin.dashboard', 'title' => 'Dashboard'],
        ]
    ])

    <div class="section">
        <div class="col-8">
            @foreach($tables as $table)
                <p>
                    <a href="{{route('admin.mysql_viewer.table', ['table' => $table['name']])}}#current_table">
                        {{$table['name']}} ({{count($table['foreign_tables'])}})
                    </a>
                </p>
            @endforeach
        </div>
    </div>

@endsection

@section('scripts')
    <script>

        jQuery(document).ready(function($) {

        });

    </script>
@endsection
