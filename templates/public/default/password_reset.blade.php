
@extends("public.layout")

@section('content')

    <section id="passwrod-reset" class="section-padding1">

        <div class="container">
            <div class="row">

                <div class="mx-auto col-md-6 col-sm-12">
                    <div class="about-content">

                        <h2>Password reset</h2>

                        @if($isVerified)
                            <form action="{{route('password.change')}}" method="POST">
                                {{csrf($csrf)}}

                                <input type="hidden" name="code" value="{{$code}}">

                                <div class="form-group">
                                    <input type="password" name="pass1" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="New password">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" name="pass2" id="password" placeholder="New password again">
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Save password</button>
                                </div>

                            </form>


                        @else
                            <p>Wrong code!</p>
                        @endif


                    </div>
                </div>

            </div>
        </div>

    </section><!-- #passwrod-reset -->

@endsection
