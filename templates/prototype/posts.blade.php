
@extends("prototype.layout")

@section('content')
    <h1>Posts</h1>

    <hr>

    <h3>Posts list</h3>

    <table class="pure-table pure-table-bordered">
        <thead>
        <tr>
            <th>#</th>
            <th>Title</th>
            <th>Text</th>
            <th>Media</th>
            <th>Created</th>
        </tr>
        </thead>

        <tbody>

            @foreach($posts as $post)

                <tr>
                    <td>{{$post->id}}</td>
                    <td>{{$post->title}}</td>
                    <td>{{$post->text}}</td>
                    <td>
                        <div class="pure-g">
                            @foreach($post->mediaFiles as $mediaFile)
                                <div class="pure-u-1-8 pure-u-lg-1-8">
                                    <img class="pure-img" src="{{uploads_url($mediaFile->filename)}}" alt="">
                                </div>
                            @endforeach
                        </div>
                    </td>
                    <td>
                        {{$post->created_at}}
                    </td>
                </tr>
            @endforeach

        </tbody>
    </table>

@endsection

