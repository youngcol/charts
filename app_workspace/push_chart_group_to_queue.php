<?php

global $container;

use App\Facades\CU;
use App\Tasks\push__chart__group__to__queue__task;

$u = CU::user();


$res = task(new push__chart__group__to__queue__task,
    [
        $u,
        vo(1),
        vo(5)
    ]
);


return [
    $res,

];
