<?php

global $container;

use App\Models\Quote;
use App\Repositories\QuotesRepository;
use App\Tasks\Binary\create__binary__seria__task;
use Carbon\Carbon;


$instrument = vo('eur_usd', 'instruments');
$oandaTimeframe = vo('M5', 'oanda.timeframe');


$filepath = TEMPLATES_PATH . '/uploads/GBPJPY5.csv';



$repo = new QuotesRepository();
$repoQuotes = new QuotesRepository();
$repoQuotes->setTable1(Quote::getQuotesTable($instrument, $oandaTimeframe));

//dump(date_mysql_format(new Carbon('2020-11-17 16:25:00')));echo 'aaa_main.php:18'; exit;
$to = now();
$from = Carbon::now()->subDays(10)->startOfDay();
$quotes = $repoQuotes->getByDates($from, Carbon::now());
//dump($quotes);echo 'aaa_main.php:22'; exit;
$serias = [];
$date = new Carbon('2020-11-17 16:25:00');
$times = 5;
$seriaSize = 100;


$serias[] = task(new create__binary__seria__task,
    [
        $date,
        $quotes,
        5,
        'minutes',
        $seriaSize
    ]
);


for ($i=0;$i<$times;$i++)
{


    $serias[] = task(new create__binary__seria__task,
        [
            $date,
            $quotes,
            1,
            'hours',
            $seriaSize
        ]
    );


//
//
//    $serias[] = task(new create__binary__seria__task,
//        [
//            $date,
//            $quotes,
//            4,
//            'hours',
//            $seriaSize
//        ]
//    );
////
//    $serias[] = task(new create__binary__seria__task,
//        [
//            $date,
//            $quotes,
//            8,
//            'hours',
//            $seriaSize
//        ]
//    );


    $date->subMinutes(5);
}



return [$serias];

