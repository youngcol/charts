<?php

global $container;


use App\Facades\CU;
use App\Models\ChartGroup;
use App\Tasks\load__chart__group__data__task;

$u = CU::user();


$res = task(new load__chart__group__data__task(),
    [
        $u,
        vo(1)
    ]
);


return [
    $res
];
