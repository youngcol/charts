<?php

global $container;

use App\Facades\CU;
use App\Tasks\get__popup__timeline__task;

$u = CU::user();


$res = task(new get__popup__timeline__task,
    [
        $u
    ]
);


return [
    $res,

];
