<?php

global $container;

use App\Tasks\get__big__charts__marks__task;

$marks = task(new get__big__charts__marks__task, [
    'XAUUSD'
]);

return [$marks];
