<?php

global $container;

use App\Tasks\get__popuped__chart__groups__from__queue__task;

use App\Facades\CU;
use App\Tasks\push__chart__group__to__queue__task;

$u = CU::user();


$res = task(new get__popuped__chart__groups__from__queue__task,
    [
        $u
    ]
);

return [$res];
