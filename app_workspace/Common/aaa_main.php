<?php

global $container;


use App\Common\GifCreator;
use App\Facades\Screener;
use App\Tasks\build__indicator__renko__timeseria__task;
use App\Tasks\create__event__task;
use App\Tasks\find__true__swings__task;
use App\Tasks\save__signal__task;
use App\Tasks\update__screener__ema__swings__task;
use App\Tasks\update__screener__state__task;
use Carbon\Carbon;

$gf = new GifCreator();

$period = 5;
$pair = "XAUUSD";
$done = [];

foreach (settings('pairs_list') as $pair)
{
    foreach ([1,15,240,1440] as $period)
    {
        $folder = SCREENSHOTS_PATH . "/".$pair."/".$period."/";

        if (is_dir($folder))
        {
            $files = scandir($folder);

            $frames = [];
            foreach ($files as $filename)
            {
                if (strlen($filename) > 16) {
                    $frames[] = $folder . $filename;
                }
            }

            $durations = array_fill(0, count($frames)-1, $period == 15 ? 70 : 50);

            //dump($frames);echo"aaa_main.php:42";
// Initialize and create the GIF !
            $gc = new GifCreator();
            $gc->create($frames, $durations, 0);
            $gifBinary = $gc->getGif();
            $replay = SCREENSHOTS_PATH. "/{$pair}/{$period}_replay.gif";
            $done[] = $replay;

            file_put_contents($replay, $gifBinary);

        }
    }
}

return [
    'done' => $done,
    'frames' => $frames
];

//dump($res);echo "aaa_main.php:12";exit;
