<?php

global $container;

use App\Facades\CU;
use App\Services\OandaClient_v20;
use App\Tasks\Oanda\download__order__book__for__instrument__task;
use Carbon\Carbon;

$u = CU::user();
$date = Carbon::now();


$res = task(new download__order__book__for__instrument__task,
    [
        vo('GBP_USD'),
        $date,
        300
    ]
);

return [
    $res,

];
