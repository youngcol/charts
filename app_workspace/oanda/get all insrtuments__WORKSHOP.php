<?php

global $container;

use App\Facades\CU;

$u = CU::user();

$accId = '101-004-13968418-001';
$s = service('oanda2');

$instruments = $s->getAccountInstruments($accId);

//"name" => "USD_CNH"
//        "type" => "CURRENCY"
//        "displayName" => "USD/CNH"

$ret = [];

$ret['name'] = collect($instruments['instruments'])->pluck('name')->toArray();
$ret['type'] = collect($instruments['instruments'])->pluck('type')->toArray();
$ret['displayName'] = collect($instruments['instruments'])->pluck('displayName')->toArray();

dumpVar('oanda instruments', $ret);

return [
    $ret,
    $res,
    $instruments
];
