<?php

global $container;

use App\Facades\CU;
use App\Models\Chart;
use App\Tasks\build__chart__timeseries__task;

$u = CU::user();
$chart = Chart::find(1);

$res = task(new build__chart__timeseries__task,
    [
        $u,
        $chart
    ]
);


return [
    $res,

];
