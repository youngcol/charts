<?php

global $container;

use App\Facades\CU;
use App\Models\Chart;
use App\Tasks\get__latest__timeseria__for__charts__task;

$u = CU::user();
//vo('2020-04-23 08:30:00', 'datetime')

$res = task(new get__latest__timeseria__for__charts__task,
    [
        $u,
        vo([2,4]),
        vo('2020-04-23 08:00:00', 'datetime')
    ]
);

return [
    $res,

];
