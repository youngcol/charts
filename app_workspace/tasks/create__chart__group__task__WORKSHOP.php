<?php

global $container;

use App\Facades\CU;
use App\Tasks\create__chart__group__task;

$u = CU::user();


$res = task(new create__chart__group__task,
    [
        CU::user(),
        vo('some test name'),
        vo([2,3], 'ids.array'),
        vo(date_mysql_format(now_date()), 'datetime')
    ]
);



return [
    $res,

];
