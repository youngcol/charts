<?php

global $container;

use App\Facades\CU;
use App\Tasks\Oanda\download__latest__quotes__for__instrument__task;

$u = CU::user();

$res = task(new download__latest__quotes__for__instrument__task,
    [
        vo('eur_usd', 'instruments')
    ]
);


return [
    $res,

];
