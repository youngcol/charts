<?php

global $container;

use App\Facades\CU;
use App\Tasks\Oanda\download__instrument__quotes__task;

$u = CU::user();

$res = task(new download__instrument__quotes__task,
    [
        vo('eur_usd', 'instruments'),
        vo('M1', 'oanda.timeframe'),
        false
    ]
);

return [
    $res,

];
