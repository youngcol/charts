<?php

global $container;

use App\Tasks\Screenshots\upload__screenshot__task;
use Slim\Http\UploadedFile;

$uploadedFile1 = 'fds';
$tag = 'MAS';
$period = 5;
$instrument = 'GBPUSD';

$res = task(new upload__screenshot__task(), [
    $uploadedFile1,
    $tag,
    vo($period, 'screenshots.period'),
    vo($instrument),
]);
