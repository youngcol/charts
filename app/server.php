<?php

error_reporting(E_ERROR | E_PARSE | E_COMPILE_ERROR);
//error_reporting(E_ALL);

//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);


define("PATH_ROOT", __DIR__ . '/../');

define('COMMANDS_PATH',      PATH_ROOT . 'app/Console/Commands');
define('CODE_TEMPLATE_PATH', PATH_ROOT . 'app/Console/CodeTemplates');
define('MIGRATIONS_PATH',    PATH_ROOT . 'db/migrations');
define('SEEDS_PATH',         PATH_ROOT . 'db/seeds');
define('DB_STATES_PATH',     PATH_ROOT . 'db/states');
define('LOGS_PATH',          PATH_ROOT . 'logs');
define('HTDOC_PATH',         PATH_ROOT . 'htdocs');
define('TESTS_PATH',         PATH_ROOT . 'tests');
define('TEMPLATES_PATH',     PATH_ROOT . 'templates');
define('MODELS_PATH',        PATH_ROOT . 'app/Models');
define('TASKS_PATH',         PATH_ROOT . 'app/Tasks');
define('VO_PATH',            PATH_ROOT . 'app/VO');
define('UPLOADS_PATH',       PATH_ROOT . 'htdocs/uploads');
define('SCREENSHOTS_PATH',   PATH_ROOT . 'htdocs/screenshots');
define('SCREENSHOTS_DRAWNINGS_PATH',   PATH_ROOT . 'htdocs/screenshots_drawnings');
define('SCREENSHOTS_DRAWNINGS', 'screenshots_drawnings');
define('CONFIG_PATH',        PATH_ROOT . '/config');
define('API_REQUESTS_PATH',  PATH_ROOT . 'app/Api tasks requests');

define('TEMP_UPLOADS_PATH',  PATH_ROOT . 'temp/uploads');
define('APP_PATH',           PATH_ROOT . 'app');
define('APP_VUE_PATH',       PATH_ROOT . 'app_vue');
define('APP_WORKSPACE',      PATH_ROOT . 'app_workspace');
define('DB_PATH',            PATH_ROOT . 'db');
define('API_TASKS_REQUESTS_PATH', PATH_ROOT . 'app/Api tasks requests');



ini_set('session.entropy_length', 128);
ini_set('session.entropy_file', '/dev/urandom');
ini_set('session.hash_function', 'sha256');
ini_set('session.hash_bits_per_character', 5);


//define('CACHE_PATH',          __DIR__ . '/cache' );
//define('PUBLIC_PATH',          __DIR__ . '/public' )
//define('SCHEMAS_PATH',       __DIR__ . '/app/src/Schema');
//define('CONFIG_PATH',        __DIR__ . '/config');
//define('APP_PATH',           __DIR__ . '/app');
//define('APP_SRC_PATH',       __DIR__ . '/app/src');
//define('MODULES_PATH',       __DIR__ . '/app/modules');
//define('VIEWS_PATH',         __DIR__ . '/app/resources/views');

require PATH_ROOT . 'vendor/autoload.php';

use DI\Container;
$container = new Container();

require PATH_ROOT . 'app/bootstrap_dependencies.php';
require PATH_ROOT . 'app/dependencies.php';
require PATH_ROOT . 'app/validation.php';
require PATH_ROOT . 'app/validation_accounts.php';


