<?php
/**
 * User have this media files ids check
 */

namespace App\VO\User;

use App\Common\ValidationException;
use App\Models\SocialEngine\MediaFile;
use App\Models\SocialEngine\UserSocialAccount;
use App\Models\User;
use App\VO\Vo;

class MediaFiles extends Vo
{
    public function __construct(User $user, array $mediaIds)
    {
        $this->val($user, $mediaIds);
        $this->args = $mediaIds;
    }

    protected function val(User $user, array $mediaIds)
    {

        $mediaFiles = MediaFile::where('user_id', $user->id)->whereIn('id', $mediaIds)->get();

        if (count($mediaFiles) !== count($mediaIds))
        {
            throw new ValidationException('User media files are wrong');
        }
    }

}
