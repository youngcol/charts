<?php

declare(strict_types=1);
namespace App\VO;

use App\Common\VoException;
use App\VO\Vo;

/**
 *
 * Class value object from validation rule
 *
 * @package App\VO
 */
class VoVal extends Vo
{
    public function __construct($args, $valRuleName, $valRule)
    {
        $this->rule_name_ = $valRuleName;

        if (!is_callable($valRule))
        {
            $this->rule_val_ = $valRule;
        }

        parent::__construct($args);
    }

    protected function val($val)
    {

    }
}
