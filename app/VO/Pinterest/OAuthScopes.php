<?php

namespace App\VO\Pinterest;

use App\Common\VoException;
use App\VO\Vo;

class OAuthScopes extends Vo
{
    protected function val(array $scopes)
    {
        foreach ($scopes as $scope) {
            val($scope, 'pinterest.oauth.scopes');
        }
    }
}
