<?php

declare(strict_types=1);

use App\Common\Res;
use App\Common\ValidationException;
use App\Common\ValidationRuleException;
use App\Facades\Redis2;
use App\Repositories\Repository;
use App\Services\Language;
use Carbon\Carbon;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\ServerRequest;
use Psr\Http\Message\ResponseInterface;
use Rakit\Validation\ErrorBag;
use Ramsey\Uuid\Uuid;
use Psr\Http\Message\ServerRequestInterface as Request;
use Symfony\Component\Finder\Finder;

/**
 * @param $text
 * @return string
 */
function slugify($text)
{
	// replace non letter or digits by -
	$text = preg_replace('~[^\pL\d]+~u', '-', $text);

	// transliterate
	$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

	// remove unwanted characters
	$text = preg_replace('~[^-\w]+~', '', $text);

	// trim
	$text = trim($text, '-');

	// remove duplicate -
	$text = preg_replace('~-+~', '-', $text);

	// lowercase
	$text = strtolower($text);

	if (empty($text)) {
		return 'n-a';
	}

	return $text;
}

/**
 * @return false|string
 */
function now(): string
{
	return date('Y-m-d H:i:s');
}
function now_local(): string
{
    return Carbon::now(settings('timezone'))->format('Y-m-d H:i:s');
}
/**
 * @param $route
 * @param array $params
 * @return mixed
 */
function route($route, $params=[])
{
    global $app;
    $collector = $app->getRouteCollector();
    return $collector->getRouteParser()->urlFor($route, $params);
}

function url($route, $params=[], $https=false)
{
    return ($https ? 'https://' : 'http://') . settings('host') . route($route, $params);
}

function uploads_url($filename, $https=false)
{
    return ($https ? 'https://' : 'http://') . settings('host') . '/uploads/' . $filename;
}

if (! function_exists('evalBool')) {
    function evalBool($value)
    {
        return $value === 'true';
    }
}

$__start_timer = null;
function start_timer()
{
    $__start_timer = microtime(true);
}

/**
 * @return string
 */
function end_timer()
{
    global $__start_timer;
    return sprintf("%0.4f", microtime(true) - $__start_timer);
}

function grab_output($f)
{
    ob_start();
    $f();
    return ob_get_clean();
}

function grab_dump_output($var)
{
    ob_start();
    dump($var);
    return ob_get_clean();
}

/**
 * @param $task
 * @param $args
 * @return \App\Common\ResFail
 * @throws Exception
 */
function task($task, $args)
{
    $catchedException = null;
    $startTime = microtime(true);

    try
    {
        $runRes = $task->run(...$args);
    }
    catch (\App\Common\FlowException $e)
    {
        handle_task_exception($e);
        $catchedException = $e;
    }
    catch (\Exception $e)
    {
        handle_task_exception($e);
        $catchedException = $e;
    }

    $duration = sprintf("%0.4f", microtime(true) - $startTime);

    ob_start();
    debug_print_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
    $backtrace = ob_get_clean();

    if (settings('is_tasks_invoke_logging'))
    {
        \App\Models\TaskLog::create([
            'class_name' => get_class($task),
            'backtrace' => $backtrace,
            'args'      => serialize($args),
            'duration'  => $duration,
            'status'    => eN($catchedException) ? 'success' : 'exception',
        ]);
    }

    if ($catchedException)
    {
        return resFail('last_tier_task_handle_exceptions','', [
            'e' => [
                'class' => get_class($e),
                'message' => $e->getMessage(),
                'code' => $e->getCode()
            ],
        ]);
    }
    else
    {
        return $runRes;
    }
}

/**
 * Save param from array
 *
 * @param $array
 * @param $key
 * @param null $default
 * @return null
 */
function sp($array, $key, $default=null)
{
    return isset($array[$key]) ? $array[$key] : $default;
}

/**
 *
 * @param $value
 * @param $ruleName
 * @param null $arg1
 * @param null $arg2
 * @return Res
 * @throws ValidationException
 */
function val($value, $complexRuleName, $arg1=null, $arg2=null)
{
    global $val;

//    check(!isset($val[$ruleName]), "Rule name `$ruleName` doesn't exists in validation.php");
    $ruleNames = explode('|', $complexRuleName);

    foreach ($ruleNames as $ruleName)
    {
        if (isset($val[$ruleName]) and is_callable($val[$ruleName]))
        {
            $ruleValidationErrors = null;

            /** @var Res $ret */
            try
            {
                $res = $val[$ruleName]($value, $arg1, $arg2);
            }
            catch (\Exception $e)
            {
                $ruleValidationErrors = [$e->getMessage()];
            }

            if (eN($ruleValidationErrors) and $res->fail())
            {
                $ruleValidationErrors = $res->toArray();
            }

            if (!eN($ruleValidationErrors))
            {
                throw new ValidationException(
                    'Value object error',
                    [
                        'CURRENT VALUE' => $value,
                        'RULE'      => $ruleName,
                        'ERRORS'    => $ruleValidationErrors,
                    ]
                );
            }

        } else {

            $validator = new Rakit\Validation\Validator();

//        $validator->setMessages([
//            'required' => ':attribute harus diisi',
//            'email' => ':email tidak valid',
//            // etc
//        ]);
//        dump($ruleName, [sp($val, $ruleName, $ruleName)]);echo 'helpers.php:133'; exit;
            $validation = $validator->make(
                [
                    'val_value' => $value
                ],
                [
                    'val_value' => sp($val, $ruleName, $ruleName)
                ]
            );

            $validation->validate();

            if ($validation->fails())
            {
                $errors = $validation->errors();

                throw new ValidationException(
                    'Value object error',
                    [
                        'CURRENT VALUE' => $value,
                        'RULE' => $ruleName,
                        'ERRORS' => $errors->all(),
                    ]
                );
            }
        }

    }

    return new Res(['valRule' => sp($val, $ruleName, $ruleName)]);
}

/**
 * Build value object
 *
 * @param $val
 * @param string $valRule
 * @return \App\VO\VoVal
 * @throws ValidationException
 */
function vo($val, $valRule=null)
{
    /**
     * default values for vo input data
     */
    if (eN($valRule))
    {
        if (is_array($val))
        {
            $valRule = 'array';
        }
        elseif(is_string($val))
        {
            $valRule = 'filled.string';
        }
        elseif(is_numeric($val))
        {
            $valRule = 'numeric';
        }
        else
        {
            crit("No def value for^ `$val`");
        }
    }

    $valRes = val($val, $valRule);

    return new \App\VO\VoVal($val, $valRule, $valRes->valRule);
}

/**
 * @param ServerRequest $r
 * @param $reqName
 * @return void
 * @throws ValidationException
 */
function val_request(Request $r, $reqName)
{
    global $val_request;
    $rules = sp($val_request, $reqName);
    if (eN($rules))
    {
        throw new ValidationException("No such key in val_request: `$reqName`");
    }
    $validator = new Rakit\Validation\Validator();


//        $validator->setMessages([
//            'required' => ':attribute harus diisi',
//            'email' => ':email tidak valid',
//            // etc
//        ]);

    $body = $r->getParsedBody();
    $requestParams = [];
    $params_rules = array_keys($rules);

    foreach ($body as $k => $v)
    {
        if (in_array($k, $params_rules)) {
            $requestParams[$k] = $v;
        }
    }

    $validation = $validator->make($requestParams, $rules);
    $validation->validate();

//    new ErrorBag();

    if ($validation->fails())
    {
        $errors = $validation->errors();

//        $err = map($errors->toArray(), function ($item, $key) {
//            return '`'.$key. '` -> '. implode(" | ",$item);
//        });

        $log = [
            'request' => $requestParams,
            'ruleName' => $reqName,
            'rules' => $rules,
            'errors' => $errors->toArray(),
        ];

        throw new ValidationException('Request validation ', $log);
    }
}

/**
 * @param $arr
 * @param $func
 * @return array
 */
function map($arr, $func)
{
    $ret = [];

    foreach ($arr as $key => $item)
    {
        $ret[] = $func($item, $key);
    }

    return $ret;
}

/**
 * Equal null
 * @param $val
 * @return bool
 */
function eN($val)
{
    return $val === null;
}

/**
 * Equal false
 * @param $val
 * @return bool
 */
function eF($val)
{
    return $val === false;
}

/**
 * @param $csrf
 */
function csrf(array $csrf)
{
    echo '<input type="hidden" name="'.$csrf['nameKey'].'" value="'.$csrf['name'] .'">';
    echo '<input type="hidden" name="'.$csrf['valueKey'].'" value="'.$csrf['value'] .'">';
}

/**
 * @param ResponseInterface $response
 * @param $route
 * @return ResponseInterface
 */
function redirect(string $route, array $params = [])
{
    $response = new Response();
    return $response->withStatus(302)->withHeader('Location', route($route, $params));
}

/**
 * @param $url
 * @return \Slim\Http\Response
 */
function redirect_url($url)
{
    $response = new Response();
    return $response->withStatus(302)->withHeader('Location', $url);
}

/**
 * Log warn event
 *
 * @param string $message
 * @param array $data
 */
function log_warn(string $message, array $data=[])
{
    $user = sp($_SESSION, 'current_user');
    $userId = sp($user, 'id');
    $userName = sp($user, 'name');
    $time = date('r');
    $url = sp($_SERVER, 'REQUEST_URI');

    $log = [
        " ",
        " ",
        "======================",
        "USER: $userName ID: $userId TIME: $time",
        "URL: ". $url,
        "GET:". json_encode($_GET, JSON_PRETTY_PRINT),
        "POST:". json_encode($_POST, JSON_PRETTY_PRINT),
        '~~~~~~~~~~~~~~~~~~~~~~',
        '',
        $message,
        json_encode($data, JSON_PRETTY_PRINT),
        "~~~~~~~~~~~~~~~~~~~~~~",
    ];


    file_put_contents(
        PATH_ROOT. '/logs/warn.log',
        implode("\n", $log) . "\n\n",
        FILE_APPEND
    );
}

/**
 * Логирование по тэгам
 *
 * @param $fileName
 * @param $title
 * @param null $data
 */
function log_tag(string $tagName, string $title, $data = null)
{
    $time = date('r');

    $content = [
        $time,
        $title
    ];

    if ($data)
    {
        $content[] = serialize($data);
    }

    file_put_contents(
        PATH_ROOT. "/logs/tag__$tagName.log",
        implode("\n", $content) . "\n\n",
        FILE_APPEND
    );
}

/**
 * Log error event
 *
 * @param string $message
 * @param array $data
 */
function log_err(string $message, array $data=[])
{
    $user = sp($_SESSION, 'current_user');
    $userId = sp($user, 'id');
    $userName = sp($user, 'name');
    $time = date('r');
    $url = sp($_SERVER, 'REQUEST_URI');

    $log = [
        "",
        "======================",
        "USER: $userName ID: $userId TIME: $time",
        "URL: ". $url,
        "GET:". json_encode($_GET, JSON_PRETTY_PRINT),
        "POST:". json_encode($_POST, JSON_PRETTY_PRINT),
        '~~~~~~~~~~~~~~~~~~~~~~',
        '',
        $message,
        json_encode($data, JSON_PRETTY_PRINT),
        "~~~~~~~~~~~~~~~~~~~~~~",
    ];


    file_put_contents(
        PATH_ROOT. '/logs/err.log',
        implode("\n", $log) . "\n\n",
        FILE_APPEND
    );
}

/**
 * @param Exception $e
 */
function log_exception($e)
{
    $log = [
        date('r'),
        jTraceEx($e)
    ];

    $sep = "\n\n=======================================\n\n";

    file_put_contents(
        PATH_ROOT. 'logs/exceptions.log',
        implode("\n", $log) . $sep,
        FILE_APPEND
    );
}

/**
 * @param Request $request
 */
function log_forex_command(string $header, array $data=[])
{
    file_put_contents(
        PATH_ROOT. '/logs/forex_commands.log',

        $header . "\r\n\r\n" . json_encode($data, 1). "\r\n\r\n\r\n\r\n",
        FILE_APPEND
    );
}

function log_uploaded_picture_command(string $header, array $data=[])
{
    file_put_contents(
            PATH_ROOT. '/logs/upload_pcitures.log',

        $header . "\r\n\r\n" . json_encode($data, 1). "\r\n\r\n\r\n\r\n",
        FILE_APPEND
    );
}

function log_command_on_sheet(\App\Models\Forex\ForexCommand $cmd)
{
    file_put_contents(
        PATH_ROOT. '/logs/forex_commands_sheet.log',
        json_encode($cmd->toArray()). "\n\n",
        FILE_APPEND
    );
}


function json_200($data)
{
    return new Response(200, ['Content-Type' => 'application/json'], (string)json_encode(['data' => $data]));
}

function json_201($data)
{
    return new Response(201, ['Content-Type' => 'application/json'], (string)json_encode(['data' => $data]));
}

/**
 * @param $data
 * @return Response
 */
function json_400($data)
{
    return new Response(400, ['Content-Type' => 'application/json'], (string)json_encode(['message' => 'Invalid payload', 'data' => $data]));
}

/**
 * Forbidden
 * Was the user correctly authenticated, but they don’t have the required permissions to access the resource? 👉 403 Forbidden.
 * @param $message
 * @param $data
 * @return \Slim\Http\Response
 */
function json_403($data=[])
{
    return new Response(403, ['Content-Type' => 'application/json'], (string)json_encode(['message' => 'Current user don\'t have the required permissions', 'data' => $data]));
}

function json_404($message, $data)
{
    return new Response(404, ['Content-Type' => 'application/json'], (string)json_encode(['message' => $message, 'data' => $data]));
}


/**
 * 401 Unauthorized	The requested page needs a username and a password.
 *
 */
function json_401($message, $data=[])
{
    return new Response(401, ['Content-Type' => 'application/json'], (string)json_encode(['message' => $message, 'data' => $data]));
}

/**
 * Api bad request error
 *
 */
function bad_request_error($errors=[])
{
    return json_400($errors);
}

/**
 * Get settings key from settings.php file
 *
 * @param $k
 * @return mixed
 * @throws Exception
 */
function settings($k)
{
    $ret = $GLOBALS['settings']['settings'];
    if (eN($k)) return $ret;

    //=-=-=-=-=-=-=-=-=
    $keys = explode('.', $k);
    $passedKeys = [];

    foreach ($keys as $key) {

        $passedKeys[] = $key;

        if (!isset($ret[$key])) {
            $passed = implode('.', $passedKeys);
            throw new Exception("No such key in settings.php: `$passed`");
        }

        $ret = $ret[$key];
    }

    return $ret;
}

/**
 * Settings value not the val
 *
 * @param $k
 * @param $val
 * @return bool
 * @throws Exception
 *
 */
function not_settings($k, $val)
{
    return settings($k) !== $val;
}

/**
 * @param Request $r
 * @param $reqVal
 * @param $rules
 * @return array
 * @throws ValidationException
 */
function params(Request $r, $reqVal, $rules)
{
    $err = [];

    if ($reqVal)
    {
        try
        {
            val_request($r, $reqVal);
        }
        catch (Exception $e)
        {
            if (settings('env') === 'dev')
            {
                throw $e;
            }

            $err[] = $e;
        }
    }

    $req_body = $r->getParsedBody();

    $ret = map($rules, function ($voRule, $field) use ($r, &$err, $req_body)
    {
        $requestValue = sp($req_body, $field);
//        dump([$field]);echo "helpers.php:769"; exit;

//         = $r->getParam($field);

        try
        {
            $vo = vo($requestValue, $voRule);
            return $vo;
        }
        catch (Exception $e)
        {
            if (settings('env') === 'dev')
            {
                throw $e;
            }

            $err[] = $e;
        }
    });

    if (count($err))
    {
        $ret = array_fill(0, count($rules) + 1, null);
        $ret[0] = $err;
    }
    else
    {
        array_unshift($ret, null);
    }

    return $ret;
}

/**
 * Redirect back to previous page
 *
 * @param $err
 * @return \Slim\Http\Response
 */
function back_with_errors($exceptions)
{
    $list = [];
    foreach ($exceptions as $exception)
    {
        foreach ($exception->data['errors'] as $field => $error)
        {
            $list[$field] = array_values($error);
        }
    }

    $_SESSION['last_bad_request_errors'] = $list;

    return redirect_url($_SESSION['last_requests'][0]);
}

/**
 * @param $folderPath
 */
function create_folder_tree_by_pathname($folderPath)
{
    $folderPath = str_replace('\\','/', $folderPath);
    $folders = explode('/', $folderPath);
    if($folderPath[0] == '/')
    {
        $path = '/';
    }
    else
    {
        $path = '';
    }

    foreach ($folders as $folder)
    {
        if(strlen($folder))
        {
            $path .= $folder. '/';

            if(!file_exists($path))
            {
                mkdir($path);
            }
        }
    }
}

/**
 * Generate uuid-like string from id
 *
 * @param $id
 * @return string
 * @throws Exception
 */
function uuid1FromId($id): string
{
    $uuid1 = Uuid::uuid1();
    $uuid1->toString();

    $appKey = settings('key');
    $idMd5 = md5((string)$id);
    $keyMd5 = md5($appKey . $idMd5);

    return implode('-', [
        $uuid1->toString(),
        substr($idMd5, 3, 4),
        substr($keyMd5, 6, 4),
        substr($idMd5, 17, 4),
        substr($keyMd5, 17, 4)
    ]);
}

function files($folder)
{
    $files = scandir($folder);
    array_shift($files);
    array_shift($files);
    return $files;
}

/**
 * @param $message
 * @param array $data
 * @throws \App\Common\FlowException
 */
function flow_exception($message, $data=[])
{
    throw new \App\Common\FlowException($message, json_encode($data));
}

/**
 * @param $message
 * @param array $data
 * @throws Exception
 */
function crit($message, $data=[])
{
    throw new Exception($message . json_encode($data));
}

/**
 * Set default value if variable is null
 *
 * @param $value
 * @param $defalt
 */
function ifNullDef(&$variable, $defalt)
{
    if (eN($variable))
    {
        $variable = $defalt;
    }
}

/**
 * Throw exception if $assertion is true
 *
 * @param $assertion
 * @param $message
 * @param array $data
 * @throws \App\Common\FlowException
 */
function test($assertion, $message, $data=[])
{
    if ($assertion === true)
    {
        throw new \App\Common\FlowException($message, json_encode($data));
    }
}

/**
 * @param $assertion
 * @param $message
 * @param array $data
 * @throws \App\Common\GateException
 */
function test_gate($assertion, $message, $data=[])
{
    if ($assertion === true)
    {
        throw new \App\Common\GateException($message, json_encode($data));
    }
}

/**
 * @param Exception $e
 * @throws Exception
 */
function handle_task_exception(Exception $e)
{
    if (settings('env') === 'dev')
    {
        throw $e;
    }
    else
    {
        log_exception($e);
    }
}

/**
 * @param $message
 * @param array $data
 * @return \App\Common\ResFail
 */
function resFail($code, $message, $data=[])
{
    $data['_status'] = 'failed';
    $data['_code'] = $code;
    $data['_message'] = $message;
    return new \App\Common\ResFail($data);
}

function resOk(array $data=[])
{
    return new Res($data);
}

function ok(array $data=[])
{
    return new Res($data);
}

function nok($message, $data=[])
{
    return new \App\Common\ResFail([
        'message' => $message,
        'data' => $data
    ]);
}

/**
 * @param Carbon $date
 * @return mixed
 */
function day_of_week(Carbon $date)
{
    $days = [
        0 => 'sunday',
        1 => 'monday',
        2 => 'tuesday',
        3 => 'wednesday',
        4 => 'thursday',
        5 => 'friday',
        6 => 'saturday',
    ];

    return $days[$date->dayOfWeek];
}


/**
 * @return Carbon
 */
function now_date($tz='Etc/GMT')
{
    return Carbon::now($tz);
}

/**
 * @param Carbon $date
 * @return string
 */
function date_mysql_format(Carbon $date)
{
    return $date->format('Y-m-d H:i:s');
}

/**
 * Printf с параметрами в виде массива
 *
 * sprintf2('[v] and [v2]', array('v'=> '11', 'v2'=>'22'))
 *
 * @param $s
 * @param array $data
 * @return mixed
 */
function sprintf2($s, $data=array())
{
    $loop = 3;
    $ret = $s;

    $keys = array_map(function($v){
        return '['.$v.']';
    }, array_keys($data));

    do
    {
        $ret = str_replace($keys, $data, $ret);
        $is_need_again = !eF(strpos($ret, '['));

    }while($loop-- && $is_need_again);

    return $ret;
}


/**
 * Run
 *
 * @param $cmd
 * @return array
 */
function run_php_command_async($cmd)
{
    $tmpl_str = "$cmd > /dev/null &";
//    $tmpl_str = "$cmd";

    $output = array();
    $return_var = array();

    $ret = exec($tmpl_str, $output, $return_var);

    return array(
        'ret'           => $ret,
        'output'        => $output,
        'return_var'    => $return_var,
        'exec_str'      => $tmpl_str
    );
}

/**
 * @param $commandName
 * @param array $params
 * @return array
 */
function run_partisan_async($commandName, $params=[])
{
    $paramsString = '';
    foreach ($params as $key => $val)
    {
        $paramsString .= sprintf2('--[k]="[v]" ',
            array('k' => $key, 'v' => $val));
    }

    $c = "php ../partisan $commandName $paramsString";

    return run_php_command_async($c);
}

/**
 * Run partisan command
 *
 * @param $commandName
 * @param array $params
 * @return array
 */
function run_partisan($commandName, $params=[])
{
    $paramsString = '';
    foreach ($params as $key => $val)
    {
        $paramsString .= sprintf2('--[k]="[v]" ',
            array('k' => $key, 'v' => $val));
    }

    $c = "php ../partisan $commandName $paramsString";

    $output = array();
    $return_var = array();

    $ret = exec($c, $output, $return_var);

    return array(
        'ret'           => $ret,
        'output'        => $output,
        'return_var'    => $return_var,
        'exec_str'      => $c
    );
}

/**
 * Get uploads path
 */
function uploads_path($filename)
{
    return UPLOADS_PATH . DIRECTORY_SEPARATOR . $filename;
}

/**
 * @param $tmpl
 * @param array $data
 * @return mixed
 */
function render($tmpl, $data=[])
{
    global $container;
    return $container->view2->fetch($tmpl, $data);
}

/**
 * @param $title
 * @param $url
 * @param $pageName
 * @param array $submenu
 * @param $currentPage
 * @param $currentSubmenu
 * @return mixed
 */
function menu_item($title, $url, $pageName, $submenu, $currentPage, $currentSubmenu)
{
    global $container;
    $container['menu.current_page'] = $currentPage;
    $container['menu.current_submenu'] = $currentSubmenu;
    $container["menu.page.$pageName.submenu"] = $submenu;

    return render('app/menu_item', [
        'title'     => $title,
        'url'       => $url,
        'page_name' => $pageName,
        'submenu'   => $submenu,
        'current_page' => $currentPage,
        'current_submenu' => $currentSubmenu
    ]);
}

/**
 * @param $currentSubmenu
 * @return mixed|string
 */
function render_submenu($currentPage, $currentSubmenu)
{
    global $container;

//    if (eN($currentSubmenu))
//    {
//        return '';
//    }

    $submenu = null;
    if (isset($container["menu.page.$currentPage.submenu"]))
    {
        $submenu = $container["menu.page.$currentPage.submenu"];
    }

    if (!$submenu)
    {
        return '';
    }

    return render('app/submenu', [
        'current_submenu'   => $currentSubmenu,
        'submenu'           => $submenu,
    ]);
}

/**
 * @param $arr
 * @param $key
 * @param array $def
 */
function array_create_key(&$arr, $key, $def = [])
{
    if (eN(sp($arr, $key)))
    {
        $arr[$key] = $def;
    }
}

/**
 * Get default repo for model
 *
 * @param $class
 * @return Repository
 */
function repo($class)
{
    return new Repository($class);
}

/**
 * Get locale string
 *
 * @param $route
 * @return mixed
 */
function L($route)
{
    global $container;
    return $container['cu_language']->o($route);
}

/**
 * Get list of all keys in array recursivly
 * @param $array
 * @param int $level
 * @return array
 */
function array_key_routes_recursive($array, $level = 1){
    $ret = [];

    foreach($array as $key => $value){
        //If $value is an array.
        if(is_array($value)){
            //We need to loop through it.
            $rec = array_key_routes_recursive($value, $level + 1);
            foreach ($rec as $item) {
                $ret[] = $key. '.'.$item;
            }

        } else{
            //It is not an array, so print it out.
            $ret[] = $key;
        }
    }

    return $ret;
}

/**
 * Get static file version
 *
 * @param $fileUrl
 * @return string
 */
function mix($fileUrl)
{
    $version = 'v';
    if (file_exists(HTDOC_PATH. $fileUrl))
    {
        $version .= filemtime(HTDOC_PATH. $fileUrl);
    }

    return $fileUrl . "?$version=1";
}

/**
 *
 * @param $class
 * @param $needle
 * @return string
 */
function isActive($class, $needle)
{
    return $class == $needle ? 'is-active' : '';
}

/**
 * Check languages folders for consistency
 *
 * @return array
 */
function validateLanguagesConsistence()
{
    $finder = new Finder();
    $finder->directories()->in(PATH_ROOT . 'lang');

    $dics = [];
    $locales = [];

    foreach ($finder as $folder)
    {
        $locale = $folder->getBasename();

        $lang = new Language($locale);
        $dics[$locale] = array_key_routes_recursive($lang->getDictionary());
        $locales[] = $locale;
    }

    $diffs = [];

    foreach ($dics as $locale => $keys)
    {
        foreach ($dics as $locale2 => $keys2) {
            $diff = array_diff($keys, $keys2);

            if (count($diff))
            {
                $diffs[] = [
                    'locale1' => $locale,
                    'locale2' => $locale2,
                    'message' => "`$locale2` miss keys that `$locale` has",
                    'keys' => $diff
                ];
            }

        }
    }

    return $diffs;
}

/**
 * Save variable dump into file
 *
 * @param $name
 * @param $var
 * @return mixed
 */
function store_dump_var($name, $var)
{
    $folder = APP_WORKSPACE . '/data_examples/' . $name. '.php';
    return !eF(file_put_contents($folder, serialize($var)));
}

/**
 * Load variable dump from file
 *
 * @param $name
 * @return mixed
 * @throws \App\Common\GateException
 */
function load_dump_var($name)
{
    $folder = APP_WORKSPACE . '/data_examples/' . $name. '.php';
    test_gate(!file_exists($folder), "Cant find stored var `$name`");

    return unserialize(file_get_contents($folder));
}

/**
 * @param $var
 * @return int
 */
function bool2int(bool $var)
{
    return $var ? 1 : 0;
}

/**
 * Return service from container (dependencies.php)
 *
 * @param string $name
 * @return mixed
 */
function service(string $name)
{
    global $container;
    return $container->get($name);
}

/**
 * @return \Slim\Container
 */
function c()
{
    global $container;
    return $container;
}

function removeOldScreenshots(string $folder, string $pairName, string $tag, int $period, string $percent="")
{
    $cropFolder = HTDOC_PATH . "/$folder/$tag/$pairName/$period/$percent";

    if (file_exists($cropFolder)) {
        $pics = files($cropFolder);
        array_pop($pics);

        foreach ($pics as $item) {
            if (file_exists($cropFolder . $item)) {
                unlink($cropFolder . $item);
            }
        }
    }
}

/**
 * @param  string  $pairName
 * @param  string  $tag
 * @param  string  $period
 * @param  int  $percent
 * @return string|string[]|null
 */
function cropScreenshot(string $pairName, string $tag, int $period, int $percent, int $heightPercent)
{
    $ret = null;
    $folder = HTDOC_PATH . "/screenshots/$tag/$pairName/$period";
    if (!file_exists($folder)) {
        return $ret;
    }

    $pics = files($folder);
    $picture = array_pop($pics);

    if (!$picture) {
        return $ret;
    }

    // cehck if picture already exists
    $cropFolder = HTDOC_PATH . "/screenshots_crops/$tag/$pairName/$period/$percent";
    $destSrc = str_replace(".gif", ".jpeg", "$cropFolder/picture");

    if (file_exists($destSrc)) {
        unlink($destSrc);
    }

    $im = @imagecreatefromgif($folder . '/' . $picture );
    if (!$im) {
        return $ret;
    }

    $height = imagesy($im);
    $width = imagesx($im);
    $cropWidth = round($width * $percent / 100);
    $cropHeight = round($height * $heightPercent / 100);

    $im2 = imagecrop($im, [
        'x' => $width - $cropWidth,
        'y' => $height - $cropHeight,
        'width' => $cropWidth,
        'height' => $cropHeight
    ]);


    create_folder_tree_by_pathname($cropFolder);

    if ($im2 !== FALSE) {
        imagejpeg($im2, $destSrc);
        imagedestroy($im2);
        imagedestroy($im);
        return $destSrc;
    } else {
        imagedestroy($im);
        return null;
    }
}


/**
 * @param  string  $picsFolder
 * @param  string  $pairName
 * @param  string  $tag
 * @param  string  $period
 * @return array
 */
function getTagScreenshot(string $folder, string $pairName, string $tag, int $period)
{
    $pic = null;
    $picsFolder = HTDOC_PATH . "/$folder/$pairName/$period/";

    if (file_exists($picsFolder)) {
        $tagPics = files($picsFolder);
        $picture = array_pop($tagPics);

//        [, $time] = explode('__', $picture);
//        $time = str_replace('.gif', '', $time);
//        $time = str_replace('.jpeg', '', $time);
//        $time = str_replace('_', ':', $time);

        $pic = [
            'src' => "/$folder/$pairName/$period/$picture",
            'time' => "",
            'period' => $period,
            'tag' => $tag,
//            'crop_percent' => $percent,
            '_id' => "pic-$pairName-$period",
        ];
    }

    return $pic;
}

function build_pair_pics_feed(string $pair, int $period, int $pic_percents, array &$ret_pics)
{
    foreach(settings('pic_names') as $pic_name)
    {
        array_create_key($screenshots[$pair][$pic_name], $period);
        $pic = getTagScreenshot('screenshots_crops', $pair, $pic_name, $period, $pic_percents);
        $ret_pics[$pair][$pic_name][$period] = $pic;
    }
}

function build_drawning_pics_feed(string $pair, int $period, int $pic_percents, array &$ret_pics)
{
    $pics = [];

    foreach(settings('pic_names') as $pic_name) {
        $pics[$pic_name] = drawning_url($pic_name, $pair, $period, $pic_percents);
    }

    $ret_pics[$pair][$period] = $pics;
}

function merge_png(string $pic1, string $pic2, string $result_pic)
{
    $image_1 = imagecreatefrompng($pic1);
    $image_2 = imagecreatefrompng($pic2);
    imagealphablending($image_1, true);
    imagesavealpha($image_1, true);
    $height = imagesy($image_2);
    $width = imagesx($image_2);

    imagecopy($image_1, $image_2, 0, 0, 0, 0, $width, $height);
    return imagepng($image_1, $result_pic);
}

/**
 * @param string $instrument
 * @param int $period
 * @return string|null
 */
function drawning_url(string $pic_name, string $instrument, int $period)
{
    if(!drawning_path($pic_name, $instrument, $period)) return NULL;
    $path = ["", SCREENSHOTS_DRAWNINGS, $instrument, $period, $pic_name . '_drawning.png'];
    return std_path($path);
}

function drawning_path(string $pic_name, string $instrument, int $period)
{
    $path = std_path([SCREENSHOTS_DRAWNINGS_PATH, $instrument, $period, $pic_name . '_drawning.png']);
    if(!file_exists($path)) return NULL;
    return $path;
}

function std_path(array $path)
{
    return implode("/", $path);
}

function period_prev(int $period)
{
    switch($period)
    {
        case 5: return 1;
        case 15: return 5;
        case 60: return 15;
        case 240: return 60;
        case 1440: return 240;
    }
    return $period;
}

function period_next(int $period)
{
    switch($period)
    {
        case 5: return 15;
        case 15: return 60;
        case 60: return 240;
        case 240: return 1440;
    }
    return $period;
}

function instrument_screener__state_key(string $pair)
{
    return 'instrument-state-' . $pair;
}

function timeseria_last_values_key(string $tag)
{
    return 'timeseriea-last-values-' . $tag;
}

function timeseria_last_candels_key(string $tag)
{
    return 'timeseriea-last-candels-' . $tag;
}
function chart_objects_key(string $symbol)
{
    return "chart-objects-$symbol";
}

function chart_hlines_key(string $symbol)
{
    return "chart-hlines-$symbol";
}


function ohlc_pivots_key()
{
    return 'ohlc-candles-pivots';
}

function array_to_hash($arr, $key)
{
    $ret = [];
    foreach ($arr as $a)
    {
        $ret[$a[$key]] = $a;
    }
    return $ret;
}

/**
 * @param $name
 * @return mixed
 */
function load_data_dump($name)
{
    $ret = file_get_contents(APP_WORKSPACE . '/data_examples/' . $name . '.php');
    return unserialize($ret);
}

function get_last_value(string $tag, string $pair, int $period) : float
{
    $last_values = Redis2::get(timeseria_last_values_key($tag));
    return (float) sp($last_values[$pair][$period], 'val', 0);
}

/**
 * @param Carbon $time
 * @param int $period
 * @return string
 */
function normalize_time(Carbon $time, int $period): string
{
    $ret = '';

    switch($period)
    {
        case 1:
            $ret = $time->format('Y.m.d H:i:00');
            break;
        case 5:
            $minutes = floor($time->format('i') / $period);
            $ret = $time->format("Y.m.d H:".sprintf("%02d", $minutes * 5).":00");
            break;
        case 15:
            $minutes = floor($time->format('i') / $period);
            $ret = $time->format("Y.m.d H:".sprintf("%02d", $minutes * 15).":00");
            break;
        case 60:
            $ret = $time->format('Y.m.d H:00:00');
            break;
        case 240:
            $hours = floor($time->format('H') / 4);
            $ret = $time->format("Y.m.d ". sprintf("%02d", ($hours*4)) .":00:00");
            break;
        case 1440:
            $ret = $time->format("Y.m.d 00:00:00");
            break;
    }

    return $ret;
}

function filter_tlines_by_tag(array $tlines, $tag)
{
    $ret = array_filter($tlines, function ($i) use ($tag) {
        return $i['chart_tag'] == $tag;
    });

    return array_values($ret);
}

function push_to_voice_queue(string $notify)
{
    Redis2::lPush('voice_queue', $notify);
}

function push_to_beep_queue(string $pair, int $period, int $direction, int $beep)
{
    $item = json_encode([
        'pair' => $pair,
        'period' => $period,
        'direction' => $direction,
        'beep' => $beep,
    ]);
    Redis2::lPush('beep_queue', $item);
}

function push_to_queue(string $queue, array $item)
{
    Redis2::lPush($queue, json_encode($item));
}

function symbol_localization(string $symbol)
{
    switch ($symbol)
    {
        case "US_DX": return "индекс";
        case "XAUUSD": return "золото";
        case "BTCUSD": return "биткоин";
        case "EURUSD": return "евро";
        case "USDCHF": return "франк";
        case "EURJPY": return "евро ена";
        case "NZDJPY": return "новозеландия ена";
        case "USDJPY": return "ена";
        case "CHFJPY": return "франк ена";
        case "GBPUSD": return "фунт";
        case "GBPCHF": return "фунт франк";
        case "GBPJPY": return "джиджей";
        case "CADJPY": return "канада ена";
        case "AUDUSD": return "австралия";
        case "AUDJPY": return "австралия ена";
        case "USDCAD": return "канада";
        case "NZDUSD": return "новозеландия";
        case "USDNOK": return "доллар нок";

        default:
            return $symbol;
    }
}
function period_localization(int $period)
{
     switch ($period)
     {
         case 1: return "минутки";
         case 5: return "5 мин";
         case 15: return "15 мин";
         case 30: return "30 мин";
         case 60: return "1 час";
         case 240: return "4 часа";
         case 1440: return "дни";
         case 10080: return "недели";
     }

     return $period;
}

function calc_symbol_stop_loss(string $symbol, float $price, float $price2)
{
    $sl = abs($price - $price2);

    switch ($symbol)
    {
        case "EURJPY":
        case "GBPJPY":
        case "USDJPY":
        {
            $sl *= 100;
            $sl = (int)$sl;
            break;
        }
        case "USDCAD":
        case "GBPUSD":
        case "EURUSD":
        {
            $sl *= 10000;
            $sl = (int)$sl;
            break;
        }
        case "XAUUSD":
        {
            $sl *= 10;
            $sl = (int)$sl;
            break;
        }
    }

    return $sl;
}

function hline_trading_params(string $symbol, int $period, string $type, string $uniq_id, float $price, float $price2)
{
    $sl = calc_symbol_stop_loss($symbol, $price, $price2);

    $trading_settings = Redis2::get('chart_trading_settings');
    $trading_settings = $trading_settings[$symbol][$period];

    $risk = isset($trading_settings['risk']) ? $trading_settings['risk'] : 1;

    if ($symbol == 'XAUUSD')
    {
        $risk /= 100;
//        $sl = 20;
    }

    return [
        "type" => $type,
        "sl" => $sl,
        "sl_val" => $type == "buy" ? min($price, $price2) : max($price, $price2),
        "be" => isset($trading_settings['be']) ? $trading_settings['be'] : 5,
        "tp_coef" => 1,
        "risk" => $risk,
        "risk_type" => "depo_percents",
        "pair" => $symbol,
        "period" => $period,
        "uniq_id" => $uniq_id,
        "is_skip_be" => 0
    ];
}

function trading_params(string $symbol, int $period, string $type, string $uniq_id, int $force_risk = 0, int $force_sl = 0)
{
    $trading_settings = Redis2::get('chart_trading_settings');
    $trading_settings = $trading_settings[$symbol][$period];

    if ($force_sl)
    {
        $sl = $force_sl;
    }
    else
    {
        $sl = isset($trading_settings['sl']) ? $trading_settings['sl'] : 10;
    }

    if ($force_risk)
    {
        $risk = $force_risk;
    }
    else
    {
        $risk = isset($trading_settings['risk']) ? $trading_settings['risk'] : 30;
    }

    if ($symbol == 'XAUUSD')
    {
        $risk /= 100;
//        $sl = 20;
    }

    return [
        "type" => $type,
        "sl" => $sl,
//        'sl_type' => 'dynamic_extremum',
        "be" => isset($trading_settings['be']) ? $trading_settings['be'] : 5,
        "tp_coef" => 1,
        "risk" => $risk,
        "risk_type" => "depo_percents",
        "pair" => $symbol,
        "period" => $period,
        "uniq_id" => $uniq_id,
        "is_skip_be" => 0
    ];
}

function date_mt4_date(string $date)
{
    return str_replace('-', '.', $date);
}

function mt4_date__mysql_date(string $date)
{
    return str_replace('.', '-', $date);
}

function array_flatten($array = null) {
    $result = array();

    if (!is_array($array)) {
        $array = func_get_args();
    }

    foreach ($array as $key => $value) {
        if (is_array($value)) {
            $result = array_merge($result, array_flatten($value));
        } else {
            $result = array_merge($result, array($key => $value));
        }
    }

    return $result;
}

function send_telegram_notification($text)
{
    $telegram = new Telegram\Bot\Api(getenv('TELEGRAM_BOT_TOKEN'));

    $channelId = "@charts22";
    $response = $telegram->sendMessage([
        'chat_id' => $channelId,
        'text' => $text,
    ]);
}
