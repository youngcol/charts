<?php

use Slim\Routing\RouteCollectorProxy;

$app->group('/api_1', function (RouteCollectorProxy $group) {

    $group->post('/run/task', 'App\Api\AppController:runTask')->setName('api.run.task');

//    $this->group('/social_engine', function () {
//
//        $this->post('/post',        'App\Api\SocialEngineController:postStoreAction')->setName('api.post.store');
//        $this->post('/post/update', 'App\Api\SocialEngineController:postUpdateAction')->setName('api.post.update');
//
//    })->add(new App\Middlewares\Auth($this->getContainer(), 'api'));

//    $this->group('/app', function () {
//
//        $this->post('/media/upload', 'App\Api\AppController:uploadMediaFile')->setName('api.media.upload');
//
//        $this->delete('/user_social_account', 'App\Api\SocialEngineController:deleteUserSocialAccountAction')->setName('user_social_account.delete');
//
//        $this->post('/res/GetLastPostsTask', 'App\Api\AppController:resGetLastPostsTask')->setName('GetLastPostsTask');
//        $this->post('/res/UpdatePostTask', 'App\Api\AppController:resUpdatePostTask')->setName('UpdatePostTask');
//        $this->post('/res/GetPinterestAccountsBoardsTask', 'App\Api\AppController:resGetPinterestAccountsBoardsTask')->setName('GetPinterestAccountsBoardsTask');
//
//
//    })->add(new App\Middlewares\Auth($this->getContainer(), 'api'));
//
//
//    // app webhooks
//    $this->group('/webhooks', function () {
//
////        $this->post('/hook_name', 'App\Api\AppController:resGetLastPostsTask')->setName('GetLastPostsTask');
////        $this->get('/hook_name', 'App\Api\AppController:resGetLastPostsTask')->setName('GetLastPostsTask');
//
//    });
});
