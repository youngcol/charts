<?php

namespace App\Api;

use App\Common\ValidationException;
use App\Controllers\Controller;
use App\Facades\CU;
use App\Models\SocialEngine\UserSocialAccount;
use App\Tasks\GetModelsCollectionTask;
use App\Tasks\MediaFile\UploadMediaToLocalFolderTask;
use App\Tasks\SocialEngine\GetLastPostsTask;
use App\Tasks\SocialEngine\GetPinterestAccountsBoardsTask;
use Exception;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class AppController extends Controller
{
    public function resGetPinterestAccountsBoardsTask(Request $request, Response $response, $args)
    {
        list
            (
            $err,
            $voAccountIds,
            ) = params(
            $request,
            'res.GetPinterestAccountsBoardsTask',
            [
                'accountIds' => 'ids.string'
            ]
        );

        if ($err)
        {
            $messages = map($err, function ($item) {
                return $item->getMessage();
            });
            return bad_request_error($messages);
        }

        $res = task(new GetPinterestAccountsBoardsTask,
            [
                CU::user(),
                $voAccountIds->toIds()
            ]
        );

        return json_200([
            'resGetPinterestAccountsBoardsTask' => $res
        ]);
    }

    public function resGetLastPostsTask(Request $request, Response $response, $args)
    {
        list
            (
            $err,
            $type,
            $page,
            $limit,
            ) = params(
            $request,
            'res.GetLastPostsTask',
            [
                'type' => 'post.status',
                'page' => 'numeric',
                'limit' => 'numeric',
            ]
        );

        if ($err)
        {
            $messages = map($err, function ($item) {
                return $item->getMessage();
            });
            return bad_request_error($messages);
        }

        $res = task(new GetLastPostsTask,
            [
                $type,
                $page->toInt(),
                $limit->toInt()
            ]
        );

        return json_200([
            'resGetLastPostsTask' => $res
        ]);
    }

    public function publishAction(Request $request, Response $response, $args)
    {
        $params = [
            'title' => 'Publish',
            'page' => 'publish'
        ];

        return $this->appRender('app/index', $params, $request, $response);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return \Slim\Http\Response
     * @throws Exception
     */
    public function runTask(Request $request, Response $response, $args)
    {
        list
            (
            $err,
            $taskName
            ) = params(
            $request,
            'run.task',
            [
                'task_name' => 'filled.string',
            ]
        );

        if ($err)
        {
            $messages = map($err, function ($item) {
                return $item->getMessage();
            });
            return bad_request_error($messages);
        }

        // check task api request
        $taskRequest = sp($this->container['tasks_api_requests'], $taskName->_());

        if (!$taskRequest)
        {
            $taskNameParts = explode('.', $taskName->_());

            if (count($taskNameParts) === 1)
            {
                $folder = '';
                $taskFile = sp($taskNameParts, 0);
            }
            else
            {
                $folder = '/' . sp($taskNameParts, 0, '');
                $taskFile = sp($taskNameParts, 1);
            }

            $requestFile = API_TASKS_REQUESTS_PATH . $folder. '/api-request--' . $taskFile . '.php';

            if (file_exists($requestFile))
            {
                $taskRequest = include $requestFile;
            }
        }

        if (!$taskRequest)
        {
            return bad_request_error(['No api request for this task']);
        }

        // check user permissions
        $missedPermissions = [];
        foreach ($taskRequest['allowed_permissions'] as $permission)
        {
            if (!CU::user()->can($permission))
            {
                $missedPermissions[] = $permission;
            }
        }

        if (count($missedPermissions))
        {
            return json_403([
                'permissions' => $missedPermissions
            ]);
        }

        $taskParams = $request->getParsedBody()["task_params"];
        if(is_string($taskParams))
        {
            $taskParams = json_decode($taskParams, 1);
        }
        $params = !is_array($taskParams) ? $taskParams->toArray() : $taskParams;


        $err = [];
        $voTaskParams = [];

        map($taskRequest['params'], function ($voRule, $field) use ($params, &$err, &$voTaskParams)
        {
            $voSplittedRules = explode('|', $voRule);
            $vo = null;

            foreach ($voSplittedRules as $voSplittedRule)
            {
                try
                {
                    $vo = vo(sp($params, $field), $voSplittedRule);
                }
                catch (Exception $e)
                {
                    $e->setDataField('FIELD NAME', $field);
                    $err[] = $e;
                    break;
                }
            }

            $voTaskParams[$field] = $vo;
        });

        if (count($err))
        {
            return bad_request_error($err);
        }
        //-=-=-=-=-=-=-=-=-=-=-=-=

        try
        {
            $res = $taskRequest['runner']($voTaskParams);
        }
        catch (\Exception $e)
        {
            handle_task_exception($e);($e);


            return json_400([
                'res__'. $taskName => resFail('last_tier_runner_handle_exceptions','', [
                    'e' => [
                        'message' => $e->getMessage(),
                        'code' => $e->getCode()
                    ],
                ])
            ]);
        }

        return json_200([
            'res__'. $taskName => $res
        ]);
    }

    public function uploadMediaFile(Request $request, Response $response)
    {
        $uploadedFiles = $request->getUploadedFiles();
        $uploadedFile1 = $uploadedFiles['media'];

        $res = task(new UploadMediaToLocalFolderTask(), [
            CU::user(),
            $uploadedFile1
        ]);

        return json_200([
            'res' => $res
        ]);
    }
}
