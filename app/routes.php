<?php
use Slim\Routing\RouteCollectorProxy;

$app->group('/admin', function (RouteCollectorProxy $group) {

	$group->get('/development', 'App\Controllers\Admin\AdminController:development')->setName('admin.development');
	$group->get('/mysql_viewer', 'App\Controllers\Admin\MysqlViewerController:indexAction')->setName('admin.mysql_viewer');
	$group->get('/mysql_viewer/table/{table}', 'App\Controllers\Admin\MysqlViewerController:showTableAction')->setName('admin.mysql_viewer.table');

    $group->get('/phpunit', 'App\Controllers\Admin\AdminController:phpunitViewerAction')->setName('admin.phpunit');
    $group->get('/tasks_viewer', 'App\Controllers\Admin\AdminController:tasksViewerAction')->setName('admin.tasks.viewer');
    $group->get('/tasks/logs', 'App\Controllers\Admin\AdminController:tasksLogsAction')->setName('admin.tasks.logs');
    $group->get('/php_stories/viewer', 'App\Controllers\Admin\AdminController:phpStoriesViewerAction')->setName('admin.php_stories.viewer');


	if (settings('env') === 'dev' || settings('env') === 'test')
    {
        $group->get('/mysql_builder', 'App\Controllers\Admin\AdminController:mysqlBuilder')->setName('admin.mysql_builder');
        $group->get('/routes', 'App\Controllers\Admin\AdminController:routes')->setName('admin.routes');
        $group->get('/settings', 'App\Controllers\Admin\AdminController:settingsAction')->setName('admin.settings');

        $group->post('/development/command', 'App\Controllers\Admin\AdminController:developmentFormAction')->setName('admin.development.command');
        $group->get('/database/state', 'App\Controllers\Admin\AdminController:databaseStateAction')->setName('admin.database.state');
        $group->get('/database/state/run/{file}', 'App\Controllers\Admin\AdminController:runDatabaseStateAction')->setName('admin.database.state.run');
        $group->get('/variables/viewer', 'App\Controllers\Admin\AdminController:variablesViewerAction')->setName('admin.variables.viewer');

    }

    $group->get('/languages', 'App\Controllers\Admin\AdminController:languagesAction')->setName('admin.languages');
	$group->get('/dashboard', 'App\Controllers\Admin\AdminController:dashboard')->setName('admin.dashboard');
	$group->get('/logs', 'App\Controllers\Admin\AdminController:logs')->setName('admin.logs');


	$group->get('/users', 'App\Controllers\Admin\UserController:index')->setName('admin.users');
	$group->get('/users/add', 'App\Controllers\Admin\UserController:add')->setName('usersadd');
	$group->post('/users/save', 'App\Controllers\Admin\UserController:save')->setName('userssave');
	$group->get('/users/edit/{id}', 'App\Controllers\Admin\UserController:edit')->setName('usersedit');
	$group->post('/users/update/{id}', 'App\Controllers\Admin\UserController:update')->setName('usersupdate');
	$group->get('/users/delete/{id}', 'App\Controllers\Admin\UserController:delete')->setName('usersdelete');

});//->add(new App\Middlewares\Auth($container, 'web', 'admin'));


//if (settings('env') === 'dev')
//{
    $app->get('/workspace/file', 'App\Controllers\Admin\AdminController:workspaceFileContent')->setName('admin.workspace');

    $app->group('/w', function (RouteCollectorProxy $group) {
        $group->get('/search', 'App\Controllers\Admin\WorkspaceController:search');
        $group->post('/folder', 'App\Controllers\Admin\WorkspaceController:folderCreate')->setName('w.folder.create');
        $group->post('/file', 'App\Controllers\Admin\WorkspaceController:fileCreate')->setName('w.file.create');
        $group->get('/file/content', 'App\Controllers\Admin\WorkspaceController:fileContent')->setName('admin.file.content');
        $group->get('/file/output', 'App\Controllers\Admin\WorkspaceController:fileOutput')->setName('admin.file.output');
        $group->get('', 'App\Controllers\Admin\WorkspaceController:index')->setName('w.index');
        $group->get('/left-column', 'App\Controllers\Admin\WorkspaceController:leftColumn')->setName('w.leftColumn');
    });//->add(new App\Middlewares\Auth($container, 'web'));

    $app->map(['post', 'get', 'put', 'patch'],'/webhook-sandbox/{hookName}', 'App\Controllers\Admin\AdminController:webhookAction')->setName('catch-webhook');
//}



//=================================
//======= PUBLIC ====================
//=================================

// user login, reset password, signup routes
$app->get('/email-verify/{code}', 'App\Controllers\HomeController:emailVerify')->setName('email.verify');
$app->get('/password/forgot', 'App\Controllers\HomeController:passwordForgot')->setName('password.forgot');
$app->post('/password/forgot', 'App\Controllers\HomeController:passwordForgot')->setName('password.forgot');
$app->get('/password/reset/{code}', 'App\Controllers\HomeController:passwordReset')->setName('password.reset');
$app->post('/password/change', 'App\Controllers\HomeController:passwordChange')->setName('password.change');
$app->post('/login', 'App\Controllers\HomeController:loginAction')->setName('login');
$app->get('/logout', 'App\Controllers\HomeController:logoutAction')->setName('logout');
$app->get('/signin', 'App\Controllers\HomeController:signinAction')->setName('signin');

if (settings('env') !== 'live_closed')
{
    $app->get('/signup', 'App\Controllers\HomeController:signup')->setName('signup');
    $app->post('/signup/store', 'App\Controllers\HomeController:signupStore')->setName('signup.store');
}

// public pages
$app->get('/', 'App\Controllers\HomeController:index')->setName('index');
$app->get('/features', 'App\Controllers\HomeController:features')->setName('features');
$app->get('/prices', 'App\Controllers\HomeController:prices')->setName('prices');
$app->get('/blog', 'App\Controllers\BlogController:index')->setName('blog');
$app->get('/terms', 'App\Controllers\HomeController:termsAction')->setName('terms');
$app->get('/privacy-policy', 'App\Controllers\HomeController:privacyPolicyAction')->setName('privacy-policy');


$app->get('/new-layout', 'App\Controllers\HomeController:newLayoutAction')->setName('blog');
$app->post('/test', 'App\Controllers\HomeController:testAction')->setName('olymp');

$app->get('/electrician', 'App\Controllers\LandingsController:electricianIndex')->setName('landings.electrician');

$app->get('/timeline/{timeline}', 'App\Controllers\App\AppController:timelineAction')->setName('app.timeline');


// create signals

//$app->get('/adx_signal', 'App\Controllers\HomeController:adxSignal')->setName('adx_signal');
$app->get('/ripper_signal', 'App\Controllers\HomeController:ripperSignalAction')->setName('ripper_signal');
$app->post('/ripper_event', 'App\Controllers\HomeController:ripperEventAction')->setName('ripper_signal');
$app->post('/swings_state', 'App\Controllers\HomeController:swingsStateAction')->setName('swings_state');
$app->get('/order_closed', 'App\Controllers\HomeController:orderClosedAction')->setName('order_closed');

$app->post('/timeseries', 'App\Controllers\HomeController:timeseriaAction')->setName('timeseria');
$app->post('/timeseries_grouped', 'App\Controllers\HomeController:timeseriaGroupedAction')->setName('timeseries_grouped');
$app->post('/screenshot/image', 'App\Controllers\HomeController:screenshotImageAction')->setName('screenshotImageAction');
$app->post('/screenshot/upload', 'App\Controllers\HomeController:uploadScreenshotAction');
$app->post('/tickdata', 'App\Controllers\HomeController:ticksAction')->setName('ticks');

$app->group('/forex', function (RouteCollectorProxy $group) {
    $group->get('/commands/sheet', 'App\Controllers\App\ForexCoreController:commandsSheetAction');
    $group->post('/commands/webhook', 'App\Controllers\App\ForexCoreController:commandsWebhookAction');
    $group->post('/log', 'App\Controllers\App\ForexCoreController:logAction');
    $group->get('/status', 'App\Controllers\App\ForexCoreController:statusAction');
});


//=================================
//===== APP ROUTES =====================
//=================================


// reserved area
$app->group('/app', function (RouteCollectorProxy $group) {
    $group->get('/settings', 'App\Controllers\App\AccountSettingsController:indexAction')->setName('app.settings');
    $group->get('/settings/change-password', 'App\Controllers\App\AccountSettingsController:changePasswordAction')->setName('app.settings.change-password');

    $group->get('', 'App\Controllers\App\AppController:indexAction')->setName('app.index');
    $group->get('/timeline/{timeline}', 'App\Controllers\App\AppController:timelineAction')->setName('app.timeline');
    $group->get('/screenshots', 'App\Controllers\App\AppController:screenshotsAction')->setName('app.screenshots');
    $group->get('/screenshots/small', 'App\Controllers\App\AppController:screenshotsSmallAction')->setName('app.screenshots.small');
    $group->get('/screenshots/tape', 'App\Controllers\App\AppController:screenshotsTapeAction')->setName('app.screenshots.tape');

    $group->get('/screenshots/overview', 'App\Controllers\App\AppController:screenshotsOverviewAction')->setName('app.screenshots.tape');
    $group->get('/chartgroup/{chart_group}', 'App\Controllers\App\AppController:chartGroupAction')->setName('app.chartgroup');

    $group->get('/screenshots/push', 'App\Controllers\App\AppController:screenshotsPushAction')->setName('app.screenshots.push');
    $group->get('/screenshots/panel', 'App\Controllers\App\AppController:panelAction')->setName('app.screenshots.panel');
    $group->get('/panel2', 'App\Controllers\App\AppController:panel2Action')->setName('app.screenshots.panel2');
    $group->get('/panel3', 'App\Controllers\App\AppController:panel3Action')->setName('app.screenshots.panel3');
    $group->get('/game', 'App\Controllers\App\AppController:gameAction')->setName('app.screenshots.panel');
    $group->get('/game2', 'App\Controllers\App\AppController:game2Action')->setName('app.screenshots.panel');
    $group->get('/mobile', 'App\Controllers\App\AppController:mobileAction')->setName('app.screenshots.panel');
    $group->get('/signals', 'App\Controllers\App\AppController:signalsAction')->setName('app.screenshots.panel');

    if (settings('env') === 'dev')
    {
        $group->get('/templates', 'App\Controllers\App\AppController:templatesAction')->setName('app.templates');
        $group->get('/whole-ui-kit', 'App\Controllers\App\AppController:wholeUiKitAction')->setName('app.whole-ui-kit');
        $group->get('/vue-stories', 'App\Controllers\App\AppController:vueStoriesAction')->setName('app.vue.stories');
        $group->get('/vue-stories/{folder}/{story}', 'App\Controllers\App\AppController:vueStoriesAction')->setName('app.vue.story');

    }

});//->add(new App\Middlewares\Auth($container));

