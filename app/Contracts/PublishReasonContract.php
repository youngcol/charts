<?php
declare(strict_types=1);

namespace App\Contracts;

use App\Models\SocialEngine\Post;
use App\Models\SocialEngine\Post\PostPublishParams;

interface PublishReasonContract
{
    public function getPost(): Post;
    public function getPostPublishParams(): PostPublishParams;
}
