<?php

namespace App\Controllers\App;

use App\Controllers\Controller;
use App\Tasks\ForexCore\get__execution__commands__list__task;
use App\Tasks\ForexCore\process__command__result__task;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

class ForexCoreController extends Controller
{
    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return \Slim\Http\Response
     * @throws \Exception
     */
    public function commandsSheetAction(Request $request, Response $response, $args)
    {
        $list = task(new get__execution__commands__list__task, []);
        return json_200($list->toArray());
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return \Slim\Http\Response
     */
    public function logAction(Request $request, Response $response, $args)
    {
        log_forex_command($request->getParam('processed_at') . " RIPPER: " . $request->getParam('header'), [$request->getParam('line')]);
        return json_200(true);
    }

    public function statusAction(Request $request, Response $response, $args)
    {
        $params = [
            'page' => 'home',
            'title' => 'status',
        ];
        return $this->appRender('/app/forex/status', $params, $request, $response);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return \Slim\Http\Response
     * @throws \Exception
     */
    public function commandsWebhookAction(Request $request, Response $response, $args)
    {
        store_dump_var("ForexCoreController_commandsWebhookAction", $request->getParams());

        $res = task(new process__command__result__task(), [
            (int)$request->getParam('command_id'),
            $request->getParam('command_name'),
            json_decode($request->getParam('result'), 1)
        ]);

        return json_200([true]);
    }
}
