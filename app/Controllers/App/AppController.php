<?php

namespace App\Controllers\App;

use App\Controllers\Controller;
use App\Facades\CU;
use App\Models\ChartGroup;
use App\Models\SocialEngine\Post;
use App\Models\SocialEngine\UserSocialAccount;
use App\Models\Timeline;
use App\Models\User;
use App\Models\Watchlist;
use App\Tasks\ForexCore\get__pairs__by__group__task;
use App\Tasks\get__popup__timeline__task;
use App\Tasks\get__unmapped__symbols__task;
use App\Tasks\get__watchlist__items__task;
use App\Tasks\load__chart__group__data__task;
use App\Tasks\Screenshots\get__overview__pairs__task;
use App\Tasks\Screenshots\get__tape__pairs__task;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Rakit\Validation\Rules\Uppercase;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Finder\Finder;

class AppController extends Controller
{
    public function indexAction(Request $request, Response $response, $args)
    {
        return redirect('app.screenshots.tape');

        $timelines = repo(Timeline::class)->get();

        $params = [
            'title' => 'Dashboard',
            'page' => 'dashboard',
            'submenu' => 'dashboard',
            'timelines' => $timelines,
        ];

        return $this->appRender('app/index', $params, $request, $response);
    }

    public function chartGroupAction(Request $request, Response $response, $args)
    {
        $groupId = sp($args, 'chart_group');


        $res = task(new load__chart__group__data__task,
            [
                CU::user(),
                vo($groupId, 'numeric'),
                null
            ]
        );

        $params = [
            'title' => 'Chart group',
            'page' => 'chart_group',

            'resChartGroup' => $res
        ];

        return $this->appRender('app/chart_group', $params, $request, $response);
    }

    /**
     * @param  Request  $request
     * @param  Response  $response
     * @param $args
     * @return mixed
     */
    public function screenshotsAction(Request $request, Response $response, $args)
    {
        $params = [
            'title' => 'Screenshots',
            'page' => 'screenshots',
            'pairs' => [],
            'tags' => [],
            'pics' => []
        ];

        $period = '5';
        $overview = [];

        $folder = HTDOC_PATH . '/screenshots/';
        $pairs = files($folder);
        $tags = [];

        foreach ($pairs as $index => $pair) {
            $pairTags = files($folder . $pair);
            $tags[] = $pairTags;

            foreach ($pairTags as $tag) {
                $pic = getTagScreenshot(
                    $folder . $pair . '/' . $tag. '/' . $period,
                    $pair,
                    $tag,
                    $period
                );

                if ($pic) {
                    $overview[$pair] = $pic;
                }
            }

        }

        $params['pairs'] = $pairs;
        $params['tags'] = $tags;
        $params['overview'] = $overview;

        return $this->appRender('app/screenshots', $params, $request, $response);
    }

    public function screenshotsTapeAction(Request $request, Response $response, $args)
    {
        $params = [
            'title' => 'Screenshots small',
            'page' => 'screenshots_small',
            'pairs' => [],
            'tags' => [],
            'pics' => []
        ];

        $res = task(new get__tape__pairs__task, [array_flatten(settings('pairs_list')), [5]]);

        $params['tape'] = $res->get('res');
        $params['original'] = $res->get('original');

        return $this->appRender('app/screenshots_tape', $params, $request, $response);
    }

    public function panel2Action(Request $request, Response $response, $args)
    {
        $params = [
            'title' => 'ver 6 panel',
            'page' => 'panel2',
            'pairs' => array_chunk(array_map(function ($pair) {
                return ['pair' => $pair];
            },
                array_flatten(settings('pairs_list'))
            ), 5),
            'pairs_list' => settings('pairs_list')
        ];
        return $this->appRender('app/panel2', $params, $request, $response);
    }
    public function panel3Action(Request $request, Response $response, $args)
    {
        $params = [
            'title' => 'ver 7 panel',
            'page' => 'panel3',
            'pairs' => array_chunk(array_map(function ($pair) {
                return ['pair' => $pair];
            },
                array_flatten(settings('pairs_list'))
            ), 5),
            'pairs_list' => settings('pairs_list')
        ];
        return $this->appRender('app/panel3', $params, $request, $response);
    }

    public function panelAction(Request $request, Response $response, $args)
    {
        $params = [
            'title' => 'ver 5 panel',
            'page' => 'screenshots_small',
            'pairs' => array_chunk(array_map(function ($pair) {
                    return ['pair' => $pair];
                },
                array_flatten(settings('pairs_list'))
            ), 5)
        ];
        return $this->appRender('app/screenshots_panel', $params, $request, $response);
    }

    public function mobileAction(Request $request, Response $response, $args)
    {
        $events_tags = ['nh', 'nl', 'tlb'];
        $history = 50;

        $params = [
            'title' => 'Mobile',
            'page' => 'mobile',
            'symbols' => settings('pairs_list'),
            'markup_requests' =>  task(new get__unmapped__symbols__task, [
                settings('indicator.tag'),
                settings('charts.markup_periods')
            ]),
            'events' => task(new \App\Tasks\get__events__history__task,
                [
                    $history,
                    $events_tags
                ]
            ),
        ];
        return $this->appRender('app/mobile', $params, $request, $response);
    }

    public function gameAction(Request $request, Response $response, $args)
    {
        $params = [
            'title' => 'Game',
            'page' => 'screenshots_small',
            'symbols' => settings('pairs_list')
        ];
        return $this->appRender('app/game', $params, $request, $response);
    }

    public function game2Action(Request $request, Response $response, $args)
    {
        $params = [
            'title' => 'Game2',
            'page' => 'game2',
            'symbols' => settings('pairs_list')
        ];
        return $this->appRender('app/game2', $params, $request, $response);
    }

    public function signalsAction(Request $request, Response $response, $args)
    {
        $params = [
            'title' => 'Signals',
            'page' => 'signals',
            'symbols' => settings('pairs_list')
        ];
        return $this->appRender('app/signals', $params, $request, $response);
    }
    public function screenshotsPushAction(Request $request, Response $response, $args)
    {
        $params = [
            'title' => 'ver 3 push',
            'page' => 'screenshots_small',
            'pairs' => [],
            'tags' => [],
            'pics' => []
        ];

        $res = task(new get__pairs__by__group__task, [
            'push'
        ]);

        $res = task(new get__tape__pairs__task, [
            $res->pairs,
            settings('screenshots.periods')
        ]);

        $params['tape'] = $res->get('tape');
        return $this->appRender('app/screenshots_push', $params, $request, $response);
    }

    public function screenshotsOverviewAction(Request $request, Response $response, $args)
    {
        $params = [
            'title' => 'Screenshots small',
            'page' => 'screenshots_small',
            'pairs' => [],
            'tags' => [],
            'pics' => []
        ];


        $params['list_flat'] = array_map(function ($item) {
            return ['instrument' => $item, 'period' => 60];
        }, array_flatten(settings('pairs_list')));

        $params['list'] = settings('pairs_list');

        $r = repo(Watchlist::class);
        $params['watchlistUp'] = $r->where('direction', 'up')->get();
        $params['watchlistDown'] = $r->where('direction', 'down')->get();

        return $this->appRender('app/screenshots_overview', $params, $request, $response);
    }

    public function timelineAction(Request $request, Response $response, $args)
    {
        $timelineId = $args['timeline'];
        $user = User::find(1);

        $timeline = repo(Timeline::class)->find($timelineId);
        $resPopuped = task(new get__popup__timeline__task,
            [
                $user,
                vo($timelineId, 'numeric')
            ]
        );

        $allChartGroupsIds = array_merge(
            $resPopuped->popuped->pluck('id')->toArray(),
            $resPopuped->waiting->pluck('id')->toArray(),
            $resPopuped->not_set->pluck('id')->toArray()
        );

        $chartGroups = [];
        $lastCandles = [];
        foreach ($allChartGroupsIds as $key => $chartGroupId)
        {
            $chartGroup = task(new load__chart__group__data__task,
                [
                    $user,
                    vo($chartGroupId, 'numeric'),
                    null
                ]
            );

            foreach ($chartGroup->charts as $chart)
            {
                $lastCandles[] = $chart['resChartTimeseries']->min_last_candles_timestamp;
            }

            $chartGroups[] = $chartGroup;

//            break;
//            if ($key >= 0) {
//                break;
//            }
        }

        $minLatestLoadedTimeseriaDate = Carbon::createFromTimestamp(min($lastCandles));
//        dump($minLatestLoadedTimeseriaDate);echo 'AppController.php:112'; exit;
        $params = [
            'title' => 'Timeline',
            'page' => 'timeline',
            'timeline' => $timeline,
            'chartGroups' => $chartGroups,
            'minLatestLoadedTimeseriaDate' => $minLatestLoadedTimeseriaDate
        ];

        return $this->appRender('app/timeline', $params, $request, $response);
    }

    public function wholeUiKitAction(Request $request, Response $response, $args)
    {
        $params = [
            'title' => 'Templates',
            'page' => 'templates'
        ];

        return $this->appRender('app/ui-templates/whole-ui-kit', $params, $request, $response);
    }
}
