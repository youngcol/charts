<?php

namespace App\Controllers\App;

use App\Controllers\Controller;
use App\Facades\CU;
use App\Models\SocialEngine\UserSocialAccount;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class ReportsController extends Controller
{
    public function indexAction(Request $request, Response $response, $args)
    {
        $params = [
            'title' => 'Publish',
            'page' => 'posts'
        ];

        return $this->appRender('app/index', $params, $request, $response);
    }
}
