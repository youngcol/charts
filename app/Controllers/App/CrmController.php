<?php

namespace App\Controllers\App;

use App\Controllers\Controller;
use App\Facades\CU;
use App\Models\SocialEngine\UserSocialAccount;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class CrmController extends Controller
{

    public function contactsAction(Request $request, Response $response, $args)
    {
        $params = [
            'page' => 'crm',
            'submenu' => 'contacts',
            'title' => 'Contacts',
        ];

        return $this->appRender('app/crm/contacts', $params, $request, $response);
    }

    public function pitchesAction(Request $request, Response $response, $args)
    {
        $params = [
            'page' => 'crm',
            'submenu' => 'pitches',
            'title' => 'Pitches',
        ];

        return $this->appRender('app/crm/pitches', $params, $request, $response);
    }

    public function promotionsAction(Request $request, Response $response, $args)
    {
        $params = [
            'page' => 'crm',
            'submenu' => 'promotions',
            'title' => 'Promotions',
        ];

        return $this->appRender('app/crm/promotions', $params, $request, $response);
    }

    public function contactsListsAction(Request $request, Response $response, $args)
    {
        $params = [
            'title' => 'Contacts lists',
            'page' => 'crm',
            'submenu' => 'contacts.lists',
        ];

        return $this->appRender('app/crm/contacts_lists', $params, $request, $response);
    }

}
