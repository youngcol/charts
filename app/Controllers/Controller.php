<?php

namespace App\Controllers;

use App\Facades\CU;
use App\Models\User;
use App\Tasks\MediaFile\UploadMediaToLocalFolderTask;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class Controller
{
    private $container;
    private $view2;

	public function __construct()
	{
        global $app;
		$this->container = $app->getContainer();
        $this->view2 = $this->container->get('view2');

		$this->requests_queue_service();
	}

    protected function appVueData($data)
    {
        $user = User::find(1);

        $data['current_user'] = $user->toArray();
        $components_html = [];

        array_create_key($data, 'global_js_models');

        global $app;
        $data['global_js_models']['routes'] = [];
        $routes = $app->getRouteCollector()->getRoutes();

        foreach ($routes as $route)
        {
            $data['global_js_models']['routes'][$route->getName()] = $route->getPattern();
        }


        $data['global_js_models']['i18n'] = unserialize(file_get_contents(PATH_ROOT . 'i18n.php'));

        $data['global_js_models']['oanda'] = settings('oanda');

        // user social accounts
        $data['components_html'] = $components_html;

        $data['settings'] = settings(null);


        //-=-=-=-=-=-=-=-=-=-=-=-=
//        $boardsInfo = CU::user()->getInfo('pinterest.accounts.boards');
//        $data['global_js_models']['user_pinterest_boards'] = $boardsInfo->get();


        return $data;
    }

    private function requests_queue_service()
    {
        $_SESSION['last_requests'][] = $_SERVER['REQUEST_URI'];
        $c = count($_SESSION['last_requests']);

        if ($c > 2)
        {
            $_SESSION['last_requests'] = [
                $_SESSION['last_requests'][$c-2],
                $_SESSION['last_requests'][$c-1]
            ];
        }
	}

    public function fetch($tmpl, $data, Request $request)
    {
        $this->templateData($request, $data);
        return $this->view2->fetch($tmpl, $data);
    }

    public function render($tmpl, $data, Request $request, Response $response)
    {
        $this->templateData($request, $data);

        return $this->view2->render($response, $tmpl, $data);
    }

    private function templateData($request, &$data)
    {
        $data['csrf']['nameKey'] 	= "csrf_key";//$this->container->csrf->getTokenNameKey();
        $data['csrf']['valueKey'] 	= "csrf_value";//$this->container->csrf->getTokenValueKey();

        $data['csrf']['name'] 		= $request->getAttribute($data['csrf']['nameKey']);
        $data['csrf']['value'] 		= $request->getAttribute($data['csrf']['valueKey']);

        $data['name'] = settings('name');
        $data['host'] = settings('host');
        $data['env'] = settings('env');
        $data['seo'] = settings('seo');

        if (isset($_SESSION['last_bad_request_errors']))
        {
            $data['errors'] = $_SESSION['last_bad_request_errors'];
        }
    }


    protected function appRender(string $tmpl, array $data, Request $request, Response $response)
    {
        $this->templateData($request, $data);
        $data = $this->appVueData($data);

        return $this->render($tmpl, $data, $request, $response);
    }

    /**
     * @param Request $request
     * @param $mediaFiles
     * @return array
     */
    protected function uploadMediaIntoTempFolder(Request $request, $mediaFiles)
    {
        $uploadedFiles = $request->getUploadedFiles();
        $ret = [];

        foreach ($mediaFiles as $mediaFile)
        {
            $uploadedFile = sp($uploadedFiles, $mediaFile);
            if ($uploadedFile)
            {
                $res = task(new UploadMediaToLocalFolderTask(), [CU::user(), $uploadedFile]);

                if ($res->ok())
                {
                    $ret[] = $res->mediaFile;
                }
            }
        }

        return $ret;
    }
}
