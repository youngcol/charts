<?php


namespace App\Controllers\Admin;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use App\Controllers\Controller;
use Symfony\Component\Finder\Finder;

class WorkspaceController extends Controller
{
    public function __construct()
    {
        global $container;
        parent::__construct($container);

        set_time_limit(0);
    }

    public function leftColumn(Request $request, Response $response, $args)
    {
        $finder = new Finder();
        $finder->files()->name('*.php')->in(PATH_ROOT . 'app_workspace');
        $leftColumn = $this->renderLeftColumn($request, $finder);

        return json_200(['column' => $leftColumn]);
    }

    public function index(Request $request, Response $response, $args)
    {
        $finder = new Finder();
        $finder->files()->name('*.php')->in(PATH_ROOT . 'app_workspace');
        $leftColumn = $this->renderLeftColumn($request, $finder);


//        dump();echo "WorkspaceController.php:37"; exit;
        $file = sp($request->getQueryParams(), 'file');
//        dump($file);echo "WorkspaceController.php:38"; exit;

        $openedFile = null;
//dump($file);echo "WorkspaceController.php:42"; exit;
        if ($file)
        {
            include(PATH_ROOT . $file);
        }

        return $this->render('admin/workspace_layout', [
            'leftColumn' => $leftColumn,
            'openedFile' => $openedFile,
        ], $request, $response);
    }

    public function search(Request $request, Response $response, $args)
    {
        $needle = sp($request->getQueryParams(), 'needle');

        $finder = new Finder();
        $finder->in(PATH_ROOT . 'app_workspace');
        $finder->files()->contains($needle);
        $leftColumn = $this->renderLeftColumn($request, $finder);

        return json_200(['column' => $leftColumn]);
    }

    public function renderLeftColumn($request, $finder)
    {
        $grouped = [];
        $folders = [];
        foreach ($finder as $file) {
            /* @var \Symfony\Component\Finder\SplFileInfo $file */
            $relativePath = $file->getRelativePath();

            if (sp($grouped, $relativePath) === null ) {
                $grouped[$relativePath] = [];
                $folders[] = $relativePath;
            }

            $grouped[$relativePath][] = [
                'path' => $file->getPathname(),
                'name' => $file->getBasename()
            ];
        }

        sort($folders);

        foreach ($grouped as $folder => $files)
        {
            usort($files, function ($f1, $f2) {
                return $f1['name'] > $f2['name'] ? 1 : 0;
            });

            $grouped[$folder] = $files;
        }

        return $this->fetch('admin/workspace_files', [
            'grouped' => $grouped,
            'folders' => $folders,
        ], $request);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed
     */
    public function fileCreate(Request $request, Response $response, $args)
    {
        $folder = vo(sp($request->getQueryParams(), 'folder'), 'filled.string');
        $filename = vo(sp($request->getQueryParams(), 'file'), 'filled.string');

        $newPathName = PATH_ROOT . 'app_workspace/' . $folder . '/' . $filename.'__WORKSHOP';
        $newPathName = str_replace('.php', '', $newPathName);
        $newPathName .= '.php';

        $fileContent = file_get_contents(CODE_TEMPLATE_PATH . '/WorkspaceFile.tpl');
        file_put_contents($newPathName, $fileContent);

        $finder = new Finder();
        $finder->files()->name('*.php')->in(PATH_ROOT . 'app_workspace/' . $folder);
        $leftColumn = $this->renderLeftColumn($request, $finder);

        return json_200(['column' => $leftColumn]);
    }

    public function folderCreate(Request $request, Response $response, $args)
    {

        $folder = sp($request->getQueryParams(), 'folder');
        $newPathName = PATH_ROOT . 'app_workspace/' . $folder;
        $newFileName = $newPathName . '/aaa_main.php';

        if (!file_exists($newPathName))
        {
            mkdir($newPathName);
        }

        if (!file_exists($newFileName))
        {
            file_put_contents($newFileName, "<?php\n\nglobal \$container;\n\n");
        }

        $finder = new Finder();
        $finder->files()->name('*.php')->in($newPathName);
        $leftColumn = $this->renderLeftColumn($request, $finder);

        return json_200(['column' => $leftColumn]);
    }

    public function fileContent(Request $request, Response $response, $args)
    {
        $filePath = sp($request->getQueryParams(), 'file');
        return json_200(['content' => file_get_contents($filePath)]);
    }

    public function fileOutput(Request $request, Response $response, $args)
    {
        $filePath = sp($request->getQueryParams(), 'file');
        $ret = include($filePath);

        dump($ret);echo 'WorkspaceController.php:156'; exit;
    }
}
