<?php


namespace App\Controllers\Admin;

use App\Controllers\Controller;
use App\Facades\db;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use App\Models\User;
use App\Models\Session;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Finder\Finder;
use Illuminate\Database\Capsule\Manager as Capsule;
use Underscore\Types\Arrays;


class MysqlViewerController extends Controller
{

    public function indexAction(Request $request, Response $response, $args)
    {
        $tables = $this->buildTablesMap();
//        dump($tables);echo 'MysqlViewerController.php:24'; exit;
        $params = [
            'tables' => $tables
        ];

        return $this->render('admin/mysql-viewer/index', $params, $request, $response);
    }

    public function showTableAction(Request $request, Response $response, $args)
    {
        $table = $args['table'];
        $tables = $this->buildTablesMap();

        $tableTiers = $this->buildTableTiers($table, $tables);

        $params = [
            'tableTiers' => $tableTiers
        ];

        return $this->render('admin/mysql-viewer/table', $params, $request, $response);
    }

    protected function buildTableTiers($needle, $tables)
    {
        $ret = [
            'table'         => $tables[$needle],
            'down'          => [],
            'up'            => [],
            'bold_fields'   => []
        ];


        // build up tier
        $needleTable = $tables[$needle];
        $addedTables = [];
        $currentTier = [];

        array_create_key($ret['bold_fields'], $needleTable['name']);


        foreach ($needleTable['foreign_tables'] as $tableName)
        {
            $curTable = $tables[$tableName];
            $currentTier[] = $curTable;
            $addedTables[] = $tableName;
        }

        foreach ($needleTable['foreign'] as $item) {

            array_create_key($ret['bold_fields'], $item->foreign_table);
            $ret['bold_fields'][$item->foreign_table][] = $item->foreign_column;
            $ret['bold_fields'][$needleTable['name']][] = $item->column_name;
        }

        if (count($currentTier))
        {
            $ret['up'] = $currentTier;
        }

        // build down tiers
        $tablesList = map($tables, function ($item) {
            return $item['name'];
        });

        $currentTier = [];

        foreach ($tablesList as $tableName)
        {
            $currentTable = $tables[$tableName];


            if (in_array($needleTable['name'], $currentTable['foreign_tables']))
            {
                $currentTier[] = $currentTable;

                foreach ($currentTable['foreign'] as $item) {

                    array_create_key($ret['bold_fields'], $item->foreign_table);
                    $ret['bold_fields'][$item->foreign_table][] = $item->foreign_column;
                    $ret['bold_fields'][$currentTable['name']][] = $item->column_name;
                }
            }

        }

        if (count($currentTier))
        {
            $ret['down'] = $currentTier;
        }

        $ret['up'] = array_reverse($ret['up']);

        return $ret;
    }

    protected function getChildsForTable($needle, $tables, $exclude=[])
    {
        $ret = [];

        foreach ($tables as $table)
        {
            if (in_array($needle, $table['foreign_tables']))
            {
                if (in_array($table['name'], $exclude))
                {
                    continue;
                }

                $ret[] = $table['name'];
            }
        }


        return $ret;
    }

    protected function buildTablesMap()
    {
        $tables = map(db::select('show tables'), function ($item) {
            $vars = get_object_vars($item);
            return array_pop($vars);
        });

        $ret = [];

        foreach ($tables as $table) {

            $tableFields = db::select('DESCRIBE '. $table);

            $tableForeignKeys = db::select("SELECT
                    `column_name`, 
                    `referenced_table_schema` AS foreign_db, 
                    `referenced_table_name` AS foreign_table, 
                    `referenced_column_name`  AS foreign_column 
                FROM
                    `information_schema`.`KEY_COLUMN_USAGE`
                WHERE
                    `constraint_schema` = SCHEMA()
                AND
                    `table_name` = '$table'
                AND
                    `referenced_column_name` IS NOT NULL
                ORDER BY
                    `column_name`;");


            $foreignTables = map($tableForeignKeys, function ($item) {
                return $item->foreign_table;
            });

            $ret[$table] = [
                'name' => $table,
                'fields' =>  $tableFields,
                'foreign' => $tableForeignKeys,
                'foreign_tables' => $foreignTables
            ];
        }

        return $ret;
    }
}
