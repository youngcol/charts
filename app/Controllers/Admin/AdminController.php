<?php

namespace App\Controllers\Admin;

use App\Controllers\Controller;
use App\Facades\db;
use App\Models\TaskLog;
use App\Services\Language;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use App\Models\User;
use App\Models\Session;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Finder\Finder;
use Illuminate\Database\Capsule\Manager as Capsule;
use Underscore\Types\Arrays;

class AdminController extends Controller
{

	public function dashboard (Request $request, Response $response, $args)
	{
	    $params['users_count'] = 6;
        return $this->render('admin/dashboard', $params, $request, $response);
	}

	public function developmentFormAction(Request $request, Response $response, $args)
    {
        $command = $request->getParam('command');
        $param1 = $request->getParam('param1');

        switch($command)
        {
            case "drop:whole:database":
            {
                $ret = run_partisan('database:drop');

                return json_200(
                    [
//                        'exec' => implode("<br/>", $out1)
//                            . "<br>". implode("<br/>", $out2)
//                            . "<br> Whole database dropped!"

                        'exec' => "Whole database dropped!"
                    ]
                );
                break;
            }
            case "run:seed":
            {
//                db::table('seeds')->where('version', $param1)->delete();
                $param1 = '';
                break;
            }

            case "migrate:up":
            {
                $param1 = '';
                break;
            }
        }


        $cmd = implode(';', [
            'cd '. PATH_ROOT,
            "php partisan $command $param1"
        ]);

        $output = [];

        $execResult = exec($cmd, $output);

        return json_200(['exec' => $execResult]);
    }

	public function routes(Request $request, Response $response, $args)
    {
        $routes = $this->container->router->getRoutes();
        $params = [
            'routes' => $routes,
        ];

        return $this->render('admin/routes', $params, $request, $response);

    }

	public function development(Request $request, Response $response, $args)
    {

        $params = [
            'seeds' => files(SEEDS_PATH),
            'migrations' => db::table('migrations')->get()
        ];

        return $this->render('admin/development', $params, $request, $response);

    }

    public function phpunitViewerAction(Request $request, Response $response, $args)
    {
        $params = [
//            'seeds' => files(SEEDS_PATH),
//            'migrations' => db::table('migrations')->get()
        ];

        $cmd = "";
        if ($request->getParam('filter_file'))
        {
            if ($request->getParam('filter_method')) {
                $filter_file = $request->getParam('filter_file');
                $filter_method = $request->getParam('filter_method');

                $filter_path = "";
                $finder = new Finder();
                $finder->files()->name($filter_file . '.php')->in(TESTS_PATH);

                foreach ($finder as $file) {

                    $temp = explode("/../", $file->getPathname());
                    $filter_path=$temp[1];

//                    dump([
//                        $file->getPathname(),
//                        $relativePath
//
//                    ]);echo "AdminController.php:120";exit;
                    break;
                }

                $cmd .= "--filter $filter_method $filter_file $filter_path";
            } else {
                $cmd .= "--filter " . $request->getParam('filter_file');
            }
        }


        // phpunit --filter test_breakout_big_periods ChartTrendlineTest tests/models/ChartTrendlineTest.php
        //phpunit --filter methodName ClassName path/to/file.php

        $phpunit_exec = "phpunit $cmd";

        $cmd = implode(';', [
            'cd '. PATH_ROOT,
            $phpunit_exec
        ]);

        $output = [];
        $execResult = exec($cmd, $output);
        $params['output'] = implode("\r\n", $output);
        $params['exec_result'] = $execResult;
        $params['phpunit_exec'] = $phpunit_exec;

        return $this->render('admin/phpunit', $params, $request, $response);

    }

	public function logs(Request $request, Response $response, $args)
	{
        $logFile = $request->getParam('log');
        $fileContent = null;
        $parsedContent = null;
        $isTag = (strpos($logFile, 'tag__') === 0);
        $isExceptions = $logFile === 'exceptions.log';

        if ($logFile) {
            $logPathName = LOGS_PATH . '/'. $logFile;
            $filesize = filesize($logPathName);

            $blockLength = $filesize > 10000 ? 10000 : $filesize;
            $fh = fopen($logPathName, 'r');

            // go to the end of the file
            fseek($fh, 0, SEEK_END);
            fseek($fh, -$blockLength, SEEK_CUR);
            $fileContent = fread($fh, $blockLength);
        }

        if ($isTag)
        {
            $separated = explode("\n\n", $fileContent);

            foreach ($separated as $block)
            {
                if (strlen(trim($block)) == 0) continue;
                //-=-=-=-=-=-=-=-=-=-=-=-=

                $item = explode("\n", $block);

                if (!isset($item[1]))
                {
                    dump($item);echo 'AdminController.php:187'; exit;
                }

                $push = [
                    'time' => $item[0],
                    'title' => $item[1]
                ];

                if (count($item) == 3)
                {
                    $push['data'] = unserialize($item[2]);
                }

                $parsedContent[] = $push;
            }
        }

        if ($isExceptions)
        {
            $blocks = explode("\n\n=======================================\n\n", $fileContent);

            $parsedContent = [];

            foreach ($blocks as $block)
            {
                $lines = explode("\n", $block);

                $header = [];
                $isHeaderOk = false;

                foreach ($lines as $k => $line) {
                    $header[] = $line;

                    if ($line === '')
                    {
                        $isHeaderOk = true;
                        break;
                    }
                }

                if ($isHeaderOk)
                {
                    $parsedContent[] = [
                        'header' => $header,
                        'lines' => array_splice($lines, count($header))
                    ];
                }
                else
                {
                    $parsedContent[] = [
                        'header' => '',
                        'lines' => $lines
                    ];
                }
            }
        }

	    $params = [
	        'title' => 'Logs',
	        'files' => scandir(LOGS_PATH),
            'fileContent' => $fileContent,
            'parsedContent' => $parsedContent,
            'isTag' => $isTag,
            'logFile' => $logFile,
            'isExceptions' => $isExceptions,
        ];

		return $this->render('admin/logs/index', $params, $request, $response);
	}

    public function mysqlBuilder(Request $request, Response $response, $args)
    {

        $sql = DB::table('users')->where('perss', '=', '555')->toSql();
//        dump($sql);echo 'AdminController.php:262'; exit;

        $params = [

        ];

        return $this->render('admin/mysql_builder', $params, $request, $response);
	}

    public function webhookAction(Request $request, Response $response, $args)
    {
        $params = $request->getParams();

        $data = [
            $args['hookName'],
            'getParams' => $params
        ];

        log_tag(
            'webhook__'. $args['hookName'],
            'Webhook call',
            $data
        );

        dump($data);echo 'AdminController.php:219'; exit;
	}

    public function databaseStateAction(Request $request, Response $response, $args)
    {

        $finder = new Finder();
        $finder->files()->name('*.php')->in(DB_PATH.'/states');

        $list = map($finder, function ($file) {
            return $file->getBasename('.php');
        });

        $params = [
            'list' => $list,
            'isRun' => false
        ];

        return $this->render('admin/database_state', $params, $request, $response);
	}


    public function runDatabaseStateAction(Request $request, Response $response, $args)
    {
        $file = $args['file'];

        require_once (DB_PATH.'/states/DbState.php');
        require_once (DB_PATH.'/states/'.$file.'.php');

        $report = [];

        $report['partisan'] = run_partisan('database:truncate');

        $objStateBuilder = new $file;
        $report['state_builder'] = $objStateBuilder->run();

        $params = [
            'list' => [],
            'file' => $file,
            'isRun' => true,
            'report' => $report
        ];

        return $this->render('admin/database_state', $params, $request, $response);
	}


    public function languagesAction(Request $request, Response $response, $args)
    {
        $diffs = validateLanguagesConsistence();

        $params = [
            'diffs' => $diffs,
        ];

        return $this->render('admin/languages', $params, $request, $response);
	}


    public function tasksLogsAction(Request $request, Response $response, $args)
    {

        $r = repo(TaskLog::class);
        $logs = $r->orderBy(['id' => 'desc'])->get();

        $params = [
            'logs' => $logs
        ];

        return $this->render('admin/tasks_logs', $params, $request, $response);
	}

    public function settingsAction(Request $request, Response $response, $args)
    {
        global $settings;

        $params = [
            'settings' => $settings
        ];

        return $this->render('admin/settings', $params, $request, $response);
	}

    public function tasksViewerAction(Request $request, Response $response, $args)
    {

        $tasks = [
            'Global' => [],

        ];

        $finder = new Finder();
        $finder->in(TASKS_PATH);

        foreach ($finder as $file) {
            /* @var \Symfony\Component\Finder\SplFileInfo $file */
            $relativePath = $file->getRelativePath();

            if ($file->isFile())
            {
                if ($relativePath)
                {
                    array_create_key($tasks, $relativePath);
                    $tasks[$relativePath][] = $file->getBasename('.php');

                }
                else
                {
                    $tasks['Global'][] = $file->getBasename('.php');
                }
            }

        }

        $params = [
            'tasks' => $tasks
        ];

        return $this->render('admin/tasks_viewer', $params, $request, $response);
	}

    public function variablesViewerAction(Request $request, Response $response)
    {
        $finder = new Finder();
        $finder->files()->in(APP_WORKSPACE . '/data_examples');

        $dumps = [];

        foreach ($finder as $file)
        {
            $dumps[] = [
                'var' => unserialize(file_get_contents($file->getPathname())),
                'name' => $file->getBasename('.php')
            ];
        }

        $params = [
            'dumps' => $dumps
        ];

        return $this->render('admin/variables_viewer', $params, $request, $response);
	}


    public function phpStoriesViewerAction(Request $request, Response $response)
    {
        $stories = DB::table('stories')
            ->where('status', '<>', 'finished')
            ->orderBy('status')
            ->get();


        $params = [
            'stories' => $stories
        ];

        return $this->render('admin/php_stories_viewer', $params, $request, $response);
    }
}
