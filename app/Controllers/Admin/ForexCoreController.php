<?php


namespace App\Controllers\Admin;

use App\Controllers\Controller;
use App\Facades\db;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use App\Models\User;
use App\Models\Session;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Finder\Finder;
use Illuminate\Database\Capsule\Manager as Capsule;
use Underscore\Types\Arrays;


class ForexCoreController extends Controller
{

    public function indexAction(Request $request, Response $response, $args)
    {
//        $tables = $this->buildTablesMap();
//        dump($tables);echo 'MysqlViewerController.php:24'; exit;
        $forex = settings('forex');


        $params = [
            'forex' => $forex
        ];

        return $this->render('admin/forex-core/index', $params, $request, $response);
    }
}
