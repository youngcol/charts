<?php

namespace App\Controllers;

use App\Facades\Redis2;
use App\Facades\Screener;
use App\Facades\Timer;
use App\Models\UserInfo;
use App\Tasks\calc__price__movements__task;
use App\Tasks\calc__tline__pivot__point__task;
use App\Tasks\create__adx__signal__task;
use App\Tasks\create__event__task;
use App\Tasks\find__true__swings__task;
use App\Tasks\ForexCore\generate__watchlist__item__from__signal__task;
use App\Tasks\generate__chart__hlines__breakouts__task;
use App\Tasks\generate__chart__objects__breakouts__task;
use App\Tasks\generate__indicator__swings__task;
use App\Tasks\generate__signals__on__trendlines__breakouts__task;
use App\Tasks\MediaFile\UploadMediaToLocalFolderTask;
use App\Tasks\rsi__change__tracking__task;
use App\Tasks\rsi__tracking__task;
use App\Tasks\save__signal__task;
use App\Tasks\save__timeseria__task;
use App\Tasks\Screenshots\upload__screenshot__task;
use App\Tasks\ForexCore\put__command__to__the__sheet__task;
use App\Tasks\sign__up__user__task;
use App\Facades\CU;
use App\Tasks\update__renko__screener__state__task;
use App\Tasks\update__screener____ema__swings__task;
use App\Tasks\update__screener__ema__swings__task;
use App\Tasks\update__screener__state__task;
use App\Tasks\update__swings__state__task;
use App\Facades\session as sess;
use App\Models\Session;
use App\Models\User;
use App\Tasks\watch__chartgroups__on__olymp__timeline__task;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Redis;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Ramsey\Uuid\Uuid;
use Slim\Http\UploadedFile;
use App\Tasks\create__ripper__event__task;
use App\Tasks\build__ohlc__candles__pivots__task;

class HomeController extends Controller
{

    public function screenshotImageAction(Request $request, Response $response, $args)
    {
        $params = $request->getParams();

        $img = str_replace('data:image/png;base64,', '', $params['img']);
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);
        $dest = std_path([SCREENSHOTS_DRAWNINGS_PATH, $params['pair'], $params['period']]);
        create_folder_tree_by_pathname($dest);

        $drawnig_name = "/"."_drawning.png";

        if (file_exists($dest . $drawnig_name)) {
            $cl = file_put_contents($dest . "/drawning_temp.png", $data);
            merge_png($dest . $drawnig_name, $dest . "/drawning_temp.png", $dest . $drawnig_name);
            unlink($dest . "/drawning_temp.png");
        } else {
            $cl = file_put_contents($dest . $drawnig_name, $data);
        }

        $draws = [];
        build_drawning_pics_feed($params['pair'], (int) $params['period'], (int)settings('screenshots.crop_percent.'.$params['period']), $draws);

        return json_200([
            'filename' => $dest . $drawnig_name,
            'period' => $params['period'],
            'pair' => $params['pair'],
            'drawnings' => $draws,
        ]);
    }

    public function swingsStateAction(Request $request, Response $response, $args)
    {
        $params = $request->getParams();


        store_dump_var('swingsStateAction', [
            'time' => date('r'),
            'swings34' => json_decode($params['swings34'], 1),
            'ema_swings' => json_decode($params['ema_swings'], 1)
            ]);

        task(new update__swings__state__task, [
            json_decode($params['swings34'], 1),
            json_decode($params['swings144'], 1),
            json_decode($params['ema_swings'], 1)
        ]);
    }

    public function ripperEventAction(Request $request, Response $response, $args)
    {
        $res = task(new create__ripper__event__task,
            [
                $request->getParam('event'),
                json_decode($request->getParam('params'), 1)
            ]);

        return json_200($res->toArray());
    }

    public function ripperSignalAction(Request $request, Response $response, $args)
    {
        $res = task(new create__event__task,
            [
                substr($request->getParam('symbol'), 0, 6),
                (int)$request->getParam('period'),
                (int)$request->getParam('direction'),
                $request->getParam('tag'),
                'raw'
            ]);

        task(new update__screener__state__task, [
            $request->getParam('symbol'),
            (int)$request->getParam('period'),
            $request->getParam('tag'),
            (int)$request->getParam('direction'),
            'init'
        ]);

        return json_200($res->toArray());
    }


    public function orderClosedAction(Request $request, Response $response, $args)
    {
        $res = task(new create__order__closed__item__task,
            [

            ]);

        return json_200($res->toArray());
    }

    public function update_rsi_swing_screener_state(array $rsi_slow_values, array $rsi_values)
    {

        store_dump_var('update_rsi_swing_screener_state', [date('r'), $rsi_slow_values, $rsi_values]);

        foreach(settings('screener_periods') as $period)
        {
            foreach($rsi_values as $pair => $periodData)
            {
                $rsi_val = $periodData[$period]['val'];
                $rsi_slow = $rsi_slow_values[$pair][$period]['val'];

                $direction = $rsi_val > $rsi_slow ? 1 : 0;

                $res = task(new update__screener__state__task,
                [
                    $pair,
                    $period,
                    "rsi_swing",
                    $direction,
                    'init'
                ]);

                // if($res->is_changed)
                // {
                //     $res = task(new create__event__task,
                //     [
                //         $pair,
                //         $period,
                //         $direction,
                //         "rsi_swing",
                //         'rsi_heiken'
                //     ]);
                // }
            }
        }


    }

    public function rsi_heiken_update_screener_state_from_last_values($last_values)
    {
        //store_dump_var("update_screener_state_from_last_values", [$tag, $last_values]);
        $levels = [-40,-20,0,20,40];
        $tags_levels = ["_40","_20",0,20,40];

        foreach($last_values as $pair => $periodData)
        {
            foreach(settings('screener_periods') as $period)
            {
                $datum = $periodData[$period];

                foreach($levels as $index => $level)
                {
                    $direction = (float)$datum['val'] < $level ? 0 : 1;

                    $res = task(new update__screener__state__task,
                    [
                        $pair,
                        $period,
                        "rsi_heiken_" . $tags_levels[$index],
                        $direction,
                        'init'
                    ]);

                    // if($res->is_changed)
                    // {
                    //     $res = task(new create__event__task,
                    //     [
                    //         $pair,
                    //         $period,
                    //         $direction,
                    //         "rsi_heiken_" . $level,
                    //         'rsi_heiken'
                    //     ]);
                    // }
                }
            }
        }
    }

    public function timeseriaGroupedAction(Request $request, Response $response, $args) {
        $params = $request->getParams();

        //Timer::start("last-timeseria-{$params['tag']}");
        $tags = explode(",", $params['tags']);

        Redis2::set("timeseries_updated_time", now_local());
        Redis2::set("open_orders", json_decode($params['open_orders'], 1));

//
//        $adx = json_decode($params['adx'], 1);
//        store_dump_var('adx_signals', [date('r'), $adx]);

        $last_values = [];

        store_dump_var('timeseriaGroupedAction_raw__'.$params['tags'], [date('r'), $params]);
        foreach($tags as $tag)
        {
            $timeseria = json_decode($params[$tag], 1);
            store_dump_var('timeseriaGroupedAction_'.$tag, [date('r'), $tag, $timeseria]);
            $res = task(new save__timeseria__task, [$timeseria, $tag]);
            $last_values[$tag] = $res->last_values;

            Redis2::set(timeseria_last_values_key($tag), $res->last_values);
            $res_generate = task(new generate__chart__objects__breakouts__task,
                [
                    $tag,
                    $res->last_values
                ]
            );

            // place orders on breakouts
            foreach($res_generate->breakout_tlines as $tline)
            {
                if ($tag == settings('indicator.tag'))
                {
                    $res = task(new calc__tline__pivot__point__task,
                        [
                            $tline
                        ]
                    );
                }

                if($tline['is_trading'])
                {
                    $res = task(new put__command__to__the__sheet__task,
                        [
                            vo("OPEN_MARKET_ORDER", "forex.commands"),
                            $tline['params']
                        ]
                    );
                }

//                task(new put__command__to__the__sheet__task,
//                    [
//                        vo("OPEN_SYMBOL", "forex.commands"),
//                        [
//                            'symbol' => $tline['pair'],
//                        ]
//                    ]
//                );

                $text = symbol_localization($tline['pair']);

                if ($tline['period'] == 5) {
                    $text .= ' бинарный ';
                }
                // voice notify
                if($tline['is_trading'])
                {
                    $text .= ' открыт ' . ($tline['trading_type'] == 'buy' ? ' бай ' : ' селл ');
                }
                else
                {
                    if ($tline['is_zone'])
                    {
                        $text .= ($tline['trading_type'] == 'buy' ? ' нью хай ' : ' нью лоу ');
                    }
                    else
                    {
                        $text .= ' пробой ' . ($tline['trading_type'] == 'buy' ? 'бай' : 'селл');
                    }
                }

                push_to_voice_queue($text);
            }

            if ($tag == 'ohlc')
            {
                $res_hlines_breakouts = task(new generate__chart__hlines__breakouts__task,
                    [
                        $res->last_values
                    ]
                );

                foreach ($res_hlines_breakouts->hlines as $hline)
                {
                    if ($hline['is_trading']) {
                        $res = task(new put__command__to__the__sheet__task,
                            [
                                vo("OPEN_MARKET_ORDER", "forex.commands"),
                                $hline['params']
                            ]
                        );

                        $text = symbol_localization($hline['pair']) . ' ' . period_localization((int)$hline['period']);
                        $text .= ' открыт ' . ($hline['trading_type'] == 'buy' ? ' бай ' : ' селл ');
                        push_to_voice_queue($text);
                    }
                }

//                task(new calc__price__movements__task,
//                    [
//                        $res->last_values
//                    ]
//                );
            }
        }

//        task(new rsi__change__tracking__task, [
//            $last_values[settings('indicator.tag')]
//        ]);


//        task(new update__swings__state__task, [
//            json_decode($params['ema_swing'], 1)
//        ]);

//        store_dump_var('update__screener__ema__swings__task', [date('r'), $last_values]);

//        task(new generate__indicator__swings__task, [
//            $last_values[settings('indicator.tag')],
//            $last_values[settings('indicator.tag_slow')],
//
//        ]);

        task(new rsi__tracking__task, [
            $last_values[settings('indicator.tag')]
        ]);

//



//         Redis2::set("rsi_last_values", $last_values['rsi']);

        // ema swings
//        task(new update__screener__ema__swings__task, [
//            $last_values['tsi'],
//            $last_values['tsi_slow'],
//            $last_values['ohlc'],
//            $last_values['ohlc_ema'],
//            true
//        ]);
//
//        task(new find__true__swings__task, [
//
//        ]);

        return json_200([
            'tags' => $tags
        ]);
    }

    public function timeseriaAction(Request $request, Response $response, $args) {
        $params = $request->getParams();

        store_dump_var('timeseriaAction_RAW', [
            $params,
            date('r'),
        ]);

        //Timer::start("last-timeseria-{$params['tag']}");
        $timeseries = json_decode($params['timeseries'], 1);

        store_dump_var('timeseriaAction', [
            $params['tag'],
            $timeseries,
            date('r'),
        ]);

        $res = task(new save__timeseria__task,
            [
                $timeseries,
                $params['tag']
            ]
        );

        store_dump_var($params['tag'] . "_timeseries__last_values", $res->last_values);
        Redis2::set($params['tag'] . "_timeseries__last_values", $res->last_values);

        return json_200([

        ]);
    }

    public function testAction(Request $request, Response $response, $args) {

        $body = $request->getBody();
        file_put_contents(UPLOADS_PATH."/".time()."__test_file", var_export($request->getParams(), 1));

        return json_200([true]);
    }

    public function newLayoutAction (Request $request, Response $response, $args)
    {
        $params = [
            'page' => 'home',
        ];

//        return $this->render('public2/login', $params, $request, $response);
        return $this->render('public2/index', $params, $request, $response);
    }

	public function index (Request $request, Response $response, $args)
	{
        //        $data['messages'] = $this->container->flash->getMessages();

        $params = [
            'page' => 'home',
        ];

        // rator | 𝗧𝗛𝗘 𝗕𝗘𝗦𝗧 𝗢𝗡𝗟𝗜𝗡𝗘 𝗖𝗦𝗦 𝗚𝗘𝗡𝗘𝗥𝗔𝗧𝗢𝗥</title>

        return $this->render('public/index', $params, $request, $response);
	}


	public function signinAction(Request $request, Response $response, $args)
    {
        if (CU::isAuthorized())
        {
            return redirect('app.index');
        }

        $params = [

        ];

        return $this->render('public/login', $params, $request, $response);
    }

    public function signupStore(Request $request, Response $response, $args)
    {
        [
                $err,
                $username,
                $email,
                $password
            ] = params(
                $request,
                'user.signup',
                [
                    'username' => 'user.username',
                    'email' => 'user.email',
                    'password' => 'user.password'
                ]
            );

        $Res = task(new sign__up__user__task, [$username, $email, $password]);

        if ($Res->ok())
        {
            return redirect('app.index');
        }
        else
        {
            return json_400($Res->toArray());
        }
    }

    public function signup(Request $request, Response $response, $args)
    {
        $params = [];

        $this->render('public/signup', $params, $request, $response);
    }


    public function features(Request $request, Response $response, $args)
    {
        $params = [];

        $this->render('public/features', $params, $request, $response);
    }

    public function prices(Request $request, Response $response, $args)
    {
        $params = [];

        $this->render('public2/pricing', $params, $request, $response);
    }

    public function termsAction(Request $request, Response $response, $args)
    {
        $params = [];

        $this->render('public/terms', $params, $request, $response);
    }

    public function privacyPolicyAction(Request $request, Response $response, $args)
    {
        $params = [];

        $this->render('public/privacy_policy', $params, $request, $response);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed
     * @throws \Exception
     */
    public function passwordForgot(Request $request, Response $response, $args)
    {
        $isPost = $request->isPost();
        $isSentEmail = false;


        if ($isPost)
        {
            $email = $request->getParam('email');
            $user = User::where('email', $email)->first();

            if ($user)
            {
                $isSentEmail = true;
                $time = substr(time(), -3 ,3);
                $user->password_reset_code = uuid1FromId(rand(1,9999) . $time);
                $user->save();

                // send email
//                $this->container->mailer->
            }
            else
            {

            }

        }

        $params = [
            'isSentEmail' => $isSentEmail,
            'isPost' => $isPost,
        ];

        return $this->render('public/default/password_forgot', $params, $request, $response);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed
     */
    public function passwordReset(Request $request, Response $response, $args)
    {
        $code = sp($args, 'code');
        $isVerified = false;

        if ($code)
        {
            $user = User::where('password_reset_code', $code)->first();

            if ($user)
            {
                $isVerified = true;
            }
        }

        $params =
            [
                'isVerified' => $isVerified,
                'code'  => $code
            ];

        return $this->render('public/default/password_reset', $params, $request, $response);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed
     */
    public function passwordChange(Request $request, Response $response, $args)
    {
        [
                $err,
                $pass1,
                $pass2,
                $code
            ] = params
            (
                $request,
                'user.password.change',
                [
                    'pass1' => 'user.password',
                    'pass2' => 'user.password',
                    'code' => 'filled.string',
                ]
            );

        if (!eN($err)) return $err;
        //-=-=-=-=-=-=-=-=-=-=-=-=

        $isVerified = false;

        $user = User::where('password_reset_code', $code)->first();

        if ($user)
        {
            $isVerified = true;
            $user->password_reset_code='';
            $user->setPassword($pass1->_());
            $user->save();
        }

        $params = [
            'isVerified' => $isVerified,
            'code' => $code
        ];

        return $this->render('public/default/password_change', $params, $request, $response);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed
     */
    public function emailVerify(Request $request, Response $response, $args)
    {
        $code = sp($args, 'code');
        $user = User::where('email_verify_code', $code)->first();

        $isVerified = false;

        if ($user)
        {
            $user->is_email_verified = 1;
            $user->save();
            $isVerified = true;
        }

        $params = [
            'isVerified' => $isVerified
        ];

        return $this->render('public/default/email_verified', $params, $request, $response);
    }


    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     * @throws \Exception
     */
    public function loginAction(Request $request, Response $response, $args)
    {
        // get the parameters
        $username = $request->getParam('username');
        $password = $request->getParam('password');
        $rememberMe = $request->getParam('remember_me');

        // search the user
        $user = User::where('email', '=', $username)
            ->where('active', 1)
            ->first();

        // if user not exist and password isn't correct redirect to index...
        if( !$user || !$user->checkPassword($password) ){
            $this->container->flash->addMessage('warning', 'Incorrect username or password');
            return redirect('signin');
        }

        $sessionId = Uuid::uuid1() . Uuid::uuid1();

        // set cookie
        $expire = time()+60*60*24*3; // 3 days
        if ($rememberMe == 'on')
        {
            $expire = time()+60*60*24*365*3; // 3 years
        }
        setcookie('_kitid', $sessionId, $expire, '/');

        // set session array
        sess::fill([
            'current_user' => $user->toArray(),
            'uniqid' => $sessionId
        ]);

        // set row into db
        $session = new Session;
        $session->user_id = $user->id;
        $session->uniqid = $sessionId;
        $session->save();

        // then redirect to admin dashboard
        return redirect('app.index');
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     */
    public function logoutAction(Request $request, Response $response, $args)
    {
        $session = $session = Session::where('uniqid', '=', $_SESSION['uniqid'])->first();

        $session->delete();
        unset($_SESSION['uniqid']);
        unset($_SESSION['current_user']);

        $this->container->flash->addMessage('success', 'You are logged out');
        return redirect('index');
    }

    public function uploadScreenshotAction(Request $request, Response $response, $args)
    {

        $uploadedFiles = $request->getUploadedFiles();
        $uploadedFile1 = $uploadedFiles['file'];

        $client_file_name = explode("__", $uploadedFile1->getClientFilename());
        $instrument = $client_file_name[0];
        $period = $client_file_name[1];

//        log_uploaded_picture_command(date('r') . '-' . $instrument . ' - '. $tag.' - '.$period, [
//                $tag, $period, $instrument, $uploadedFile1->getClientFilename()
//        ]);

        $res = task(new upload__screenshot__task(), [
            $uploadedFile1,
            vo($period, 'screenshots.period'),
            vo($instrument),
        ]);

        // create pic crop
//        $crop = cropScreenshot($instrument, $res->pic_name, (int)$period, (int)settings('screenshots.crop_percent.'.$period), 100);
//        removeOldScreenshots('screenshots_crops', $instrument, $res->pic_name, (int)$period, (int)settings('screenshots.crop_percent.'.$period));
//
//        // store timestamp in redis
//        $key = implode("-", ['pics-timestamps-all']);
//        $timestamps = Redis2::get($key, 1, []);
//        array_create_key($timestamps, $instrument);
//        $pic_key = implode("-", ['pic', $res->pic_name]);
//        $timestamps['last_timestamp'] = time();
//        $timestamps[$instrument] = time();
//        Redis2::set($key, $timestamps);



        return json_200([
            'res' => $res
        ]);
    }
}



/**
 * @api {get} /right Список прав
 * @apiName GetRights
 * @apiGroup Right
 * @apiDescription Метод для получения списка прав.
 * @apiPermission user
 *
 * @apiSuccessExample {json} Успешно (200)
 *     HTTP/1.1 200 OK
 *     {
 *       "data": [
 *         {
 *           "type": "right",
 *           "id": "1",
 *           "attributes": {
 *             "name": "manageUsers",
 *             "description": "Управление пользователями",
 *             "created_at": "2016-10-17T07:38:21+0000",
 *             "updated_at": "2016-10-17T07:38:21+0000",
 *             "created_by": 0,
 *             "updated_by": null
 *           }
 *         }
 *       ]
 *     }
 *
 * @apiUse StandardErrors
 * @apiUse UnauthorizedError
 * @apiUse NotFoundError
 */


/**
 * @api {post} /right Создание права
 * @apiName CreateRight
 * @apiGroup Right
 *
 * @apiDescription Метод для создания нового права.
 *
 * @apiPermission admin
 *
 * @apiParam {String} name Имя права (уникальный)
 * @apiParam {String} description Человекопонятное описание
 *
 * @apiParamExample {json} Пример запроса:
 *    {
 *      "data": {
 *        "attributes": {
 *          "name": "manageUsers",
 *          "description": "Управление пользователями"
 *        }
 *      }
 *    }
 *
 * @apiHeader {String} Authorization Bearer TOKEN
 *
 * @apiSuccessExample {json} Успешно (200)
 *     HTTP/1.1 200 OK
 *     {
 *       "data": {
 *         "type": "right",
 *         "id": "1",
 *         "attributes": {
 *           "name": "manageUsers",
 *           "description": "Управление пользователями",
 *           "created_at": "2016-10-17T07:38:21+0000",
 *           "updated_at": "2016-10-17T07:38:21+0000",
 *           "created_by": 1,
 *           "updated_by": null
 *         },
 *         "links": {
 *           "self": "http://bootstrapi.dev/api/right/1"
 *         }
 *       }
 *     }
 *
 * @apiUse StandardErrors
 * @apiUse UnauthorizedError
 */
