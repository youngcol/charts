<?php
/**
 * Срециальный контроллер для прототипирования пока нет UI
 */

namespace App\Controllers\Prototype;


use App\Controllers\Controller;
use App\Facades\CU;
use App\Models\SocialEngine\Post;
use App\Models\SocialEngine\UserSocialAccount;
use App\Tasks\MediaFile\UploadMediaToLocalFolderTask;
use App\Tasks\SocialEngine\CreatePostTask;
use App\Tasks\SocialEngine\Publish\PublishToSocialAccountsTask;
use App\VO\Post\PostInstantlyPublish;
use App\VO\Post\PublishParams;
use App\VO\User\MediaFiles;
use App\VO\User\SocialAccounts;
use Slim\Http\Request;
use Slim\Http\Response;

class PrototypeController extends Controller
{
    public function allModels()
    {

    }

    public function index (Request $request, Response $response, $args)
    {
        //        $data['messages'] = $this->container->flash->getMessages();


        $accounts = UserSocialAccount::where('user_id', CU::user()->id)->get();

        $params = [
            'accounts' => $accounts,
            'page' => 'home'
        ];

        return $this->render('prototype/index', $params, $request, $response);
    }

    public function postStoreTest(Request $request, Response $response)
    {
        list
            (
                $err,
                $text,
                $creationType,
                $scheduledTime,
                $socialAccounts
            ) = params(
            $request,
            'post.store',
            [
                'text' => 'filled.string',
                'creation_type' => 'post.publish_params.publish_type',
                'scheduled_time' => 'datetime',
                'social_accounts' => 'filled.string'
            ]
        );

        $publishParams = $request->getParam('publish_params');
        $publishParams = json_decode($publishParams, 1);
        if ($scheduledTime)
        {
            $publishParams['scheduled_time'] = $scheduledTime->_();
        }
        $publishParams = new PublishParams($creationType, $publishParams);
//        dump($publishParams);echo 'PrototypeController.php:66'; exit;

        $uploadedFiles = $request->getUploadedFiles();
        $mediaIds = [];

        if (isset($uploadedFiles['media1']))
        {
            $uploadedFile1 = $uploadedFiles['media1'];
            $Res = task(new UploadMediaToLocalFolderTask(), [CU::user(), $uploadedFile1]);

            if ($Res->ok())
            {
                $mediaIds[] = $Res->mediaFile->id;
            }
        }

        if (isset($uploadedFiles['media2']))
        {
            $uploadedFile2 = $uploadedFiles['media2'];
            $Res2 = task(new UploadMediaToLocalFolderTask(), [CU::user(), $uploadedFile2]);

            if ($Res2->ok())
            {
                $mediaIds[] = $Res2->mediaFile->id;
            }
        }

        $socialAccounts = new SocialAccounts(CU::user(), explode(',', $socialAccounts));

        $res = task(new CreatePostTask,
            [
                CU::user(),
                $text,
                new MediaFiles(CU::user(), $mediaIds),
                $socialAccounts,
                $publishParams
            ]
        );

        if ($creationType == 'instantly')
        {

            $res = task(new PublishToSocialAccountsTask(),
                [
                    new PostInstantlyPublish($res->post, $res->postPublishParams)
                ]
            );
        }

        return redirect('proto.posts');
    }

    public function posts(Request $request, Response $response, $args)
    {

        $params = [
            'posts' => Post::where('user_id', CU::user()->id)->with('mediaFiles')->get(),
            'page' => 'posts'
        ];

        return $this->render('prototype/posts', $params, $request, $response);

    }
}
