<?php

declare(strict_types=1);
namespace App\Models;

use App\Facades\Redis2;
use Carbon\Carbon;

/**
 * Class ChartTrendline
 *
 * @property integer $id
 * @property integer $chart_id
 * @property float $start_value
 * @property float $end_value
 * @property \Carbon\Carbon $start_date
 * @property \Carbon\Carbon $end_date
 * @property integer $is_fired
 *
 * @package App\Model
 */
final class ChartTrendline extends Model
{
    protected $table = 'chart_trendlines';
    public $timestamps = false;

    /**
    * Hidden from json resource
    * @var array
    */
    protected $hidden = [
    ];

    protected $appends = [
        'trading_type',
    ];

    protected $fillable = [
        'uniq_id',
        'start_value',
        'end_value',
        'start_date',
        'end_date',
        'is_fired',
        'is_trainer',
        'chart_tag',
        'period',
        'fired_at',
        'fired_value',
        'is_trading',
        'pair',
        'params',
        'mockup_tag',
        'calc',
        'tline_fired_value',
        'is_zone',
        'is_zone_buy',
    ];

    /* These fields can be updated by UpdateModelsCollectionTask */
    protected $api_fillable = [

    ];

    // integer, real, float, double, decimal:<digits>, string, boolean,  object, array, collection, date, datetime, timestamp
    protected $casts = [
        'start_date' => 'datetime',
        'end_date' => 'datetime',
        'start_value' => 'double',
        'end_value' => 'double',
        'period' => 'integer',
        'params' => 'array',
        'calc' => 'array',
        'mockup_tag' => 'array',
    ];

    /**
    * Model constructor.
    * @param array $attributes
    */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     *
     */
    protected static function boot()
    {
        parent::boot();

    }

    /**
     * @return float
     */
    public function getValueDelta(): float {
        return $this->end_value - $this->start_value;
    }

    /**
     * @return bool
     */
    public function isSupportLine()
    {

        if ($this->is_zone)
        {
            return $this->is_zone_buy ? false : true;
        } else {
            if(($this->end_value - $this->start_value) == 0) {
                return NULL;
            }

            return  $this->end_value > $this->start_value;
        }
    }

    /**
     * @return bool
     */
    public function isResistanceLine(): bool
    {
        return !$this->isSupportLine();
    }

    /**
     * @return int
     */
    public function diffInMinutes() {
        return $this->end_date->diffInMinutes($this->start_date);
    }

    public function getDigitsDivider()
    {
        $parts = explode(".", (string)$this->end_value);


        $len = strlen($parts[1])-1;
        switch($len)
        {
            case 2: $divider = 100; break;
            case 3: $divider = 1000;break;
            case 4: $divider = 10000;break;
            case 5: $divider = 10000;
            break;
        }


        $divider = str_contains(strtolower($this->pair), 'usd') ? 10000 : 100;
        return $divider;
    }

    public function getDeltaPoints()
    {
        $delta = abs($this->getValueDelta());
        return round($delta * $this->getDigitsDivider());
    }

    public function getValueByTime(Carbon $time): ?float
    {
        $norm_time = normalize_time($time, $this->period);
        return isset($this->calc[$norm_time]) ? (float)$this->calc[$norm_time] : NULL;
    }
    /**
     * @param float $val
     * @param Carbon $time
     * @return int
     */
    public function isBreakOut(float $val, Carbon $time): ?int
    {
        if ($this->is_zone)
        {
            if ($this->isSupportLine() && ($val < $this->start_value)) // sell
            {
                return 0;
            }
            else if(!$this->isSupportLine() && ($val > $this->start_value)) // buy
            {
                return 1;
            }
        } else {
            $norm_time = normalize_time($time, $this->period);
            //
            store_dump_var("isBreakOut", [
                'tline_id' => $this->id,
                'date' => date('r'),
                'norm_time' => $norm_time,
                'calc_value' => $this->calc[$norm_time],
                'period' => $this->period,
                'pair' => $this->pair,
                'val' => $val,

            ]);

            $tline_val = $this->getValueByTime($time);

            if($tline_val !== NULL)
            {
                if ($this->isSupportLine() && ($val < $tline_val))
                {
                    return 0;
                }
                else if(!$this->isSupportLine() && ($val > $tline_val))
                {
                    return 1;
                }
            }

        }

        return NULL;
    }

    public function getTradingTypeAttribute()
    {
        if($this->is_zone)
        {
            return $this->is_zone_buy ? 'buy' : 'sell';
        }
        else
        {
            return $this->isSupportLine() ? 'sell' : 'buy';
        }
    }

    static public function getCandelsTimeArray(string $start_date, string $end_date, int $period)
    {
        $ret = [];
        $time = Carbon::parse($start_date);
        do
        {
            $ret[] = $time->format('Y.m.d H:i:s');
            $time->addMinutes($period);
        }
        while($time->lte($end_date));

        return $ret;
    }

    public function setFired(float $value, Carbon $time)
    {
        $this->is_fired = 1;
        $this->fired_at = $time;
        $this->fired_value = $value;
        $this->tline_fired_value = $this->getValueByTime($time);
    }
}
