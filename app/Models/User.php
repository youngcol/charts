<?php
declare(strict_types=1);
namespace App\Models;

use App\Models\SocialEngine\UserSocialAccount;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class User
 * @package App\Models
 *
 * @property string permissions
 * @property string password
 * @property int is_admin
 * @property int active
 * @property string email
 * @property string timezone
 * @property string language
 */
class User extends Model
{
    use UserTrait;

    /**
     * Hidden from json resource
     * @var array
     */
    protected $hidden = [
        'password',
        'password_reset_code',
        'random_key',
        'is_email_verified',
        'email_verify_code'
    ];

    protected $fillable = [
        'id',
        'name',
        'is_admin',
        'active',
        'email',
        'timezone',
        'language',
        'created_at'
    ];

    // integer, real, float, double, decimal:<digits>, string, boolean,  object, array, collection, date, datetime, timestamp
    protected $casts = [
        'active' => 'integer',
        'is_admin' => 'integer'
    ];

    protected $api_task_fillable = [
        'name',
        'timezone',
        'language'
    ];

    protected static function boot()
    {
        parent::boot();

        static::saving(function($user)  // Функция обработчика в качестве аргумента принимает объект модели
        {
            if (eN($user->random_key)) {
                $user->random_key = bin2hex (random_bytes(16));
            }
        });
    }

    public function toCustomJson(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'permissions' => $this->permissions,
            'is_admin' => $this->is_admin,
            'active' => $this->active,
            'email' => $this->email,
            'created_at' => date_mysql_format($this->created_at),
            'timezone' => $this->timezone,
            'language' => $this->language,
        ];
    }

    /**
     * @return string
     */
    public function getFirstNameAttribute(): string
    {
        return $this->name . $this->permissions;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function socialAccount()
    {
        return $this->hasMany(UserSocialAccount::class);
	}

    /**
     * @param $networkId
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Relations\HasMany|null|object
     */
    public function getFirstSocialAccount(int $networkId)
    {
        return $this->socialAccount()
            ->where('social_network_id', $networkId)
            ->first();
	}
}
