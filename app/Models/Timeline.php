<?php

declare(strict_types=1);
namespace App\Models;

use App\Models\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Timeline
 *
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property date $deleted_at
 * @property string $name
 *
 * @package App\Model
 */
final class Timeline extends Model
{
    use SoftDeletes;

    protected $table = 'timelines';

    /**
    * Hidden from json resource
    * @var array
    */
    protected $hidden = [

    ];

    protected $fillable = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
        'name',
    ];

    /* These fields can be updated by UpdateModelsCollectionTask */
    protected $api_fillable = [

    ];

    // integer, real, float, double, decimal:<digits>, string, boolean,  object, array, collection, date, datetime, timestamp
    protected $casts = [
//        'social_network_id' => 'integer'
    ];

    /**
    * Model constructor.
    * @param array $attributes
    */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function chartGroups()
    {
//        'contact_list__have__contact', 'contact_list_id', 'contact_id'

        return $this->belongsToMany(ChartGroup::class, 'timeline_have_chart_group', 'timeline_id', 'chart_group_id');
    }
}
