<?php

declare(strict_types=1);
namespace App\Models;

use App\Models\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Ohl
 *
 * @property \Carbon\Carbon $time
 * @property integer $period
 * @property string $instrument
 * @property float $o
 * @property float $h
 * @property float $l
 * @property float $c
 *
 * @package App\Model
 */
final class Ohlc extends Model
{
    use SoftDeletes;

    protected $table = 'ohlc';

    /**
    * Hidden from json resource
    * @var array
    */
    protected $hidden = [

    ];

    protected $fillable = [
        'time',
        'period',
        'instrument',
        'o',
        'h',
        'l',
        'c',
    ];

    /* These fields can be updated by UpdateModelsCollectionTask */
    protected $api_fillable = [

    ];

    // integer, real, float, double, decimal:<digits>, string, boolean,  object, array, collection, date, datetime, timestamp
    protected $casts = [
        'social_network_id' => 'integer'
    ];

    /**
    * Model constructor.
    * @param array $attributes
    */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
