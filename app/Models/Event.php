<?php

declare(strict_types=1);
namespace App\Models;

use App\Models\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Event
 *
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $tag
 * @property string $instrument
 * @property string $period
 * @property integer $direction
 *
 * @package App\Model
 */
final class Event extends Model
{
//    use SoftDeletes;

    protected $table = 'events';

    /**
    * Hidden from json resource
    * @var array
    */
    protected $hidden = [

    ];

    protected $fillable = [
        'id',
        'created_at',
        'generated_at',
        'updated_at',
        'tag',
        'instrument',
        'period',
        'direction',
        'params',
        'place_tag',
        'tline_id',
        'price',
        'is_trading'
    ];

    /* These fields can be updated by UpdateModelsCollectionTask */
    protected $api_fillable = [

    ];

    // integer, real, float, double, decimal:<digits>, string, boolean,  object, array, collection, date, datetime, timestamp
    protected $casts = [
        'params' => 'array',
        'price' => 'float'
    ];

    /**
    * Model constructor.
    * @param array $attributes
    */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
