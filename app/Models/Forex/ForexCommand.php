<?php

declare (strict_types = 1);
namespace App\Models\Forex;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Model;

/**
 * Class ForexCommand
 *
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $name
 * @property text $params
 *
 * @package App\Model
 */
final class ForexCommand extends Model
{
    use SoftDeletes;

    protected $table = 'forex_commands';

    /**
     * Hidden from json resource
     * @var array
     */
    protected $hidden = [

    ];

    protected $fillable = [
        'created_at',
        'updated_at',
        'name',
        'params',
        'uniq_id'
    ];

    /* These fields can be updated by UpdateModelsCollectionTask */
    protected $api_fillable = [

    ];

    // integer, real, float, double, decimal:<digits>, string, boolean,  object, array, collection, date, datetime, timestamp
    protected $casts = [
        'params' => 'object',
    ];

    /**
     * Model constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @param string $name
     * @param int $uniq_id
     * @param array $params
     * @return ForexCommand
     */
    public static function create(string $name, int $uniq_id, array $params)
    {
        $r = new ForexCommand();
        $r->name = $name;
        $r->params = $params;
        $r->uniq_id = $uniq_id;

        return $r;
    }

}
