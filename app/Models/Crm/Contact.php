<?php

declare(strict_types=1);
namespace App\Models\Crm;

use App\Models\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Contact
 *
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $name
 * @property string $email
 * @property string $site_url
 * @property string $phone
 * @property string $address
 * @property string $facebook_link
 * @property string $youtube_link
 * @property string $linkedin_link
 * @property string $success_item
 * @property string $greet_item
 * @property string $yelp_url
 * @property integer $yelp_reviews_count
 * @property integer $yelp_reviews
 * @property integer $category_id
 * @property integer $location_id
 *
 * @package App\Model
 */
final class Contact extends Model
{
    use SoftDeletes;

    protected $table = 'contacts';

    protected $attributes = [
        'email' => '',
        'site_url'=> '',
        'phone'=> '',
        'address'=> '',
        'youtube_link'=> '',
        'facebook_link'=> '',
        'linkedin_link'=> '',
        'yelp_url'=> '',
        'yelp_reviews_count'=> 0,
        'yelp_reviews'=> 0,
        'success_item'=> '',
        'greet_item'=> '',
    ];

    protected $fillable = [
        'name',
        'email',
        'site_url',
        'phone',
        'address',
        'youtube_link',
        'facebook_link',
        'linkedin_link',
        'yelp_url',
        'yelp_reviews_count',
        'yelp_reviews',
        'category_id',
        'location_id',
        'success_item',
        'greet_item',
    ];

    /* These fields can be updated by UpdateModelsCollectionTask */
    protected $api_fillable = [

    ];

    // integer, real, float, double, decimal:<digits>, string, boolean,  object, array, collection, date, datetime, timestamp
    protected $casts = [

    ];

}
