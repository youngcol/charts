<?php

declare(strict_types=1);
namespace App\Models\Crm;

use App\Models\Model;
use App\Models\User;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ContactList
 *
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $name
 * @property integer $user_id
 *
 * @package App\Model
 */
final class ContactList extends Model
{
//    use SoftDeletes;

    protected $table = 'contact_lists';

    protected $fillable = [
        'id',
        'created_at',
        'updated_at',
        'name',
        'user_id',
    ];

    protected $casts = [
//        'social_network_id' => 'integer'
    ];

    public function contacts()
    {
        return $this->belongsToMany(Contact::class, 'contact_list__have__contact', 'contact_list_id', 'contact_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
