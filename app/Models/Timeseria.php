<?php

declare(strict_types=1);
namespace App\Models;

use App\Models\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TrendScalpTimeseri
 *
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $instrument
 * @property integer $period
 * @property float $value
 *
 * @package App\Model
 */
final class Timeseria extends Model
{
//    use SoftDeletes;

    protected $table = 'timeseries';

    /**
    * Hidden from json resource
    * @var array
    */
    protected $hidden = [

    ];

    protected $fillable = [
        'id',
        'created_at',
        'updated_at',
        'instrument',
        'period',
        'value',
        'tag',
        'time'
    ];

    /* These fields can be updated by UpdateModelsCollectionTask */
    protected $api_fillable = [

    ];

    // integer, real, float, double, decimal:<digits>, string, boolean,  object, array, collection, date, datetime, timestamp
    protected $casts = [
//        'social_network_id' => 'integer'
    ];

    /**
    * Model constructor.
    * @param array $attributes
    */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
