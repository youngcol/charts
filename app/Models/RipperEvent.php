<?php

declare(strict_types=1);
namespace App\Models;

use App\Models\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class RipperEvent
 *
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $event
 * @property string $params
 *
 * @package App\Model
 */
final class RipperEvent extends Model
{
    use SoftDeletes;

    protected $table = 'ripper_events';

    protected $appends = [
        //'appends_attr'
    ];

    /**
    * Hidden from json resource
    * @var array
    */
    protected $hidden = [

    ];

    protected $fillable = [
        'id',
        'created_at',
        'updated_at',
        'event',
        'params',
    ];

    /* These fields can be updated by UpdateModelsCollectionTask */
    protected $api_fillable = [

    ];

    // integer, real, float, double, decimal:<digits>, string, boolean,  object, array, collection, date, datetime, timestamp
    protected $casts = [
        'params' => 'array'
    ];

    /**
    * Model constructor.
    * @param array $attributes
    */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
    public function getTagTranslatedAttribute()
    {
        return 'the translated tag';
    }
    */
}
