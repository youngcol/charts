<?php

namespace App\Models\SocialEngine\Post;
use App\Models\Model;

class PostGroup extends Model
{
    protected $table = 'posts_group';

    protected $fillable = [
        'title',
        'user_id'
    ];
}
