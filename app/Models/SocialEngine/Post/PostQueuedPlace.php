<?php

declare(strict_types=1);

namespace App\Models\SocialEngine\Post;
use App\Models\Model;
use App\Models\SocialEngine\Post;
use App\Contracts\PublishReasonContract;
use App\Models\User;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PostQueuedPlace
 *
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $post_id
 *
 * @package App\Model
 */
final class PostQueuedPlace extends Model implements PublishReasonContract
{
//    use SoftDeletes;

    protected $table = 'post_queued_places';

    protected $fillable = [
        'id',
        'created_at',
        'updated_at',
        'post_id',
        'user_id',
        'publish_params_id',
        'process_started_time',
        'is_processed',
        'order'
    ];

    public function getPostPublishParams(): PostPublishParams
    {
        return $this->publishParams;
    }

    public function getPost(): Post
    {
        return $this->post;
    }

    public function publishParams()
    {
        return $this->belongsTo(PostPublishParams::class, 'publish_params_id', 'id');
    }

    public function post()
    {
        return $this->belongsTo(Post::class, 'post_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
