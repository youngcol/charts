<?php

/**
 * Post - article to post into social engines
 */
namespace App\Models\SocialEngine;

use App\Models\Model;
use App\Models\SocialEngine\Post\PostScheduledTime;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Post
 * @package App\Models\SocialEngine
 * @property string title
 * @property string text
 * @property string link
 * @property string user_id
 * @property string is_draft
 */
class Post extends Model
{
    protected $table = 'posts';

    protected $fillable = [
        'title',
        'text',
        'link',
        'user_id',
        'is_draft'
    ];

    protected $rules = [];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @return BelongsToMany
     */
    public function mediaFiles()
    {
        return $this->belongsToMany(MediaFile::class, 'post_media_files', 'post_id', 'media_file_id');
    }

    /**
     * @return HasOne
     */
    public function scheduledTime()
    {
        return $this->hasOne(PostScheduledTime::class, 'post_id', 'id');
    }
}
