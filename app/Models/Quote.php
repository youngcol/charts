<?php

declare(strict_types=1);

namespace App\Models;

use App\VO\VoVal;

/**
 * Class Chart
 *
 * @property integer $id
 *
 * @package App\Model
 */
final class Quote extends Model
{
//    use SoftDeletes;

//    protected $table = 'quotes';
    public $timestamps = false;

    /**
     * Hidden from json resource
     * @var array
     */
    protected $hidden = [

    ];

    protected $fillable = [
        'o',
        'h',
        'l',
        'c',
        'date'
    ];

    /* These fields can be updated by UpdateModelsCollectionTask */
    protected $api_fillable = [

    ];

    // integer, real, float, double, decimal:<digits>, string, boolean,  object, array, collection, date, datetime, timestamp
    protected $casts = [
        'date' => 'datetime',

    ];

    /**
     * Model constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @param string $quotesName
     * @return \App\Models\Model|void
     */
    public function setTable1(string $quotesTableName)
    {
        parent::setTable($quotesTableName);
    }

    public function getOpenCloseDeltaAttribute()
    {
        return round(abs($this->o - $this->c), 4);
    }

    public function buildNewClose()
    {

    }

    /**
     * @param Quote $q
     */
    public function merge(Quote $q)
    {
        $this->l = min($this->l, $q->l);
        $this->h = max($this->h, $q->h);
        $this->c = $q->c;
    }

    /**
     * @return float
     */
    public function getPipsStep()
    {
        $close = (string)$this->c;
        $parts = explode('.', $close);
        return strlen($parts[0]) === 1 ? 0.0001 : 0.01;
    }

    public function getHighLowDelta()
    {
        if ($this->isNothingCandle()) {
            return 0;
        }

        return $this->h - $this->l;
    }

    /**
     * @return Quote
     */
    public function clonePointQuote()
    {
        $ret = clone $this;
        $ret->date = $this->date->addSeconds(1);
        $ret->o = $ret->c;
        $ret->setHighLowToOpenClose();

        return $ret;
    }

    public function setHighLowToOpenClose()
    {
        if ($this->isUpCandle())
        {
            $this->l = $this->o;
            $this->h = $this->c;
        }
        else
        {
            $this->l = $this->c;
            $this->h = $this->o;
        }
    }

    /**
     * @return bool
     */
    public function isUpCandle(): bool
    {
        return $this->o < $this->c;
    }

    /**
     * @return bool
     */
    public function isNothingCandle()
    {
        return $this->o == $this->c;
    }
    /**
     * @param VoVal $instrument
     * @param VoVal $oandaTimeframe
     * @return string
     * @throws \Exception
     */
    public static function getQuotesTable(VoVal $instrument, VoVal $oandaTimeframe)
    {
        $ret = 'quotes_' . $instrument->_();
        $ret .= '_' . strtolower($oandaTimeframe->_());

        return $ret;
    }
}
