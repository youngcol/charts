<?php

declare(strict_types=1);
namespace App\Models;

use App\Models\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ChartLevel
 *
 * @property integer $id
 * @property integer $chart_id
 * @property float $price
 * @property string $type
 * @property integer $is_fired
 *
 * @package App\Model
 */
final class ChartLevel extends Model
{
    protected $table = 'chart_levels';

    public $timestamps = false;
    /**
    * Hidden from json resource
    * @var array
    */
    protected $hidden = [

    ];

    protected $fillable = [
        'is_fired',
        'value',
        'type',
        'fired_at',
        'is_trading',
        'params',
        'pair',
        'period',
        'fired_value'
    ];

    /* These fields can be updated by UpdateModelsCollectionTask */
    protected $api_fillable = [

    ];

    // integer, real, float, double, decimal:<digits>, string, boolean,  object, array, collection, date, datetime, timestamp
    protected $casts = [
        'params' => 'array'
    ];

    /**
    * Model constructor.
    * @param array $attributes
    */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function isBreakout(float $value): ?int
    {
        if($this->type === 'buy') {
            if ($value>=$this->value) return 1;
        } else {
            if ($value <= $this->value) return 0;
        }
        return null;
    }
}
