<?php

namespace App\Models;

class Model extends \Illuminate\Database\Eloquent\Model
{
    /**
     * Model constructor.
     * @param array $attributes
     */
	public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
	}

    /**
     * @return mixed
     */
    public function getApiFillable()
    {
        return $this->api_task_fillable;
	}

    public function save($options=[])
    {
        if ($this->is_model_saving_ok)
        {

        }

        parent::save($options);
	}
}
