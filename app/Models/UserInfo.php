<?php

declare(strict_types=1);
namespace App\Models;

use App\Models\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class UserInfo
 * Can store info about user related data
 *
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property date $deleted_at
 * @property text $data
 * @property integer $user_id
 *
 * @package App\Model
 */
final class UserInfo extends Model
{
    use SoftDeletes;

    /**
     * @var string
     */
    protected $table = 'user_infos';

    protected $fillable = [
        'data',
        'user_id',
        'field'
    ];

    /**
     * UserInfo constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = []) {

        if (isset($attributes['field']))
        {
            $this->field = $attributes['field'];
        }

        if (isset($attributes['user_id']))
        {
            $this->user_id = $attributes['user_id'];
        }

//        dump($attributes);echo 'UserInfo.php:35'; exit;
        parent::__construct([]);
    }


    // integer, real, float, double, decimal:<digits>, string, boolean,  object, array, collection, date, datetime, timestamp
    protected $casts = [
        'user_id' => 'integer',
        'data' => 'array'
    ];

    /**
     * @param $key
     * @param $val
     * @return $this
     */
    public function setField($key, $val)
    {
        $temp = $this->data;
        $temp[$key] = $val;
        $this->data = $temp;

        return $this;
    }

    /**
     * @param array $data
     * @return $this
     */
    public function set(array $data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @param null $key
     * @return text|null
     */
    public function get($key=null)
    {
        if (!eN($key))
        {
            return sp($this->data, $key);
        }

        return $this->data;
    }
}
