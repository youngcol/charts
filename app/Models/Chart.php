<?php

declare(strict_types=1);
namespace App\Models;

use App\Models\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Chart
 *
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $instrument
 * @property string $view
 * @property integer $timeframe
 * @property integer $scale
 * @property integer $user_id
 * @property integer $group_id
 *
 * @package App\Model
 */
final class Chart extends Model
{

    const MINUTES_TIMEFRAME = 'M';
    const SECONDS_TIMEFRAME = 'S';

    use SoftDeletes;

    protected $table = 'charts';

    /**
    * Hidden from json resource
    * @var array
    */
    protected $hidden = [

    ];

    protected $fillable = [
        'instrument',
        'view',
        'timeframe',
        'indicators',
        'scale',
        'user_id',
        'group_id',
        'timeframe_type'
    ];

    /* These fields can be updated by UpdateModelsCollectionTask */
    protected $api_fillable = [

    ];

    // integer, real, float, double, decimal:<digits>, string, boolean,  object, array, collection, date, datetime, timestamp
    protected $casts = [
        'timeframe' => 'integer',
        'scale'     => 'integer'
    ];

    /**
    * Model constructor.
    * @param array $attributes
    */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo(ChartGroup::class, 'group_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function levels()
    {
        return $this->hasMany(ChartLevel::class, 'chart_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function trendlines()
    {
        return $this->hasMany(ChartTrendline::class, 'chart_id', 'id');
    }

}
