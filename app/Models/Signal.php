<?php

declare(strict_types=1);
namespace App\Models;

use App\Models\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Signal
 *
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $time
 * @property string $name
 * @property string $instrument
 * @property integer $period
 * @property text $params
 *
 * @package App\Model
 */
final class Signal extends Model
{
    use SoftDeletes;

    protected $table = 'signals';

    protected $appends = [
        //'appends_attr'
    ];

    /**
    * Hidden from json resource
    * @var array
    */
    protected $hidden = [

    ];

    protected $fillable = [
        'id',
        'created_at',
        'updated_at',
        'time',
        'name',
        'instrument',
        'period',
        'params',
    ];

    /* These fields can be updated by UpdateModelsCollectionTask */
    protected $api_fillable = [

    ];

    // integer, real, float, double, decimal:<digits>, string, boolean,  object, array, collection, date, datetime, timestamp
    protected $casts = [
        'time' => 'datetime'
    ];

    /**
    * Model constructor.
    * @param array $attributes
    */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
    public function getTagTranslatedAttribute()
    {
        return 'the translated tag';
    }
    */
}
