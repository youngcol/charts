<?php

//.Validation rules for val() function
// https://github.com/rakit/validation#available-rules


use App\Facades\CU;
use App\Models\Crm\Contact;
use App\Models\SocialEngine\SocialNetwork;
use App\Models\User;


$val = [
    'middleware.context' => 'in:app,admin',
    'middleware.mode' => 'in:web,api',

    'filled.string' => 'min:1',
    'string' => 'min:1',

    'datetime' => 'date:Y-m-d H:i:s',
    'time' => 'date:H:i',
    'array' => 'array',

    'instruments' => 'in:' . settings('instruments'),
    'chart.timeframe' => 'numeric',
    'chart.timeframe.type' => 'in:M,S',
    'chart.view' => 'in:default,renko,tick,renko3,renko5,renko2',
    'chart.scale' => 'numeric',
    'chart.levels.type' => 'in:resistance,support',

    'oanda.timeframe' => 'in:M15,M5',
    'oanda.timeframes.in_use' => 'in:M15',

    'binary.interval' => 'in:days,hours,minutes',

    'screenshots.period' => 'in:1,5,15,30,60,240,1440',


    'forex.seeds' => 'in:qqe,qqe_cross,ohlc',
    'forex.commands' => 'in:' . implode(',', array_keys(settings('forex.commands'))),

];


$val['ids.string'] = function ($type)
{
    $ids = explode(',', $type);

    foreach ($ids as $id)
    {
        test($id !== (int)$id, "It is not an integer value");
    }
};

/**
 * Check array of ids
 *
 * @param $ids
 * @return \App\Common\Res
 */
$val['ids.array'] = function ($ids)
{
    test(!is_array($ids), 'Input is not an array');

    foreach ($ids as $id)
    {
        test($id !== (int) $id, "It is not an integer value: ". $id);
    }

    return ok();
};

/**
 * Validations requests for controllers input
 */
$val_request =
[
    'user.signin' => [
        'username' => 'required',
        'password' => 'required',
        'remember_me' => '',
    ],

    'user.signup' => [
        'email' => 'required',
        'username' => '',
        'password' => 'required'
    ],

    'user.password.change' => [
        'pass1' => 'required',
        'pass2' => 'required',
        'code' => 'required',
    ],

    'run.task' => [
        'task_name' => 'required',
        'task_params' => '',
    ],
];


/**
 * List of dictionary models that are accessable from api
 */
$dictionary_models = [
    //    'App\Models\SocialEngine\SocialNetwork'
//    'SocialNetwork' => SocialNetwork::class,

];

$forex_commands = [
[
    'name' => 'STATUS',
    'params' => []
],
[
    'name' => 'GET_HISTORY_TIMESERIA',
    'params' => [
        'name' => 'string',
        //'name' => 'string',
    ]
],
[
    'name' => 'OPEN_MARKET_ORDER',
    'params' => [
        'type' => 'in:buy,sell',
        'sl' => 'numeric',
        'tp' => 'numeric',
    ]
],
    [
        'name' => 'GET_OPEN_ORDERS',
        'params' => [
            'type' => 'in:buy,sell',
            'sl' => 'numeric',
            'tp' => 'numeric',
        ]
    ],

    [
        'name' => 'CLOSE_ORDER',
        'params' => [
            'id' => 'string',

        ]
    ],

];
