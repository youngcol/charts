<?php

namespace App\Middlewares;

use App\Facades\CU;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use App\Models\Session;
use App\Models\User;

class AdminAuth extends Middleware
{
    public function __invoke(Request $request, Response $response, callable $next)
    {
        if (!CU::user()->is_admin)
        {
            return redirect( 'signin');
        }

        return $next($request, $response);
    }
}
