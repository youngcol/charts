<?php

namespace App\Common;

/**
 * Class Res - returned resource
 */

class Res
{
    private $_keys;

    public function __construct(array $data = [])
    {
        foreach ($data as $k => $v) {
            $this->{$k} = $v;
            $this->_keys[] = $k;
        }
    }

    public function set($k, $v) {
        if (property_exists($this, $k)) {
            crit('Property exists' . $k);
        }

        $this->{$k} = $v;
        $this->_keys[] = $k;
    }

    public function toJson()
    {
        return json_encode($this->toArray());
    }

    public function toArray()
    {
        $ret = [];

        foreach ($this->_keys as $key) {
            $ret[$key] = $this->{$key};
        }

        return $ret;
    }

    public function ok()
    {
        return true;
    }

    public function fail()
    {
        return false;
    }

    /**
     * Get data from object
     * $rotue [tweet.result.id, tweet]
     * @param $route
     * @return mixed
     */
    public function get($route)
    {
        return data_get($this, $route);
    }
}

