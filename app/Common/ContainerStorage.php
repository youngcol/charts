<?php

namespace App\Common;


class ContainerStorage
{
    /**
     * @param $groupName
     * @param $key
     * @param $value
     */
    public static function add($groupName, $key, $value)
    {
        if (!c()->has($groupName)) {
            c()[$groupName] = [];
        }

        $group = c()[$groupName];
        $group[$key] = $value;
        c()[$groupName] = $group;
    }

    /**
     * @param $group
     * @return mixed
     */
    public static function get($group)
    {
        return c()[$group];
    }
}
