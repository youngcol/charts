<?php

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;

$dotenv = \Dotenv\Dotenv::createUnsafeImmutable(dirname(__DIR__));
$dotenv->load();
session_start();

// Instantiate the app
$settings = require CONFIG_PATH . '/settings.php';
$GLOBALS['settings'] = $settings;

$capsule = new Capsule;
$capsule->addConnection($settings['settings']['db']);
$capsule->setEventDispatcher(new Dispatcher(new Container));
$capsule->setAsGlobal();
$capsule->bootEloquent();


//
//$container['phpErrorHandler'] = function ($c) {
//    return function ($request, $response, $exception) use ($c) {
//
//        log_exception($exception);
//
//        if ($c['settings']['displayErrorDetails']) {
//
//            $view = new SimpleView('develop/error');
//            echo $view
//                ->assign('e', $exception)
//                ->assign('trace', $exception->getTrace())
//                ->render();
//            exit;
//
//        } else {
//
//            $contentType = $request->getHeader('Content-Type');
//            $isJson = false;
//
//            if (is_array($contentType))
//            {
//                $first = array_pop($contentType);
//                $isJson = !eF(strpos($first, '/json'));
//            }
//
//            if ($isJson) {
//                return $response->withJson([
//                    'message' => 'Internal server error',
//                    'code' => 500
//                ], 500);
//            } else {
//                return $response->withStatus(500)
//                    ->withHeader('Content-Type', 'text/html')
//                    ->write('<h1>500 | Interanal server error!</h1>');
//            }
//        }
//    };
//};
//
//
//$container['errorHandler'] = function ($c) {
//    return function ($request, $response, $exception) use ($c) {
//
//        log_exception($exception);
//
//        if ($c['settings']['displayErrorDetails']) {
//
//            $view = new SimpleView('develop/exception');
//            echo $view
//                ->assign('e', $exception)
//                ->assign('class', get_class($exception))
//                ->assign('trace', $exception->getTrace())
//                ->render();
//            exit;
//        }
//        else
//        {
//            $contentType = $request->getHeader('Content-Type');
//            $isJson = false;
//
//            if (is_array($contentType))
//            {
//                $first = array_pop($contentType);
//                $isJson = !eF(strpos($first, '/json'));
//            }
//
//            if ($isJson)
//            {
//                return $response->withJson([
//                    'message' => 'Internal server exception',
//                    'code' => 500
//                ], 500);
//            } else {
//                return $response->withStatus(500)
//                    ->withHeader('Content-Type', 'text/html')
//                    ->write('500 | Interanal server exception!');
//            }
//        }
//
//    };
//};
//
///**
// * @param $c
// * @return Closure
// */
//$container['notFoundHandler'] = function ($c) {
//    return function ($request, $response) use ($c) {
//
//        $view = new SimpleView('public/default/page_404');
//        return $response->withStatus(404)
//            ->withHeader('Content-Type', 'text/html')
//            ->write($view->render());
//    };
//};

//Predis\Autoloader::register();


