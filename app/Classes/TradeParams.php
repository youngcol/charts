<?php

namespace App\Classes;

class TradeParams
{
    private string $type;
    private int $sl;
    private int $tp;
    private string $risk;
    private string $risk_type;

    public function __construct(string $type, int $sl, int $tp, string $risk, string $risk_type)
    {
        $this->type = $type;
        $this->sl = $sl;
        $this->tp = $tp;
        $this->risk = $risk;
        $this->risk_type = $risk_type;
    }

    public function toArray()
    {
        return [

        ];
    }
}
