<?php
declare(strict_types=1);

use App\Common\MailRenderer;

use App\Facades\CU;
use App\Facades\Redis2;
use App\Models\User;
use App\Repositories\UserRepository;
use App\Services\OandaClient_v20;
use App\Services\OANDAv20;
use App\Services\Pinner;

use DirkGroenen\Pinterest\Pinterest;
use seregazhuk\PinterestBot\Factories\PinterestBot;

//// Register component on container
//$container['view'] = function ($container)
//{
//    $view = new \Slim\Views\Twig(PATH_ROOT . 'templates', [
//		'debug' => true
//		// TODO: use cache
//		// 'cache' => __DIR__ . '/cache'
//	]);
//
//	// Instantiate and add Slim specific extension
//	$basePath = rtrim(str_ireplace('index.php', '', $container['request']->getUri()->getBasePath()), '/');
//	$view->addExtension(new Slim\Views\TwigExtension($container['router'], $basePath));
//	$view->addExtension(new Twig_Extension_Debug());
//
//	return $view;F
//};
global  $container;

$container->set('view2', function (){
    return new \Slim\Views\Blade(
        [
            PATH_ROOT . 'templates',
            PATH_ROOT . 'app_vue',
            PATH_ROOT . 'app/actions',
        ],
        PATH_ROOT . 'cache/blade'
    );
});

//
//$container['view2'] = function ($container) {
//
//
//};
//
//// Service for the tokens
//$container['csrf'] = function ($container)
//{
//    $storage = null;
//	return new \Slim\Csrf\Guard(
//        $prefix = 'csrf',
//        $storage,
//        null,
//        200,
//        16,
//        $persistentTokenMode = true
//    );
//};
//
//$container['mailer'] = function() {
//    return [];
//};
//
//$container['mailRenderer'] = function() {
//    $renderer = new MailRenderer(PATH_ROOT. 'templates/mails');
//
//    return $renderer;
//};
//
//// Flash Messages
//$container['flash'] = function ($container)
//{
//	return new \Slim\Flash\Messages();
//};
//
///**
// * @param $container
// * @return string
// */
//$container['s3_client'] = function ($container)
//{
//    return 'client';
//
////	return new Aws\S3\S3Client([
////        'region'  => settings('amazon.s3.region'),
////        'version' => 'latest',
////        'credentials' => [
////            'key'    => settings('amazon.s3.key'),
////            'secret' => settings('amazon.s3.secret'),
////        ]
////    ]);
//};
//
//$container['pinner'] = function ($container)
//{
//    $pinterest = new Pinterest(
//        getenv('PINTEREST_CLIENT_ID'),
//        getenv('PINTEREST_CLIENT_SECRET'));
//
//	return new Pinner($container, $pinterest);
//};
//
//$container['pinner_b'] = function ($container)
//{
//    $bot = PinterestBot::create();
//    return $bot;
//};
//
//$container['twitter'] = function ($container)
//{
//    return new \App\Services\Twitter($container);
//};
//
//$container['facebook'] = function ($container)
//{
//    return new \App\Services\Facebook(
//        $container,
//        getenv('FACEBOOK_APP_ID'),
//        getenv('FACEBOOK_APP_SECRET')
//    );
//};
//
///**
// * Bootstrap part of application
// */
//$container['file_uploader'] = function ($container)
//{
//    return new \App\Services\FileUploader($container);
//};
//
//$container['cu_language'] = function ($container)
//{
//
//    //CU::user()->language
//    $lang = 'ru';
//    return new \App\Services\Language($lang);
//};
//
//$container['oanda'] = function ($container)
//{
//    return new OandaClient_v20(
//        OandaClient_v20::URL_PRACTICE,
//        settings('oanda.api_key')
//    );
//};
//
//$container['oanda2'] = function ($container)
//{
//    // deprecated
//    return new OANDAv20(
//        OANDAv20::URL_PRACTICE,
//        settings('oanda.api_key')
//    );
//};
$container->set('redis', function ($container)
{
    $settings = settings('redis');

    $redis = new Redis();
    if (getenv('APP_ENV') == 'dev') {
        $redis->connect('127.0.0.1', (int)$settings['default']['port']);
    } else {
        $redis->connect($settings['default']['host']);
    }

    return $redis;
});

Redis2::__setup();

//$container['repo.User'] = function ($container)
//{
//    return new UserRepository(User::class);
//};
