<?php

declare(strict_types=1);
namespace App\Facades;


class Redis2
{
    private static $redis;

    public static function __setup()
    {
        self::$redis = service('redis');
    }

    public static function r()
    {
        return self::$redis;
    }

    /**
     * @param $k
     * @param array $v
     */
    public static function set($k, $v)
    {
        return self::r()->set(is_array($k) ? implode("-", $k) : $k, is_array($v) ? json_encode($v) : $v);
    }

    /**
     * @param $k
     * @param false $decode
     * @return mixed|null
     */
    public static function get($k, $decode=true, $def=NULL)
    {
        $ret = self::r()->get($k);
        if ($ret == FALSE)
        {
            return $def;
        }

        if ($decode)
        {
            $ret = json_decode($ret, true);
        }

        return $ret;
    }

    public static function lPush($k, $v)
    {
        return self::r()->lPush($k, $v);
    }

    public static function lPop($k)
    {
        return self::r()->lPop($k);
    }

    public static function del($k)
    {
        return self::r()->del($k);
    }
}
