<?php

declare(strict_types=1);
namespace App\Facades;
use App\Common\Res;
use App\Tasks\get__ohlc__timeseries__task;
use App\Tasks\get__timeseria__task;

class OhlcFeed
{
    private $feed;

    function __construct(array $pair_periods, array $tlines, string $indicator_tag, bool $is_only_pics)
    {
        $this->feed = $this->build_feed_data($pair_periods, $tlines, $indicator_tag, $is_only_pics);
        return $this;
    }

    function get()
    {
        return $this->feed;
    }

    function build_feed_data(array $pair_periods, array $tlines, string $indicator_tag, bool $is_only_pics = false)
    {
        $ret = [
            'is_only_pics' => $is_only_pics
        ];
        $series_requests = [];

        foreach ($pair_periods as $pair => $periods) {
            foreach ($periods as $period) {
                array_create_key($series_requests, $period);
                $series_requests[$period][] = $pair;

//                $screenshots = [];
//                $drawnings = [];
//                array_create_key($ret[$pair], $period);
//
//                build_pair_pics_feed($pair, $period, (int)settings('screenshots.crop_percent.' . $period), $screenshots);
//                build_drawning_pics_feed($pair, $period, (int)settings('screenshots.crop_percent.' . $period), $drawnings);
//
//                $ret[$pair][$period] = [
//                    'screenshots' => $screenshots[$pair],
//                    'drawnings' => $drawnings[$pair],
//                ];
//
//                $ret[$pair]['is_only_pics'] = $is_only_pics;
            }
        }

//        dump($tlines);echo "OhlcFeed.php:52"; exit;
        // series
        if (!$is_only_pics) {
            $history = settings('charts.history');
            foreach ($series_requests as $period => $pairs) {
                $resOhlc = task(new get__ohlc__timeseries__task,
                    [
                        $pairs,
                        $period,
                        $history[$period]
                    ]
                );

                // @rsx timeseria
                $resRsx = task(new get__timeseria__task, [
                    $pairs,
                    (int) $period,
                    $history[$period],
                    $indicator_tag
                ]);
                //            var_export($tlines->list);
                foreach ($pairs as $pair)
                {
                    $ret[$pair][$period]['ohlc'] = [
                        'seria' => isset($resOhlc->seria[$pair]) ? $resOhlc->seria[$pair] : [],
                        'empty_bars' => isset($resOhlc->empty_bars[$pair]) ? $resOhlc->empty_bars[$pair] : [],
                        'tlines' => [
                            'ohlc' => !empty($tlines[$pair]) ? filter_tlines_by_tag($tlines[$pair][$period], 'ohlc') : [],
                            'rsx' => !empty($tlines[$pair]) ? filter_tlines_by_tag($tlines[$pair][$period], settings('indicator.tag')) : []
                        ],
                        'rsx' => [
                            'series' => [
                                $pair => isset($resRsx->series[$pair]) ? $resRsx->series[$pair] : []
                            ],
                            'empty_bars' => [
                                $pair => isset($resRsx->empty_bars[$pair]) ? $resRsx->empty_bars[$pair] : []
                            ],
                        ],
                    ];

                }
            }
        }

        return $ret;
    }
}
