<?php

namespace App\Facades;

class Screener
{
    function set(string $instrument, int $period, string $chart_tag, string $screener_tag, int $direction, array $meta = null)
    {
        $screener = Redis2::get(instrument_screener__state_key($instrument));

        if ($screener_tag == 'tlb')
        {
            list($screener, $updates) = $this->update_tlb_screener_state(
                $period, $chart_tag, $screener_tag, $direction, $screener, $meta
            );

        }
        else
        {
            list($screener_list, $updates) = $this->update_binary_screener_state($period, $chart_tag, $screener_tag, $direction, $screener['list']);
            $screener['list'] = $screener_list;
        }


        Redis2::set(instrument_screener__state_key($instrument), $screener);

        return [
            'updates' => $updates
        ];
    }

    function get(string $instrument, int $period, string $chart_tag, string $screener_tag)
    {
        $ret = -1;
        $screener = Redis2::get(instrument_screener__state_key($instrument));
        $full_name = $this->screener_tag_name($chart_tag, $screener_tag);

        if (isset($screener['list'][$period][$full_name])) {
            $ret = $screener['list'][$period][$full_name];
        }

        return $ret;
    }

    /**
     * @param string $instrument
     * @param int $period
     * @param string $chart_tag
     * @param string $screener_tag
     * @param string $direction
     * @return \int[][]
     */
    function delete(string $instrument, int $period, string $chart_tag, string $screener_tag, string $direction)
    {
        $updates = ['deleted' => 0];

        $screener = Redis2::get(instrument_screener__state_key($instrument));

        $state = array_flip($screener['list'][$period]);
        $full_state_name = implode('_', [$chart_tag, $screener_tag, $direction]);

        if(isset($state[$full_state_name]))
        {
            unset($state[$full_state_name]);
            $updates['deleted']++;
        }

        $screener['list'][$period] = array_keys($state);
        Redis2::set(instrument_screener__state_key($instrument), $screener);

        return [
            'updates' => $updates
        ];
    }

    function remove_outdated_tlb()
    {

    }


    /**
     * @param int $period
     * @param string $chart_tag
     * @param string $screener_tag
     * @param string $direction
     * @param array $screener
     * @return void
     */
    private function update_binary_screener_state(int $period, string $chart_tag, string $screener_tag, string $direction, array $screener)
    {
        $ret = [
            'added' => 0,
            'deleted' => 0
        ];

        $state = array_flip($screener[$period]);
        $full_state_name = implode('_', [$chart_tag, $screener_tag, $direction]);
        $opposite_full_state_name = implode('_', [$chart_tag, $screener_tag, $direction == 'sell' ? 'buy' : 'sell']);

        if(!isset($state[$full_state_name]))
        {
            $state[$full_state_name] = 1;
            $ret['added']++;
        }

        if(isset($state[$opposite_full_state_name]))
        {
            unset($state[$opposite_full_state_name]);
            $ret['deleted']++;
        }
        $screener[$period] = array_keys($state);

        return [$screener, $ret];
    }

    private function update_tlb_screener_state(int $period, string $chart_tag, string $screener_tag, int $direction, array $screener, array $meta)
    {
        $ret = [
            'added' => 1
        ];

        $full_name = $this->screener_tag_name($chart_tag, $screener_tag);
        $screener['list'][$period][$full_name] = $direction;

        array_create_key($screener['meta'], $period);
        $screener['meta'][$period][$full_name] = $meta;

        return [$screener, $ret];
    }

    private function screener_tag_name(string $chart_tag, string $screener_tag)
    {
        return $chart_tag . '__' . $screener_tag;
    }

}
