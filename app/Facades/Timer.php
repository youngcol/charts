<?php

declare(strict_types=1);
namespace App\Facades;


use Carbon\Carbon;

class Timer
{
      function __construnct(string $name)
      {

      }

    /**
     * @param string $name
     * @return int
     */
    public static function diffInMinutes(string $name): int
    {
        $redis = service('redis');
        $last_time = $redis->get($name);
        if ($last_time) return -1;
        //=-=-=-=-=-=-=-=-=

        return abs(Carbon::parse($last_time)->diffInMinutes(Carbon::now()));
    }

    public static function start(string $name)
    {
        $redis = service('redis');
        $redis->set("timer-$name", now());
    }
}
