<?php
use App\Tasks\<task_name>;

$params = [

    'id' => 'required|numeric',
    'accounts' => 'required|ids.array',
    'accounts' => 'required|string',
    'params' => 'required|array',
];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new <task_name>,
        [
            (int)$params['id']->_(),
            $params['accounts']->_(),
            $params['params']->toArray(),
        ]
        );

        return $res;
    }
];
