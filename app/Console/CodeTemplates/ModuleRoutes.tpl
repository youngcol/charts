<?php

global $app;

$app->group('/api', function() {

    $this->group('/<module>', function() {

        $this->get('', 'Module\<module>\Controller\MainController:actionIndex');

    });

});
