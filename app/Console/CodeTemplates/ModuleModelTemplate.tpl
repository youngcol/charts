<?php
namespace Module\<module>\Model;


use App\Model\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class <class>
 *
<phpdoc>
 *
 * @package App\Model
 */
final class <class> extends Model
{
    use SoftDeletes;

    protected $table = '<tableName>';

    protected $casts = [
        'birthday' => 'date:Y-m-d',
    ];

    protected $fillable = [
<fillable>
    ];

    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
    }


}
