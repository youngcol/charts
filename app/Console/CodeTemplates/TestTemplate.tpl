<?php
declare(strict_types=1);
namespace <test_namespace>;

use PHPUnit\Framework\TestCase;
use App\Common\GateException;


class <test_class> extends TestCase
{
    public function setUp():void
    {
        $this->markTestSkipped();
    }

    public function test_run_ok()
    {

        $res = task(new <task_class>, [

        ]);

        // $this->assertSame($res, new Res));
        // $this->assertSame(1, 0);
        // $this->assertSame([2, 3, 4], $collection);
    //  $this->assertInternalType('int', $carry);
    //  $this->assertInternalType('int', $value);
    }

    public function test_fired_gates()
    {
        $this->expectException(GateException::class);
        task(new <task_class>, [

        ]);

        // $this->assertSame(1, 0);
        // $this->assertSame([2, 3, 4], $collection);
        // $this->assertInternalType('int', $carry);
        // $this->assertInternalType('int', $value);
    }
}
