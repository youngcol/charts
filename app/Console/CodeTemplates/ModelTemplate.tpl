<?php

declare(strict_types=1);
namespace App\Models;

use App\Models\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class <class>
 *
<phpdoc>
 *
 * @package App\Model
 */
final class <class> extends Model
{
    use SoftDeletes;

    protected $table = '<tableName>';

    protected $appends = [
        //'appends_attr'
    ];

    /**
    * Hidden from json resource
    * @var array
    */
    protected $hidden = [

    ];

    protected $fillable = [
<fillable>
    ];

    /* These fields can be updated by UpdateModelsCollectionTask */
    protected $api_fillable = [

    ];

    // integer, real, float, double, decimal:<digits>, string, boolean,  object, array, collection, date, datetime, timestamp
    protected $casts = [
        'social_network_id' => 'integer'
    ];

    /**
    * Model constructor.
    * @param array $attributes
    */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
    public function getTagTranslatedAttribute()
    {
        return 'the translated tag';
    }
    */
}
