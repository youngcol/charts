<?php

namespace Module\<module>\Actions;

use App\Common\Res;
use App\Actions\BaseAction;

class <action> extends BaseAction
{
    public function run()
    {
        return new Res([]);
    }
}
