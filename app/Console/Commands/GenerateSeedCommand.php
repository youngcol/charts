<?php

namespace App\Console\Commands;

use App\Common\Helper;
use App\Console\Traits\CodeGenerate;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

/**
 * GenerateSeedCommand
 */
class GenerateSeedCommand extends Command
{
    use CodeGenerate;

    /**
     * Configuration of command
     */
    protected function configure()
    {
        $this
            ->setName('generate:seed')
            ->setDescription('Command generate:seed')
            ->addArgument('seed', InputArgument::REQUIRED, 'What table name?')
            ->setHelp('php partisan generate:seed seed</info>')
        ;
    }

    /**
     * Execute method of command
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(['<comment>Welcome to the seed generator</comment>']);

        $seedName = $input->getArgument('seed');
        $baseName     = date('YmdHis').'_'.$seedName.'.php';
        $path         = $this->getPath($baseName, SEEDS_PATH);
        $placeHolders = [
            '<class>',
        ];
        $replacements = [
            Helper::underscoreToCamelCase($seedName, true),
        ];

        $this->generateCode($placeHolders, $replacements, 'SeedTemplate.tpl', $path);

        $output->writeln(sprintf('Generated new seed class to "<info>%s</info>"', realpath($path)));

        return;
    }
}
