<?php

namespace App\Console\Commands;

use App\Common\Helper;
use App\Console\Traits\CodeGenerate;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * GenerateTaskCommand
 */
class GenerateTaskCommand extends Command
{
    use CodeGenerate;

    /**
     * Configuration of command
     */
    protected function configure()
    {
        $this
            ->setName('generate:task')
            ->setDescription('Command generate:task')
            ->addArgument('task_class_path', InputArgument::REQUIRED, 'What task class folder path?')
            ->setHelp('php partisan generate:task task_class_path</info>')
        ;
    }

    /**
     * Execute method of command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(['<comment>Welcome to the seed generator</comment>']);
        $taskClass = $input->getArgument('task_class_path');

        $taskClass = str_replace(' ', '__', $taskClass);
        $taskClass = str_replace('_', '__', $taskClass);
        $pathnameInfo = Helper::classPathname($taskClass, '__task');

        $path = $this->getPath($pathnameInfo['basename'], TASKS_PATH);
        create_folder_tree_by_pathname(dirname($path));

        $namespaceReplacements = 'App\Tasks';
        if (count($pathnameInfo['namespace'])) {
            $namespaceReplacements .= '\\' . implode('\\', $pathnameInfo['namespace']);
        }

        $placeHolders = [
            '<class>',
            '<namespace>'
        ];
        $replacements = [
            $pathnameInfo['className'],
            $namespaceReplacements
        ];

        $this->generateCode($placeHolders, $replacements, 'TaskTemplate.tpl', $path);
        $output->writeln(sprintf('Generated new task class to "<info>%s</info>"', realpath($path)));


        $this->create_task_test($pathnameInfo);

        return;
    }

    /**
     * @param array $pathnameInfo
     * @throws \Exception
     */
    protected function create_task_test(array $pathnameInfo)
    {
        $classname = str_replace(".php", "", $pathnameInfo['className']) . "__Test";

        $placeHolders = [
            '<test_class>',
            '<task_class>',
            '<test_namespace>'
        ];

        $replacements = [
            $classname,
            $pathnameInfo['className'],
            implode("\\", array_merge(['tasks'],  $pathnameInfo['namespace']))
        ];


        $path = $this->getPath(str_replace(".php", "", 'tasks/' . $pathnameInfo['basename']) . "__Test.php", TESTS_PATH);

        create_folder_tree_by_pathname(dirname($path));
        $this->generateCode($placeHolders, $replacements, 'TestTemplate.tpl', $path);
    }

}
