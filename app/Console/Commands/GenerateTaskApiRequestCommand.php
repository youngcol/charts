<?php

namespace App\Console\Commands;

use App\Common\Helper;
use App\Console\Traits\CodeGenerate;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * GenerateTaskCommand
 */
class GenerateTaskApiRequestCommand extends Command
{
    use CodeGenerate;

    /**
     * Configuration of command
     */
    protected function configure()
    {
        $this
            ->setName('generate:task-api-request')
            ->setDescription('Command generate:task-api-request')
            ->addArgument('task_class_path', InputArgument::REQUIRED, 'What task class folder path?')
            ->setHelp('php partisan generate:task-api-request task_class</info>')
        ;
    }

    /**
     * Execute method of command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(['<comment>Welcome to the api task request generator</comment>']);
        $taskClass = $input->getArgument('task_class_path');

        $taskClass = str_replace(' ', '__', $taskClass);
        $pathnameInfo = Helper::classPathname($taskClass, '__task');

        $path = $this->getPath('api-request--' . $pathnameInfo['basename'], API_REQUESTS_PATH);
        var_dump($path);

        $placeHolders = [
            '<task_name>',
        ];

        $replacements = [
            $pathnameInfo['className']
        ];

        $this->generateCode($placeHolders, $replacements, 'TaskApiRequestTemplate.tpl', $path);
        $output->writeln(sprintf('Generated new task api request class to "<info>%s</info>"', realpath($path)));

        return;
    }
}
