<?php

namespace App\Console\Commands\App;

use App\Models\Biz;
use App\Models\ScrapeRequest;
use App\Tasks\Oanda\download__latest__quotes__for__instrument__task;
use DOMDocument;
use DOMXPath;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


class DownloadFutureCandlesForAllInsturumentsCommand extends Command
{
    /**
     * Configuration of command
     */
    protected function configure()
    {
        global $container;
        $this->container = $container;

        $this
            ->setName('download:future:candles:all:instruments')
            ->setDescription('Download last candles for all instruments')
            ->addOption('instrument', null, InputOption::VALUE_OPTIONAL, 'If set, update only one instrument')
            ->addOption('timeframe', null, InputOption::VALUE_OPTIONAL, 'Timeframe to download')
//            ->addArgument('id', InputArgument::REQUIRED, 'Scrape request id?')
//            ->addArgument('pagesCount', InputArgument::REQUIRED, 'How much list pages to scrape?')
        ;
    }

    /**
     * Execute method of command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
//        $requestId = $input->getArgument('oanda_timeframe');
//        $pagesCount = $input->getArgument('pagesCount');
        //-=-=-=-=-=-=-=-=-=-=-=-=
        try
        {
            $this->run__($input, $output);
        }
        catch (\Exception $e)
        {
            log_exception($e);
        }
    }

    /**
     * @param $input
     * @param $output
     * @throws \App\Common\ValidationException
     */
    protected function run__($input, $output)
    {
        $output->writeln("<info>Command started: " . now() . " ***</info>");
        $instrument = $input->getOption('instrument');
        if ($instrument)
        {
            $voInstrument = vo($instrument, 'instruments');
            $instruments = [$voInstrument->_()];
        }
        else
        {
            $instruments = explode(',', settings('instruments'));
        }

        $timeframes = $input->getOption('timeframe')
            ? [vo($input->getOption('timeframe'), 'oanda.timeframe')->_()]
            : ['M15'];

        $startTime = microtime(true);
        foreach ($instruments as $instrument)
        {
            foreach ($timeframes as $timeframe)
            {
                $output->writeln("<info>Loading candles for $instrument and timeframe $timeframe</info>");

                $res = task(new download__latest__quotes__for__instrument__task,
                    [
                        vo($instrument, 'instruments'),
                        $timeframe
                    ]
                );
            }

        }
        $duration = sprintf("%0.4f", microtime(true) - $startTime);

        $output->writeln("<info>Command finished in $duration seconds</info>");
    }
}
