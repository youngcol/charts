<?php


namespace App\Console\Commands\App;


use App\Models\Quote;
use App\Repositories\QuotesRepository;
use Carbon\Carbon;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DeleteHistoryCandlesForAllInsturumentsCommand extends Command
{
    const HISTORY_DAYS = 7;

    /**
     * Configuration of command
     */
    protected function configure()
    {
        global $container;
        $this->container = $container;

        $this
            ->setName('delete:history:candles:all:instruments')
            ->setDescription('Delete old history candles for all instruments')
        ;
    }

    /**
     * Execute method of command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
//        $requestId = $input->getArgument('id');
//        $pagesCount = $input->getArgument('pagesCount');
        //-=-=-=-=-=-=-=-=-=-=-=-=
        try
        {
            $this->run__($input, $output);
        }
        catch (\Exception $e)
        {
            log_exception($e);
        }
    }

    protected function run__($input, $output)
    {
        $instruments = $input->getOption('instrument') ? [$input->getOption('instrument')] : explode(',', settings('instruments'));
        $timeframes = $input->getOption('timeframe')
            ? [vo($input->getOption('timeframe'), 'oanda.timeframe')->_()]
            : ['M15'];

        $startTime = microtime(true);

        foreach ($instruments as $instrument)
        {
            foreach ($timeframes as $timeframe) {

                $repoQuotes = new QuotesRepository();
                $tableName = Quote::getQuotesTable(vo($instrument), vo($timeframe));
                $repoQuotes->setTable1($tableName);

                $repoQuotes->where('date<', date_mysql_format(Carbon::now()->subDays(self::HISTORY_DAYS)))->delete();
            }
        }

        $duration = sprintf("%0.4f", microtime(true) - $startTime);
        $output->writeln("<info>Command finished in $duration seconds</info>");
    }
}
