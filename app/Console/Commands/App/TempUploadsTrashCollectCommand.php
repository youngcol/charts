<?php
declare(strict_types=1);
namespace App\Console\Commands\App;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use App\Console\Traits\CodeGenerate;

/**
 * TempUploadsTrashCollectCommand
 */
class TempUploadsTrashCollectCommand extends Command
{
    use CodeGenerate;

    /**
     * Configuration of command
     */
    protected function configure()
    {
        $this
            ->setName('temp:uploads:trash:collect')
            ->setDescription('Command temp:uploads:trash:collect')
        ;
    }

    /**
     * Execute method of command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(['<info>Deleting temp uploads:</info>']);

        $now = time();
        $files = scandir(TEMP_UPLOADS_PATH);

        $lifeTime = settings('temp_uploads_lifetime');

        foreach ($files as $file)
        {
            if ($file === '.' or $file === '..')
            {
                continue;
            }

            $pathname = TEMP_UPLOADS_PATH . '/' . $file;
            $fileTime = filemtime($pathname);

            if ($now - $fileTime > $lifeTime)
            {
                unlink($pathname);
                $output->writeln(['Deleted: '. $pathname]);
            }
        }
    }
}
