<?php


namespace App\Console\Commands\App;


use App\Tasks\Oanda\download__instrument__quotes__task;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class DownloadHistoryCandlesForAllInsturumentsCommand extends Command
{
    /**
     * Configuration of command
     */
    protected function configure()
    {
        global $container;
        $this->container = $container;

        $this
            ->setName('download:history:candles:all:instruments')
            ->setDescription('Download last candles for all instruments')
            ->addOption('instrument', null, InputOption::VALUE_OPTIONAL, 'Instrument name to download')
            ->addOption('timeframe', null, InputOption::VALUE_OPTIONAL, 'Timeframe to download')

//            ->addArgument('id', InputArgument::REQUIRED, 'Scrape request id?')
//            ->addArgument('pagesCount', InputArgument::REQUIRED, 'How much list pages to scrape?')
        ;
    }

    /**
     * Execute method of command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
//        $requestId = $input->getArgument('id');
//        $pagesCount = $input->getArgument('pagesCount');
        //-=-=-=-=-=-=-=-=-=-=-=-=
        try
        {
            $this->run__($input, $output);
        }
        catch (\Exception $e)
        {
            log_exception($e);
        }
    }

    protected function run__($input, $output)
    {
        $instruments = $input->getOption('instrument') ? [$input->getOption('instrument')] : explode(',', settings('instruments'));
        $timeframes = $input->getOption('timeframe')
            ? [vo($input->getOption('timeframe'), 'oanda.timeframe')->_()]
            : ['M15'];

        $startTime = microtime(true);

        foreach ($instruments as $instrument)
        {
            foreach ($timeframes as $timeframe) {
                $output->writeln("<info>Loading candles for $instrument and timeframe $timeframe</info>");

                for ($i = 0; $i < 1; $i++) {
                    $res = task(new download__instrument__quotes__task,
                        [
                            vo($instrument, 'instruments'),
                            vo($timeframe, 'oanda.timeframe'),
                            false
                        ]
                    );
                }
            }
        }

        $duration = sprintf("%0.4f", microtime(true) - $startTime);
        $output->writeln("<info>Command finished in $duration seconds</info>");
    }
}
