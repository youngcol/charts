<?php
declare(strict_types=1);
namespace App\Console\Commands\Admin;

use App\Console\Commands\BaseCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use App\Console\Traits\CodeGenerate;

/**
 * Admin/ValidateEnvConsistencyCommand
 */
class ValidateEnvFilesConsistencyCommand extends BaseCommand
{
    use CodeGenerate;

    /**
     * Configuration of command
     */
    protected function configure()
    {
        $this
            ->setName('validate:env:consistency')
            ->setDescription('Command admin:validate:env:consistency')
            //->addOption(
            //    'iterations',
            //    null,
            //    InputOption::VALUE_REQUIRED,
            //    'How many times should the message be printed?',
            //    1
            //);
            //->addArgument('command_class', InputArgument::REQUIRED, 'What command class name?')
        ;
    }

    /**
    * @param InputInterface $input
    * @param OutputInterface $output
    * @return int|void|null
    * @throws Exception
    */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->executeMain($input, $output);
    }

    /**
    * @param InputInterface $input
    * @param OutputInterface $output
    * @throws Exception
    */
    protected function main(InputInterface $input, OutputInterface $output)
    {
//        $output->writeln(['<info>Running command...</info>']);

        $projectEnv = '.env';

        $dotenv = new \Dotenv\Dotenv(PATH_ROOT, $projectEnv);
        $dotenv->load();

        $envNames = $dotenv->getEnvironmentVariableNames();

        $dotenvTemplate = new \Dotenv\Dotenv(PATH_ROOT, '.env.template');
        $dotenvTemplate->load();

        $envTemplateNames = $dotenvTemplate->getEnvironmentVariableNames();


        // template env misses keys
        $diff1 = array_diff($envNames, $envTemplateNames);

        if (count($diff1))
        {
            flow_exception('.env.template misses keys: ' . print_r($diff1, true));
        }


        // main env misses keys
        $diff2 = array_diff($envTemplateNames, $envNames);
        if (count($diff2))
        {
            flow_exception('Current .env misses keys: ' . print_r($diff2, true));
        }

    }
}
