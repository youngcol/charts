<?php

namespace App\Console\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * GenerateDocsCommand
 */
class GenerateDocsCommand extends Command
{
    /**
     * Configuration of command
     */
    protected function configure()
    {
        $this
            ->setName('generate:docs')
            ->setDescription('Generate documentation for api')
            ->setHelp('<info>php partisan generate docs</info>')
        ;
    }

    /**
     * Execute method of command
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     * @throws \Interop\Container\Exception\ContainerException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $apidocPath = APP_PATH . '/apidoc/apidoc.php';
        if (false === file_exists($apidocPath)) {
            throw new \RunTimeException(sprintf('The apidoc file `%s` not found', $apidocPath));
        };

//        $path = APP_PATH;
//        if (!is_writeable($path)) {
//            throw new \RunTimeException(sprintf('The directory `%s` is not writeable', $path));
//        }

        $baseName = PATH_ROOT . 'apidoc.json';
        $content  = require($apidocPath);

        $content['url']       = getenv('API_HOST');
        $content['sampleUrl'] = getenv('API_HOST');

        $content = json_encode($content);
        if (false === file_put_contents($baseName, $content)) {
            throw new \RunTimeException(sprintf('The file `%s` could not be written to', $baseName));
        };

        $exec = 'apidoc -i ./app -o ./docs/apidocs_generated -t ./docs/apidocs_template';
        $output->writeln([exec($exec)]);

        return;
    }
}
