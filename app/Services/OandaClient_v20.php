<?php

namespace App\Services;

use Carbon\Carbon;
use \PHPMailer\PHPMailer\PHPMailer;
use \PHPMailer\PHPMailer\Exception;

class OandaClient_v20
{

    /**
     * Defines the LIVE API url
     *
     * @const URL_LIVE
     */
    const URL_LIVE = 'https://api-fxtrade.oanda.com';

    /**
     * Defines the PRACTICE API url
     *get order book__WORKSHOP
     * @const URL_PRACTICE
     */
    const URL_PRACTICE = 'https://api-fxpractice.oanda.com';

    /**
     * Defines the LIVE API environment
     *
     * @const ENV_LIVE
     */
    const ENV_LIVE = 1;

    /**
     * Defines the PRACTICE API environment
     *
     * @const ENV_PRACTICE
     */
    const ENV_PRACTICE = 2;


    protected $apiKey;


    public function __construct($apiEnvironment, $apiKey)
    {
        $this->apiEnvironment = $apiEnvironment;
        $this->apiKey = $apiKey;

    }

    public function getApiEnvironment()
    {
        return $this->apiEnvironment;
    }

    protected function baseUri()
    {
        return $this->getApiEnvironment() == static::ENV_LIVE ? static::URL_LIVE : static::URL_PRACTICE;
    }

    /**
     * @param $endpoint
     * @param $outputFile
     * @return false|string
     */
    protected function sendRequest(string $endpoint, string $outputFile)
    {
        $key = $this->apiKey;

        $fullpathOutput = TEMP_UPLOADS_PATH . '/oanda_quotes/'. $outputFile . time();

        $curl = "curl \
                  -H \"Content-Type: application/json\" \
                  -H \"Authorization: Bearer $key\" \
                  \"$endpoint\"";
        $curl .= ' > ' . $fullpathOutput;

        $output=[];
        $return_var = [];

        $exec = exec($curl, $output, $return_var);
        $ret = file_get_contents($fullpathOutput);
        unlink($fullpathOutput);

        return $ret;
    }


    /**
     * @param $instrumentName
     * @param array $data
     * @return mixed
     */
    public function getInstrumentCandles($instrumentName, $data=[])
    {
        return $this->makeGetRequest('/v3/instruments/' . $instrumentName . '/candles', $instrumentName, $data);
    }

    /**
     * @param $instrumentName
     * @param Carbon $date
     * @return mixed
     */
    public function getInsrumentOrderBook($instrumentName, Carbon $date)
    {
        $data=[
            'time' => $date->format("Y-m-d\TH:00:00.000\Z")
        ];

        return $this->makeGetRequest('/v3/instruments/' . $instrumentName . '/orderBook', $instrumentName, $data);
    }

    /**
     * @param $endpoint
     * @param $instrumentName
     * @param array $data
     * @param array $headers
     * @return mixed
     */
    protected function makeGetRequest($endpoint, $instrumentName, $data = [], $headers = [])
    {
        $endpoint = $this->absoluteEndpoint($endpoint, $data);

        $outputFile = $instrumentName . '_candles_' . sp($data, 'granularity', 'no_granularity');
        $response = $this->sendRequest($endpoint, $outputFile);

        return json_decode($response, 1);
    }

    protected function absoluteEndpoint($endpoint, $data = [])
    {
        $url = parse_url($endpoint);

        if (isset($url['query'])) {
            parse_str($url['query'], $data);
        }

        return $this->baseUri()
            . '/'
            . trim($url['path'], '/')
            . (!empty($data) ? '?' . http_build_query($data) : '');
    }
}
