<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Tasks\Task;
use App\VO\VoVal;
use Carbon\Carbon;

class generate__signals__on__hlines__breakouts__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        array $values
    )
    {
        $count = 0;
        $breakout_tlines = [];

        $pairs = array_keys($values);
        $periods = array_keys($values[$pairs[0]]);
        
        $resHlines = task(new get__hlines__task,
            [
                $pairs,
                $periods,
                true,
                0
            ]
        );
        

        $breakout_hlines = $this->check_hlines_breakouts(array_flatten($resHlines->list), $values);
        
        return new Res(
            [
                'count' => count($breakout_hlines),
                'breakout_hlines' => $breakout_hlines,
                'generated_signals' => $this->generated_signals
            ]
        );
    }
    
    public function check_hlines_breakouts(array $hlines, array $values)
    {
        $ret = [];

        foreach($hlines as $hline) {
            $periodData = $values[$hline->pair][$hline->period];
            
            $time = Carbon::parse(str_replace(".", "-", $periodData['time']));
            $value = (float)$periodData['val'];
            
            store_dump_var('hline_breokout', [
                $hline,
                        $time,
                        $value,
                        $hline->isBreakOut($value, $time) 
            ]);

            if ($hline->isBreakOut((float)$value) !== NULL) {
                $hline->is_fired = 1;
                $hline->fired_at = $time;
                $hline->fired_value = $value;
                $hline->save();

                $res = task(new create__event__task,
                    [
                        $hline->pair,
                        $hline->period,
                        $hline->type == 'sell' ? 0 : 1,
                        'hline_breakout',
                        'ohlc',
                        $hline
                    ]);

                $this->generated_signals[] = $res->model;
                $ret[] = $hline;
            }         
        }

        return $ret;
    }

}
