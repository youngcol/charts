<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Models\Signal;
use App\Tasks\Task;
use App\VO\VoVal;

class get__signals__list__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        int $history,
        array $signal_names
    )
    {
        $ret = [];
        $r = repo(Signal::class);

        $list = $r->orderBy(['id' => 'desc'])
            ->whereIn('name', $signal_names)
            ->limit($history)
            ->get()
            ->toArray();

        foreach ($list as $signal)
        {
            array_create_key($ret, $signal['instrument']);
            array_create_key($ret[$signal['instrument']], $signal['period']);
            $ret[$signal['instrument']][$signal['period']][] = $signal;
        }

        return new Res([
            'list' => array_slice($list, 0, 40),
            'grouped' => $ret
        ]);
    }
}
