<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Models\User;
use App\Tasks\Task;
use App\VO\VoVal;
use function _\orderBy;

class add__chart__to__chart__group__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    protected function gates
    (


    )
    {



//        test_gate(true, 'add__chart__to__chart__group__task');



        //-=-=-=-=-=-=-=-=-=-=-=-=
        return [resOk([

        ]),


        ] ;
        }

    public function run
    (
        User $user,
        VoVal $chartGroupId,
        VoVal $instrument,
        VoVal $timeframe,
        VoVal $timeframeType,
        VoVal $view
    )
    {
        list(
            $resGates
        ) = $this->gates
            (

            );
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=

        $res = task(new create__chart__task,
            [
                $user,
                $instrument,
                $timeframe,
                $timeframeType,
                $view,
                vo(200, 'chart.scale'),
                vo('ma:1,7;macd')
            ]
        );

        test(!$res->ok(), 'Cant create chart');


        $res->chart->group_id = $chartGroupId->_();
        $res->chart->save();

        return new Res(
            [
                'chart' => $res->chart
            ]
        );
    }

}
