<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Facades\db;
use App\Models\ChartGroup;
use App\Models\User;
use App\Tasks\Task;
use App\VO\VoVal;

class get__popup__timeline__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    protected function gates
    (

    )
    {

        //test_gate(true, 'get__popup__timeline__task');



        //-=-=-=-=-=-=-=-=-=-=-=-=
        return [resOk([

        ]),


        ] ;
        }

    public function run
    (
        User $user,
        VoVal $timelineId
    )
    {
        list(
            $resGates
        ) = $this->gates
            (

            );
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=

        $r = repo(ChartGroup::class);

        $chartGroups = $r
            ->where([
                'user_id' => $user->id,
                'timeline_id' => $timelineId->_()
            ])
            ->whereNotIn('id', [1])
            ->get();

        $now = now_date();

        $popuped = $chartGroups->filter(function ($item) use ($now) {
            if (eN($item->next_popup_time))
            {
                return false;
            }
            return $now->greaterThanOrEqualTo($item->next_popup_time);
        })->unique();

        $waiting = $chartGroups->filter(function ($item) use ($now) {
            return $now->lessThan($item->next_popup_time);
        })->unique();

        $notSet = $chartGroups->filter(function ($item) use ($now) {
            return eN($item->next_popup_time);
        })->unique();

        return new Res(
            [
                'popuped' => $popuped,
                'waiting' => $waiting,
                'not_set' => $notSet,
            ]
        );
    }
}
