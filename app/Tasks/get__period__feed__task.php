<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Tasks\Task;
use App\VO\VoVal;
use App\Tasks\Charts\get__trendlines__task;
use App\Tasks\get__ohlc__timeseries__task;

class get__period__feed__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        array $symbols,
        int $period,
        int $history
    )
    {
//        test_gate(true, 'get__period__feed__task');
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=

        $ohlc = task(new get__ohlc__timeseries__task, [
            $symbols,
            $period,
            $history
        ]);
        
        $resTLines = task(new get__trendlines__task,
            [
                $symbols,
                ['ohlc', 'rsx'],
                false,
                false
            ]
        );

        return new Res(
            [
                'symbols' => array_chunk($symbols, 2),
                'ohlc' => $ohlc,
                'tlines' => $resTLines
            ]
        );
    }

}
