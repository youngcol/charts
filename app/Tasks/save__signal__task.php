<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Facades\db;
use Illuminate\Support\Carbon;

class save__signal__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        int $event_id,
        Carbon $time,
        string $signal_name,
        string $pair,
        int $period,
        int $direction,
        float $price,
        array $params = []
    )
    {
        $values = implode(",", [
            "'" . normalize_time($time, $period) . "'",
            "'" . $signal_name . "'",
            "'" . $pair . "'",
            $period,
            $price,
            $direction,
            $event_id,
            "'" . json_encode($params) . "'"
        ]);

        $s = "insert into `signals` (`time`, `name`, `instrument`, `period`, `price`, `direction`, `event_id`, `params`)
                        values($values)
                            ON DUPLICATE KEY UPDATE `time`=VALUES(`time`), `name`=VALUES(`name`), `instrument`=VALUES(instrument), `period`=VALUES(`period`), `price`=VALUES(price), `event_id`=VALUES(event_id), `period`=VALUES(period), `params`=VALUES(params);";
        $ins = DB::statement($s);

        return new Res(
            [
                'ins' => $ins
            ]
        );
    }
}
