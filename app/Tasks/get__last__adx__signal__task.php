<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Models\UserInfo;
use App\Tasks\Task;
use App\VO\VoVal;

class get__last__adx__signal__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (

    )
    {
//        test_gate(true, 'get__last__adx__signal__task');
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=
        $r = repo(UserInfo::class);
        $info = $r->find(1);

        $res = null;

        if ($info) {
            $res = json_decode($info->data);
        }

        return $res;
    }

}
