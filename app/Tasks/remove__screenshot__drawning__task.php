<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Tasks\Task;
use App\VO\VoVal;

class remove__screenshot__drawning__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        string $pic_view,
        string $instrument,
        int $period
    )
    {
        $del_paths = [
            drawning_path('', $instrument, $period),
        ];

        foreach($del_paths as $path) {
            if($path && file_exists($path)) {
                unlink($path);
            }
        }

        return new Res(
            [
                'res' => 1
            ]
        );
    }

}
