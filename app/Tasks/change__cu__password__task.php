<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 * Current user changes his own password
 *
 */

use App\Common\Res;
use App\Models\User;
use App\Tasks\Task;
use App\VO\VoVal;

class change__cu__password__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        User $user,
        VoVal $oldPass,
        VoVal $newPass
    )
    {
        list(
            $resGates
            ) = $this->gates
        (
            $user,
            $oldPass
        );
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=


        $user->password_reset_code = '';
        $user->setPassword($newPass->_());
        $upd = $user->save();


        return new Res([
            'upd' => $upd
        ]);
    }


    private function gates(
        User $user,
        VoVal $oldPass
    )
    {

        test_gate(!$user->checkPassword($oldPass->_()), 'Old password incorrect');

    }
}
