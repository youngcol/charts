<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Models\Chart;
use App\Models\User;
use App\Tasks\Task;
use App\VO\VoVal;

class create__chart__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        User $user,
        VoVal $instrument,
        VoVal $timeframe,
        VoVal $timeframeType,
        VoVal $view,
        VoVal $scale,
        ?VoVal $indicators
    )
    {
        list(
            $resGates
        ) = $this->gates
        (

        );
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=

        $m = new Chart();
        $m->user_id = $user->id;
        $m->timeframe = $timeframe->_();
        $m->timeframe_type = $timeframeType->_();
        $m->instrument = $instrument->_();
        $m->view = $view->_();
        $m->scale = $scale->_();

        if ($indicators)
        {
            $m->indicators = $indicators;
        }

        $m->save();

        return new Res([
            'chart' => $m
        ]);
    }

    /**
     * @return array
     * @throws \App\Common\GateException
     */
    protected function gates
    (

    )
    {

//        test_gate(true, 'create__chart__task');

        return [resOk()] ;
    }
}
