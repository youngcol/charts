<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Models\ChartGroup;
use App\Models\Quotes;
use App\Models\User;
use App\Tasks\Task;
use App\VO\VoVal;

class load__chart__group__data__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        User $user,
        VoVal $groupId,
        ?VoVal $timeframe
    )
    {
        list(
            $resGates,
            $chartGroup
        ) = $this->gates
            (
                $user,
                $groupId
            );
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=

        $charts = [];

        foreach ($chartGroup->charts as $chart)
        {

            $resChartTimeseries = task(new build__chart__timeseries__task,
                [
                    $user,
                    $chart,
                    null
                ]
            );

            $charts[] = [
                'chart'                 => $chart,
                'resChartTimeseries'    => $resChartTimeseries
            ];
        }

        $ret = $chartGroup->toArray();
        $ret['charts'] = $charts;

        return new Res($ret);
    }

    /**
     * @param User $user
     * @param VoVal $groupId
     * @return array
     * @throws \App\Common\GateException
     */
    protected function gates
    (
        User $user,
        VoVal $groupId
    )
    {
        $chartGroup = repo(ChartGroup::class)
            ->where('user_id', $user->id)
            ->where('id', $groupId->_())
            ->with('charts', 'charts.levels', 'charts.trendlines')
            ->first();

        test_gate(eN($chartGroup), 'Wrong chart group id');

        return [resOk([
            'chartGroup' => '',
        ]),
            $chartGroup,

        ];
    }
}
