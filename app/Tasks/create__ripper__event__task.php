<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Tasks\Task;
use App\VO\VoVal;
use App\Models\RipperEvent;

class create__ripper__event__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        string $event,
        array $params
    )
    {
        $r = repo(RipperEvent::class);
        $m = $r->insert([
            'event' => $event,
            'params' => $params
        ]);
            
        return new Res(
            [
                'res' => $m
            ]
        );
    }

}
