<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Facades\Redis2;
use App\Tasks\Task;
use App\VO\VoVal;

class save__chart__trading__params__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        string $pair,
        int $period,
        int $sl,
        int $be,
        int $risk
    )
    {
//        test_gate(true, 'save__chart__trading__params__task');
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=

        $data = Redis2::get('chart_trading_settings');
        $data[$pair][$period] = [
            'sl' => $sl,
            'be' => $be,
            'risk' => $risk,
        ];
        $set = Redis2::set('chart_trading_settings', $data);

        return new Res(
            [
                'set' => $set,
                'updated' => $data[$pair][$period]
            ]
        );
    }

}
