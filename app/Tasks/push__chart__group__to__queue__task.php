<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Models\ChartGroup;
use App\Models\Queue;
use App\Models\User;
use App\Tasks\Task;
use App\VO\VoVal;
use Carbon\Carbon;


class push__chart__group__to__queue__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        User $user,
        VoVal $groupId,
        VoVal $minutesDeltaFromNow
    )
    {
        list(
            $resGates,
            $chartGroup
        ) = $this->gates
        (
            $user,
            $groupId
        );
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=

        $popupDate = Carbon::now()->addMinutes($minutesDeltaFromNow->toInt());

        $chartGroup->next_popup_time = date_mysql_format($popupDate);
        $chartGroup->save();

        return new Res([
            'chartGroup' => $chartGroup
        ]);
    }

    /**
     * @param User $user
     * @param VoVal $groupId
     * @return array
     * @throws \App\Common\GateException
     */
    protected function gates
    (
        User $user,
        VoVal $groupId
    )
    {

        $chartGroup = repo(ChartGroup::class)
            ->where('user_id', $user->id)
            ->find($groupId->toInt());

        test_gate(eN($chartGroup), 'Cant find chart group');


        //-=-=-=-=-=-=-=-=-=-=-=-=
        return [resOk([

        ]),
            $chartGroup

        ] ;
    }
}
