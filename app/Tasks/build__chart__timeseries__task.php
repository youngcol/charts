<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Models\Chart;
use App\Models\Quote;
use App\Models\Quotes;
use App\Models\User;
use App\Tasks\Task;
use App\VO\VoVal;
use Carbon\Carbon;

class build__chart__timeseries__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    protected function gates
    (

    )
    {

//        test_gate(true, 'build__chart__quotes__view__task');

        return [resOk(
            [

            ]
        )];
    }

    public function run
    (
        User $user,
        Chart $chart,
        Carbon $fromDate = null
    )
    {
        list(
            $resGates
        ) = $this->gates
            (

            );
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=


//        $m1Timeseria = $this->buildMinutesTimeseria($chart, vo('M1', 'oanda.timeframes.in_use'), $fromDate);
        $m15Timeseria = $this->buildMinutesTimeseria($chart, vo('M15', 'oanda.timeframes.in_use'), $fromDate);
//        $secondsTimeseria = $this->buildSecondsTimeseria($chart, $fromDate);

        $lastCandles = [
//            'M1' => $m1Timeseria->last(),
            'M15' => $m15Timeseria->last(),
//            'S10' => $secondsTimeseria->last(),
        ];

        $minDates = [];
        foreach (['M15'] as $key)
        {
            if ($lastCandles[$key])
            {
                $minDates[] = $lastCandles[$key]->date->timestamp;
            }
        }

        $min_last_candles_timestamp = count($minDates) ? min($minDates) : null;

        return new Res(
            [
                'chart' => $chart,
                'timeframe' => $chart->timeframe,
                'timeframe_type' => $chart->timeframe_type,
                'view'          => $chart->view,
                'scale'      => $chart->scale,
                'timeseries' => [
//                    'M1' => array_values($m1Timeseria->toArray()),
                    'M15' => array_values($m15Timeseria->toArray()),
//                    'S10' => array_values($secondsTimeseria->toArray()),
                ],
                "last_candles" => $lastCandles,
                "min_last_candles_timestamp" => $min_last_candles_timestamp,
            ]
        );
    }

    protected function timeseriaToArray()
    {

    }

    /**
     * @param Chart $chart
     * @return array
     * @throws \App\Common\ValidationException
     */
    protected function buildSecondsTimeseria(Chart $chart, Carbon $fromDate = null)
    {
        $tableName = Quote::getQuotesTable(vo($chart->instrument), vo('S10', 'oanda.timeframes.in_use'));
        $quotesRepo = repo(Quote::class);
        $quotesRepo->setTable1($tableName);
        $ret = [];

        if (eN($fromDate))
        {
            $quotes = $quotesRepo
                ->orderBy(['date' => 'desc'])
                ->page(0, 1000)
                ->get()
                ->reverse()
            ;
        }
        else
        {
            $quotes = $quotesRepo
                ->orderBy(['date' => 'asc'])
                ->where('date>=', $fromDate)
                ->get();
        }

        return $quotes;
    }

    /**
     * @param Chart $chart
     * @param VoVal $oandaTimeframe
     * @param Carbon|null $fromDate
     * @return array
     * @throws \App\Common\ValidationException
     */
    protected function buildMinutesTimeseria(Chart $chart, VoVal $oandaTimeframe, Carbon $fromDate = null)
    {
        $ret = [];
        $count = $chart->scale;

        $tableName = Quote::getQuotesTable(vo($chart->instrument), $oandaTimeframe);
        $quotesRepo = repo(Quote::class);
        $quotesRepo->setTable1($tableName);

        if (eN($fromDate))
        {
            $quotes = $quotesRepo
                ->orderBy(['date' => 'desc'])
                ->page(0, $count)
                ->get()
                ->reverse()
            ;
        }
        else
        {
            $quotes = $quotesRepo
                ->orderBy(['date' => 'desc'])
                ->where('date>=',  $fromDate)
                ->get()
                ->reverse();
        }

        return $quotes;
    }
}
