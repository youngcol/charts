<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Facades\Redis2;
use App\Tasks\Task;
use App\VO\VoVal;

class rsi__change__tracking__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        array $last_values
    )
    {
//        test_gate(true, 'rsi__change__tracking__task');
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=
        $change_points = Redis2::get('indicator_change_points');

        foreach ($change_points as $pair => &$datum)
        {
            foreach (settings("charts.big_periods") as $period)
            {
                $data = &$datum[$period];

                $last_val = $last_values[$pair][$period]['val'];
                if (!isset($data['value']))
                {
                    $data['value'] = (float)$last_val;
                } else {
                    $delta = $data['value'] - $last_val;
                    if (abs($delta) >= 1)
                    {
                        $data['value'] = (float)$last_val;
                        push_to_queue('rsi_change_queue', [
                            'pair' => $pair,
                            'direction' => $delta < 0 ? -1 : 1
                        ]);
                    }
                    $data['delta'] = $delta;
                }
            }

        }

        Redis2::set('indicator_change_points', $change_points);

        return new Res(
            [
                'res' => 1
            ]
        );
    }

}
