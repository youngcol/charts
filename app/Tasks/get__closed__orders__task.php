<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */
use App\Facades\Redis2;
use App\Common\Res;
use App\Tasks\Task;
use App\VO\VoVal;
use App\Tasks\ForexCore\put__command__to__the__sheet__task;

class get__closed__orders__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (

    )
    {
        $res = task(new put__command__to__the__sheet__task,
            [
                vo("GET_CLOSED_ORDERS", "forex.commands"),
                [
                    'history' => 10
                ]
            ]
        );
        $orders = Redis2::get("closed_orders", 1);

        return new Res(
            [
                'orders' => $orders
            ]
        );
    }

}
