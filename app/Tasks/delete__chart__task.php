<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Models\Chart;
use App\Models\User;
use App\Tasks\Task;
use App\VO\VoVal;

class delete__chart__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    protected function gates
    (

    )
    {


//        test_gate(true, 'delete__chart__task');



        //-=-=-=-=-=-=-=-=-=-=-=-=
        return [resOk([

        ]),


        ] ;
        }

    public function run
    (
        User $user,
        VoVal $id
    )
    {
        list(
            $resGates
        ) = $this->gates
            (

            );
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=


        $del = repo(Chart::class)
            ->where('user_id', $user->id)
            ->delete([$id->toInt()]);


        return new Res(
            [
                'del' => $del
            ]
        );
    }

}
