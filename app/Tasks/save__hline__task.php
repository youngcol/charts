<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Tasks\Task;
use App\VO\VoVal;
use App\Models\ChartLevel;

class save__hline__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        ?int $id,
        string $pair,
        ?string $type,
        int $period,
        float $value,
        int $is_trading,
        array $params
    )
    {
        $r = repo(ChartLevel::class);
        $ret = $r->updateOrCreate([
            'id' => $id,
                ], [
                    'params' => count(array_keys($params)) == 0 ? null : $params,
                    'value' => $value,
                    'type' => $type,
                    'is_trading' => $is_trading,
                    'period' => $period,
                    'pair' => $pair,
                    'is_fired' => 0
                ]);


        return new Res(
            [
                'hline' => $ret
            ]
        );
    }

}
