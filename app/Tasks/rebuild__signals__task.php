<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Facades\db;
use App\Common\Res;
use App\Models\Event;
use App\Models\Signal;
use App\Tasks\Task;
use App\VO\VoVal;

class rebuild__signals__task extends Task
{
    const PER_PAGE = 100;

    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (

    )
    {
        $page = 0;
        DB::statement('TRUNCATE TABLE `signals`');
        $r = repo(Event::class);

        do{
            $events = $r
                ->page($page++, self::PER_PAGE)
                ->orderBy(['id' => 'asc'])
                ->get();

            $events->each(function ($e) {
                task(new save__signal__task, [
                    $e->id,
                    $e->created_at,
                    $e->tag,
                    $e->instrument,
                    (int) $e->period,
                    $e->direction,
                    $e->price
                ]);
            });

        } while($events->count() >= self::PER_PAGE);

        return new Res(
            [
                'res' => 1
            ]
        );
    }

}
