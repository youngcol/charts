<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Models\Chart;
use App\Models\User;
use App\Tasks\Task;
use App\VO\VoVal;

class update__chart__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        User $user,
        VoVal $chartId,
        VoVal $view,
        VoVal $timeframe,
        VoVal $scale,
        VoVal $indicators,
        VoVal $timeframeType
    )
    {
        list(
            $resGates,
            $chart
        ) = $this->gates
        (
            $user,
            $chartId
        );
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=

        if ($view->isFilled())
        {
            $chart->view = $view->_();
        }

        if ($timeframe->isFilled())
        {
            $chart->timeframe = $timeframe->_();
        }

        if ($scale->isFilled())
        {
            $chart->scale = $scale->_();
        }

        if ($indicators->isFilled())
        {
            $chart->indicators = $indicators->_();
        }

        if ($timeframeType->isFilled())
        {
            $chart->timeframe_type = $timeframeType->_();
        }

        $saved = $chart->save();

        return new Res([
            'chart' => $chart,
            'saved' => $saved
        ]);
    }

    /**
     * @param User $user
     * @param VoVal $chartId
     * @return array
     * @throws \App\Common\GateException
     */
    protected function gates
    (
        User $user,
        VoVal $chartId
    )
    {

        $chart = repo(Chart::class)
            ->where('user_id', $user->id)
            ->find($chartId->toInt());

        test_gate(eN($chart), 'Cant find chart');

        return [resOk([
            'sdfds' => 'asdsa'
        ]), $chart] ;
    }
}
