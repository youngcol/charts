<?php
declare(strict_types=1);
namespace App\Tasks\Binary;

/**
 *
 *
 */

use App\Common\Res;
use App\Tasks\Task;
use App\VO\VoVal;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class create__binary__seria__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        Carbon $lookupDate,
        Collection $quotes,
        int $delta,
        string $period,
        int $maxSeria = 10
    )
    {
        vo($period, 'binary.interval');

//        test_gate(true, 'create__binary__seria__task');
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=
//        dump($quotes);echo 'create__binary__seria__task.php:36'; exit;
        $dates = [];
        $seria = [];
        $date = $lookupDate->copy();
        $lastQuote = $quotes->last();

        for ($i=0;$i<$maxSeria;)
        {
            $this->get_next_date($date, $delta, $period);
            $quote = sp($quotes, date_mysql_format($date));

            if ($date->isBefore($lastQuote->date)) {
                break;
            }

            if ($quote) {
                $isNothing = $quote->isNothingCandle();
                $isUp = bool2int($quote->isUpCandle());

                $symbol = $isNothing ? '_' : ($isUp ? '+' : '-');

                $lastDate = implode(' ', [date_mysql_format($date), $symbol, $quote->getHighLowDelta()]);
                $dates[] = $lastDate;
                $seria[] = $symbol;

                $i++;
            }
        }

        return new Res(
            [
                'strategy' => $delta . ' ' . $period,
                'lookup_date' => date_mysql_format($lookupDate),
                'seria' => $seria,
                'seria_string' => implode('', $seria),
                'dates' => $dates,
                'last_date' => $lastDate
            ]
        );
    }

    /**
     * @param Carbon $date
     * @param int $delta
     * @param string $period
     * @return Carbon
     */
    protected function get_next_date(Carbon $date, int $delta, string $period)
    {
        if ($period === 'minutes')
        {
            $date->subMinutes($delta);
        }
        elseif ($period === 'hours')
        {
            $date->subHours($delta);
        }
        elseif ($period === 'days')
        {
            $date->subDays($delta);
        }

        return $date;
    }

}
