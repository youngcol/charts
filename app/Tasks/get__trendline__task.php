<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Facades\Redis2;
use App\Tasks\Task;
use App\VO\VoVal;
use App\Models\ChartTrendline;

class get__trendline__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        string $uniq_id,
        string $pair,
        int $period
    )
    {
        $objects = Redis2::get(chart_objects_key($pair));

        $ids = array_map(function ($i) {
            return $i["uniq_id"];
        }, $objects[$period]);

        $object_index = array_search($uniq_id, $ids);
        $ret = null;

        if($object_index !== false)
        {
            $ret = $objects[$period][$object_index];
        }

        return new Res(
            [
                    'res' => $ret
            ]
        );
    }
}
