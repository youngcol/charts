<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Facades\Redis2;
use App\Tasks\Task;
use App\VO\VoVal;
use App\Models\ChartTrendline;
use Carbon\Carbon;

class move__tline__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        string $pair,
        int $period,
        string $uniq_id,
        string $start_date,
        string $end_date,
        float $startValue,
        float $endValue
    )
    {
        $objects = Redis2::get(chart_objects_key($pair));

        $ids = array_map(function ($i) {
            return $i["uniq_id"];
        }, $objects[$period]);

        $data = [
            'start_value' => $startValue,
            'end_value' => $endValue,
            'start_date' => $start_date,
            'end_date' => $end_date,

            // mt4 values
            'start_time' => Carbon::createFromFormat('Y-m-d H:i:s', $start_date, "UTC")->timestamp,
            'end_time' => Carbon::createFromFormat('Y-m-d H:i:s', $end_date, "UTC")->timestamp,
            'price' => $startValue,
            'price2' => $endValue,

            //'is_fired' => 0,
            //'fired_value' => null,
            //'fired_at' => null,
            'tline_fired_value' => null,
        ];

        $object_index = array_search($uniq_id, $ids);

        if($object_index !== false)
        {
            if ($objects[$period][$object_index]['is_trading'] == 0)
            {
                $data['is_fired'] = 0;
                $data['fired_value'] = null;
                $data['fired_at'] = null;
            }

            $objects[$period][$object_index] = array_merge($objects[$period][$object_index], $data);
            unset($objects[$period][$object_index]['calc']);
        }
        Redis2::set(chart_objects_key($pair), $objects);

        return new Res(
            [
                'tline' => $objects[$period][$object_index]
            ]
        );
    }

}
