<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Facades\Redis2;
use App\Models\Ohlc;
use App\Tasks\Task;
use App\VO\VoVal;

class build__ohlc__candles__pivots__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        array $last_candels
    )
    {
        $pairs = array_keys($last_candels);
        $four_hours_time = $last_candels[$pairs[0]][240]['time'];
        $pivots = Redis2::get(ohlc_pivots_key(), true, [
            "time" => '',
            'list' => []
        ]);
        $is_do = $pivots['time'] != $four_hours_time;
        $periods = [240, 1440];

        if ($is_do)
        {
            $pivots['time'] = $four_hours_time;
            $r = repo(Ohlc::class);
            foreach ($periods as $period)
            {
                array_create_key($pivots['list'], $period);

                $candels = $r
                    ->whereIn('instrument', $pairs)
                    ->where('period', $period)
                    ->where('time<', $four_hours_time)
                    ->limit(count($pairs))
                    ->orderBy(['time' => 'desc'])
                    ->get()
                    ->toArray();

                foreach ($candels as $candel)
                {
                    $delta = abs($candel['o'] - $candel['c']) / 2;
                    $pivot = $candel['o'] >= $candel['c'] ? $candel['o'] + $delta : $candel['o'] - $delta;
                    $pivots['list'][$period][$candel['instrument']] = $pivot;
                }

            }
        }


        Redis2::set(ohlc_pivots_key(), $pivots);

        return new Res(
            [
                'res' => 1
            ]
        );
    }

}
