<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Models\Timeseria;
use App\Tasks\Task;
use App\VO\VoVal;

class calc__tline__pivot__point__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        array $tline
    )
    {
//        test_gate(true, 'calc__tline__pivot__point__task');
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=

        if ($tline['is_zone'])
        {
            task(new add__indicator__pivot__point__task, [
                $tline['pair'],
                (int)$tline['period'],
                (float)$tline['start_value'],
                $tline['trading_type'] === 'sell' ? 0 : 1
            ]);
        }
        else
        {
            $times = array_keys($tline['calc']);
            $start_time = $times[0];
            $end_time = last($times);
            // get series
            $r = repo(Timeseria::class);
            $timeseria = $r
                ->where('time>=', mt4_date__mysql_date($start_time))
                ->where('time<=', mt4_date__mysql_date($end_time))
                ->where('period', (int)$tline['period'])
                ->where('instrument', $tline['pair'])
                ->orderBy(['time' => 'asc'])
                ->get();

            $timeseria_calc = [];
            foreach ($timeseria as $item)
            {
                $timeseria_calc[date_mt4_date($item['time'])] = $item['value'];
            }

//            store_dump_var('calc__tline__pivot__point__task', [
//                $timeseria_calc,
//                $tline['calc']
//            ]);

            if ($tline['trading_type'] == 'sell')
            {
                $pivot = $this->calc_sell_tline($timeseria_calc, $tline['calc']);
            }
            else
            {
                $pivot = $this->calc_buy_tline($timeseria_calc, $tline['calc']);
            }

            if ($pivot !== null)
            {
                task(new add__indicator__pivot__point__task, [
                    $tline['pair'],
                    (int)$tline['period'],
                    (float)$pivot,
                    $tline['trading_type'] === 'sell' ? 0 : 1
                ]);
            }
        }

        return new Res(
            [
                'res' => 1
            ]
        );
    }

    //        "calc" => array:7 [
//        "2023.12.18 21:00:00" => 0
//            "2023.12.19 01:00:00" => 0
//            "2023.12.19 05:00:00" => 0
//            "2023.12.19 09:00:00" => 0
//            "2023.12.19 13:00:00" => 0
//            "2023.12.19 17:00:00" => 0
//            "2023.12.19 21:00:00" => 0
//          ]

    protected function calc_sell_tline(array $timeseria_calc, array $calc)
    {
        $ret = null;

        foreach ($timeseria_calc as $time => $value)
        {
            if($calc[$time] >= $value)
            {
                $ret = $calc[$time];
                break;
            }
        }

        return $ret;
    }

    protected function calc_buy_tline(array $timeseria_calc, array $calc)
    {
        $ret = null;

        foreach ($timeseria_calc as $time => $value)
        {
            if($calc[$time] <= $value)
            {
                $ret = $calc[$time];
                break;
            }
        }
        return $ret;
    }
}
