<?php
declare(strict_types=1);
namespace App\Tasks;

use App\Common\Res;
use App\Models\Chart;
use App\Models\User;
use App\Tasks\Oanda\download__instrument__quotes__task;
use App\Tasks\Task;
use App\VO\VoVal;
use Carbon\Carbon;

class get__latest__timeseria__for__charts__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    protected function gates
    (

    )
    {


//        date_mysql_format($fromDate)
//
//        $chart = repo(Chart::class)
//            ->where('user_id', $user->id)
//            ->find($chartId->_());
//
//        test(eN($chart), 'Cant find chart');
//        test_gate(true, 'get__latest__timeseria__for__charts__task');

        //-=-=-=-=-=-=-=-=-=-=-=-=
        return [resOk([

            ]),
//                $chart
//
            ];
        }

    public function run
    (
        User $user,
        VoVal $chartGroupIds,
        VoVal $fromDate
    )
    {
        list(
            $resGates
        ) = $this->gates
            (

            );
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=

        $charts = repo(Chart::class)->whereIn('group_id', $chartGroupIds->_())->get();
        $dateFrom = Carbon::createFromFormat("Y-m-d H:i:s", $fromDate->_());

        $resTimeseries = [];
        $lastCandlesTimestamps = [];

        foreach ($charts as $chart)
        {
            $resTimeseria = task(new build__chart__timeseries__task,
                [
                    $user,
                    $chart,
                    $dateFrom
                ]
            );

            array_create_key($resTimeseries, $chart->group_id, []);
            $resTimeseries[$chart->group_id][] = $resTimeseria;

            if (!eN($resTimeseria->min_last_candles_timestamp))
            {
                $lastCandlesTimestamps[] = $resTimeseria->min_last_candles_timestamp;
            }
        }

        $minLatestLoadedTimeseriaDate = count($lastCandlesTimestamps)
                                            ? Carbon::createFromTimestamp(min($lastCandlesTimestamps))
                                            : null;

        return new Res(
            [
                'chartGroupIds'             => $chartGroupIds->_(),
                'resChartGroupTimeseries'    => $resTimeseries,
                'minLatestLoadedTimeseriaDate' => date_mysql_format($minLatestLoadedTimeseriaDate)
            ]
        );
    }

}
