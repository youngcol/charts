<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Tasks\Task;
use App\VO\VoVal;
use App\Tasks\get__ohlc__timeseries__task;


class get__pair__overview__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        string $pair,
        int $period
    )
    {
        //test_gate(true, 'get__pair__overview__task');
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=
        $ret = [];
        $periods = [1440,240,60,15,5,1];
        $screenshots = [];
        $tag = "ripper_pics";
        $pic_percents = 50;

        foreach($periods as $period)
        {

            $res = task(new get__ohlc__timeseries__task,
                [
                    [$pair],
                    $period,
                    settings('charts.big_history')
                ]
            );

            $ret[$period] = $res;

            cropScreenshot($pair, $tag, $period, $pic_percents, 100);
            //removeOldScreenshots('screenshots', $pair, $tag, $period);
            $pic = getTagScreenshot('screenshots_crops', $pair, $tag, $period, $pic_percents);
            removeOldScreenshots('screenshots_crops', $pair, $tag, $period, "$pic_percents");


            $screenshots[$period] = $pic;


//            $pic['drawning'] = [
//                    'url' => drawning_url($pair, $period, 90)
//                ];
        }



        return new Res(
            [
                'screenshots' => $screenshots,
                'res' => $ret,
                'period' => $period
            ]
        );
    }

}
