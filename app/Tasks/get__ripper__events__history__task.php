<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Tasks\Task;
use App\VO\VoVal;
use App\Models\RipperEvent;

class get__ripper__events__history__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        int $fromId,
        int $limit
    )
    {
        $r = repo(RipperEvent::class);
        $list = $r
            ->where('id>', $fromId)
            ->orderBy(['id' => 'desc'])
            ->limit($limit)
            ->get()
            ->toArray()
        ;
        
        return new Res(
            [
                'list' => $list
            ]
        );
    }

}
