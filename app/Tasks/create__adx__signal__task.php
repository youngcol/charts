<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Models\AdxSignal;
use App\Models\SignalsHistory;
use App\Tasks\Task;
use App\VO\VoVal;

class create__adx__signal__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        string $pair,
        int $period,
        string $signal
    )
    {
//        test_gate(true, 'create__adx__signal__task');
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=

        $resEvent = task(new create__event__task,
            [
                $pair,
                $period,
                $signal == "buy" ? 1 : 0,
                'adx',
            ]);

        $text = symbol_localization($pair);
        $text .= ' adx ' . $period . ($signal == "buy" ? " бай " : " селл ");
        push_to_voice_queue($text);

//        $r = repo(AdxSignal::class);
//
//        $found = $r->where([
//            'instrument=' => $pair,
//            'period=' => $period,
//            'direction=' => $directon,
//        ])->get()->count();
//
//        $res = $r->updateOrCreate([
//            'instrument' => $pair,
//            'period' => $period,
//        ], [
//            'instrument' => $pair,
//            'period' => $period,
//            'direction' => $directon,
//        ]);

        return new Res(
            [
                'event_id' => $resEvent->model->id,
//                'updated' => $found == 0
            ]
        );
    }

}
