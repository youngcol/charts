<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Models\Event;
use App\Tasks\Task;
use App\VO\VoVal;
use Carbon\Carbon;

class get__big__charts__marks__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        string $pair
    )
    {
        $marks = [];

        $r = repo(Event::class);
        $events = $r->where('instrument', $pair)
            ->whereIn('tag', ['tlb', 'nh', 'nl'])
            ->whereNotIn('period', [1])

            ->orderBy(['id' => 'desc'])
            ->limit(30)
            ->get();

        foreach ($events as $event)
        {
            $price_range = $this->get_price_range($pair, $event->price);
            $created_at = date_mysql_format($event->created_at);

            $marks[$event->id] = [
                'id' => $event->id,
                'created_at' => $created_at,
                'start_time' => Carbon::createFromFormat('Y-m-d H:i:s', $created_at, "UTC")->addMinutes(-5)->timestamp,
                'end_time' => Carbon::createFromFormat('Y-m-d H:i:s', $created_at, "UTC")->addMinutes(5)->timestamp,
                'start_price' => $price_range[0],
                'end_price' => $price_range[1],
                'price' => $event->price,
                'direction' => $event->direction,
                'tag' => $event->tag,
                'period' => $event->period
            ];
        }

        return new Res(
            [
                'marks_ids' => array_keys($marks),
                'marks' => $marks
            ]
        );
    }

    protected function get_price_range(string $pair, float $price)
    {
        $price1 =  $price;
        $price2 =  $price;

        switch ($pair)
        {
            case "XAUUSD":
                $price1 -= 0.1;
                $price2 += 0.1;
            break;

            case "USDCAD":
            case "USDCHF":
            case "NZDUSD":
            case "GBPUSD":
                $price1 -= 0.0001;
                $price2 += 0.0001;
                break;
            case "USDJPY":
            case "GBPJPY":
            case "EURJPY":
                $price1 -= 0.01;
                $price2 += 0.01;
                break;
        }

        return [$price1, $price2];
    }

}
