<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Facades\Redis2;
use App\Tasks\ForexCore\Trading\get__open__orders__task;
use App\Tasks\Task;
use App\VO\VoVal;

class get__game__frontend__events__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (

    )
    {
        $prices = [];
        $last_values = Redis2::get(timeseria_last_values_key('ohlc'), 1);
        foreach ($last_values as $symbol => $datum)
        {
            $prices[$symbol] = sprintf("%0.4f", $datum[1]['val']);
        }

        $ret = [
            'voice_notify' => Redis2::lPop('voice_queue'),
            'updated_time' => Redis2::get("timeseries_updated_time", false),
            'prices' => $prices,
            'open_orders' => task(new get__open__orders__task, []),
        ];

        return new Res($ret);
    }

}
