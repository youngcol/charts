<?php
declare (strict_types = 1);
namespace App\Tasks\ForexCore;

/**
 *
 *
 */

use App\Common\Res;
use App\Models\Forex\ForexCommand;
use App\Tasks\ForexCore\CommandHandlers\get__history__timeseria__command__handler__task;
use App\Tasks\ForexCore\CommandHandlers\status__command__handler__task;
use App\Tasks\Task;
use App\Facades\Redis2;
use App\Models\ChartTrendline;
use App\Tasks\update__screener__state__task;
use App\Tasks\create__event__task;

class process__command__result__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        int    $command_id,
        string $command_name,
        array  $result
    )
    {
        //test_gate(true, 'process__command__result__task');
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=

        store_dump_var("$command_name command handler", $result);

        switch ($command_name) {
            case "GET_HISTORY_TIMESERIA":

                break;

            case "STATUS":
                task(new status__command__handler__task, [
                    $result
                ]);
                break;

            case "OPEN_MARKET_ORDER":

                break;
            case "CLOSE_ORDER":

                break;
            case "CLOSE_OPEN_ORDERS":

                break;
            case "SET_ORDER_BREAK_EVEN":

                break;
            case "DEL_ORDER_BREAK_EVEN":

                break;
            case "CHANGE_TAKE_PROFIT":

                break;

            case "GET_SCREENSHOTS":

                break;
            case "GET_OPEN_ORDERS":
                task(new get__open__orders__command__handler__task, [
                    $result
                ]);
                break;
            case "GET_CLOSED_ORDERS":
                $this->process_GET_CLOSED_ORDERS($result);
                break;

            case "CALC_TLINE_VALUES":
                $this->process_CALC_TLINE_VALUES($result);
                break;

            case "OPEN_SYMBOL":
                break;

            case "GET_SWINGS_STATE":
                $this->process_GET_SWINGS_STATE($result);

                try {

                } catch (Exception $e) {
                    store_dump_var("process_GET_SWINGS_STATE", [
                        $e,
                        $result,
                    ]);
                }

                break;

            default:
                crit('No such command ' . $command_name);
        }

        $r = repo(ForexCommand::class);
        $r->where('id', $command_id)->update(['deleted_at' => now_local()]);

        return new Res(
            [
                'res' => 1,
            ]
        );
    }

    function process_GET_CLOSED_ORDERS(array $result)
    {
//        $orders = array_to_hash($result['orders'], "ticket_number");
//        $now_tickets = array_keys($orders);
//
//        $prev = Redis2::get("closed_orders");
//        $prev = array_to_hash($prev, "ticket_number");
//        $prev_tickets = array_keys($prev);
//
//        $fresh = array_diff($now_tickets, $prev_tickets);
//        foreach ($fresh as $ticket_number)
//        {
//            $closed = $orders[$ticket_number];
//            $event_type = "";
//
//            if ($closed['ticket_profit_points'] < 0)
//            {
//                $event_type = "order_closed_by_sl";
//            }
//            elseif($closed['ticket_profit_points'] < 10)
//            {
//                $event_type = "order_closed_by_be";
//            }
//            else
//            {
//                $event_type = "order_closed_by_tp";
//            }
//
//            $res = task(new create__event__task,
//                [
//                    substr($closed['symbol'],0,6),
//                    0,
//                    $closed['type'] == 'sell' ? 0 : 1,
//                    $event_type,
//                    'ohlc',
//                    null
//                ]);
//        }

        Redis2::set("closed_orders", $result['orders']);
    }

    function process_CALC_TLINE_VALUES(array $result)
    {
        $objects = Redis2::get(chart_objects_key($result['symbol']));
        $ids = array_map(function ($i) {
            return $i["uniq_id"];
        }, $objects[$result['period']]);
        $object_index = array_search($result['uniq_id'], $ids);

        if($object_index !== false)
        {
            $objects[$result['period']][$object_index]['calc'] = $result['calc'][0];
        }
        Redis2::set(chart_objects_key($result['symbol']), $objects);
    }

    function process_GET_SWINGS_STATE(array $result)
    {
        $swings_prev = Redis2::get("swings_state");

        store_dump_var('process_GET_SWINGS_STATE', [
            $result['swings20'][0],
            $result['ema_swings'][0]
        ]);

        $this->update_screener_state($result['swings20'][0],  'swing20');
        $this->update_screener_state($result['ema_swings'][0], 'ema_swing');

        $set = Redis2::set("swings_state", $result);
    }

    function update_screener_state(array $state, string $tag)
    {

        foreach($state as $pair => $periods)
        {
            $periods = array_pop($periods);
            foreach($periods as $period => $val)
            {
                $res = task(new update__screener__state__task,
                [
                    $pair,
                    $period,
                    $tag,
                    $val,
                    'init'
                ]);
            }
        }
    }

}
