<?php
declare(strict_types=1);
namespace App\Tasks\ForexCore;

/**
 *
 *
 */

use App\Common\Res;
use App\Facades\Redis2;
use App\Tasks\Task;
use App\VO\VoVal;

class get__open__orders__command__handler__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        array $result
    )
    {
//        test_gate(true, 'get__open__orders__command__handler__task');
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=

        $set = Redis2::set("open_orders", $result);

        return new Res(
            [
                'res' => $set
            ]
        );
    }

}
