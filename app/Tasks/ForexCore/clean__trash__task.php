<?php
declare(strict_types=1);
namespace App\Tasks\ForexCore;

/**
 *
 *
 */

use App\Common\Res;
use App\Models\ChartTrendline;
use App\Models\Timeseria;
use App\Tasks\Task;
use App\VO\VoVal;

class clean__trash__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (

    )
    {
//        test_gate(true, 'clean__trash__task');
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=
        $pairs = array_flatten(settings('pairs_list'));
        $periods = settings('periods');

        foreach ($periods as $period) {
            foreach ($pairs as $pair) {
                $res = $this->remove_old_timeseria($pair, $period);
                if ($res->last)
                {
                    $_trendlines = $this->remove_old_trendlines($res->last);
                }
            }
        }

        return new Res(
            [
                'pairs' => $pairs,
                'periods' => $periods
            ]
        );
    }

    public function remove_old_trendlines(Timeseria $last)
    {
        $del = ChartTrendline::where('uniq_id', "{$last->instrument}{$last->period}")
            ->where('end_date', '<', $last->time)
            ->delete();

        return ok(['deleted' => $del]);
    }

    public function remove_old_timeseria(string $pair, int $period)
    {
        // find last row for each period and pair
        $r = repo(Timeseria::class);

        $list = $r->where('instrument', $pair)
            ->where('period', $period)
            ->orderBy(["id" => "desc"])
            ->limit(100)
            ->get();

        $last = $list->last();
        $deleted = 0;

        if ($list->count())
        {
            $deleted = Timeseria::where('time', '<', $last->time)
                ->where('period', $period)
                ->where('instrument', $pair)
                ->delete();
        }

        return resOk(['last'=> $last, 'deleted' => $deleted]);
    }
}
