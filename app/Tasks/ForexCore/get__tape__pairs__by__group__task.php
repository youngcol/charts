<?php
declare(strict_types=1);
namespace App\Tasks\ForexCore;

/**
 *
 *
 */

use App\Common\Res;
use App\Tasks\Screenshots\get__tape__pairs__task;
use App\Tasks\Task;
use App\VO\VoVal;

class get__tape__pairs__by__group__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        string $group
    )
    {
        $res = task(new get__pairs__by__group__task, [
            $group
        ]);

        $res = task(new get__tape__pairs__task, [
            $res->pairs,
            settings('screenshots.periods')
        ]);

        return $res;
    }
}
