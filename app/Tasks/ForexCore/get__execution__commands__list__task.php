<?php
declare (strict_types = 1);
namespace App\Tasks\ForexCore;

/**
 *
 *
 */

use App\Common\Res;
use App\Models\Forex\ForexCommand;
use App\Tasks\Task;

class get__execution__commands__list__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (

    ) {
        $r = repo(ForexCommand::class);
        $list = $r->whereNull('deleted_at')->get()->toArray();

        return new Res(
            [
                'list' => $list,
                'count' => count($list)
            ]
        );
    }

}
