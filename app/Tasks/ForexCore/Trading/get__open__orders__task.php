<?php
declare(strict_types=1);
namespace App\Tasks\ForexCore\Trading;

use App\Common\Res;
use App\Facades\Redis2;
use App\Tasks\Task;
use App\VO\VoVal;

class get__open__orders__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (

    )
    {
//        test_gate(true, 'get__open__orders__task');
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=
        $open_orders = Redis2::get("open_orders", 1);

//        if (settings('app_debug_data'))
//        {
//            $open_orders['free_margin'] = 2834.71;
//            $open_orders['balance'] = 7604.6;
//            $open_orders['equity'] = 5307.32;
//            $open_orders['margin'] = 2472;
//
//            $open_orders['orders'][] = $open_orders['orders'][0];
//            $open_orders['orders'][0]['type'] = 'sell';
//            $open_orders['orders'][1]['type'] = 'buy';
//            $open_orders['orders'][1]['profit'] = 23;
//
//            $open_orders['orders'][0]['ext_param__be'] = 1;
//            $open_orders['orders'][1]['ext_param__be'] = 0;
//        }

        $summary = [];
        if (isset($open_orders['summary'])) {
            $summary = $open_orders['summary'][0];

            $summary['balance_percent_change'] = (float) sprintf("%0.2f", ($summary['equity'] - $summary['balance']) * 100 / $summary['balance']);
            $summary['balance_change'] = (float) sprintf("%0.2f", $summary['equity'] - $summary['balance']);
            $summary['locked_percent'] = (float) sprintf("%0.2f", ($summary['margin'] * 100 / $summary['balance']));
            $summary['free_percents'] = (float) sprintf("%0.2f", ($summary['free_margin'] * 100 / $summary['balance']));
        }

        $orders = [];
        $orders_list = [];
        $symbols_summary = [];

        if (isset($open_orders['orders']) && is_array($open_orders['orders']))
        {
            foreach ($open_orders['orders'] as &$order)
            {
                $order['symbol'] = substr($order['symbol'], 0, 6);
                array_create_key($orders, $order['symbol']);
                $orders[$order['symbol']][] = $order;
                $orders_list[] = $order;
            }

            foreach ($orders as $symbol => &$datum)
            {
                $profit = array_reduce($datum, function ($carry, $order) {
                    return $carry + $order['profit'];
                });
                $symbols_summary[$symbol] = (float) sprintf("%0.2f", (($summary['balance'] + $profit) - $summary['balance']) * 100 / $summary['balance']);
            }
        }

        return new Res(
            [
                'summary' => $summary,
                'symbols_summary' => $symbols_summary,
                'orders' => $orders,
                'orders_list' => $orders_list
            ]
        );
    }

}
