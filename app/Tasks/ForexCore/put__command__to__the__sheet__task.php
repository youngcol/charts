<?php
declare(strict_types=1);
namespace App\Tasks\ForexCore;

/**
 *
 *
 */

use App\Common\Res;
use App\Models\Forex\ForexCommand;
use App\Tasks\Task;
use App\VO\VoVal;

class put__command__to__the__sheet__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        VoVal $name,
        array $params
    )
    {
        $uniq_id = $this->comand_name__id($name->_());
        $r = repo(ForexCommand::class);
        $skip_check = [12, 1, 13, 3];
        $m = null;
        $id = 0;

        if (!in_array($uniq_id, $skip_check))
        {
            $m = $r->where('uniq_id', $uniq_id)->whereNull('deleted_at')->first();
        }

        foreach (settings('accounts') as $account)
        {
            $params['account'] = $account;

            if(!$m)
            {
                $c = ForexCommand::create($name->_(), $uniq_id, $params);
                $c->save();
                log_command_on_sheet($c);
                $id = $c->id;
            }
        }

        return new Res(
            [
                'id' => $id,
            ]
        );
    }

    private function comand_name__id($name)
    {
        $list = settings('forex.commands');
        if (!isset($list[$name])) {
            crit('No such command '. $name);
        }
        return $list[$name];
    }
}
