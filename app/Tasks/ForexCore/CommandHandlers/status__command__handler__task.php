<?php
declare(strict_types=1);
namespace App\Tasks\ForexCore\CommandHandlers;

/**
 *
 *
 */

use App\Common\Res;
use App\Tasks\Task;
use App\VO\VoVal;

class status__command__handler__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        array $result
    )
    {
//        test_gate(true, 'status__command__handler__task');
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=

        store_dump_var('STATUS command', $result);

        return new Res(
            [
                'res' => ''
            ]
        );
    }

}
