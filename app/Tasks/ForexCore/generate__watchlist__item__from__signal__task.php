<?php
declare(strict_types=1);
namespace App\Tasks\ForexCore;

/**
 *
 *
 */

use App\Common\Res;
use App\Models\SignalsHistory;
use App\Models\Watchlist;
use App\Tasks\Task;


class generate__watchlist__item__from__signal__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        array $signals
    )
    {
//        test_gate(true, 'generate__watchlist__item__from__signal__task');
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=
        foreach ($signals as $signal) {
            /** @var SignalsHistory $signal*/
            $w = new Watchlist();
            $w->instrument = $signal->instrument;
            $w->period = $signal->period;
            $w->direction = $signal->direction;
            $w->save();
        }

        return new Res(
            [
                'res' => 1
            ]
        );
    }

}
