<?php
declare(strict_types=1);
namespace App\Tasks\ForexCore;

/**
 *
 *
 */

use App\Common\Res;
use App\Facades\Redis2;
use App\Facades\Timer;
use App\Tasks\get__chart__objects__task;
use App\Tasks\Task;
use App\VO\VoVal;
use Carbon\Carbon;
use App\Tasks\get__screener__state__task;

class get__status__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (

    )
    {
        //test_gate(true, 'get__status__task');
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=
        //$ripper_status = load_dump_var('STATUS command');

        $tlines = [
            "GBPJPY" => Redis2::get(chart_objects_key("GBPJPY")),
            "GBPUSD" => Redis2::get(chart_objects_key("GBPUSD")),
            "XAUUSD" => Redis2::get(chart_objects_key("XAUUSD")),
        ];
        $hlines = [
            "GBPJPY" => Redis2::get(chart_hlines_key("GBPJPY")),
            "GBPUSD" => Redis2::get(chart_hlines_key("GBPUSD")),
            "XAUUSD" => Redis2::get(chart_hlines_key("XAUUSD")),
        ];

        $last_values = Redis2::get(timeseria_last_values_key('ohlc'));
        $chart_trading_settings = Redis2::get('chart_trading_settings');
        $screener_GJ = Redis2::get(instrument_screener__state_key('GBPJPY'));
        $prices_movements = Redis2::get('prices_movements');
        $indicator_pivot_points = Redis2::get('indicator_pivot_points');
        $indicator_change_points = Redis2::get('indicator_change_points');
        $rsi_last_values = Redis2::get('rsi_last_values');

        $closed_orders = Redis2::get('closed_orders');

        $web_status = [
            'voice_notify' => Redis2::lPop('voice_queue'),
//            'screenshots' => $screenshots,
//            'qqe_seed_ok' => $qqe_seed_ok,
//            'qqe_crosses_ok' => false,
//            'ohlc_seed_ok' => false,
            'carbon now time' => Carbon::now(settings('timezone'))->format('Y-m-d H:i:s'),
            'now_local' => now_local(),
            //'open_orders' => Redis2::get("open_orders", 1),
            //'closed_orders' => Redis2::get("closed_orders", 1),

            '-------------' => '--------------',
            //'chart_trading_settings' => $chart_trading_settings,
            'prices_movements' => $prices_movements,
            'screener_GJ' => $screener_GJ,
            '$indicator_pivot_points' => $indicator_pivot_points,
            '$indicator_change_points' => $indicator_change_points,
            '$rsi_last_values' => $rsi_last_values,
            'tlines' => $tlines,
            'hlines' => $hlines,
            'closed_orders' => $closed_orders,
            'timeseries_updated_time' => Redis2::get("timeseries_updated_time", false),

            //'last_values' => $last_values
            //'screener' => task(new get__screener__state__task,[])->get('state'),
        ];

        return new Res(
            [
                'web_status' => $web_status,
                'ripper_status' => [],
                'web_status_dump' => grab_dump_output($web_status),
                'ripper_status_dump' => grab_dump_output([]),
            ]
        );
    }

}
