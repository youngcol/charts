<?php
declare(strict_types=1);
namespace App\Tasks\ForexCore;

/**
 *
 *
 */

use App\Common\Res;
use App\Facades\Redis2;
use App\Models\ChartTrendline;
use App\Tasks\Task;
use App\VO\VoVal;

class save__trendline__params__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        string $uniq_id,
        string $pair,
        int $period,
        array $params
    )
    {
        $objects = Redis2::get(chart_objects_key($pair));

        $ids = array_map(function ($i) {
            return $i["uniq_id"];
        }, $objects[$period]);
        $object_index = array_search($uniq_id, $ids);

        if($object_index !== false)
        {
            $objects[$period][$object_index]['is_fired'] = 0;
            $objects[$period][$object_index]['is_trading'] = 1;
            $objects[$period][$object_index]['params'] = $params;
        }

        Redis2::set(chart_objects_key($pair), $objects);

        return new Res(
            [
                'tline' => $objects[$period][$object_index]
            ]
        );
    }
}
