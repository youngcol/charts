<?php
declare(strict_types=1);
namespace App\Tasks\ForexCore;

/**
 *
 *
 */

use App\Common\Res;
use App\Facades\Redis2;
use App\Tasks\Task;
use App\VO\VoVal;
use function Symfony\Component\String\u;

class get__pairs__by__group__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        string $group
    )
    {
        switch($group)
        {
            case "push": return $this->get_push_group();
            case "in_work": return $this->get_in_work_group();
            case "signals": return $this->get_signals_group();
        }
    }

    protected function get_push_group()
    {
        $last_values = Redis2::get("ohlc_timeseries__last_values", 1);
        // get last dates for series

        if ( $last_values)
        {
            $periods = settings('screenshots.periods');
            foreach ($last_values as $pair => $pairData) {
                foreach ($pairData as $period => $datum) {
                    if (in_array($period, $periods))
                    {
                        dump($datum['time']);echo "get__pairs__by__group__task.php:50";exit;

                    }
                }
            }
        }

        // find pairs that dont have trendlines running


        return new Res(
            [
                'pairs' => array_flatten(settings('pairs_list'))
            ]
        );
    }

    protected function get_in_work_group()
    {
        $push_res = $this->get_push_group();
        $pairs = array_diff(array_flatten(settings('pairs_list')), $push_res->pairs);

        return new Res(
            [
                'pairs' => $pairs
            ]
        );
    }

    protected function get_signals_group()
    {
        return new Res(
            [
                'pairs' => ["USDCAD", "GBPJPY", "EURJPY"]
            ]
        );
    }

}
