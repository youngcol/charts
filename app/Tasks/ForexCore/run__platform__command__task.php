<?php
declare(strict_types=1);
namespace App\Tasks\ForexCore;

use App\Common\Res;
use App\Facades\db;
use App\Facades\Redis2;
use App\Models\Event;
use App\Models\Timeseria;
use App\Tasks\create__event__task;
use App\Tasks\generate__signals__on__trendlines__breakouts__task;
use App\Tasks\init__chart__objects__redis__structure__task;
use App\Tasks\rebuild__signals__task;
use App\Tasks\Task;
use App\Tasks\init__screener__state__task;

class run__platform__command__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        string $command,
        array $params = null
    )
    {
//        test_gate(true, 'run__platform__command__task');
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=
        $res = false;

        switch($command)
        {
            case "CREATE_SIGNAL":
            {
                $resEvent = task(new create__event__task,
                    [
                        $params['pair'],
                        $params['period'],
                        $params['direction'],
                        'indicator_renko',
                        'indicator'
                    ]);
//                dump($resEvent);echo"run__platform__command__task.php:45";exit;
                break;
            }
            case "REBUILD_SIGNALS":
            {
                $res = task(new rebuild__signals__task, []);
                $res = [
                    "res" => $res
                ];

                break;
            }

            case "INIT_SCREENER_STATE":
            {
                task(new init__screener__state__task, []);
                task(new init__chart__objects__redis__structure__task, []);
                repo(Event::class)->delete();


                $res = [
                    "ok" => 1
                ];

                break;
            }
            case "drop_timeseries":
            {

                db::statement("TRUNCATE `ohlc`");
                db::statement("TRUNCATE `timeseries`");
//                db::statement("TRUNCATE `chart_trendlines`");
//                db::statement("TRUNCATE `events`");


                Redis2::set(ohlc_pivots_key(), [
                    "time" => '',
                    'list' => []
                ]);

                $res = [
                    "ok" => 1
                ];
                break;
            }
            case "beep_offline":
            {
                push_to_beep_queue("XAUUSD", 5, 0, 5);


                break;
            }

            case "timeseria_tick":
            {
                $trendlines = $this->timeseria_tick();

                $res = [
                    "trendlines" => $trendlines
                ];
                break;
            }
        }

        return new Res(
            [
                'res' => $res
            ]
        );
    }

    protected function timeseria_tick()
    {
        $r = repo(Timeseria::class);
        $last_values = [];

        $symbols = ["XAUUSD", "GBPUSD", "GBPJPY"];
        $periods = [1, 15,60,240,1440];

        foreach ($symbols as $symbol) {
            $last_values[$symbol] = [];

            foreach ($periods as $period) {
                $period_value= $r->where('instrument', $symbol)
                    ->where('tag', 'qqe')
                    ->where(['period' => $period])
                    ->orderBy(['id' => 'desc'])
                    ->limit(1)
                    ->get()->toArray();

                $period_value = array_pop($period_value);

                $last_values[$symbol][$period] = [
                    'time' => $period_value['time'],
                    'val' => $period_value['value']
                ];
            }
        }

        //dump($last_values);echo "run__platform__command__task.php:74";exit;
        return task(new generate__signals__on__trendlines__breakouts__task,
            [
                $last_values
            ]
        );
    }

}
