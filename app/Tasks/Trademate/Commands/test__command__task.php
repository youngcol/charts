<?php
declare(strict_types=1);
namespace App\Tasks\Trademate\Commands;

/**
 *
 *
 */

use App\Common\Res;
use App\Tasks\Task;
use App\VO\VoVal;

class test__command__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (

    )
    {
        test_gate(true, 'test__command__task');
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=



        return new Res(
            [
                'res' => 1
            ]
        );
    }

}
