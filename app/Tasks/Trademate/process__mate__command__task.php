<?php
declare(strict_types=1);
namespace App\Tasks\Trademate;

/**
 *
 *
 */

use App\Common\Res;
use App\Tasks\Task;
use App\Tasks\Trademate\Commands\test__command__task;
use App\VO\VoVal;

class process__mate__command__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        string $cmd,
        array $params
    )
    {
//        test_gate(true, 'process__mate__command__task');
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=

        switch ($cmd)
        {
            case "тест": return task(new test__command__task, $params);

        }


        return new Res(
            [
                'res' => 1
            ]
        );
    }

}
