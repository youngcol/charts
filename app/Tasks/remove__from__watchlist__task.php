<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Models\Watchlist;
use App\Tasks\Task;
use App\VO\VoVal;

class remove__from__watchlist__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        int $watchlistId
    )
    {
        //test_gate(true, 'remove__from__watchlist__task');
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=

        $r = repo(Watchlist::class);
        $ret = $r->delete([$watchlistId]);


        return new Res(
            [
                'res' => $ret
            ]
        );
    }

}
