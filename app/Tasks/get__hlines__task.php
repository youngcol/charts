<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Tasks\Task;
use App\VO\VoVal;
use App\Models\ChartLevel;


class get__hlines__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        array $pairs,
        $period, // array, int
        bool $is_only_trading=false,
        ?int $is_fired = null
    )
    {
        $ret = [];
        $found = [];

        foreach($pairs as $pair)
        {
            array_create_key($ret, $pair);
        }

        $r = repo(ChartLevel::class);
        $req = $r
            ->whereIn('pair',  $pairs)

            ->orderBy(['id' => 'desc']);

        if($is_fired !== null){
            $req->where('is_fired',  $is_fired);
        }

        if(is_array($period)) {
            $req->whereIn('period', $period);
        } else {
            $req->where('period', $period);
        }

        if($is_only_trading) {
            $r->where('is_trading', 1);
        }
        $list = $req->get();
        
        foreach($list as $tline)
        {
            array_create_key($found, $tline['pair']);
            $found[$tline['pair']][] = $tline['period'];
            $ret[$tline['pair']][] = $tline;
        }

        foreach($found as $pair => &$periods)
        {
            $periods = array_unique($periods);
        }
        
        return new Res(
            [
                'found' => $found,
                'pairs' => $pairs,
                'list' => $ret
            ]
        );
    }

}
