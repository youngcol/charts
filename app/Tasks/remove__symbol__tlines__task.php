<?php
declare(strict_types=1);
namespace App\Tasks;


use App\Common\Res;
use App\Facades\Redis2;
use App\Tasks\Task;
use App\VO\VoVal;
use App\Models\ChartTrendline;

class remove__symbol__tlines__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        string $pair,
        int $period,
        string $chart_tag
    )
    {
        $objects = Redis2::get(chart_objects_key($pair));
        $objects[$period] = [];
        Redis2::set(chart_objects_key($pair), $objects);

        return new Res(
            [
                'res' => 1
            ]
        );
    }

}
