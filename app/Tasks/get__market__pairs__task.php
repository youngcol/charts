<?php
declare(strict_types=1);
namespace App\Tasks;



use App\Common\Res;
use App\Facades\Redis2;
use App\Tasks\Task;
use App\VO\VoVal;
use App\Models\Ohlc;
use App\Tasks\Charts\get__trendlines__task;
use App\Facades\OhlcFeed;
use Carbon\Carbon;

class get__market__pairs__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        array $pairs,
        array $periods,
        int $is_only_pics
    )
    {
        $tlines = [];

        $ohlc_req = [];
        foreach($pairs as $pair)
        {
            $ohlc_req[$pair] = $periods;
            $tlines[$pair] = Redis2::get(chart_objects_key($pair));
        }

        $trading_settings = Redis2::get('chart_trading_settings');
        return new Res(
            [
                'now_date' => now_date(),
                'trading_settings' => $trading_settings,
                'market' => (new OhlcFeed($ohlc_req, $tlines, settings('indicator.tag'), (bool)$is_only_pics))->get(),
                'instruments' => array_chunk($pairs, 4),
                'instruments_all' => $pairs,
                'periods' => settings("charts.big_periods")
            ]
        );
    }
}
