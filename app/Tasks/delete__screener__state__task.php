<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Tasks\Task;
use App\VO\VoVal;
use App\Facades\Redis2;

class delete__screener__state__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        string $instrument,
        int $period,
        string $state_tag
    )
    {

        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=
        $screener = Redis2::get(instrument_screener__state_key($instrument));

        $screener[$period]['list'][$state_tag]['active'] = 0;

        Redis2::set(instrument_screener__state_key($instrument), $screener);
        return new Res(
            [
                'res' => 1
            ]
        );
    }

}
