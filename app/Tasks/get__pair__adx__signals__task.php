<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Models\AdxSignal;
use App\Tasks\Task;
use App\VO\VoVal;

class get__pair__adx__signals__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        string $pair
    )
    {
//        test_gate(true, 'get__pair__adx__signals__task');
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=

        $r = repo(AdxSignal::class);
        $adx_signals = $r
            ->where('instrument', $pair)
            ->get()->toArray();

        $res = [];
        foreach ($adx_signals as $adx_signal) {
            $res[$adx_signal['period']] = $adx_signal;
        }

        return new Res(
            [
                'res' => $res
            ]
        );
    }

}
