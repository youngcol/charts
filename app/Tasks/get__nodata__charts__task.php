<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Tasks\Task;
use App\VO\VoVal;

class get__nodata__charts__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (

    )
    {
        //test_gate(true, 'get__nodata__charts__task');
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=



        return new Res(
            [
                'res' => 1
            ]
        );
    }

}
