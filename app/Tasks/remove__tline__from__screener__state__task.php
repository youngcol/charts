<?php
declare(strict_types=1);
namespace App\Tasks;


use App\Common\Res;
use App\Tasks\Task;
use App\VO\VoVal;
use App\Models\ChartTrendline;
use App\Facades\Redis2;


class remove__tline__from__screener__state__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        ChartTrendline $tline
    )
    {
        $screener = Redis2::get(instrument_screener__state_key($tline->pair));

        if($screener[$tline->period]['list']['tlb']['tline_id'] == $tline->id)
        {
            $screener[$tline->period]['list']['tlb']['active'] = 0;
        }
         
        if($screener[$tline->period]['list']['hzone']['tline_id'] == $tline->id)
        {
            $screener[$tline->period]['list']['hzone']['active'] = 0;
        }
        
        Redis2::set(instrument_screener__state_key($tline->pair), $screener);

        return new Res(
            [
                'res' => 1
            ]
        );
    }

}
