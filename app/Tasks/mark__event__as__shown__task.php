<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Models\Event;
use App\Tasks\Task;
use App\VO\VoVal;

class mark__event__as__shown__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        int $id
    )
    {
//        test_gate(true, 'mark__event__as__shown__task');
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=

        $r = repo(Event::class);
        $e = $r->find($id);

        $r->where('instrument', $e->instrument)
            ->where('is_shown', 0)
            ->update(['is_shown' => 1]);


        return new Res(
            [
                'res' => 1
            ]
        );
    }

}
