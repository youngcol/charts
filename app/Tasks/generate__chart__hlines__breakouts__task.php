<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Facades\Redis2;
use App\Tasks\Task;
use App\VO\VoVal;
use Carbon\Carbon;

class generate__chart__hlines__breakouts__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        array $last_values
    )
    {
//        test_gate(true, 'generate__chart__hlines__breakouts__task');
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=
        $breakouts = [];

        foreach ($last_values as $pair => $last_value)
        {
            $objects = Redis2::get(chart_hlines_key($pair));

            foreach ($last_value as $period => $datum)
            {
                if (!is_array($objects[$period])) continue;

                foreach ($objects[$period] as &$hline)
                {
                    $is_breakout = false;
                    if($hline['is_fired']) continue;
                    if(!$hline['is_trading']) continue;
                    $datumTime = Carbon::parse(str_replace('.', '-', $datum['time']));
                    $endDate = Carbon::parse(str_replace('.', '-', $hline['end_time_str']));
                    store_dump_var('generate__chart__hlines__breakouts__task', [
                        date('r'),
                        $datumTime,
                        $endDate
                    ]);

                    if (!$endDate->gte($datumTime))
                    {
                        continue;
                    }

                    if($hline['trading_type'] == 'sell')
                    {
                        if($datum['val'] < $hline['price2'])
                        {
                            $is_breakout = true;
                        }
                    } else {
                        if($datum['val'] > $hline['price2'])
                        {
                            $is_breakout = true;
                        }
                    }

                    if($is_breakout)
                    {
                        $hline['is_fired'] = 1;
                        $hline['fired_time'] = $datum['time'];
                        $hline['fired_value'] = $datum['val'];
                        $hline['fired_calc_value'] = $hline['price2'];
                        $breakouts[] = $hline;
                    }
                }
            }

            $res = Redis2::set(chart_hlines_key($pair), $objects);
        }


        return new Res(
            [
                'hlines' => $breakouts
            ]
        );
    }

}
