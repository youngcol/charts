<?php

namespace App\Tasks;

use Slim\Container;

class Task
{
    /**
     * @var Container
     */
    protected $c;

    public function __construct()
    {
        global $container;
        $this->c = $container;
    }

}
