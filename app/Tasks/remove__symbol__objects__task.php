<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Facades\Redis2;
use App\Tasks\Task;
use App\VO\VoVal;

class remove__symbol__objects__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        string $symbol
    )
    {
//        test_gate(true, 'remove__symbol__objects__task');
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=
        $objects = [];
        foreach(settings('charts.all_periods') as $period)
        {
            $objects[$period] = [];
        }

        Redis2::set(chart_hlines_key($symbol), $objects);

        return new Res(
            [
                'res' => 1
            ]
        );
    }

}
