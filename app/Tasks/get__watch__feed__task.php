<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Tasks\Task;
use App\VO\VoVal;
use App\Tasks\get__tlines__task;
use App\Tasks\Charts\get__trendlines__task;
use App\Tasks\get__timeseria__task;
use App\Facades\OhlcFeed;

class get__watch__feed__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (

    )
    {
        $ret = [];
        $pairs = [];

        // trendlines
        $resTlines = task(new get__trendlines__task,
            [
                array_flatten(settings('pairs_list')),
                ['ohlc', settings('indicator.tag')],
                true,
                true
            ]
        );

        foreach($resTlines->found as $pair => &$periods)
        {
            $periods = settings('charts.big_periods');

//            if(in_array($periods[0], settings('charts.big_periods2')))
//            {
//
//            }
//            else if(in_array($periods[0], settings('charts.small_periods')))
//            {
//                $periods = settings('charts.small_periods');
//            }
        }

        return new Res(
            [
                'data' => (new OhlcFeed($resTlines->found, $resTlines, settings('indicator.tag'), false))->get(),
                'found_pairs' => array_keys($resTlines->found),
                'found' => $resTlines->found
            ]
        );
    }

}
