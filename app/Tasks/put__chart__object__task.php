<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Facades\Redis2;
use App\Tasks\Task;
use App\VO\VoVal;

class put__chart__object__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        string $symbol,
        int $period,
        string $object_type,
        string $object_name,
        int $start_time,
        int $end_time,
        string $start_time_str,
        string $end_time_str,
        float $price,
        float $price2,
        string $type,
        array $calc = NULL,
        string $chart_tag,
        int $force_risk,
        int $force_sl
    )
    {
        $symbol = substr($symbol, 0, 6);
        if($object_type == 'tline')
        {
            $objects = Redis2::get(chart_objects_key($symbol));
        } else {
            $objects = Redis2::get(chart_hlines_key($symbol));
        }

        $names = array_map(function ($i) {
            return $i["object_name"];
        }, $objects[$period]);

        $object_index = array_search($object_name, $names);

        $data = [
            'pair' => $symbol,
            'period' => $period,
            'object_name' => $object_name,
            'uniq_id' => $object_name,
            'chart_tag' => $chart_tag,

            'start_value' => $price,
            'end_value' => $price2,
            'start_date' => mt4_date__mysql_date($start_time_str),
            'end_date' => mt4_date__mysql_date($end_time_str),

            // mt4 values
            'start_time_str' => $start_time_str,
            'end_time_str' => $end_time_str,
            'start_time' => $start_time,
            'end_time' => $end_time,
            'price' => $price,
            'price2' => $price2,

            'trading_type' => $type,
            'is_zone' => 0,
            'is_fired' => 0,
            'fired_at' => 0,
            'fired_value' => 0,
            'is_trading' => 0,
//            'is_trading' => $is_trading,
//            'params' => $trading_params,
            'calc' => $calc[0],
            'points' => 0
        ];

        //            $trading_params = [];
//            if ($is_trading) {
//                $trading_params = trading_params($symbol, $period, $type, $object_name);
//            }

//        if ($object_type == 'tline')
//        {
//            if (in_array($period, [1]))
//            {
//                $data['params'] = trading_params($symbol, $period, $type, $object_name, $force_risk, $force_sl);
//                $data['is_trading'] = 1;
//            }
//        }

        $data['_sl'] = $data['params']['sl'];
        $data['_risk'] = $data['params']['risk'];

//        if ($object_type == 'hline')
//        {
//            $data['_sl'] =
//        }


        if($object_index !== false)
        {
            $objects[$period][$object_index] = array_merge($objects[$period][$object_index], $data);
            $object = $objects[$period][$object_index];
        } else { // new object
            $objects[$period][] = $data;
            $object = $data;
        }

        if($object_type == 'tline')
        {
            $res = Redis2::set(chart_objects_key($symbol), $objects);
        } else {
            $res = Redis2::set(chart_hlines_key($symbol), $objects);
        }

        return new Res(
            [
                'res' => $res,
                'object' => $object,
            ]
        );
    }
}
