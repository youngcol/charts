<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Tasks\Task;
use App\VO\VoVal;
use App\Models\Ohlc;
use Carbon\Carbon;
use App\Tasks\Charts\get__trendlines__task;

class get__ohlc__timeseries__task extends Task
{
    const FIVE_MIN_MIN_MAX_PIPS = 30;

    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        array $pairs,
        int $period,
        int $history,
        bool $is_single_item = false
    )
    {
        //test_gate(true, 'get__ohlc__timeseries__task');
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=

        $r = repo(Ohlc::class);
        $series = [];

        $seria = $r
            ->whereIn('instrument', $pairs)
            ->where('period', $period)
            ->limit($history * count($pairs))
            ->orderBy(['time' => 'desc'])
            ->get()
            ->toArray();

        foreach($seria as &$item)
        {
            $item['value'] = $item['c'];
            array_create_key($ret, $item['instrument']);
            $series[$item['instrument']][] = $item;
        }
        foreach($series as $pair => &$seria) {
            $series[$pair] = array_reverse($series[$pair]);
        }

        $empty_bars = $this->build_empty_bars($series, settings('charts.empty_bars.'.$period));

        return new Res(
            [
                'period' => $period,
                'seria' => $is_single_item ? array_pop($series) : $series,
                'empty_bars' => $is_single_item ? array_pop($empty_bars) : $empty_bars,
            ]
        );
    }

    /**
     * @param array $series
     * @param int $count
     * @return array
     */
    protected function build_empty_bars(array $series, int $count)
    {
        $ret = [];

        foreach($series as $pair => $candles) {
            $last = last($candles);
            array_create_key($ret, $pair);

            $time = Carbon::parse($last['time']);

            for($i=0;$i<$count;$i++) {

                $bar = $last;
                $time = $time->addMinutes($bar['period']);
                $bar['time'] = $time->format('Y-m-d H:i:s');

                $bar['o'] = $bar['c'];
                $bar['h'] = $bar['c'];
                $bar['l'] = $bar['c'];

                $ret[$pair][] = $bar;
            }


            // insert min max values
            $high = (double)max(array_column($candles, 'h'));
            $low = (double)min(array_column($candles, 'l'));

            $last = last($ret[$pair]);
            if ($last)
            {
                $pips = $bar['period'] == 5 ? self::FIVE_MIN_MIN_MAX_PIPS : self::FIVE_MIN_MIN_MAX_PIPS / 2;
                $min_max_bars = $high ? $this->build_min_max_bars($pair, $last, $bar['period'], $high, $low,  $pips) : [];
                $ret[$pair] = array_merge($ret[$pair], $min_max_bars);
            }
        }

        return $ret;
    }

    protected function build_min_max_bars(string $pair, array $last, int $period, float $high, float $low, int $pips)
    {
        $ret = [];
        $parts = explode(".", (string)$high);
        $time = Carbon::parse($last['time']);

        $divider = 100;
        if(isset($parts[1]))
        {
            switch(strlen($parts[1]))
            {
                case 2: $divider = 100; break;
                case 3: $divider = 100;break;
                case 4:
                    case 5: $divider = 10000;break;
            };

            $time = $time->addMinutes($period);
            $last['h'] = $high + $pips / $divider;
            $last['l'] = $last['h'];
            $last['o'] = $last['h'];
            $last['c'] = $last['h'];
            $last['time'] = $time->format('Y-m-d H:i:s');
            $ret[] = $last;

            $time = $time->addMinutes($period);
            $last['l'] = $low - $pips / $divider;
            $last['h'] = $last['l'];
            $last['o'] = $last['l'];
            $last['c'] = $last['l'];
            $last['time'] = $time->format('Y-m-d H:i:s');
            $ret[] = $last;
        }


        return $ret;
    }

}
