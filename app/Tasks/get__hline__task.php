<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Tasks\Task;
use App\VO\VoVal;
use App\Models\ChartLevel;

class get__hline__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        int $id
    )
    {
        $r = repo(ChartLevel::class);
        $tline = $r->find($id);

        return new Res(
            [
                    'res' => $tline
            ]
        );
    }

}
