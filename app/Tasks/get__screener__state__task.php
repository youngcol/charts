<?php
declare(strict_types=1);
namespace App\Tasks;

use App\Common\Res;
use App\Tasks\Task;
use App\VO\VoVal;
use App\Tasks\init__screener__state__task;
use App\Facades\Redis2;

class get__screener__state__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (

    )
    {
        $state = [];
        foreach(settings('pairs_list') as $pair)
        {
            $state[$pair] = Redis2::get(instrument_screener__state_key($pair));
        }

        return new Res(
            [
                'state' => $state
            ]
        );
    }

}
