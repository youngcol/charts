<?php
declare(strict_types=1);
namespace App\Tasks\MediaFile;

use App\Common\Res;
use App\Models\SocialEngine\MediaFile;
use App\Models\User;
use App\Tasks\Task;

use Slim\Http\UploadedFile;

class UploadMediaToLocalFolderTask extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run(User $user, UploadedFile $uploadedFile)
    {
        if ($uploadedFile->getError() !== UPLOAD_ERR_OK)
        {
            return resFail('request_file_upload_error','', []);
        }

        $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
        $clientMediaType = $uploadedFile->getClientMediaType();

        $type = vo(explode('/', $clientMediaType)[0], 'mediafile.type');

        $mediaFile = MediaFile::create([
            'type'      => $type->_(),
            'user_id'   => $user->id,
        ]);

        $filename = uuid1FromId($mediaFile->id) . '.' . $extension;
        $uploadedFile->moveTo(UPLOADS_PATH . DIRECTORY_SEPARATOR . $filename);

//        dump($isMoved);echo 'UploadMediaToLocalFolderTask.php:39'; exit;
        if (!file_exists(UPLOADS_PATH . DIRECTORY_SEPARATOR . $filename))
        {
            return resFail('failed_upload_file', 'Cant upload file', [
                'mediaFile' => $mediaFile
            ]);
        }
        //-=-=-=-=-=-=-=-=-=-=-=-=


        $mediaFile->filename = $filename;
        $mediaFile->save();

        return new Res([
            'mediaFile' => $mediaFile
        ]);
    }
}
