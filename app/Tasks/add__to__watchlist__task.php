<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Models\Watchlist;
use App\Tasks\Task;
use App\VO\VoVal;

/**
 * @deprecated
 */
class add__to__watchlist__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        VoVal $instrument,
        VoVal $direction,
        int $period
    )
    {
        //test_gate(true, 'add__to__watchlist__task');
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=

        $r = repo(Watchlist::class);

        $data = [
            'instrument' => $instrument->_(),
            'direction' => $direction->_(),
            'period' => $period
        ];

        $ret = $r->updateOrCreate([
            'instrument' => $instrument->_(),
            'period' => $period
        ], $data);

        return new Res(
            [
                'res' => ''
            ]
        );
    }

}
