<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Tasks\Task;
use App\VO\VoVal;
use App\Models\ChartLevel;

class update__hline__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        int $id,
        float $value,
        int $is_fired
    )
    {
        $r = repo(ChartLevel::class);
        $hline = $r->find($id);
        $hline->value = $value;
        $hline->is_fired = $is_fired;
        $hline->save();
        
        return new Res(
            [
                'hline' => $hline
            ]
        );
    }

}
