<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Models\Event;
use App\Tasks\Task;
use App\VO\VoVal;
use App\Models\ChartTrendline;

class create__event__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        string $pair,
        int $period,
        int $directon,
        string $tag,
        string $place_tag='',
        array $trendline = null,
        float $price = null
    )
    {
//        test_gate(true, 'create__signal__history__item__task');
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=

        $r = repo(Event::class);
        $ins = $r->insert([
            'tag' => $tag,
            'instrument' => $pair,
            'period' => $period,
            'direction' => $directon,
            'params' => $trendline && $trendline['params'] ? $trendline['params'] : [],
            'place_tag' => $place_tag,
            'price' => $price ?? get_last_value('ohlc', $pair, (int) $period),
            'tline_id' => $trendline ? $trendline['uniq_id']: null,
            'is_trading' => $trendline ? $trendline['is_trading'] : 0,
        ]);

        return new Res(
            [
                'model' => $ins
            ]
        );
    }

}
