<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Tasks\Task;
use App\VO\VoVal;

class update__screener__ema__swings__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param array $indicator_last_values
     * @param array $indicator_slow_last_values
     * @param array $ohlc
     * @param array $ohlc_ema
     * @param bool $is_skip_create_event
     * @return Res
     * @throws \Exception
     */
    public function run
    (
        array $indicator_last_values,
        array $indicator_slow_last_values,
        array $ohlc,
        array $ohlc_ema,
        bool  $is_skip_create_event = false
    )
    {
//        store_dump_var('update__screener____ema__swings__task', [
//            'now' => now_local(),
//            'indicator_last_values' => $indicator_last_values,
//            'indicator_slow_last_values' => $indicator_slow_last_values,
//            'ohlc' => $ohlc,
//            'ohlc_ema' => $ohlc_ema,
//        ]);

        $indicator = $this->update_screener_state($indicator_last_values, $indicator_slow_last_values, 'indicator', $is_skip_create_event);
//        $ohlc = $this->update_screener_state($ohlc, $ohlc_ema, 'ohlc', $is_skip_create_event);

        return new Res(
            [
                'indicator' => $indicator,
                'ohlc' => $ohlc
            ]
        );
    }

    /**
     * @param array $last_values
     * @param array $slow_last_values
     * @param string $state_prefix
     * @param bool $is_skip_create_event
     * @return void
     * @throws \Exception
     */
    protected function update_screener_state(array $last_values, array $slow_last_values, string $state_prefix, bool $is_skip_create_event)
    {
        $ret = [];
        $s = new \App\Facades\Screener();

        foreach (settings('pairs_list') as $pair)
        {
            $periods = $last_values[$pair];
            foreach ($periods as $period => $indicator_data)
            {
                $new_state = (int)((float)$indicator_data['val'] > (float)$slow_last_values[$pair][$period]['val']);
                $screener_upd = $s->set($pair, $period, $state_prefix, 'ema_swing', $new_state ? 'buy' : 'sell');

                if ($screener_upd['updates']['added'])
                {
                    // push_to_voice_queue("$pair $period свинг " . ($new_state ? ' вверх ' : ' вниз '));
                }
            }
        }

        return $ret;
    }
}
