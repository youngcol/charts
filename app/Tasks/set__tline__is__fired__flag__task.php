<?php
declare(strict_types=1);
namespace App\Tasks;

use App\Common\Res;
use App\Tasks\Task;
use App\VO\VoVal;
use App\Models\ChartTrendline;

class set__tline__is__fired__flag__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        int $id
    )
    {
        $r = repo(ChartTrendline::class);
        $m = $r->find($id);
        $m->is_fired = 1;
        $m->save();

        return new Res(
            [
                'tline' => $m
            ]
        );
    }

}
