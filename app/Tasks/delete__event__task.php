<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Tasks\Task;
use App\VO\VoVal;
use App\Models\Event;

class delete__event__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        int $id
    )
    {
        $r = repo(Event::class);
        $res = $r->delete([$id]);

        return new Res(
            [
                'res' => $res
            ]
        );
    }

}
