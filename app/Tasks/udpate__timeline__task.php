<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Models\Timeline;
use App\Models\User;
use App\Tasks\Task;
use App\VO\VoVal;
use Cassandra\Time;

class udpate__timeline__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    protected function gates
    (

    )
    {

//        test_gate(true, 'udpate__timeline__task');


        //-=-=-=-=-=-=-=-=-=-=-=-=
        return [resOk([

        ]),


        ] ;
        }

    public function run
    (
        User $u,
        int $id,
        string $chartGroupsSorting
    )
    {
        list(
            $resGates
        ) = $this->gates
            (

            );
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=

        $upd = repo(Timeline::class)
            ->where('id', $id)->update([
                'chart_groups_sorting' => $chartGroupsSorting
            ]);


        return new Res(
            [
                'upd' => $upd
            ]
        );
    }

}
