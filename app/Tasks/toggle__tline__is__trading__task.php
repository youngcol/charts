<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Facades\Redis2;
use App\Tasks\Task;
use App\VO\VoVal;

class toggle__tline__is__trading__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        string $symbol,
        int $period,
        string $object_name
    )
    {
        $objects = Redis2::get(chart_hlines_key($symbol));

        $ids = array_map(function ($i) {
            return $i["object_name"];
        }, $objects[$period]);
        $object_index = array_search($object_name, $ids);

        if($object_index !== false)
        {
            $objects[$period][$object_index]['is_fired'] = 0;

            if ($objects[$period][$object_index]['is_trading'])
            {
                $objects[$period][$object_index]['is_trading'] = 0;
                $objects[$period][$object_index]['params'] = [];
            }
            else
            {
                $hline = $objects[$period][$object_index];
                $objects[$period][$object_index]['is_trading'] = 1;
                $objects[$period][$object_index]['params'] = hline_trading_params(
                    $symbol, $period, $objects[$period][$object_index]['trading_type'], $object_name, $hline['price'], $hline['price2']
                );

                $objects[$period][$object_index]['_sl_val'] = $objects[$period][$object_index]['params']['sl_val'];
                $objects[$period][$object_index]['_sl'] = $objects[$period][$object_index]['params']['sl'];
                $objects[$period][$object_index]['_risk'] = $objects[$period][$object_index]['params']['risk'];
            }
        }

        Redis2::set(chart_hlines_key($symbol), $objects);

        return new Res(
            [
                'object_index' => $object_index,
                'tline' => isset($objects[$period][$object_index]) ? $objects[$period][$object_index] : null
            ]
        );
    }

}
