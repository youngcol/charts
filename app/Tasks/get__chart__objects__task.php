<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Facades\Redis2;
use App\Facades\Screener;
use App\Tasks\Task;
use App\VO\VoVal;

class get__chart__objects__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        string $symbol,
        int $period,
        string $tag
    )
    {
        $ret = [];
        $objects = Redis2::get(chart_objects_key(substr($symbol, 0, 6)));
        $found_tlines = array_filter($objects[$period], function ($i) use ($tag) {
            return in_array($i['chart_tag'], ["ohlc", $tag]);
        });
        $ret['tline_keys'] = implode(",", array_map(function ($i) {
            return $i['object_name'];
        }, $found_tlines));
        $ret['tlines'] = array_to_hash($found_tlines, "object_name");

        $hline_objects = Redis2::get(chart_hlines_key(substr($symbol, 0, 6)));

        $found_hlines = [];
        foreach (settings('charts.mt4_periods') as $period)
        {
            $found_hlines = array_merge($found_hlines, $hline_objects[$period]);
        }

        $ret['hline_keys'] = implode(",", array_map(function ($i) {
            return $i['object_name'];
        }, $found_hlines));
        $ret['hlines'] = array_to_hash($found_hlines, "object_name");



        if ($period === 1)
        {
            $rMarks = task(new get__big__charts__marks__task, [$symbol]);
            $ret['marks'] = $rMarks->marks;
            $ret['marks_ids'] = implode(',', $rMarks->marks_ids);
        }

        $scr = new Screener();
        $ret['trading_direction'] = $scr->get($symbol, $period, 'indicator', 'tlb');

        return new Res($ret);
    }
}
