<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Tasks\Task;
use App\VO\VoVal;
use App\Models\ChartTrendline;

class remove__trainer__tlines__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        string $pair
    )
    {
       
        $r = repo(ChartTrendline::class);
        $res = $r->where('pair', $pair)->where('is_trainer', 1)->delete();
        
        return new Res(
            [
                'res' => $res
            ]
        );
    }

}
