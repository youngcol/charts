<?php
declare(strict_types=1);
namespace App\Tasks;

use App\Common\Res;
use App\Tasks\Task;
use App\VO\VoVal;
use App\Facades\Redis2;

class init__screener__state__task extends Task
{
    public $points;

    public function __construct()
    {
        parent::__construct();

        $this->points = settings('screener_points');
    }

    public function run
    (
        string $symbol = '',
        int $period = 0
    )
    {
        if ($period)
        {
            $screener = Redis2::get(instrument_screener__state_key($symbol));
            $screener['meta'][$period] = [];
            $screener['list'][$period] = [];
            Redis2::set(instrument_screener__state_key($symbol), $screener);
        }
        else
        {
            $data = [
                'meta' => [],
                'list' => []
            ];

            foreach(settings('pairs_list') as $pair)
            {
                foreach(settings('charts.all_periods') as $period)
                {
                    $data['list'][$period] = [];
                }

                Redis2::set(instrument_screener__state_key($pair), $data);
            }
        }


        return new Res(
            [
                'res' => 1
            ]
        );
    }
}
