<?php
declare(strict_types=1);
namespace App\Tasks\SocialEngine;

/**
 *
 *
 */

use App\Common\Res;
use App\Facades\CU;
use App\Models\SocialEngine\SocialNetwork;
use App\Models\SocialEngine\UserSocialAccount;
use App\Tasks\Task;
use App\VO\VoVal;

class StoreFacebookAccountTask extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        string $accessToken
    )
    {
//            $longLivedAccessToken = $accessToken->extend();
//            dump($longLivedAccessToken);echo 'SocialEngineController.php:53'; exit;

        $this->c->facebook->setAccessToken($accessToken);
        $meUser = $this->c->facebook->getMeUser();

        UserSocialAccount::where([
            'social_network_id' => SocialNetwork::FACEBOOK,
            'user_id' => CU::user()->id,
            'social_account_id' => $meUser['id']
        ])->delete();

        $userSocialAccount = UserSocialAccount::create([
            'social_network_id' => SocialNetwork::FACEBOOK,
            'user_id' => CU::user()->id,
            'access_token' => (string)$accessToken,
            'username' => $meUser['name'],
            'social_account_id' => $meUser['id'],
            'profile_image' => '',
        ]);

        return new Res([
            'res' => ''
        ]);
    }
}
