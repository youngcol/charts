<?php
declare(strict_types=1);
namespace App\Tasks\SocialEngine;

/**
 *
 *
 */

use App\Common\Res;
use App\Contracts\PublishReasonContract;
use App\Models\SocialEngine\Post;
use App\Models\SocialEngine\Post\PostPublishLog;
use App\Models\SocialEngine\UserSocialAccount;
use App\Tasks\Task;
use App\VO\VoVal;

class PostPublishedAfterJobTask extends Task
{
    public function __construct()
    {
        parent::__construct();
    }


    public function run
    (
        bool $isSuccess,
        Post $post,
        PublishReasonContract $publishReason,
        UserSocialAccount $userSocialAccount,
        array $info
    )
    {
        // create post log
        $gmtNow = now_date();
        PostPublishLog::create([
            'user_social_account_id' => $userSocialAccount->id,
            'post_id' => $post->id,
            'is_success' => bool2int($isSuccess),
            'publish_time' => date_mysql_format($gmtNow),
            'info' => json_encode($info),
            'publich_reason_class' => get_class($publishReason),
            'publich_reason_id' => $publishReason->id
        ]);

        return new Res([
            'res' => ''
        ]);
    }
}
