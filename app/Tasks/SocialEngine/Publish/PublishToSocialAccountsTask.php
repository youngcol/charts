<?php
declare(strict_types=1);
namespace App\Tasks\SocialEngine\Publish;

use function _\find;
use App\Common\FlowException;
use App\Common\Res;
use App\Models\SocialEngine\MediaFile;
use App\Models\SocialEngine\Post;
use App\Models\SocialEngine\Post\PostPublishLog;
use App\Models\SocialEngine\Post\PostPublishParams;
use App\Models\SocialEngine\SocialNetwork;
use App\Models\SocialEngine\UserSocialAccount;
use App\Contracts\PublishReasonContract;
use App\Tasks\MediaFile\DownloadMediaFromS3Task;
use App\Tasks\SocialEngine\PostPublishedAfterJobTask;
use App\Tasks\Task;
use Carbon\Carbon;
use Illuminate\Support\Arr;


class PublishToSocialAccountsTask extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        PublishReasonContract $publishReason
    )
    {
        $published = [];

        /** @var Post $post */
        $post = $publishReason->getPost();

        /** @var Post\PostPublishParams $publishParams */
        $publishParams = $publishReason->getPostPublishParams();


        $socialAccounts = UserSocialAccount::query()
            ->whereIn('id', $publishParams->social_accounts)
            ->with('user')
            ->get();

        $post->scheduledTime->process_status = 'processing';
        $post->scheduledTime->save();

        foreach ($socialAccounts as $userSocialAccount)
        {
            /** @var UserSocialAccount $userSocialAccount */
            $isSuccess = false;
            $info = [];
            $isAfterJob = false;

            if ($userSocialAccount->isBelongsTo(SocialNetwork::PINTEREST))
            {
                $isAfterJob = true;
                try
                {
                    $resPublished = $this->publishToPinterest($post, $publishParams, $userSocialAccount);
                    $pinId = data_get($resPublished, 'pin.id');

                    if ($pinId)
                    {
                        $isSuccess = true;
                        $info =
                            [
                                'id' => $pinId
                            ];
                    }
                }
                catch (\Exception $e)
                {
                    log_exception($e);
                }
            }
            elseif($userSocialAccount->isBelongsTo(SocialNetwork::TWITTER))
            {
                $isAfterJob = true;
                try
                {
                    $resPublished = $this->publishToTwitter($post, $userSocialAccount);

                    if ($resPublished->get('tweet.result.id'))
                    {
                        $isSuccess = true;
                        $info =
                            [
                                'id' => $resPublished->tweet->result->id,
                                'created_at' => $resPublished->tweet->result->created_at
                            ];
                    }
                }
                catch (\Exception $e)
                {
                    log_exception($e);
                }

            }
            elseif($userSocialAccount->isBelongsTo(SocialNetwork::INSTAGRAM))
            {
                $resPublished = $this->publishToInstagram($post, $publishReason, $userSocialAccount);

            }

            if ($isAfterJob)
            {
                $res = task(new PostPublishedAfterJobTask,
                    [
                        $isSuccess,
                        $post,
                        $publishReason,
                        $userSocialAccount,
                        $info
                    ]
                );

                $published[] = [
                    'userSocialAccount' => $userSocialAccount,
                    'success' => $isSuccess,
                ];
            }
        }

        $post->scheduledTime->process_status = 'done';
        $post->scheduledTime->save();

        return new Res([
            'published' => $published
        ]);
    }

    protected function publishToInstagram(Post $post, PublishReasonContract $publishReason, UserSocialAccount $userSocialAccount)
    {

        run_partisan_async('se:instagram:publish', [
            'publish_reason'        => $publishReason->id,
            'user_social_account'   => $userSocialAccount->id
        ]);

    }

    protected function publishToTwitter(Post $post, UserSocialAccount $userSocialAccount)
    {
        $this->c->twitter->makeConnection(
            vo($userSocialAccount->access_token, 'filled.string'),
            vo($userSocialAccount->access_token_secret, 'filled.string')
        );

        $mediaFilesPaths = map($post->mediaFiles, function ($item)
        {
            if (strlen($item->s3_url) > 0)
            {
                $res = task(new DownloadMediaFromS3Task,
                    [
                        $item
                    ]
                );

                test($res->fail(), 'Can\'n download media file to local server', ['res' => $res, 'mediaFile' => $item]);

                return uploads_path($res->destFileName);
            }
            elseif(strlen($item->filename) > 0)
            {
                return uploads_path($item->filename);
            }
            else
            {
                flow_exception('MediaFile model doesnt contain filename', [$item]);
            }
        });

        $resTweet = $this->c->twitter->createTweet($post->text, $mediaFilesPaths);

        return new Res(
            [
                'tweet' => $resTweet,
            ]
        );
    }

    /**
     * @param Post $post
     * @param PostPublishParams $publishParams
     * @param UserSocialAccount $userSocialAccount
     */
    protected function publishToPinterest(Post $post, PostPublishParams $publishParams, UserSocialAccount $userSocialAccount)
    {
        /** @var MediaFile $mediaFile */
        $mediaFile = sp($post->mediaFiles, 0);
        $mediaFile->val();


        $accountInfo = Arr::first
            (
                sp($publishParams->custom_params, 'accounts', []),
                function ($value, $key) use ($userSocialAccount)
                {
                    return $value['account_id'] == $userSocialAccount->id;
                }
            );


        test(eN($accountInfo), 'Cant find account info', [$publishParams]);
        //-=-=-=-=-=-=-=-=-=-=-=-=

        $bot = $this->c->pinner_b;
        $bot->auth->login($userSocialAccount->username, $userSocialAccount->getPassword());


        foreach ($accountInfo['board_ids'] as $boardId)
        {
            if ($mediaFile->isUploadedToS3())
            {
                $resCrt = $bot->pins->create(
                    uploads_path($mediaFile->filename),
                    $boardId,
                    $post->text,
                    $post->link
                );
            }
            elseif ($mediaFile->isUploadedLocally())
            {
                $resCrt = $bot->pins->create(
                    uploads_path($mediaFile->filename),
                    $boardId,
                    $post->text
                );
            }
        }

        return new Res(
            [
                'pin' => $resCrt,
            ]
        );
    }
}
