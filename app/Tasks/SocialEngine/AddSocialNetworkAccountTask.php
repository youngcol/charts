<?php
declare(strict_types=1);
namespace App\Tasks\SocialEngine;

/**
 * Add user social network task
 *
 */

use App\Common\Res;
use App\Models\SocialEngine\SocialNetwork;
use App\Models\SocialEngine\UserSocialAccount;
use App\Models\User;
use App\Services\UnsafeCrypto;
use App\Tasks\Task;
use App\VO\VoVal;
use function Symfony\Component\VarDumper\Dumper\esc;

class AddSocialNetworkAccountTask extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        User $user,
        SocialNetwork $network,
        VoVal $username,
        VoVal $password
    )
    {

        $isAccountOk = false;
        $userSocialAccount = [];

        if ($network->id == SocialNetwork::PINTEREST)
        {
            $this->c->pinner_b->auth->login($username, $password);

            try
            {
                $profile = $this->c->pinner_b->user->profile();
                $userSocialAccount['profile_image'] = $profile['profile_image_url'];
                $userSocialAccount['username'] = $profile['username'];

                $isAccountOk = true;
            }
            catch (\Exception $e)
            {

            }
        }
        elseif($network->id == SocialNetwork::TWITTER)
        {

        }
        elseif($network->id == SocialNetwork::INSTAGRAM)
        {

        }

        test(!$isAccountOk, 'wrong account credentials', []);
        //-=-=-=-=-=-=-=-=-=-=-=-=


        $encrypted = UnsafeCrypto::encrypt($password, $user->random_key, true);
        $userSocialAccount = UserSocialAccount::create(array_merge($userSocialAccount, [
            'user_id'           => $user->id,
            'social_network_id' => $network->id,
            'password'          => $encrypted,
        ]));

        return new Res([
            'isAccountOk' => $isAccountOk,
            'userSocialAccount' => $userSocialAccount->toArray()
        ]);
    }
}
