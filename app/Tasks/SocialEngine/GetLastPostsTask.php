<?php
declare(strict_types=1);
namespace App\Tasks\SocialEngine;

/**
 * Get last posts list
 *
 */
use App\Common\Res;
use App\Models\SocialEngine\Post;
use App\Models\SocialEngine\Post\PostPublishLog;
use App\Models\SocialEngine\Post\PostScheduledTime;
use App\Tasks\Task;
use App\VO\VoVal;
use Illuminate\Support\Collection;

class GetLastPostsTask extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        VoVal $postStatus,
        int $page,
        int $limit
    )
    {
        if ($postStatus->_() === 'draft')
        {
            $q = Post::query();
            $q->where('is_draft', '=', 1);
            $q->offset($page * $limit)->limit($limit);
            $q->orderBy('id', 'desc');
            $q->with('mediaFiles', 'scheduledTime', 'scheduledTime.publishParams');
            $rows = $q->get();

            $list = $this->group_posts_list($rows);
        }
        elseif($postStatus->_() === 'failed')
        {
            $q = PostPublishLog::query();
            $q->with('post');
            $q->offset($page * $limit)->limit($limit);
            $q->orderBy('id', 'desc');
            $q->where('is_success', 0);

            $rows = $q->get();


            $list = $this->group_posts_publish_log_list($rows);
        }
        elseif($postStatus->_() === 'queued')
        {
            $q = PostScheduledTime::query()
                ->where('process_status', '=', 'scheduled')
                ->with( 'publishParams')
                ->offset($page * $limit)->limit($limit)
                ->orderBy('scheduled_time', 'desc');
            $rows = $q->get();

            $list = $this->group_post_scheduled_time_list($rows);
        }
        elseif($postStatus->_() === 'sent')
        {
            $q = PostScheduledTime::query()->where('process_status', '=', 'done');
            $q->with( 'publishParams');
            $q->with('post');
            $q->offset($page * $limit)->limit($limit);
            $q->orderBy('scheduled_time', 'desc');
            $q->orderBy('id', 'desc');
            $rows = $q->get();

            $list = $this->group_post_scheduled_time_list($rows);
        }
        
        return new Res([
            'list' => $list,
            'list_items_count' => $rows->count(),
            'page' => $page,
            'limit' => $limit,
            'type' => $postStatus->_()
        ]);
    }


    /**
     * @param Collection $list
     * @return array
     */
    protected function group_posts_list(Collection $list): array
    {
        $ret = [];

        $grouped = $list->groupBy(function ($item) {
            return $item->created_at->format('Y-m-d');
        });

        foreach ($grouped as $date => $items)
        {
            $ret[] = [
                'date' => $date,
                'items' => $items
            ];
        }

        return $ret;
    }

    /**
     * @param Collection $list
     * @return array
     */
    protected function group_posts_publish_log_list(Collection $list): array
    {
        $ret = [];

        $grouped = $list->groupBy(function ($item) {
            return $item->created_at->format('Y-m-d');
        });

        foreach ($grouped as $date => $items)
        {
            $posts = $items->map(function ($item) {
                return $item->post;
            });

            $ret[] = [
                'date' => $date,
                'items' => $posts
            ];
        }

        return $ret;
    }

    protected function group_post_scheduled_time_list(Collection $list): array
    {
        $ret = [];

        $postIds = map($list, function ($item) {
            return $item->post_id;
        });

        $allPosts = Post::with('mediaFiles')
            ->whereIn('id', $postIds)
            ->get()
            ->keyBy('id')
        ;

        $grouped = $list->groupBy(function ($item) {
            return $item->scheduled_time->format('Y-m-d');
        });

        foreach ($grouped as $date => $items)
        {
            $posts = map($items, function ($item) use ($allPosts) {

                $post = sp($allPosts, $item->post_id);
                $post->scheduled_time = $item;
                return $post;

            });

            $ret[] = [
                'date' => $date,
                'items' => $posts
            ];
        }

        return $ret;
    }
}
