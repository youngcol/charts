<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Models\Watchlist;
use App\Tasks\Task;
use App\VO\VoVal;

class get__watchlist__items__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (

    )
    {
//        test_gate(true, 'get__watchlist__items__task');
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=
        $r = repo(Watchlist::class);
        $items = $r->orderBy(["id" => 'desc'])->get()->toArray();

        $buy = [];
        $sell = [];
        $res = [];
        foreach ($items as $item) {
            $pic = getTagScreenshot('screenshots_crops', $item['instrument'], 'MAS', $item['period'], 25);

            $res[] = [
                'id' => $item['id'],
                'pair' => $item['instrument'],
                'pic' => $pic,
                'direction' => $item['direction'],
                '_id' => "pic-{$item['instrument']}-{$item['period']}",
                'drawning' => [
                    'url' => drawning_url($item['instrument'], (int)$item['period'], 25)
                ],
            ];
        }

        return new Res(
            [
                'res' => array_chunk($res, 5)
            ]
        );
    }

}
