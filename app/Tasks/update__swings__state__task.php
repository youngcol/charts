<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Facades\Redis2;
use App\Tasks\Task;
use App\VO\VoVal;

class update__swings__state__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        array $ema_swings
    )
    {

        $prev_ema_swings = Redis2::get("ema_swings");

        store_dump_var('update__swings__state__task', [
            $prev_ema_swings,
            $ema_swings
        ]);

        if ($prev_ema_swings)
        {
            foreach (settings('ema_swing_periods') as $period)
            {
                foreach ($ema_swings as $pair => $swings)
                {
                    if ($prev_ema_swings[$pair][0][$period] != $swings[0][$period])
                    {
                        $rEvent = task(new create__event__task,
                            [
                                $pair,
                                (int)$period,
                                (int)$swings[0][$period],
                                'ema_swing',
                                'raw'
                            ]);

                        $res = task(new save__signal__task, [
                            $rEvent->model->id,
                            $rEvent->model->created_at,
                            'ema_swing',
                            $pair,
                            (int)$period,
                            (int)$swings[0][$period],
                            get_last_value('ohlc', $pair, $period)
                        ]);
                    }
                }
            }
        }

        Redis2::set("ema_swings", $ema_swings);

        return new Res(
            [
                'res' => 1
            ]
        );
    }

}
