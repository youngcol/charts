<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Tasks\Task;
use App\VO\VoVal;
use App\Models\ChartTrendline;

class calc__trainer__tlines__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        string $pair,
        int $depo
    )
    {
       
        $result = 3000;
        $tlines_count = 23;

        $r = repo(ChartTrendline::class);
        $tlines = $r->where('pair', $pair)->where('is_trainer', 1)->get();

        foreach($tlines as $tline)
        {
            if(count($tline->params) > 0)
            {
                $sl = $tline->params['sl'];
                $points = $tline->getDeltaPoints();
                $risk = $tline->params['risk'];

                $percents = $points / $sl * $risk;
                $depo += $depo * $percents / 100;
            }

        }

        return new Res(
            [
                'tlines_count' => count($tlines),
                'result' => number_format(round($depo), 2)
            ]
        );
    }

}
