<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Models\Timeseria;
use App\Tasks\Task;
use App\VO\VoVal;
use Carbon\Carbon;

class get__timeseria__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        array $pairs,
        int $period,
        int $limit,
        string $tag
    )
    {
        $chart = [];
        $empty_bars = [];
        $series = $this->get_pairs_timeseries($pairs, $tag, $period, $limit);

        foreach(array_keys($series) as $pair) {
            $tail = last($series[$pair]);
            $empty_bars[$pair] = [];
            if ($tail) {
                $empty_bars[$pair] = $this->appendEmptyBars($tail, $pair, $period, settings('charts.empty_bars.'.$period));
            }
        }

        return new Res(
            [
                'period' => $period,
                'series' => $series,
                'empty_bars' => $empty_bars
            ]
        );
    }

    function get_pairs_timeseries(array $pairs, string $tag, int $period, int $limit)
    {
        $ret = [];
        $r = repo(Timeseria::class);
        $items = $r
            ->where('tag', $tag)
            ->whereIn('instrument', $pairs)
            ->where('period', $period)
            ->limit($limit * count($pairs))
            ->orderBy(['time' => 'desc'])
            ->get()
            ->toArray();

        foreach($items as $item) {
            array_create_key($ret, $item['instrument']);
            $ret[$item['instrument']][] = $item;
        }

        foreach($ret as &$seria) {
            $seria = array_reverse($seria);
        }

        return $ret;
    }

    /**
     * @param array $chart
     * @param string $pair
     * @param int $period
     * @param $count
     * @return array
     */
    function appendEmptyBars(array $tail, string $pair, int $period, $count): array
    {
        $time = Carbon::parse($tail['time']);

        for ($i = 0; $i < $count; $i++)
        {
            $time = $time->addMinutes($period);
            $item = new Timeseria();

            $item->id = 0;
            $item->time = $time->format('Y-m-d H:i:s');
            $item->instrument = $pair;
            $item->period = $period;
            $item->value = $tail['value'];

            $chart[] = $item->toArray();
        }

        // append min max
        $chart[$count-1]["value"] = settings('indicator.min_max')[0];
        $chart[$count-2]["value"] = settings('indicator.min_max')[1];

        return $chart;
    }
}
