<?php
declare(strict_types=1);
namespace App\Tasks;

use App\Common\Res;
use App\Tasks\ForexCore\Trading\get__open__orders__task;
use App\Facades\Redis2;
use App\Models\ChartTrendline;
use Carbon\Carbon;

class get__frontend__events__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        int $from_signal_id,
        int $from_ripper_event_id,
        int $history
    )
    {

        $ohlcLast = task(new get__timeseria__last__values__task, [['ohlc', settings('indicator.tag')]]);
        $empty_bars = [
            'ohlc' => [],
            'indicator' => []
        ];

//        foreach ($ohlcLast->candels as $pair => $datum)
//        {
//            $empty_bars['ohlc'][$pair] = [];
//            foreach ($datum as $period => $data)
//            {
//                $empty_bars['ohlc'][$pair][$period] = $this->build_empty_bars($data, $period, settings('charts.empty_bars.' . $period));
//            }
//        }

//        $events_tags = array_map(function ($tag) {
//            return array_map(function ($suffix) use ($tag) {
//                return $tag . $suffix;
//            }, ['_tlb', '_nl', '_nh']);
//        }, ['ohlc', settings('indicator.tag')]);
//        $events_tags = array_flatten($events_tags);

        $beep_queue = Redis2::lPop('beep_queue');
        if ($beep_queue)
        {
            $beep_queue = json_decode($beep_queue, true);
        }

        $rsi_change = Redis2::lPop('rsi_change_queue');
        if ($rsi_change)
        {
            $rsi_change = json_decode($rsi_change, true);
        }

        $events_tags = ['nh', 'nl', 'tlb', 'adx'];
        $ret = [
            'voice_notify' => Redis2::lPop('voice_queue'),
            'beep_queue' => $beep_queue,
//            'rsi_change_queue' => $rsi_change,
            'pivot_points' => Redis2::get('indicator_pivot_points'),
            'screener' => task(new get__screener__state__task,[]),
            'events' => task(new \App\Tasks\get__events__history__task,
                [
                    $history,
                    $events_tags
                ]
            ),
            'ripper_events' => task(new \App\Tasks\get__ripper__events__history__task,
                [
                    $from_ripper_event_id,
                    $history
                ]
            ),
            'signals' => task(new get__signals__list__task, [10, $events_tags]),
            'ohlc_pivots' => Redis2::get(ohlc_pivots_key()),
            'closed_orders' => Redis2::get("closed_orders", 1),
            'markup_requests' => task(new get__unmapped__symbols__task, [
                settings('indicator.tag'),
                settings('charts.markup_periods')
            ]),
            'ohlc_last' => $ohlcLast,
            'empty_bars' => $empty_bars,
            'open_orders' => task(new get__open__orders__task,[]),
            'pics_timestamps' => $this->get_pics_timestamps(),
            'timeseries_updated_time' => Redis2::get("timeseries_updated_time", false)
        ];

        return new Res($ret);
    }

    function get_active_tlines()
    {
        $r = repo(ChartTrendline::class);
        $ret = $r->where('end_date>=', now_local())->get();
        return $ret;
    }

    function get_pics_timestamps()
    {
        $key = implode("-", ['pics-timestamps-all']);
        $timestamps = Redis2::get($key, 1, []);

        if(!isset($timestamps['last_timestamp'])) {
            $timestamps['last_timestamp'] = time();
            Redis2::set($key, $timestamps);
        }

        $timestamps['pairs'] = array_keys($timestamps);
        $timestamps['last_timestamp_delta'] = time() - $timestamps['last_timestamp'];

        return $timestamps;
    }

    protected function build_empty_bars(array $last, int $period, int $count)
    {
        $ret = [];

        $time = Carbon::parse(str_replace('.', '-', $last['time']));
        for($i=0;$i<$count;$i++)
        {
            $bar = $last;
            $time = $time->addMinutes($period);
            $bar['time'] = $time->format('Y-m-d H:i:s');
            $bar['open'] = $bar['close'];
            $bar['high'] = $bar['close'];
            $bar['low'] = $bar['close'];

            $ret[] = $bar;
        }

        return $ret;
    }
}
