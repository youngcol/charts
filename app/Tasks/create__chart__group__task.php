<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Models\Chart;
use App\Models\ChartGroup;
use App\Models\User;
use App\Repositories\Repository;
use App\Tasks\Task;
use App\VO\VoVal;

class create__chart__group__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        User $user,
        VoVal $name,
        VoVal $charts,
        VoVal $timeframe,
        VoVal $timeframeType,
        ?VoVal $timelineId,
        ?VoVal $nextPopupTime
    )
    {
        list(
            $resGates
        ) = $this->gates
        (

        );
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=

        $m = new ChartGroup();
        $m->name = $name->_();
        $m->user_id = $user->id;
        $m->timeframe = $timeframe->_();
        $m->timeframe_type = $timeframeType->_();

        if ($timelineId)
        {
            $m->timeline_id = $timelineId->_();
        }

        if ($nextPopupTime)
        {
            $m->next_popup_time = $nextPopupTime->_();
        }

        $m->save();


        /** @var Repository $chartsRepo */
        $chartsRepo = repo(Chart::class);

        $chartsRepo
            ->whereIn('id', $charts->_())
            ->update(['group_id' => $m->id]);

        return new Res([
            'res' => ''
        ]);
    }

    /**
     * @return array
     * @throws \App\Common\GateException
     */
    protected function gates
    (

    )
    {

//        test_gate(true, 'create__chart__group__task');

        return [resOk()] ;
    }
}
