<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Facades\db;
use App\Tasks\Task;
use App\VO\VoVal;
use Carbon\Carbon;

class save__timeseria__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        array $timeseries,
        string $tag
    )
    {
//        test_gate(true, 'save__timeseria__task');
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=

        if($tag == 'ohlc') {
            return $this->update_ohlc_table($timeseries);
        } else {
            return $this->update_timeseries_table($timeseries, $tag);
        }

    }

    public function update_ohlc_table(array $timeseries)
    {
        $updated = 0;
        $last_values = [];
        $last_val = 0;
        $last_time = 0;
        $last_candel = [];
        $sql = '';

        foreach ($timeseries as $pair => $pairData)
        {
            $pair = substr($pair, 0, 6);
            if (!in_array($pair, settings('pairs_list'))) continue;
            // ==========

            $last_values[$pair] = [];

            foreach ($pairData as $data)
            {
                $period = array_keys($data)[0];
                $updated++;
                $last_values[$pair][$period] = [];

                $lines = explode(";", rtrim(ltrim($data[$period], ';'), ';'));
                $values = [];
                $prev_candle = null;

                foreach ($lines as $key => $line)
                {
                    $c = $this->parse_line($line);

                    $last_val = $c['close'];
                    $last_time = $c['time'];
                    $last_values[$pair][$period] = ['time' => $c['time'], 'val' => $c['close']];

                    $values[] = "(\"$pair\", $period, \"{$c['time']}\", {$c['open']}, {$c['high']}, {$c['low']}, {$c['close']})";

                    if ($key-1 > 0)
                    {
                        $prev_candle = $this->parse_line($lines[$key-1]);
                    }
                }

                $last_candle = $this->parse_line(end($lines));
                $last_candel[$pair][$period] = $last_candle;

                $values = implode(",", $values);

                if(settings('charts.storing_timeseries'))
                {
                    $s = "insert into `ohlc` (`instrument`, `period`, `time`, `o`, `h`, `l`, `c`)
                        values $values
                            ON DUPLICATE KEY UPDATE `o`=VALUES(o), `h`=VALUES(h), `l`=VALUES(l), `c`=VALUES(c);";
                    $sql .= $s;
                    DB::statement($s);
                }
            }
        }

//        store_dump_var( 'ohlc__insert_into_timeseries', [
//             'date' => date('r'),
//             'sql' => $sql
//        ]);


        return new Res(
                [
                'updated' => $updated,
                'last_candel' => $last_candel,
                'last_values' => $last_values,
                'last_val' => (float)$last_val,
                'last_time' => Carbon::parse(str_replace('.', '-', $last_time)),
            ]
        );
    }

    public function parse_line(string $line)
    {
        $candle = explode(',', $line);
        return [
            'time' => $candle[0],
            'open' => $candle[1],
            'high' => $candle[2],
            'low' => $candle[3],
            'close' => $candle[4],
        ];
    }


    public function update_timeseries_table(array $timeseries, string $tag)
    {
        $updated = 0;
        $last_values = [];

        $last_val = 0;
        $last_time = 0;

        $sql = '';
        foreach ($timeseries as $pair => $pairData)
        {
            $pair = substr($pair, 0, 6);
            if (!in_array($pair, settings('pairs_list'))) continue;
            // ==========

            $last_values[$pair] = [];
            foreach ($pairData as $data)
            {
                $period = array_keys($data)[0];
                $updated++;
                $last_values[$pair][$period] = [];

                $lines = explode(";", rtrim(ltrim($data[$period], ';'), ';'));

//                store_dump_var($tag . '__insert_into_timeseries_tag', [
//                    'tag' => $tag,
//                    'lines' => $lines,
//                    'period' => $period
//                ]);

                foreach ($lines as $line)
                {
                    $candle = explode(',', $line);
                    $time = $candle[0];
                    $val = sprintf("%0.5f", $candle[1]);

                    $last_val = $val;
                    $last_time = $time;
                    $last_values[$pair][$period] = ['time' => $time, 'val' => $val];

                    if(settings('charts.storing_timeseries'))
                    {
                        $s = "insert into `timeseries` (`instrument`, `period`, `time`, `value`, `tag`)
                        values (\"$pair\", $period, \"$time\", $val, \"$tag\")
                            ON DUPLICATE KEY UPDATE value=$val;
                        ";
                        $sql .= $s;
                        DB::statement($s);
                    }

                }
            }
//
             store_dump_var($tag . '__insert_into_timeseries', [
                 'date' => date('r'),
                 'sql' => $sql
            ]);

        }

        return new Res(
                [
                    'updated' => $updated,
                    'last_values' => $last_values,

                    'last_val' => (float)$last_val,
                    'last_time' => Carbon::parse(str_replace('.', '-', $last_time)),
            ]
        );
    }

}
