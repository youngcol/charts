<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Tasks\Task;
use App\VO\VoVal;

class update__renko__screener__state__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        string $tag,
        array $last_values
    )
    {
        //test_gate(true, 'update__renko__screener__state__task');
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=



        return new Res(
            [
                'res' => 1
            ]
        );
    }

}
