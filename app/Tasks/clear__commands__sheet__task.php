<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 */

use App\Common\Res;
use App\Tasks\Task;
use App\VO\VoVal;
use App\Models\Forex\ForexCommand;


class clear__commands__sheet__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (

    )
    {
        //test_gate(true, 'clear__commands__sheet__task');
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=

        $r = repo(ForexCommand::class);
        $r->delete();


        return new Res(
            [
                'res' => 1
            ]
        );
    }

}
