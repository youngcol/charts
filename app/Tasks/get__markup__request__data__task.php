<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Facades\Redis2;
use App\Tasks\Task;
use App\VO\VoVal;

class get__markup__request__data__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        string $indicator_name,
        string $symbol,
        int $period
    )
    {
        $tlines = Redis2::get(chart_objects_key($symbol));
        $history = settings('charts.history');

        $resTimeseria = task(new get__timeseria__task, [
            [$symbol],
            (int) $period,
            $history[$period],
            $indicator_name
        ]);

        return new Res(
            [
                'period' => $period,
                'symbol' => $symbol,
                'seria' => $resTimeseria->series[$symbol],
                'empty_bars' => $resTimeseria->empty_bars[$symbol],
                'tlines' => filter_tlines_by_tag($tlines[$period], $indicator_name)
            ]
        );
    }

}
