<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Facades\Redis2;
use App\Tasks\Task;
use App\VO\VoVal;

class init__chart__objects__redis__structure__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (

    )
    {
//        test_gate(true, 'init__chart__objects__redis__structure__task');
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=
        $chart_trading_settings = [];
        $indicator_pivot_points = [];
//        $indicator_change_points = [];

        foreach (array_flatten(settings('pairs_list')) as $pair)
        {
            $objects = [];
            $chart_trading_settings[$pair] = [];
            $indicator_pivot_points[$pair] = [];
//            $indicator_change_points[$pair] = [];

//            foreach (settings('charts.big_periods') as $period)
//            {
//                $indicator_change_points[$pair][$period] = [];
//            }

            foreach (settings('charts.all_periods') as $period)
            {
                $objects[$period] = [];
                $chart_trading_settings[$pair][$period] = ['t' => 'temp'];
            }

            foreach (settings('charts.big_periods') as $period)
            {
                $indicator_pivot_points[$pair][$period] = ['*' => '*'];
            }

            Redis2::set(chart_objects_key($pair), $objects);
            Redis2::set(chart_hlines_key($pair), $objects);

            Redis2::del('beep_queue');
        }

        $price_movements = [];
        foreach (array_flatten(settings('pairs_list')) as $pair)
        {
            $price_movements[$pair] = [
                'price' => 0
            ];
        }
        Redis2::set('prices_movements', $price_movements);

        // chart trading settings
        Redis2::set('chart_trading_settings', $chart_trading_settings);
        Redis2::set('indicator_pivot_points', $indicator_pivot_points);

//        Redis2::set('indicator_change_points', $indicator_change_points);


        Redis2::r()->del('voice_queue');

        return new Res(
            [
                'res' => 1
            ]
        );
    }

}
