<?php
declare(strict_types=1);
namespace App\Tasks\Charts;

/**
 *
 *
 */

use App\Common\Res;
use App\Facades\Redis2;
use App\Models\ChartTrendline;
use App\Tasks\Task;
use App\VO\VoVal;
use Carbon\Carbon;

class get__trendlines__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        string $chart_tag,
        string $pair,
        int $period
    )
    {
        $tlines = Redis2::get(chart_objects_key($pair))[$period];

        return new Res(
            [
                'pair' => $pair,
                'period' => $period,
                'chart_tag' => $chart_tag,
                'list' => filter_tlines_by_tag($tlines, $chart_tag)
            ]
        );
    }

}
