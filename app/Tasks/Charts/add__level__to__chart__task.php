<?php
declare(strict_types=1);
namespace App\Tasks\Charts;

/**
 *
 *
 */

use App\Common\Res;
use App\Models\ChartLevel;
use App\Models\User;
use App\Tasks\Task;
use App\VO\VoVal;

class add__level__to__chart__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    protected function gates
    (

    )
    {


//        test_gate(true, 'create__chart__level__task');



        //-=-=-=-=-=-=-=-=-=-=-=-=
        return [resOk([

        ]),


        ] ;
        }

    public function run
    (
        User $user,
        int $chartId,
        ?int $levelId,
        float $value,
        int $isFired,
        VoVal $type
    )
    {
        list(
            $resGates
        ) = $this->gates
            (

            );
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=

        $r = repo(ChartLevel::class);
//        $r->where('');

        $ret = $r->updateOrCreate([
            'id' => $levelId,
        ], [
            'chart_id' => $chartId,
            'value' => $value,
            'type' => $type->_(),
            'is_fired' => $isFired
        ]);

        return new Res(
            [
                'level' => $ret
            ]
        );
    }

}
