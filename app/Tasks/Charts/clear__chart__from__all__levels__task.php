<?php
declare(strict_types=1);
namespace App\Tasks\Charts;

/**
 *
 *
 */

use App\Common\Res;
use App\Models\ChartLevel;
use App\Models\ChartTrendline;
use App\Models\User;
use App\Tasks\Task;
use App\VO\VoVal;

class clear__chart__from__all__levels__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    protected function gates
    (

    )
    {


//        test_gate(true, 'clear__chart__from__all__levels__task');



        //-=-=-=-=-=-=-=-=-=-=-=-=
        return [resOk([

        ]),


        ] ;
        }

    public function run
    (
        User $u,
        int $chartId
    )
    {
        list(
            $resGates
        ) = $this->gates
            (

            );
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=


        $r = repo(ChartLevel::class);
        $del = $r->where('chart_id', $chartId)->delete();

        $r = repo(ChartTrendline::class);
        $del2 = $r->where('chart_id', $chartId)->delete();


        return new Res(
            [
                'del' => $del,
                'del2' => $del2,
            ]
        );
    }

}
