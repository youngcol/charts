<?php
declare(strict_types=1);
namespace App\Tasks\Charts;

/**
 *
 *
 */

use App\Common\Res;
use App\Models\Chart;
use App\Models\ChartLevel;
use App\Models\User;
use App\Tasks\Task;
use App\VO\VoVal;

class delete__level__from__chart__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    protected function gates
    (
        User $u,
        int $chartId
    )
    {

        $chart = repo(Chart::class)
            ->where([
                'user_id' => $u->id,
                'id' => $chartId
            ])
            ->first();

        test_gate(eN($chart), 'Failed find chart');

        //-=-=-=-=-=-=-=-=-=-=-=-=
        return [resOk([

        ]),


        ] ;
    }

    public function run
    (
        User $u,
        int $chartId,
        int $levelId
    )
    {
        list(
            $resGates
        ) = $this->gates
            (
                $u,
                $chartId
            );
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=

        $r = repo(ChartLevel::class);
        $r->where('chart_id', $chartId);
        $del = $r->delete([$levelId]);


        return new Res(
            [
                'del' => $del
            ]
        );
    }

}
