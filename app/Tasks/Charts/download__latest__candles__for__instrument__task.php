<?php
declare(strict_types=1);
namespace App\Tasks\Charts;

/**
 *
 *
 */

use App\Common\Res;
use App\Tasks\Task;
use App\VO\VoVal;

class download__latest__candles__for__instrument__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    protected function gates
    (

    )
    {


        test_gate(true, 'download__latest__candles__for__instrument__task');



        //-=-=-=-=-=-=-=-=-=-=-=-=
        return [resOk([

        ]),


        ] ;
        }

    public function run
    (

    )
    {
        list(
            $resGates
        ) = $this->gates
            (

            );
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=



        return new Res(
            [
                'res' => ''
            ]
        );
    }

}
