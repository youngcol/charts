<?php
declare(strict_types=1);
namespace App\Tasks\Charts;

/**
 *
 *
 */

use App\Common\Res;
use App\Facades\Redis2;
use App\Models\ChartLevel;
use App\Models\ChartTrendline;
use App\Models\User;
use App\Tasks\Task;
use App\VO\VoVal;
use Carbon\Carbon;

class add__trendline__to__chart__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        string $uniqId,
        VoVal $start_date,
        VoVal $end_date,
        float $startValue,
        float $endValue,
        int $isFired,
        int $is_zone,
        int $is_zone_buy,
        int $is_trainer,
        string $chart_tag,
        int $period,
        string $pair,
        array $mockup_tags
    )
    {
        $objects = Redis2::get(chart_objects_key($pair));
        if($is_zone)
        {
            $trading_type = $is_zone_buy ? 'buy' : 'sell';
        } else {
            $trading_type = $startValue < $endValue ? 'sell' : 'buy';
        }

        $is_trading = (int) !empty($trading_params);
        $is_trading = 0;

        $data = [
            'uniq_id' => $uniqId,
            'object_name' => "Trendline " . $uniqId,

            'start_value' => $startValue,
            'end_value' => $endValue,
            'start_date' => $start_date->_(),
            'end_date' => $end_date->_(),

            // mt4 values
            'start_time' => Carbon::createFromFormat('Y-m-d H:i:s', $start_date->_(), "UTC")->timestamp,
            'end_time' => Carbon::createFromFormat('Y-m-d H:i:s', $end_date->_(), "UTC")->timestamp,
            'price' => $startValue,
            'price2' => $endValue,

            'is_finilized' => 0,
            'is_fired' => $isFired,
            'is_zone' => $is_zone,
            'is_zone_buy' => $is_zone_buy,
            'is_trainer' => $is_trainer,
            'chart_tag' => $chart_tag,
            'period' => $period,
            'pair' => $pair,
            'trading_type' => $trading_type,
            'fired_at' => null,
            'fired_value' => null,
            'mockup_tag' => $mockup_tags,
            'is_trading' => $is_trading,
            'params' => []
        ];

        if($startValue - $endValue !== 0)
        {
            $ids = array_map(function ($i) {
                return $i["uniq_id"];
            }, $objects[$period]);

            $object_index = array_search($uniqId, $ids);

            if($object_index !== false)
            {
                $objects[$period][$object_index] = $data;
            } else {
                $objects[$period][] = $data;
            }

            Redis2::set(chart_objects_key($pair), $objects);
        }

        return new Res(
            [
                'trendline' => $data,
            ]
        );
    }
}
