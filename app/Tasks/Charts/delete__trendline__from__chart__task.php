<?php
declare(strict_types=1);
namespace App\Tasks\Charts;

/**
 *
 *
 */

use App\Common\Res;
use App\Models\Chart;
use App\Models\ChartTrendline;
use App\Models\User;
use App\Tasks\Task;
use App\VO\VoVal;

class delete__trendline__from__chart__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        int $id
    )
    {
        $r = repo(ChartTrendline::class);
        $m = $r->find($id);
        $del = $r->delete([$id]);

        return new Res(
            [
                'del' => $del,
                "tline" => $m
            ]
        );
    }

}
