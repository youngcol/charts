<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Models\ChartTrendline;
use App\Tasks\Task;
use App\VO\VoVal;
use Carbon\Carbon;

/**
 * @deprecated
 */
class generate__signals__on__trendlines__breakouts__task extends Task
{
    protected $generated_signals = [];

    public function __construct()
    {

        parent::__construct();
    }

    public function run
    (
        string $chart_tag,
        array $values
    )
    {
        $breakout_tlines = [];
        $r = repo(ChartTrendline::class);


        if ($chart_tag === 'ohlc')
        {
            foreach ($values as $pair => $periods)
            {
                $time = Carbon::parse(str_replace(".", "-", $periods[1]['time']));
                $value = (float)$periods[1]['val'];

                $trendlines = $r
                    ->where('pair', $pair)
                    ->where('chart_tag', $chart_tag)
                    ->where('end_date>=', now_local())
                    ->where('is_fired', 0)
                    ->get();

                $breakout_tlines[] = $this->checkTrendlinesBreakouts($trendlines, $chart_tag, $value, $time, $pair);
            }
        } else {
            foreach ($values as $pair => $periods)
            {
                foreach($periods as $period => $datum)
                {
                    $time = Carbon::parse(str_replace(".", "-", $datum['time']));
                    $value = (float)$datum['val'];

                    $trendlines = $r
                        ->where('pair', $pair)
                        ->where('period', $period)
                        ->where('chart_tag', $chart_tag)
                        ->where('end_date>=', now_local())
                        ->where('is_fired', 0)
                        ->get();

                    $breakout_tlines[] = $this->checkTrendlinesBreakouts($trendlines, $chart_tag, $value, $time, $pair);
                }
            }
        }


        return new Res(
            [
                'count' => count(array_flatten($breakout_tlines)),
                'breakout_tlines' => array_flatten($breakout_tlines),
                'generated_signals' => $this->generated_signals
            ]
        );
    }

     public function checkTrendlinesBreakouts($trendlines, string $chart_tag, float $value, Carbon $time, string $pair)
     {
         $ret = [];

         foreach ($trendlines as $trendline)
         {
             if ($trendline->isBreakOut($value, $time) !== NULL) {
                 $trendline->setFired($value, $time);


                 $trendline->save();

                 $res = task(new create__event__task,
                    [
                        $pair,
                        $trendline->period,
                        $trendline->isSupportLine() ? 0 : 1,
                        $trendline->is_zone ? 'hzone_breakout' : 'trendline_breakout',
                        $trendline->chart_tag,
                        $trendline
                    ]);

                 $this->generated_signals[] = $res->model;
                 $ret[] = $trendline;
             }
         }

        return $ret;
     }

}
