<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Facades\Redis2;
use App\Tasks\Task;
use App\VO\VoVal;

class rsi__tracking__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        array $last_values
    )
    {

        $step = 1;
//        $period = 240;
        $pivots = Redis2::get('indicator_pivot_points');

        foreach ($pivots as $pair => &$periods)
        {
            foreach ($periods as $period => &$datum)
            {
                if (isset($datum['pivot']))
                {
                    $pivot_point = $datum['pivot'];
                    $last_val = $last_values[$pair][$period]['val'];
                    $delta = (int)floor(abs($last_val - $pivot_point));

                    if (isset($datum['delta']))
                    {
                        $change = $delta - $datum['delta'];
                        if ($change >= 1)
                        {
                            push_to_beep_queue($pair, $period, $datum['direction'], $datum['direction']);

//                            push_to_queue('rsi_change_queue', [
//                                'pair' => $pair,
//                                'period' => $period,
//                                'direction' => $datum['direction']
//                            ]);

                            // add event
                            $resEvent = task(new create__event__task,
                                [
                                    $pair,
                                    $period,
                                    $datum['direction'],
                                    'indicator_renko',
                                    'indicator'
                                ]);

                            $datum['delta'] = $delta;
                        }
                    }
                    else
                    {
                        $datum['delta'] = $delta;
                    }
                }

            }
        }

        Redis2::set('indicator_pivot_points', $pivots);

        return new Res(
            [
                'res' => 1
            ]
        );
    }

}
