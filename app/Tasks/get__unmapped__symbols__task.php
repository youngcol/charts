<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Facades\Redis2;
use App\Tasks\Task;
use App\VO\VoVal;
use App\Models\ChartTrendline;
use Carbon\Carbon;

class get__unmapped__symbols__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        string $tag,
        array $periods
    )
    {
        $c = 0;
        $list = [];
        $now = now_date();
        $pairs = array_flatten(settings('pairs_list'));
        $found_pairs = [];
        $binary_unmapped = [];

        foreach($pairs as $symbol)
        {
            $objects = Redis2::get(chart_objects_key($symbol));
            foreach($periods as $period)
            {
                if(!empty($objects[$period]))
                {
                    $running_tlines = array_filter($objects[$period], function ($tline) use ($now, $tag) {
                        $end_date = isset($tline['end_time']) ? Carbon::parse($tline['end_time']) : Carbon::parse($tline['end_date']);
                        return ($tline['chart_tag'] == $tag) && $end_date->gte($now);
                    });

                    if (!count($running_tlines))
                    {
                        $c++;
                        $found_pairs[$symbol] = 1;

                        $list[] = ['pair' => $symbol, 'period' => $period];
                        if ($period == 5)
                        {
                            $binary_unmapped[] = $symbol;
                        }
                    }
                }
            }
        }

        return new Res([
            'binary_mapped' => array_values(array_diff($pairs, $binary_unmapped)),
            'binary_unmapped' => $binary_unmapped,
            'pairs' => array_keys($found_pairs),
            'list' => $list,
            'count' => $c
        ]);
    }
}
