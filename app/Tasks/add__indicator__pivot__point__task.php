<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Facades\Redis2;
use App\Tasks\Task;
use App\VO\VoVal;

class add__indicator__pivot__point__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        string $symbol,
        int $period,
        float $value,
        int $direction
    )
    {
//        test_gate(true, 'add__indicator__pivot__point__task');
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=

        $pivots = Redis2::get('indicator_pivot_points');
        $pivots[$symbol][$period]['pivot'] = $value;
        if (isset($pivots[$symbol][$period]['delta']))
        {
            unset($pivots[$symbol][$period]['delta']);
        }
        $pivots[$symbol][$period]['direction'] = $direction;
        Redis2::set('indicator_pivot_points', $pivots);

        return new Res(
            [
                'res' => 1
            ]
        );
    }

}
