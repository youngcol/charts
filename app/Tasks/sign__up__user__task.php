<?php

namespace App\Tasks;
use App\Common\Res;
use App\Models\User;
use App\Tasks\Task;
use App\VO\VoVal;

class sign__up__user__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run(VoVal $username, VoVal $email, VoVal $password)
    {

        $user = new User();
        $user->email 	= $email;
        $user->name 	= $username;
        $user->active 	= 1;
        $user->email_verify_code = uuid1FromId(rand(1,9999). rand(9999,999999));
        $user->is_admin = 0;
        $user->timezone='Europe/Kiev';

        $user->setPassword($password);
        $user->save();

        // send wellcome email
//        $this->container->mailer

        return new Res([
            'user' => $user
        ]);
    }
}
