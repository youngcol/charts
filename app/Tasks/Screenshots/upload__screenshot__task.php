<?php
declare(strict_types=1);
namespace App\Tasks\Screenshots;

/**
 *
 *
 */

use App\Common\Res;
use App\Facades\Timer;
use App\Models\SocialEngine\MediaFile;
use App\Tasks\Task;
use App\VO\VoVal;
use Slim\Http\UploadedFile;

class upload__screenshot__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        UploadedFile $uploadedFile,
        VoVal $period,
        VoVal $instrument
    )
    {
//        $file_name = $uploadedFile->getClientFilename();
//        $name_parts = explode("__", $file_name);
//        $pic_name = str_replace(".gif", "", array_pop($name_parts));


        $dest = implode('/', [SCREENSHOTS_PATH, $instrument->_(), $period->_(), '/']);
        $dest_filename = $dest . $uploadedFile->getClientFilename();

        create_folder_tree_by_pathname($dest);
        $uploadedFile->moveTo($dest_filename);


        return new Res(
            [
                'moved' => true,
                'file_name' => $dest_filename
            ]
        );
    }

}
