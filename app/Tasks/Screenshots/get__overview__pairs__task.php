<?php
declare(strict_types=1);
namespace App\Tasks\Screenshots;

use App\Common\Res;
use App\Tasks\Task;

class get__overview__pairs__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        int $chunkSize = 5
    )
    {
        $PAIRS = settings('pairs_list');
        $period = 15;
        $tag = 'ATS';

        //$folder = HTDOC_PATH . '/screenshots/';
        $list = [];
        //$original = [];

        foreach ($PAIRS as $index => $pair) {

            $ret = cropScreenshot($pair, $tag, $period, 90);
            $pic = getTagScreenshot('screenshots_crops', $pair, $tag, $period);

            removeOldScreenshots('screenshots_crops', $pair, $tag, $period);
            removeOldScreenshots('screenshots', $pair, $tag, $period);

            $draws = scandir(SCREENSHOTS_DRAWNINGS_PATH ."/$pair/$tag/$period/");

            $list[] = [
                'sort_index' => array_search($pair, array_flatten(settings('pairs_list'))),
                'pair' => $pair,
                'pic' => $pic,
                'drawning' => [
                    'url' => count($draws) == 3 ? "/screenshots_drawnings/$pair/$tag/$period/{$draws[2]}" : null
                ],
            ];
        }

        dump($list);echo "get__overview__pairs__task.php:48";exit;
        return new Res(
            [
                'list' => array_chunk($list, $chunkSize),
            ]
        );
    }

}
