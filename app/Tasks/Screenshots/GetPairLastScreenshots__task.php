<?php
declare(strict_types=1);
namespace App\Tasks\Screenshots;

/**
 *
 *
 */

use App\Common\Res;
use App\Tasks\Task;
use App\VO\VoVal;

class GetPairLastScreenshots__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (

    )
    {
        test_gate(true, 'GetPairLastScreenshots__task');
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=



        return new Res(
            [
                'res' => ''
            ]
        );
    }

}
