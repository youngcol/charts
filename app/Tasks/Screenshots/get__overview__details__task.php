<?php
declare(strict_types=1);
namespace App\Tasks\Screenshots;

/**
 *
 *
 */

use App\Common\Res;
use App\Tasks\Task;
use App\VO\VoVal;

class get__overview__details__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        string $pair
    )
    {
        //test_gate(true, 'get__overview__details__task');
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=

        $periods = [1,5,15,30,60,240,1440];
        $tags = ["ATS", "MAS", "TAOTR"];

        foreach ($tags as $tag) {

            $list[$tag] = [];

            foreach ($periods as $period) {
                $ret = cropScreenshot($pair, $tag, $period, 90, 100);
                $pic = getTagScreenshot('screenshots_crops', $pair, $tag, $period, 90);

                removeOldScreenshots('screenshots_crops', $pair, $tag, $period, "90");
                removeOldScreenshots('screenshots', $pair, $tag, $period);

                $pic['drawning'] = [
                    'url' => drawning_url($pair, $period, 90)
                ];

                $pic['_id'] = $pair."-".$period;
                $list[$tag][] = $pic;
            }
        }

        return new Res(
            [
                'name' => $pair,
                'pics' => $list,
                'sort_index' => array_search($pair, array_flatten(settings('pairs_list')))
            ]
        );
    }

}
