<?php
declare(strict_types=1);
namespace App\Tasks\Screenshots;

/**
 *
 *
 */

use App\Common\Res;
use App\Tasks\Task;
use App\VO\VoVal;

class get__tape__pairs__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        array $pairs,
        array $periods
    )
    {
        //test_gate(true, 'get__tape__pairs__task');
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=
        $tag = 'MAS';
        $tape = [];

        foreach ($pairs as $index => $pair) {
            $pics = [];
            foreach ($periods as $period) {
                cropScreenshot($pair, $tag, $period, 50, 100);
                $pics[$period] = getTagScreenshot('screenshots_crops', $pair, $tag, $period, 50);
                removeOldScreenshots('screenshots_crops', $pair, $tag, $period, "50");
                removeOldScreenshots('screenshots', $pair, $tag, $period);
            }

            $tape[] = [
                'sort_index' => $index,
                'pair' => $pair,
                'pics' => $pics,
                '_id' => "pic-$pair-$period"
            ];
        }

        return new Res(
            [
                'tape' => $tape,
            ]
        );
    }

}
