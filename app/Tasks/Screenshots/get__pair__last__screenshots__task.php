<?php
declare(strict_types=1);
namespace App\Tasks\Screenshots;

/**
 *
 *
 */

use App\Common\Res;
use App\Tasks\Task;
use App\VO\VoVal;

class get__pair__last__screenshots__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        VoVal $pair
    ) {
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=

        $folder = HTDOC_PATH . '/screenshots/' . $pair->_();
        $tags = files($folder);
        $pairName = $pair->_();

        $pics = [];

        foreach ($tags as $i => $tag) {
            $tagFolder = $folder . '/' . $tag;
            $periods = files($tagFolder);
            $pics[$tag] = [];

            foreach ($periods as $period) {
                $picsFolder = $tagFolder . '/' . $period . '/';

                $pic = getTagScreenshot($picsFolder, $pairName, $tag, (int)$period);

                $pics[$tag][$period] = $pic;
            }
        }

        return new Res(
            [
                'name' => $pair->_(),
                'pics' => $pics,
            ]
        );
    }
}
