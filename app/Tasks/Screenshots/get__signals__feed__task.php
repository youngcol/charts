<?php
declare(strict_types=1);
namespace App\Tasks\Screenshots;

use App\Common\Res;
use App\Facades\OhlcFeed;
use App\Facades\Redis2;
use App\Models\Event;
use App\Models\Signal;
use App\Tasks\Task;
use Illuminate\Database\Eloquent\Collection;


class get__signals__feed__task extends Task
{
    const SETUP_ITEMS_LIMIT = 50;

    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        string $mode,
        array $params = null
    )
    {
        if ($mode === 'setup')
        {
            return $this->setup_mode($params);
        }
        elseif ($mode === 'update')
        {
            return $this->update_mode($params);
        }
    }

    protected function get_events_ohlc(Collection $events)
    {
        $list = [];
        $signals_uniq_id = [];
        $ohlc_req = $tlines = [];
        foreach($events as $event)
        {
            array_create_key($ohlc_req, $event['instrument']);
            $ohlc_req[$event['instrument']][] = $event['period'];

            if ($event['period'] == 15)
            {
                $ohlc_req[$event['instrument']][] = 1;
            }
            if (in_array($event['period'], [1440, 240]))
            {
                $ohlc_req[$event['instrument']][] = 15;
            }

            $signal_uniq_id = $event['instrument'] . $event['period'] . $event['direction'];
            if (!in_array($signal_uniq_id, $signals_uniq_id)) {
                $signals_uniq_id[] = $signal_uniq_id;
                $list[] = ['event' => $event];
            }
            if (!isset($tlines[$event['instrument']]))
            {
                $tlines[$event['instrument']] = Redis2::get(chart_objects_key($event['instrument']));
            }
        }

//        dump($signals_uniq_id);echo"get__signals__feed__task.php:68";exit;
        foreach ($ohlc_req as &$req_periods)
        {
            $req_periods = array_unique($req_periods);
        }

        $ohlc = (new OhlcFeed($ohlc_req, $tlines, settings('indicator.tag'), false))->get();
        $pivots = Redis2::get('indicator_pivot_points');

        foreach ($list as &$item)
        {
            $pair_data = $ohlc[$item['event']->instrument][$item['event']->period];
            $item['pivot'] = $pivots[$item['event']->instrument][$item['event']->period];

            $item['ohlc'] = $pair_data['ohlc'];

            if ($item['event']->period == 15)
            {
                $minor_period = 1;
            }
            elseif ($item['event']->period == 240)
            {
                $minor_period = 15;
            }
            elseif ($item['event']->period == 1440)
            {
                $minor_period = 15;
            }

            $item['minor_ohlc'] = $ohlc[$item['event']->instrument][$minor_period];
            $item['minor_period'] = $minor_period;
        }

        return $list;
    }
    protected function setup_mode()
    {
        $limit = 10;
        $ohlc_req = [];
        $tlines = [];

        $r = repo(Event::class);
        $events = $r
            ->whereIn('tag', ['indicator_renko'])
            ->orderBy(['id' => 'desc'])
            //->groupBy('instrument')
            ->limit(self::SETUP_ITEMS_LIMIT)
            ->get()
//            ->reverse()
        ;
//dump($events);echo"get__signals__feed__task.php:116";exit;
//        dump($signals);echo"get__signals__feed__task.php:46";exit;
        $list = $this->get_events_ohlc($events);

//        dump([
//            $list,
//            $ohlc
//        ]);echo"get__signals__feed__task.php:64";exit;

        return new Res(
            [
                'feed' => $list
            ]
        );
    }

    protected function update_mode(array $params)
    {
        $r = repo(Event::class);
        $events = $r
            ->whereIn('tag', ['indicator_renko'])
            ->where('id>', $params['last_signal_id'])
            //->orderBy(['id' => 'desc'])
            //->limit($limit)
            ->get()
            //->reverse()
        ;
//dump($params['last_signal_id']);echo"get__signals__feed__task.php:143";exit;
        $list = $this->get_events_ohlc($events);

        return new Res(
            [
                'feed' => $list
            ]
        );
    }


}
