<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Tasks\Task;
use App\VO\VoVal;

class generate__indicator__swings__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        array $fast,
        array $slow
    )
    {
//        test_gate(true, 'generate__indicator__swings__task');
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=



        return new Res(
            [
                'res' => 1
            ]
        );
    }

}
