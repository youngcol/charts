<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Tasks\Task;
use App\VO\VoVal;
use App\Facades\Redis2;
use App\Models\ChartTrendline;

/**
 * @deprecated
 * Use Screener
 */
class update__screener__state__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        string $instrument,
        int $period,
        string $mockup_tag,
        string $chart_tag,
        int $direction,
        string $update_type, // cumulative, init, set
        int $is_active = 1
    )
    {
        return ok();
        $changes = [];
        $full_mockup_tag = $chart_tag . '_' . $mockup_tag;
        $screener = Redis2::get(instrument_screener__state_key($instrument));
        $type = $direction ? 'buy' : 'sell';
        $is_type_changed = $type != $screener[$period]['list'][$full_mockup_tag]['type'];
        $is_active_changed = $screener[$period]['list'][$full_mockup_tag]['active'] != $is_active;

        if(!isset($screener[$period]['list'][$full_mockup_tag]))
        {
            crit("No such tag in screener ". $instrument . " ". $period. " " . $full_mockup_tag);
        }

        $screener[$period]['list'][$full_mockup_tag]['active'] = $is_active;

        // save price time/value
        if ($is_type_changed || $is_active_changed)
        {
            $ohlcValues = Redis2::get(timeseria_last_values_key("ohlc"));
            $screener[$period]['list'][$full_mockup_tag]["time"] = $ohlcValues[$instrument][$period]["time"];
            $screener[$period]['list'][$full_mockup_tag]["price"] = $ohlcValues[$instrument][$period]["val"];

            $changes[$period][$full_mockup_tag] = $direction;
        }

        $screener[$period]['list'][$full_mockup_tag]['active'] = $is_active;


        if ($update_type == 'init')
        {
            $screener[$period]['list'][$full_mockup_tag]['count'] = 1;
        }
        else if($update_type == 'cumulative')
        {
            if($is_type_changed)
            {
                $screener[$period]['list'][$full_mockup_tag]['count'] = 1;
            }
            else
            {
                $screener[$period]['list'][$full_mockup_tag]['count']++;
            }
        }
        else if($update_type == 'set')
        {
            $screener[$period]['list'][$full_mockup_tag]['count'] = 1;
        }

        $screener[$period]['list'][$full_mockup_tag]['type'] = $type;

        //$screener = $this->calc_deltas($screener);
        Redis2::set(instrument_screener__state_key($instrument), $screener);

        return new Res(
            [
                'state' => $screener,
                'is_changed' => $is_type_changed || $is_active_changed
            ]
        );
    }

    function calc_deltas($screener)
    {
        $sum_delta = 0;
        foreach($screener as $period => &$datum)
        {
            $delta = 0;
            if(is_array($datum))
            {
                foreach($datum['list'] as $item)
                {
                    if($item['active'] && isset($item['points']))
                    {
                        $delta += $item['points'] * $item['count'] * ($item['type'] == 'sell' ? -1 : 1);
                    }
                }
                $datum['delta'] = $delta;
                $sum_delta += $delta;
            }
        }

        $screener['delta'] = $sum_delta;

        return $screener;
    }
}
