<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Facades\Redis2;
use App\Facades\Screener;
use App\Tasks\Task;
use App\VO\VoVal;

class find__true__swings__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (

    )
    {
        $ret = [];
        $period_sets = [
            [1440, 240],
            [240, 60],
            [60, 15],
            [15, 5],
            [5, 1],
        ];

        $s = new \App\Facades\Screener();

        foreach(settings('pairs_list') as $pair)
        {
            $screener = Redis2::get(instrument_screener__state_key($pair));
            foreach (['sell', 'buy'] as $type)
            {
                foreach ($period_sets as $period_set) {
                    $indicator_seq = 0;
                    $ohlc_seq = 0;

                    foreach ($period_set as $period)
                    {
                        if (in_array('indicator_ema_swing_' . $type, $screener['list'][$period]))
                        {
                            $indicator_seq++;
                        }

                        if (in_array('ohlc_ema_swing_' . $type, $screener['list'][$period]))
                        {
                            $ohlc_seq++;
                        }
                    }

                    $indicator_new_state = (int)($indicator_seq === count($period_set));
                    $ohlc_new_state = (int) ($ohlc_seq === count($period_set));

                    if ($indicator_new_state)
                    {
                        $this->set_screener_state($s, $pair, $period_set[0], 'indicator', $type);
                    }
                    else
                    {
                        $s->delete($pair, $period_set[0], 'indicator', 'double_swing', $type);
                    }

                    if ($ohlc_new_state)
                    {
                        $this->set_screener_state($s, $pair, $period_set[0], 'ohlc', $type);
                    }
                    else
                    {
                        $s->delete($pair, $period_set[0], 'ohlc', 'double_swing', $type);
                    }
                }

            }

        }

        return new Res(
            [
                'ret' => $ret
            ]
        );
    }

    private function set_screener_state(Screener $s, string $pair, int $period, string $chart_tag, string $type)
    {
        $upd = $s->set($pair, $period, $chart_tag, 'double_swing', $type);
        $state_name = $chart_tag . '_double_swing';

        if ($upd['updates']['added'])
        {
            $_crt = task(new create__event__task,
                [
                    $pair,
                    $period,
                    $type == 'sell' ? 0 : 1,
                    $state_name
                ]);

            if ($_crt->model->created_at)
            {
                $res = task(new save__signal__task, [
                    $_crt->model->id,
                    $_crt->model->created_at,
                    $state_name,
                    $pair,
                    $period,
                    $type == 'sell' ? 0 : 1,
                    get_last_value('ohlc', $pair, $period)
                ]);
            }
        }
    }

}
