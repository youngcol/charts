<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Facades\Redis2;
use App\Facades\Screener;
use App\Tasks\Task;
use App\VO\VoVal;
use Carbon\Carbon;

class generate__chart__objects__breakouts__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        string $tag,
        array $last_values
    )
    {
//        test_gate(true, 'generate__chart__objects__breakouts__task');
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=
        $breakout_tlines = [];
        $scr = new Screener();

        foreach ($last_values as $pair => $last_value)
        {
            $objects = Redis2::get(chart_objects_key($pair));

            foreach ($last_value as $period => $datum)
            {
                if(!is_array($objects[$period])) continue;

                foreach ($objects[$period] as &$tline)
                {
                    $is_breakout = false;

                    if($tline['is_fired'] || (!isset($tline['calc'][$datum['time']]) && ($tline['is_zone'] == 0))) continue;
                    if($tline['chart_tag'] != $tag) continue;
                    //==================

                    // check zone end time
                    if ($tline['is_zone'])
                    {
                        $datumTime = Carbon::parse(str_replace('.', '-', $datum['time']));
                        $endDate = Carbon::parse($tline['end_date']);

                        store_dump_var("generate__chart__objects__breakouts__task", [
                                'time' => now_local(),
                                'datum' => $datum,
                                '$tline' => $tline,
                                '$objects' => $objects,
                                '$datumTime' => $datumTime,
                                '$endDate' => $endDate,
                            ]);

//                        if($pair == "GBPJPY" )
//                        {
//
//                        }

                        if (!$endDate->gte($datumTime))
                        {
                            continue;
                        }
                    }
                    //==================
                    $calc_val = $tline['is_zone'] ? $tline['start_value'] : $tline['calc'][$datum['time']];

                    if($tline['trading_type'] == 'sell')
                    {
                        if($datum['val'] < $calc_val)
                        {
                            $is_breakout = true;
                        }
                    } else {
                        if($datum['val'] > $calc_val)
                        {
                            $is_breakout = true;
                        }
                    }

                    if($is_breakout)
                    {
                        $direction = $tline['trading_type'] == 'sell' ? 0 : 1;
//                        $screener_tag = ($tag == 'ohlc' ? 'ohlc' : 'indicator');
                        $breakout_tlines[] = $tline;

                        $tline['is_fired'] = 1;
                        $tline['fired_time'] = $datum['time'];
                        $tline['fired_value'] = $datum['val'];
                        $tline['fired_calc_value'] = $calc_val;
                        $event_tag = $tline['is_zone'] ? ($direction ? 'nh' : "nl") : 'tlb';

                        $resEvent = task(new create__event__task,
                            [
                                $pair,
                                $period,
                                $direction,
                                $event_tag,
                                $tag,
                                $tline,
                                (float)$calc_val
                            ]);

                        // update screener state and signals
                        $scr = new Screener();
                        $scr->set(
                            $resEvent->model->instrument,
                            (int) $resEvent->model->period,
                            'ohlc',
                            $event_tag,
                            $resEvent->model->direction,
                            [
                                'tline_id' => $tline['uniq_id']
                            ]
                        );

                        if ($tline['sl_type'] == 'dynamic_extremum')
                        {

                        }
                    }

                    // calc tline points
                    if ($tline['fired_value'])
                    {
                        if ($tline['trading_type'] == 'sell')
                        {
                            $points = $tline['fired_value'] - $datum['val'];
                        }
                        else
                        {
                            $points = $datum['val'] - $tline['fired_value'];
                        }

                        if ($points > 0)
                        {
                            $tline['points'] = (int)max($tline['points'], $points);
                        }
                    }
                }
            }

            $res = Redis2::set(chart_objects_key($pair), $objects);
        }

        return new Res(
            [
                'breakout_tlines' => $breakout_tlines
            ]
        );
    }
}
