<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Tasks\Task;
use App\VO\VoVal;

class create__order__closed__item__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (

    )
    {
        //test_gate(true, 'create__order__closed__item__task');
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=



        return new Res(
            [
                'res' => 1
            ]
        );
    }

}
