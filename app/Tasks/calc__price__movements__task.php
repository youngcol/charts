<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Facades\Redis2;
use App\Tasks\Task;
use App\VO\VoVal;

class calc__price__movements__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        array $last_values
    )
    {
//        test_gate(true, 'calc__price__movements__task');
//        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=
        $prices = [];
        foreach ($last_values as $symbol => $datum)
        {
            $prices[substr($symbol, 0,6)] = sprintf("%0.4f", $datum[1]['val']);
        }

        $prices_movements = Redis2::get('prices_movements', 1);
        if ($prices_movements)
        {

            $prices_movements['BTCUSD'] = $this->calc_BTC((float) $prices['BTCUSD'], $prices_movements['BTCUSD']);
            $prices_movements['XAUUSD'] = $this->calc_XAU((float) $prices['XAUUSD'], $prices_movements['XAUUSD']);
            $prices_movements['GBPJPY'] = $this->calc_JPY_symbols((float) $prices['GBPJPY'], $prices_movements['GBPJPY'], "Джиджей %s %s");
            $prices_movements['EURJPY'] = $this->calc_JPY_symbols((float) $prices['EURJPY'], $prices_movements['EURJPY'], "Евро ена %s %s");
//            $prices_movements['NZDJPY'] = $this->calc_JPY_symbols((float) $prices['NZDJPY'], $prices_movements['NZDJPY'], "Новозеландия ена  %s %s");
            $prices_movements['USDJPY'] = $this->calc_JPY_symbols((float) $prices['USDJPY'], $prices_movements['USDJPY'], "Ена  %s %s");

            $prices_movements['GBPUSD'] = $this->calc_USD_symbols((float) $prices['GBPUSD'], $prices_movements['GBPUSD'], "Фунт  %s %s %s");
            $prices_movements['GBPCHF'] = $this->calc_USD_symbols((float) $prices['GBPCHF'], $prices_movements['GBPCHF'], "Фунт франк  %s %s %s");
            $prices_movements['USDCAD'] = $this->calc_USD_symbols((float) $prices['USDCAD'], $prices_movements['USDCAD'], "Канада %s %s %s");
//            $prices_movements['EURUSD'] = $this->calc_USD_symbols((float) $prices['EURUSD'], $prices_movements['EURUSD'], "Евро %s %s %s");
//            $prices_movements['AUDUSD'] = $this->calc_USD_symbols((float) $prices['AUDUSD'], $prices_movements['AUDUSD'], "Австралия %s %s %s");
//            $prices_movements['NZDUSD'] = $this->calc_USD_symbols((float) $prices['NZDUSD'], $prices_movements['NZDUSD'], "Новозеландия %s %s %s");

            Redis2::set('prices_movements', $prices_movements);
        }

        return new Res(
            [
                'res' => 1
            ]
        );
    }

    protected function calc_USD_symbols(float $price, array $prev_movement, string $notify_str)
    {
        $step = 0.001;

        if ($prev_movement['price'] == 0)
        {
            return [
                'price' => $price
            ];
        }
        else
        {
            $delta = ($price - $prev_movement['price']);
            $prev_movement['delta'] = $delta;
            $prev_movement['current_price'] = $price;

            if (abs($delta) >= $step)
            {
                $prev_movement['price'] = $price;
                $str_price = (string) $price;

                $parts = explode('.', $str_price);

//                $s = sprintf($notify_str, $parts[0], substr($parts[1], 0, 2), substr($parts[1], 2, 2));
                $s = sprintf($notify_str, "","","");
//                $s = $notify_str;

                if ($delta > 0)
                {
                    $notify = $s . " растет";
                }
                else
                {
                    $notify = $s . " падает";
                }

                push_to_voice_queue($notify);
                $prev_movement['notify'] = $notify;

                $prev_movement['last_direction'] = $delta > 0 ? 1 : -1;
                $prev_movement['timestamp'] = now();
            }
        }

        return $prev_movement;
    }

    protected function calc_JPY_symbols(float $price, array $prev_movement, string $notify_str)
    {
        $step = 0.1;

        if ($prev_movement['price'] == 0)
        {
            return [
                'price' => $price
            ];
        }
        else
        {
            $delta = ($price - $prev_movement['price']);
            $prev_movement['delta'] = $delta;
            $prev_movement['current_price'] = $price;

            if (abs($delta) >= $step)
            {
                $prev_movement['price'] = $price;
                $str_price = (string) $price;

                $parts = explode('.', $str_price);
//                $s = sprintf($notify_str, $parts[0], substr($parts[1], 0, 2));
                $s = sprintf($notify_str, "", "");

                if ($delta > 0)
                {
                    $notify = $s . " растет";
                }
                else
                {
                    $notify = $s . " падает";
                }

                $prev_movement['last_direction'] = $delta > 0 ? 1 : -1;
                $prev_movement['timestamp'] = now();
                push_to_voice_queue($notify);
                $prev_movement['notify'] = $notify;
            }
        }

        return $prev_movement;
    }

    protected function calc_BTC(float $price, array $prev_movement)
    {
        $step = 300;

        if ($prev_movement['price'] == 0)
        {
            return [
                'price' => $price,
            ];
        }
        else
        {
            $delta = ($price - $prev_movement['price']);
            $prev_movement['delta'] = $delta;
            $prev_movement['current_price'] = $price;

            if (abs($delta) >= $step)
            {
                $prev_movement['price'] = $price;
                $str_price = (string) $price;

                if ($delta > 0)
                {
                    $p1 = substr($str_price, 0, 2);
                    $p2 = substr($str_price, 2, 3);
                    push_to_voice_queue("Биткоин растет");
                }
                else
                {
                    $p1 = substr($str_price, 0, 2);
                    $p2 = substr($str_price, 2, 3);
                    push_to_voice_queue("Биткоин падает");
                }

                $prev_movement['last_direction'] = $delta > 0 ? 1 : -1;
                $prev_movement['timestamp'] = now();
            }
        }

        return $prev_movement;
    }

    protected function calc_XAU(float $price, array $prev_movement)
    {
        $step = 1;

        if ($prev_movement['price'] == 0)
        {
            return [
                'price' => $price,
            ];
        }
        else
        {
            $delta = ($price - $prev_movement['price']);
            $prev_movement['delta'] = $delta;
            $prev_movement['current_price'] = $price;

            if (abs($delta) >= $step)
            {
                $prev_movement['price'] = $price;
                $str_price = (string) $price;

                if ($delta > 0)
                {
                    $p1 = substr($str_price, 0, 2);
                    $p2 = substr($str_price, 2, 2);
                    push_to_voice_queue("Золото $p1 $p2 растет");
                }
                else
                {
                    $p1 = substr($str_price, 0, 2);
                    $p2 = substr($str_price, 2, 2);
                    push_to_voice_queue("Золото $p1 $p2 падает");
                }

                $prev_movement['last_direction'] = $delta > 0 ? 1 : -1;
                $prev_movement['timestamp'] = now();
            }
        }

        return $prev_movement;
    }

}
