<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Tasks\Task;
use App\VO\VoVal;
use App\Facades\Redis2;

class get__timeseria__last__values__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        array $tags
    )
    {
        $values = Redis2::get(timeseria_last_values_key($tags[1]), 1);
        $candels = Redis2::get(timeseria_last_candels_key($tags[0]), 1);

        return new Res(
            [
                'candels' => $candels,
                'values' => $values
            ]
        );
    }
}
