<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Models\Event;
use App\Tasks\Task;
use App\VO\VoVal;

class get__events__history__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        int $limit,
        array $tags
    )
    {
        $grouped = [];
        $r = repo(Event::class);

        $items = $r
            //->whereNotIn('period', [])
            ->whereIn('tag', $tags)
            ->orderBy(['id' => 'desc'])
            ->limit($limit)
            ->get()
            ->toArray();

        $not_shown = array_values(array_filter($items, function ($i) {
            return $i['is_shown'] == 0;
        }));

        foreach ($items as $e)
        {
            array_create_key($grouped, $e['instrument']);
            $grouped[$e['instrument']][] = $e;
        }

        return new Res(
            [
                'not_shown_list' => $not_shown,
                'not_shown_counter' => count($not_shown),
                'list' => array_chunk($items, (int) $limit/4),
                'grouped' => $grouped,
            ]
        );
    }

}
