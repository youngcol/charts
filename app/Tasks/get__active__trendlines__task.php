<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Facades\Redis2;
use App\Tasks\Task;
use App\VO\VoVal;
use Carbon\Carbon;

class get__active__trendlines__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (

    )
    {
//        test_gate(true, 'get__active__trendlines__task');
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=



        return new Res(
            [
                'res' => 1
            ]
        );
    }

    public static function setIsActiveFlag(array $tlines)
    {
        $lastTime = Carbon::parse(Redis2::get("timeseries_updated_time", false));

        foreach($tlines as &$tline)
        {
            $tline['is_active'] = (int)$lastTime->lte(Carbon::parse(str_replace(".", '-', $tline['end_time_str'])));
        }

        return $tlines;
    }

}
