<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 *
 *
 */

use App\Common\Res;
use App\Tasks\Task;
use App\VO\VoVal;

class get__pair__screenshots__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        string $pair
    )
    {
//        test_gate(true, 'get__pair__screenshots__task');
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=

        $periods = settings('screenshots.periods');
        foreach ($periods as $period) {
//            $ret = cropScreenshot($pair, $tag, $period, 90, 100);
            $pic = getTagScreenshot('screenshots', $pair, '', $period);

//            removeOldScreenshots('screenshots_crops', $pair, $tag, $period, "90");
//            removeOldScreenshots('screenshots', $pair, $tag, $period);

            $pic['drawning'] = [
                'url' => drawning_url('', $pair, $period)
            ];

            $pic['_id'] = $pair."-".$period;
            $list[$period] = $pic;
        }

        return new Res(
            [
                'screenshots' => $list
            ]
        );
    }

}
