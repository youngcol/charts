<?php
declare(strict_types=1);
namespace App\Tasks\Oanda;

/**
 *
 *
 */

use App\Common\Res;
use App\Services\OandaClient_v20;
use App\Tasks\Task;
use App\VO\VoVal;
use Carbon\Carbon;

class download__order__book__for__instrument__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        VoVal $instrument,
        Carbon $date,
        int $pipsWindow
    )
    {
//        test_gate(true, 'download__order__book__for__instrument__task');
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=

        /** @var OandaClient_v20 $oanda */
        $oanda = service('oanda');
        $book = $oanda->getInsrumentOrderBook(strtoupper($instrument->_()), $date);

//        $stored = store_dump_var('orders_book_gbp_jpy', $book);
//        $book = load_dump_var('orders_book_gbp_jpy');
//        dump($book);echo 'download__order__book__for__instrument__task.php:46'; exit;

        if (!sp($book, 'errorMessage'))
        {
            $bookPrice = (float)$book['orderBook']['price'];
            $pipsDelimeter = $book['orderBook']['price'][1] == '.' ? 10000 : 100;

            $lowPrice = $bookPrice - $pipsWindow / $pipsDelimeter;
            $highPrice = $bookPrice + $pipsWindow / $pipsDelimeter;
            $indexes = ['low' => 0, 'high' => 0, 'current' => 0];

            $book['pipsWindow'] = $pipsWindow;
            $book['lowPrice'] = $lowPrice;
            $book['highPrice'] = $highPrice;

            $prices = array_pluck($book['orderBook']['buckets'], 'price');

            foreach ($prices as $index => $price)
            {
                if ($price == $bookPrice) {
                    $indexes['current'] = $index;
                }

                if ((float)$price >= $lowPrice && $indexes['low'] == 0)
                {
                    $indexes['low'] = $index;
                }

                if ((float)$price >= $highPrice)
                {
                    $indexes['high'] = $index;
                    break;
                }
            }

            $spliced = array_reverse(array_splice($book['orderBook']['buckets'], $indexes['low'], $indexes['high'] - $indexes['low']));
            $book['orderBook']['buckets'] = $spliced;

            $halfPart = count($spliced) / 2;
            $book['orderBook']['high_buckets'] = array_splice($spliced, 0, $halfPart);

            $spliced = $book['orderBook']['buckets'];
            $book['orderBook']['low_buckets'] = array_splice($spliced, $halfPart, $halfPart);
        }

        return new Res(
            [
                'book' => $book
            ]
        );
    }

}
