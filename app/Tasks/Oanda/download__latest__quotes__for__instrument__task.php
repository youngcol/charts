<?php
declare(strict_types=1);
namespace App\Tasks\Oanda;

/**
 *
 *
 */

use App\Common\Res;
use App\Tasks\Task;
use App\VO\VoVal;

class download__latest__quotes__for__instrument__task extends Task
{
    public function __construct()
    {
        parent::__construct();

        $this->gates = function ($item) {

            test_gate();
        };
    }

    protected function gates
    (

    )
    {

        //-=-=-=-=-=-=-=-=-=-=-=-=
        return [resOk([

        ]),


        ] ;
        }

    public function run
    (
        VoVal $instrument,
        string $timeframe
    )
    {
        list(
            $resGates
        ) = $this->gates
            (

            );
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=

//        $timeframes = ['M15', 'M1'];

        $reses = [];
//        foreach ($timeframes as $timeframe)
//        {
            do
            {
                $res = task(new download__instrument__quotes__task,
                    [
                        $instrument,
                        vo($timeframe, 'oanda.timeframe')
                    ]
                );

            } while($res->ok() && $res->count >= 500);

            $reses[] = $res;
//        }

        return new Res(
            [
                'res' => $reses
            ]
        );
    }

}
