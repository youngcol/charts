<?php
declare(strict_types=1);
namespace App\Tasks\Oanda;

/**
 *
 *
 */

use App\Common\Res;
use App\Models\Quote;
use App\Repositories\QuotesRepository;
use App\Tasks\Task;
use App\VO\VoVal;
use Carbon\Carbon;
use Illuminate\Support\Arr;

/**
 * Class download__instrument__quotes__task
 *
 * Downloads missed candles from head or tail
 *
 * @package App\Tasks\Oanda
 */
class download__instrument__quotes__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        VoVal $instrument,
        VoVal $oandaTimeframe,
        bool $isLoadFuture = true
    )
    {
        list(
            $resGates
        ) = $this->gates
            (

            );
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=

        $repoQuotes = new QuotesRepository();
        $tableName = Quote::getQuotesTable($instrument, $oandaTimeframe);
//        dump($tableName);echo 'download__instrument__quotes__task.php:49'; exit;
        $repoQuotes->setTable1($tableName);

        if ($isLoadFuture)
        {
            $lastQuote = $repoQuotes->orderBy(['date' => 'desc'])->first();
        }
        else
        {
            $lastQuote = $repoQuotes->orderBy(['date' => 'asc'])->first();
        }

        $store = null;
        $fromDate = eN($lastQuote) ? Carbon::now() : $lastQuote->date;
        $par = ['granularity' => $oandaTimeframe->_()];
        if ($isLoadFuture)
        {
            $par['from'] = $fromDate->toRfc3339String();
        }
        else
        {
            $par['to'] = $fromDate->toRfc3339String();
        }

        $oanda = service('oanda');
        $retCandles = $oanda->getInstrumentCandles(strtoupper($instrument->_()), $par);

        log_tag('download__latest__instrument__quotes__task', 'oanda candles', [
            'par' => $par,
            'count' => count($retCandles['candles']),
            'last' => Arr::last($retCandles['candles']),
            'first' => Arr::first($retCandles['candles'])
        ]);

        $store = $repoQuotes->storeCandles($retCandles['candles']);

        return new Res(
            [
                'oandaTimeframe' => $oandaTimeframe,
                'count'         => count($retCandles['candles']),
//                'store'         => $store
            ]
        );
    }

    /**
    * @return array
    * @throws \App\Common\GateException
    */
    protected function gates
    (

    )
    {


//        test_gate(true, 'download__latest__instrument__quotes__task');



        //-=-=-=-=-=-=-=-=-=-=-=-=
        return [resOk([

        ]),


        ] ;
    }
}
