<?php
declare(strict_types=1);
namespace App\Tasks\Oanda;

/**
 *
 *
 */

use App\Common\Res;
use App\Tasks\download__quotes__from__oanda__task;
use App\Tasks\Task;
use App\VO\VoVal;

class download__candles__for__all__instruments__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    protected function gates
    (

    )
    {

//        test_gate(true, 'download__candles__for__all__instruments__task');

        //-=-=-=-=-=-=-=-=-=-=-=-=
        return [resOk([

        ]),


        ] ;
        }

    public function run
    (

    )
    {
        list(
            $resGates
        ) = $this->gates
            (

            );
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=

        $instruments = explode(',', settings('instruments'));

        $reses = [];
        $times = 4;

        $timeframes = ['M15', 'M1', 'S10'];
        foreach ($instruments as $instrument)
        {
            foreach ($timeframes as $timeframe)
            {
                do
                {
                    $res = task(new download__instrument__quotes__task,
                        [
                            vo($instrument, 'instruments'),
                            vo($timeframe, 'oanda.timeframe'),
                        ]
                    );
                } while($res->ok() && $res->count >= 500);
            }

            $reses[] = $res;
        }

        return new Res(
            [
                'res' => $reses
            ]
        );
    }

}
