<?php


use App\Facades\CU;
use App\Tasks\ForexCore\run__platform__command__task;
use App\Tasks\SocialEngine\GetPinterestAccountsBoardsTask;

$params = [

    'command' => 'required|string',
    'params' => 'array',
];

return [
// only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {

        $res = task(new run__platform__command__task,
            [
                $params['command']->_(),
                $params['params']->toArray(),
            ]
        );

        return $res;
    }
];
