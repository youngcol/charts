<?php


use App\Facades\CU;
use App\Tasks\Oanda\download__latest__quotes__for__instrument__task;
use App\Tasks\SocialEngine\GetPinterestAccountsBoardsTask;

$params = [

    'instrument' => 'required|instruments',

];

return [
// only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {

        $res = task(new download__latest__quotes__for__instrument__task,
            [
                $params['instrument']
            ]
        );

        return $res;
    }
];
