<?php
use App\Tasks\save__hline__task;

$params = [
    'id' => 'numeric',
    'value' => 'required|numeric',
    'is_trading'  => 'required|numeric',
    'period'  => 'required|numeric',
    'pair' => 'required|string',
    'type' => 'string',
    'params' => 'array',
];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new save__hline__task,
        [
            $params['id']->_(),
            $params['pair']->_(),
            $params['type']->_(),
            (int)$params['period']->_(),
            (float)number_format($params['value']->_(), 4),
            (int)$params['is_trading']->_(),
            $params['params']->toArray(),
        ]
        );

        return $res;
    }
];
