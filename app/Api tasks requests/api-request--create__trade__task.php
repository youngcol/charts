<?php

$params = [

    'id' => 'required|numeric',
    'accounts' => 'required|ids.array',
    'accounts' => 'required|string',
    'params' => 'required|array',
];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new create__trade__task,
        [
            (int)$params['accounts']->_(),
            $params['accounts']->_(),
            $params['params']->toArray(),
        ]
        );

        return $res;
    }
];
