<?php


use App\Facades\CU;
use App\Tasks\SocialEngine\GetPinterestAccountsBoardsTask;
use App\Tasks\udpate__timeline__task;

$params = [

    'id' => 'required|numeric',
    'chart_groups_sorting' => 'required:string'

];

return [
// only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {

        $res = task(new udpate__timeline__task,
            [
                CU::user(),
                $params['id']->_(),
                $params['chart_groups_sorting']->_(),

            ]
        );

        return $res;
    }
];
