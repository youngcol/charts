<?php
use App\Tasks\remove__trainer__tlines__task;

$params = [

//    'id' => 'required|numeric',
//    'accounts' => 'required|ids.array',
    'pair' => 'required|string',
//    'params' => 'required|array',
];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new remove__trainer__tlines__task,
        [
            $params['pair']->_(),
        ]
        );

        return $res;
    }
];
