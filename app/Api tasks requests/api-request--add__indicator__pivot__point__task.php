<?php
use App\Tasks\add__indicator__pivot__point__task;

$params = [

    'period' => 'required|numeric',
    'value' => 'required|numeric',
    'direction' => 'required|numeric',
    'symbol' => 'required|string',

];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new add__indicator__pivot__point__task,
        [
            $params['symbol']->_(),
            (int)$params['period']->_(),
            (float)$params['value']->_(),
            (int)$params['direction']->_()
        ]
        );

        return $res;
    }
];
