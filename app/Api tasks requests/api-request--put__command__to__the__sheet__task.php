<?php


use App\Facades\CU;
use App\Tasks\ForexCore\put__command__to__the__sheet__task;
use App\Tasks\SocialEngine\GetPinterestAccountsBoardsTask;

$params = [

    'name' => 'required|forex.commands',
    'params' => '',
];

return [
// only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new put__command__to__the__sheet__task,
            [
                $params['name'],
                $params['params']->toArray()
            ]
        );

        return $res;
    }
];
