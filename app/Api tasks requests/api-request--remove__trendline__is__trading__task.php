<?php
use App\Tasks\remove__trendline__is__trading__task;

$params = [
    'uniq_id' => 'required|string',
    'pair' => 'required|string',
    'period' => 'required|numeric',
];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new remove__trendline__is__trading__task,
        [
            $params['uniq_id']->_(),
            $params['pair']->_(),
            (int)$params['period']->_(),
        ]
        );

        return $res;
    }
];
