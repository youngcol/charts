<?php
use App\Tasks\get__watch__feed__task;

$params = [

//    'id' => 'required|numeric',
//    'accounts' => 'required|ids.array',
//    'accounts' => 'required|string',
//    'params' => 'required|array',
];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new get__watch__feed__task,
        [
           
        ]
        );

        return $res;
    }
];
