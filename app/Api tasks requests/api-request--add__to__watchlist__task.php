<?php


use App\Facades\CU;
use App\Tasks\add__to__watchlist__task;
use App\Tasks\SocialEngine\GetPinterestAccountsBoardsTask;
use App\VO\VoVal;

$params = [

    'instrument' => 'required',
    'direction' => 'required',
    'period' => 'required',

];

return [
// only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {

        $res = task(new add__to__watchlist__task,
            [
                $params['instrument'],
                $params['direction'],
                (int)$params['period']->_()
            ]
        );

        return $res;
    }
];
