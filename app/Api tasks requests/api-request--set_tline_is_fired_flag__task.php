<?php

use App\Tasks\set_tline_is_fired_flag__task;

$params = [
//
    'id' => 'required|numeric',
//    'accounts' => 'required|ids.array',
//    'accounts' => 'required|string',
//    'params' => 'required|array',
];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new set_tline_is_fired_flag__task,
        [
            (int)$params['id']->_(),
        ]
        );

        return $res;
    }
];
