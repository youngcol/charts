<?php
use App\Tasks\calc__trainer__tlines__task;

$params = [

    'depo' => 'required|numeric',
//    'accounts' => 'required|ids.array',
    'pair' => 'required|string',
//    'params' => 'required|array',
];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new calc__trainer__tlines__task,
        [
            $params['pair']->_(),
            (int)$params['depo']->_(),
        ]
        );

        return $res;
    }
];
