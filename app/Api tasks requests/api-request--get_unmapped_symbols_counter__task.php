<?php
use App\Tasks\get_unmapped_symbols_counter__task;

$params = [

    'id' => 'required|numeric',
    'accounts' => 'required|ids.array',
    'accounts' => 'required|string',
    'params' => 'required|array',
];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new get_unmapped_symbols_counter__task,
        [
            (int)$params['id']->_(),
            $params['accounts']->_(),
            $params['params']->toArray(),
        ]
        );

        return $res;
    }
];
