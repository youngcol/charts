<?php

use App\Tasks\get__deals__feed__task;

$params = [


];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new get__deals__feed__task,
        [

        ]
        );

        return $res;
    }
];
