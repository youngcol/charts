<?php


use App\Facades\CU;
use App\Tasks\Network\SendEmailTask;
use App\VO\EmailAddress;


$params = [
    'subject' => 'required|filled.string',
    'message' => 'required|filled.string',
    'to_emails' => 'required|array',
];

return [
// only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params) {

        $toEmails = [];

        foreach ($params['to_emails']->toArray() as $email)
        {
            $toEmails[] = new EmailAddress($email);
        }

        $res = task(new SendEmailTask(),
            [
                $toEmails,
                vo('custom'),
                $params['subject'],
                $params['message']
            ]
        );

        return $res;
    }
];
