<?php


use App\Facades\CU;
use App\Tasks\load__chart__group__data__task;
use App\Tasks\SocialEngine\GetPinterestAccountsBoardsTask;

$params = [

    'group_id' => 'required|numeric',


];

return [
// only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {

        $res = task(new load__chart__group__data__task,
            [
                CU::user(),
                $params['group_id'],
                null
            ]
        );

        return $res;
    }
];
