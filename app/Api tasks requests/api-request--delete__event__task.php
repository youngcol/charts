<?php
use App\Tasks\delete__event__task;

$params = [

    'id' => 'required|numeric',
];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new delete__event__task,
        [
            (int)$params['id']->_(),
        ]
        );

        return $res;
    }
];
