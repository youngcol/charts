<?php
use App\Tasks\get__big__charts__marks__task;

$params = [

    'pair' => 'required|numeric',
//    'accounts' => 'required|ids.array',
//    'tag' => 'required|string',
//    'params' => 'required|array',
];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new get__big__charts__marks__task,
        [
            $params['pair']->_(),
        ]
        );

        return $res;
    }
];
