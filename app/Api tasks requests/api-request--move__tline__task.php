<?php

use App\Tasks\init__screener__state__task;
use App\Tasks\move__tline__task;
use App\Tasks\ForexCore\put__command__to__the__sheet__task;


$params = [
    'uniq_id' => 'required|string',
    'pair' => 'required|string',
    'period' => 'required|numeric',
    'start_date' => 'required|datetime',
    'end_date' => 'required|datetime',
    'start_value' => 'required|numeric',
    'end_value' => 'required|numeric',
];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new move__tline__task,
        [
            $params['pair']->_(),
            (int)$params['period']->_(),
            $params['uniq_id']->_(),
            $params['start_date']->_(),
            $params['end_date']->_(),
            (float)$params['start_value']->_(),
            (float)$params['end_value']->_()
        ]
        );

        $timeseria = \App\Models\ChartTrendline::getCandelsTimeArray(
            $res->tline['start_date'],
            $res->tline['end_date'],
            (int)$params['period']->_()
        );

        if($res->tline['is_zone'] == 0)
        {
            task(new put__command__to__the__sheet__task,
                [
                    vo("CALC_TLINE_VALUES", "forex.commands"),
                    [
                        'symbol' => $params['pair']->_(),
                        'uniq_id' => $res->tline['uniq_id'],
                        'start_date' => str_replace("-", ".", $res->tline['start_date']),
                        'start_value' => $res->tline['start_value'],
                        'end_date' => str_replace("-", ".", $res->tline['end_date']),
                        'end_value' => $res->tline['end_value'],
                        'period' => (int)$params['period']->_(),
                        'window' => $res->tline['chart_tag'] == 'ohlc' ? 0 : 1,
                        'timeseria' => implode(",", $timeseria)
                    ]
                ]
            );
        }

        task(new init__screener__state__task,
            [
                $params['pair']->_(),
                (int)$params['period']->_()
            ]
        );

        return $res;
    }
];
