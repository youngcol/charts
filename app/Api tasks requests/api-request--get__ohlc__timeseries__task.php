<?php
use App\Tasks\get__ohlc__timeseries__task;
use App\Tasks\get__hlines__task;
use App\Tasks\get__timeseria__task;
use App\Tasks\Charts\get__trendlines__task;

$params = [

    'period' => 'required|numeric',
    'history' => 'required|numeric',
    'instrument' => 'required|string'
];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new get__ohlc__timeseries__task,
            [
                [$params['instrument']->_()],
                (int)$params['period']->_(),
                (int)$params['history']->_(),
                true
            ]
        );

        $resTLines = task(new get__trendlines__task,
            [
                [$params['instrument']->_()],
                ['ohlc', 'cci'],
                false,
                false
            ]
        );

        $resRsx = task(new get__timeseria__task, [
            [$params['instrument']->_()],
            (int)$params['period']->_(),
            (int)$params['history']->_(),
            'cci'
        ]);

        $res->rsx = $resRsx;
        $res->tlines = $resTLines->list[$params['instrument']->_()];
        $res->tlines_hlines = $resTLines->now_vals_on_tline;
        return $res;
    }
];
