<?php


use App\Facades\CU;
use App\Tasks\ForexCore\get__status__task;
use App\Tasks\SocialEngine\GetPinterestAccountsBoardsTask;

$params = [

//    'accounts' => 'required|ids.array',

];

return [
// only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {

        $res = task(new get__status__task,
            [
            ]
        );

        return $res;
    }
];
