<?php
use App\Tasks\mark__event__as__shown__task;

$params = [

    'id' => 'required|numeric',
//    'accounts' => 'required|ids.array',
//    'accounts' => 'required|string',
//    'params' => 'required|array',
];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new mark__event__as__shown__task,
        [
            (int)$params['id']->_(),
//            $params['accounts']->_(),
//            $params['params']->toArray(),
        ]
        );

        return $res;
    }
];
