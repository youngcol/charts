<?php

use App\Tasks\init__screener__state__task;
use App\Tasks\remove__symbol__tlines__task;
use App\Tasks\delete__screener__state__task;

$params = [
    'pair' => 'required|string',
    'period' => 'required|numeric',
    'chart_tag' => 'required|string',
];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $pair = $params['pair']->_();

        $res = task(new remove__symbol__tlines__task,
        [
            $pair,
            $params['period']->_(),
            $params['chart_tag']->_(),
        ]
        );

        task(new init__screener__state__task,
            [
                $pair,
                (int)$params['period']->_()
            ]
        );

        foreach(settings('screener_periods') as $period)
        {
            task(new delete__screener__state__task,
                [
                    $pair,
                    $period,
                    "tlb",
                ]
            );
            task(new delete__screener__state__task,
                [
                    $pair,
                    $period,
                    "hzone",
                ]
            );
        }

        return $res;
    }
];
