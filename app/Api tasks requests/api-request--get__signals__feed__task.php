<?php
use App\Tasks\Screenshots\get__signals__feed__task;


$params = [

    'mode' => 'required|string',
    'params' => 'array',
    //'from_sr_zone_id' => 'required|numeric',
//    'accounts' => 'required|string',

];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new get__signals__feed__task,
        [
                $params['mode']->_(),
                $params['params']->toArray(),
        ]
        );

        return $res;
    }
];
