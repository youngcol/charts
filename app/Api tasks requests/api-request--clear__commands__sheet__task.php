<?php

use App\Tasks\clear__commands__sheet__task;

$params = [

//    'accounts' => 'required|ids.array',
//    'accounts' => 'required|string',

];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new clear__commands__sheet__task,
        [

        ]
        );

        return $res;
    }
];
