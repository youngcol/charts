<?php


use App\Facades\CU;
use App\Tasks\Oanda\download__order__book__for__instrument__task;
use Carbon\Carbon;

$params = [
    'instrument' => 'required|instruments',
    'date' => 'required|datetime',
    'pips_window' => 'required|numeric'
];

return [
// only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        strpos(null, '1');
        $res = task(new download__order__book__for__instrument__task,
            [
                $params['instrument'],
                new Carbon($params['date']->_()),
                (int)$params['pips_window']->_(),
            ]
        );

        return $res;
    }
];
