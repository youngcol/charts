<?php
use App\Tasks\remove__screenshot__drawning__task;

$params = [
    'instrument' => 'required|string',
    'period' => 'required|numeric',
//    'pic_view' => 'required|string',
];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new remove__screenshot__drawning__task,
            [
                '',
                $params['instrument']->_(),
                (int)$params['period']->_(),
            ]
        );

        return $res;
    }
];
