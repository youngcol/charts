<?php
use App\Tasks\remove__trendline__is__trading__task;

$params = [
    'id' => 'required|numeric',
];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new remove__trendline__is__trading__task,
        [
            (int)$params['id']->_(),
        ]
        );

        return $res;
    }
];
