<?php


$params = [

    'accounts' => 'required|ids.array',

];

return [
// only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {

        $res = task(new GetPinterestAccountsBoardsTask,
            [
                $params['accounts']->toArray()
            ]
        );

        return $res;
    }
];
