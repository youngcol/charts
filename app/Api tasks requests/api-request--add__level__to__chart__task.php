<?php


use App\Facades\CU;
use App\Models\User;
use App\Tasks\Charts\add__level__to__chart__task;
use App\Tasks\SocialEngine\GetPinterestAccountsBoardsTask;
use App\VO\VoVal;

$params = [

    'chart_id' => 'required|numeric',
    'value' => 'required|numeric',
    'is_fired' => 'required|numeric',
    'type' => 'required|chart.levels.type',
    'level_id' => 'numeric',
];

return [
// only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {

//        dump((float)number_format($params['price']->_(), 4));echo 'api-request--add__level__to__chart__task.php:26'; exit;
        $res = task(new add__level__to__chart__task,
            [
                CU::user(),
                (int)$params['chart_id']->_(),
                $params['level_id']->_(),
                (float)number_format($params['value']->_(), 4),
                $params['is_fired']->_(),
                $params['type']
            ]
        );

        return $res;
    }
];
