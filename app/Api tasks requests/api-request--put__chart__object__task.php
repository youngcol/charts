<?php
use App\Tasks\put__chart__object__task;

$params = [

    'symbol' => 'required|string',
    'period' => 'required|numeric',
    'object_type' => 'required|string',
    'object_name' => 'required|string',

    'start_time' => 'required|numeric',
    'end_time' => 'required|numeric',
    'start_time_str' => 'required|string',
    'end_time_str' => 'required|string',
    'price' => 'required|numeric',
    'price2' => 'required|numeric',
    'calc' => 'array',
    'type' => 'required|string',
    'chart_tag' => 'required|string',
    'force_risk' => 'required|numeric',
    'force_sl' => 'required|numeric',
];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new put__chart__object__task,
        [
            $params['symbol']->_(),
            (int)$params['period']->_(),
            $params['object_type']->_(),
            $params['object_name']->_(),
            $params['start_time']->_(),
            $params['end_time']->_(),
            $params['start_time_str']->_(),
            $params['end_time_str']->_(),
            (float)$params['price']->_(),
            (float)$params['price2']->_(),
            $params['type']->_(),
            $params['calc']->toArray(),
            $params['chart_tag']->_(),
            (int)$params['force_risk']->_(),
            (int)$params['force_sl']->_(),
        ]
        );

//        if ($res->tline['is_trading'])
//        {
//            $text = " плюс торговая область";
//            push_to_voice_queue($text);
//        }
//        else
//        {
//            $text = "да";
//        }
//

        return $res;
    }
];
