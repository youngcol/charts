<?php

use App\Common\Res;
use App\Tasks\get__hlines__task;
use App\Tasks\get__timeseria__task;

$params = [
    'pair' => 'required|string',
    'period' => 'required|numeric',
    'limit' => 'required|numeric',
    'tag' => 'required|string',
];

return [
// only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {

        $res = task(new get__timeseria__task,
            [
                $params['pair']->_(),
                (int) $params['period']->_(),
                (int) $params['limit']->_(),
                $params['tag']->_(),
            ]
        );

        $tlines = task(new get__trendlines__task,
            [
                    [$params['pair']->_()],
                    ['ohlc'],
                    (int)$params['period']->_(),
                    true
            ]
        );


        return new Res(
            [
                'timeseria' => $res,
                'tlines' => $tlines,
            ]
        );;
    }
];
