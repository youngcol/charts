<?php
use App\Tasks\get__chart__objects__task;

$params = [
    'period' => 'required|numeric',
    'symbol' => 'required|string',
    'tag' => 'required|string',
];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new get__chart__objects__task,
        [
            $params['symbol']->_(),
            (int)$params['period']->_(),
            $params['tag']->_(),
        ]
        );

        return $res;
    }
];
