<?php


use App\Facades\CU;
use App\Tasks\Charts\delete__level__from__chart__task;
use App\Tasks\SocialEngine\GetPinterestAccountsBoardsTask;

$params = [

    'chart_id' => 'required|numeric',
    'level_id' => 'required|numeric',

];

return [
// only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {

        $res = task(new delete__level__from__chart__task,
            [
                CU::user(),
                $params['chart_id']->_(),
                $params['level_id']->_(),
            ]
        );

        return $res;
    }
];
