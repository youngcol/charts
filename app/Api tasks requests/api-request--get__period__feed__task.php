<?php
use App\Tasks\get__period__feed__task;

$params = [

    'period' => 'required|numeric',
    'history' => 'required|numeric',
//    'accounts' => 'required|ids.array',
//    'accounts' => 'required|string',
//    'params' => 'required|array',
];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new get__period__feed__task,
        [
            array_flatten(settings('pairs_list')),
            (int)$params['period']->_(),
            (int)$params['history']->_()
        ]
        );

        return $res;
    }
];
