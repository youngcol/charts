<?php
use App\Tasks\get__closed__orders__task;

$params = [

//    'id' => 'required|numeric',
//    'accounts' => 'required|ids.array',
//    'accounts' => 'required|string',
//    'params' => 'required|array',
];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new get__closed__orders__task,
        [
  
        ]
        );

        return $res;
    }
];
