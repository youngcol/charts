<?php
use App\Tasks\get__renko__ohlc__timeseria__task;

$params = [

    'id' => 'required|numeric',
    'accounts' => 'required|ids.array',
    'accounts' => 'required|string',
    'params' => 'required|array',
];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new get__renko__ohlc__timeseria__task,
        [
            (int)$params['id']->_(),
            $params['accounts']->_(),
            $params['params']->toArray(),
        ]
        );

        return $res;
    }
];
