<?php

use App\Tasks\toggle__pair__to__fav__task;

$params = [

    'pair' => 'required|string',
    'period' => 'required|numeric',

];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new toggle__pair__to__fav__task,
        [
            $params['pair']->_(),
            (int)$params['period']->_(),
        ]
        );

        return $res;
    }
];
