<?php


use App\Facades\CU;
use App\Tasks\create__chart__group__task;
use App\Tasks\create__chart__task;
use App\Tasks\SocialEngine\GetPinterestAccountsBoardsTask;

$params = [

    'instrument'    => 'required|instruments',
    'name'          => 'filled.string'

];

return [
// only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {


        $res = task(new create__chart__task,
            [
                CU::user(),
                $params['instrument'],
                vo('1', 'chart.timeframe'),
                vo('M', 'chart.timeframe.type'),
                vo('default', 'chart.view'),
                vo(100, 'chart.scale'),
                null
            ]
        );


        $res = task(new create__chart__group__task,
            [
                CU::user(),
                vo(strtoupper($params['instrument']->_()) . ' ' . $params['name']->_()),
                vo([$res->chart->id], 'ids.array'),
                vo(date_mysql_format(now_date()), 'datetime')
            ]
        );

        return $res;
    }
];
