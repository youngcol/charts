<?php
use App\Tasks\get__pair__screenshots__task;

$params = [

//    'id' => 'required|numeric',
//    'accounts' => 'required|ids.array',
    'pair' => 'required|string',
//    'params' => 'required|array',
];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new get__pair__screenshots__task,
        [
            $params['pair']->_(),
        ]
        );

        return $res;
    }
];
