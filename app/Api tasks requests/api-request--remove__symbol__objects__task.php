<?php
use App\Tasks\remove__symbol__objects__task;

$params = [

//    'id' => 'required|numeric',
//    'accounts' => 'required|ids.array',
    'symbol' => 'required|string',
//    'params' => 'required|array',
];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new remove__symbol__objects__task,
        [
            $params['symbol']->_(),
        ]
        );

        return $res;
    }
];
