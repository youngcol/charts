<?php
use App\Tasks\save__chart__trading__params__task;

$params = [
    'be' => 'required|numeric',
    'sl' => 'required|numeric',
    'risk' => 'required|numeric',
    'period' => 'required|numeric',
    'pair' => 'required|string',
];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new save__chart__trading__params__task,
        [
            $params['pair']->_(),
            (int)$params['period']->_(),
            (int)$params['sl']->_(),
            (int)$params['be']->_(),
            (int)$params['risk']->_(),
        ]
        );

        return $res;
    }
];
