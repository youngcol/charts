<?php

use App\Tasks\get__pair__overview__task;


$params = [

    'period' => 'required|numeric',
    'pair' => 'required|string',

];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new get__pair__overview__task,
            [
                $params['pair']->_(),
                (int)$params['period']->_(),
            ]
        );

        return $res;
    }
];
