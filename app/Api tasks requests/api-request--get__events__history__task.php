<?php
use App\Tasks\get__events__history__task;

$params = [
    'limit' => 'required|numeric',
];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new get__events__history__task,
        [
            (int)$params['limit']->_(),
        ]
        );

        return $res;
    }
];
