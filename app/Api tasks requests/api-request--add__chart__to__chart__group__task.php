<?php


use App\Facades\CU;
use App\Tasks\add__chart__to__chart__group__task;
use App\Tasks\SocialEngine\GetPinterestAccountsBoardsTask;

$params = [

    'chartGroupId'  => 'required|numeric',
    'instrument'    => 'required|instruments',
    'timeframe'     => 'required|chart.timeframe',
    'timeframeType' => 'required|chart.timeframe.type',
    'view'          => 'required|chart.view',

];

return [
// only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {

        $res = task(new add__chart__to__chart__group__task,
            [
                CU::user(),
                $params['chartGroupId'],
                $params['instrument'],
                $params['timeframe'],
                $params['timeframeType'],
                $params['view'],
            ]
        );

        return $res;
    }
];
