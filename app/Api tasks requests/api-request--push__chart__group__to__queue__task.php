<?php


use App\Facades\CU;
use App\Tasks\push__chart__group__to__queue__task;
use App\Tasks\SocialEngine\GetPinterestAccountsBoardsTask;

$params = [

    'groupId' => 'required|numeric',
    'delta' => 'required|numeric',

];

return [
// only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {

        $res = task(new push__chart__group__to__queue__task,
            [
                CU::user(),
                $params['groupId'],
                $params['delta'],
            ]
        );

        return $res;
    }
];
