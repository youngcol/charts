<?php


use App\Facades\CU;
use App\Tasks\get__popup__timeline__task;
use App\Tasks\SocialEngine\GetPinterestAccountsBoardsTask;

$params = [

    'timelineId' => 'required|numeric',

];

return [
// only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {

        $res = task(new get__popup__timeline__task,
            [
                CU::user(),
                $params['timelineId']
            ]
        );

        return $res;
    }
];
