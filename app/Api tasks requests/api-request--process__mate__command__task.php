<?php

$params = [

    'accounts' => 'required|ids.array',
    'accounts' => 'required|string',

];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new process__mate__command__task,
        [
            $params['accounts']->toArray(),
            $params['accounts']->_(),
            (int)$params['accounts']->_(),
        ]
        );

        return $res;
    }
];
