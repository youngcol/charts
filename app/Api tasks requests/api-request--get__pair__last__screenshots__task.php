  <?php


use App\Facades\CU;
  use App\Tasks\Screenshots\get__pair__last__screenshots__task;
  use App\Tasks\SocialEngine\GetPinterestAccountsBoardsTask;

$params = [

    'instrument' => 'required|filled.string',

];

return [
// only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {

        $res = task(new get__pair__last__screenshots__task,
            [
                $params['instrument']
            ]
        );

        return $res;
    }
];
