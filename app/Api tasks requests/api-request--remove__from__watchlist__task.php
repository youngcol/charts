<?php


use App\Facades\CU;
use App\Tasks\remove__from__watchlist__task;
use App\Tasks\SocialEngine\GetPinterestAccountsBoardsTask;

$params = [

    'id' => 'required|numeric',

];

return [
// only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new remove__from__watchlist__task,
            [
                $params['id']->_()
            ]
        );

        return $res;
    }
];
