<?php
use App\Tasks\get__nodata__charts__task;

$params = [

//    'accounts' => 'required|ids.array',
//    'accounts' => 'required|string',

];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new get__nodata__charts__task,
        [
            
        ]
        );

        return $res;
    }
];
