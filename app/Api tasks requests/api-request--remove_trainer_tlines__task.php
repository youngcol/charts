<?php

$params = [

    'id' => 'required|numeric',
    'accounts' => 'required|ids.array',
    'accounts' => 'required|string',
    'params' => 'required|array',
];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new remove_trainer_tlines__task,
        [
            (int)$params['id']->_(),
            $params['accounts']->_(),
            $params['params']->toArray(),
        ]
        );

        return $res;
    }
];
