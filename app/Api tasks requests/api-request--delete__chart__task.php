<?php


use App\Facades\CU;
use App\Tasks\delete__chart__task;
use App\Tasks\SocialEngine\GetPinterestAccountsBoardsTask;

$params = [

    'chartId' => 'required|numeric'

];

return [
// only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {

        $res = task(new delete__chart__task,
            [
                CU::user(),
                $params['chartId']
            ]
        );

        return $res;
    }
];
