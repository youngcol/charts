<?php
use App\Tasks\get__trendline__task;

$params = [

    'uniq_id' => 'required|string',
    'pair' => 'required|string',
    'period' => 'required|numeric',
];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new get__trendline__task,
        [
            $params['uniq_id']->_(),
            $params['pair']->_(),
            (int)$params['period']->_()
        ]
        );

        return $res;
    }
];
