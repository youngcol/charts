<?php


use App\Facades\CU;
use App\Tasks\Charts\delete__trendline__from__chart__task;
use App\Tasks\SocialEngine\GetPinterestAccountsBoardsTask;
use App\Tasks\remove__tline__from__screener__state__task;

$params = [

    'id' => 'required|numeric',
];

return [
// only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {

        $res = task(new delete__trendline__from__chart__task,
            [
                (int) $params['id']->_()
            ]
        );

        task(new remove__tline__from__screener__state__task,
            [
                $res->tline
            ]
        );


        return $res;
    }
];
