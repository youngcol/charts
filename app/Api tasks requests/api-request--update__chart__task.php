<?php

use App\Facades\CU;
use App\Tasks\update__chart__task;

$params = [
    'chartId' => 'required|numeric',
    'timeframe' => 'numeric',
    'scale' => 'numeric',
    'view' => 'chart.view',
    'indicators' => 'string',
    'timeframeType' => 'chart.timeframe.type',
];

return [
// only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {

        $res = task(new update__chart__task,
            [
                CU::user(),
                $params['chartId'],
                $params['view'],
                $params['timeframe'],
                $params['scale'],
                $params['indicators'],
                $params['timeframeType'],
            ]
        );

        return $res;
    }
];
