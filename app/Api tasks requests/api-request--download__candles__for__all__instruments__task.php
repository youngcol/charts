<?php


use App\Facades\CU;
use App\Tasks\Oanda\download__candles__for__all__instruments__task;
use App\Tasks\SocialEngine\GetPinterestAccountsBoardsTask;

$params = [

    'accounts' => 'required|ids.array',

];

return [
// only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {

        $res = task(new download__candles__for__all__instruments__task,
            [

            ]
        );

        return $res;
    }
];
