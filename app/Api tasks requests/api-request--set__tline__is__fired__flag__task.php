<?php
use App\Tasks\set__tline__is__fired__flag__task;

$params = [

    'id' => 'required|numeric',
//    'accounts' => 'required|ids.array',
//    'accounts' => 'required|string',
//    'params' => 'required|array',
];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new set__tline__is__fired__flag__task,
        [
            (int)$params['id']->_(),
        ]
        );

        return $res;
    }
];
