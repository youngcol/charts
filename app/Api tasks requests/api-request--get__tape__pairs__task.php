<?php

use App\Tasks\Screenshots\get__tape__pairs__task;

$params = [
    'period' => 'required|numeric',
];

return [
// only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new get__tape__pairs__task,
            [
                (int) $params['period']->_()
            ]
        );

        return $res;
    }
];
