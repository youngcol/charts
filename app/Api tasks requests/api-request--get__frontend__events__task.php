<?php

use App\Tasks\get__frontend__events__task;

$params = [
    'from_signal_id' => 'required|numeric',
    'from_ripper_event_id' => 'required|numeric',
    'limit' => 'required|numeric',
];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new get__frontend__events__task,
        [
            (int)$params['from_signal_id']->_(),
            (int)$params['from_ripper_event_id']->_(),
            (int)$params['limit']->_(),
        ]
        );

        return $res;
    }
];
