<?php
use App\Tasks\ForexCore\save__trendline__params__task;

$params = [

    'uniq_id' => 'required|string',
    'pair' => 'required|string',
    'period' => 'required|numeric',
    'params' => 'required|array',
];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new save__trendline__params__task,
        [
            $params['uniq_id']->_(),
            $params['pair']->_(),
            (int)$params['period']->_(),
            $params['params']->toArray()
        ]
        );

        $text = " плюс торговая ";
        push_to_voice_queue($text);

        return $res;
    }
];
