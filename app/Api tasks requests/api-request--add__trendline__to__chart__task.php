<?php


use App\Facades\CU;
use App\Models\User;
use App\Tasks\Charts\add__trendline__to__chart__task;
use App\Tasks\SocialEngine\GetPinterestAccountsBoardsTask;
use App\VO\VoVal;
use App\Tasks\ForexCore\put__command__to__the__sheet__task;
$params = [
        'chart_tag' => 'required|string',
    'uniq_id' => 'required|string',
    'start_date' => 'required|datetime',
    'end_date' => 'required|datetime',
    'start_value' => 'required|numeric',
    'end_value' => 'required|numeric',
    'is_fired'  => 'required|numeric',
    'period'  => 'required|numeric',
    'pair' => 'required|string',
    'mockup_tags' => 'required|array',
    'is_trainer' => 'required|numeric',
    'is_zone' => 'required|numeric',
    'is_zone_buy' => 'required|numeric'
];

return [
// only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {

        $res = task(new add__trendline__to__chart__task,
            [
                $params['uniq_id']->_(),
                $params['start_date'],
                $params['end_date'],
                (float)$params['start_value']->_(),
                (float)$params['end_value']->_(),
                (int)$params['is_fired']->_(),
                (int)$params['is_zone']->_(),
                (int)$params['is_zone_buy']->_(),
                (int)$params['is_trainer']->_(),
                $params['chart_tag']->_(),
                (int)$params['period']->_(),
                $params['pair']->_(),
                $params['mockup_tags']->toArray(),
            ]
        );


        $timeseria = \App\Models\ChartTrendline::getCandelsTimeArray(
            $res->trendline['start_date'],
            $res->trendline['end_date'],
            $res->trendline['period']
        );

        if($res->trendline['is_zone'] == 0)
        {
            task(new put__command__to__the__sheet__task,
                [
                    vo("CALC_TLINE_VALUES", "forex.commands"),
                    [
                        'symbol' => $res->trendline['pair'],
                        'uniq_id' => $res->trendline['uniq_id'],
                        'start_date' => str_replace("-", ".", $res->trendline['start_date']),
                        'start_value' => $res->trendline['start_value'],
                        'end_date' => str_replace("-", ".", $res->trendline['end_date']),
                        'end_value' => $res->trendline['end_value'],
                        'period' => $res->trendline['period'],
                        'window' => $res->trendline['chart_tag'] == 'ohlc' ? 0 : 1,
                        'timeseria' => implode(",", $timeseria)
                    ]
                ]
            );
        }

//        $text = "да";
//        push_to_voice_queue($text);

        return $res;
    }
];
