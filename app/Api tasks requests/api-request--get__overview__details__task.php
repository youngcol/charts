<?php


use App\Facades\CU;
use App\Tasks\get__pair__adx__signals__task;
use App\Tasks\Screenshots\get__overview__details__task;
use App\Tasks\SocialEngine\GetPinterestAccountsBoardsTask;

$params = [

    'pair' => 'required|filled.string',

];

return [
// only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
'runner' => function (array $params)
    {

        $res = task(new get__overview__details__task,
            [
                $params['pair']->_()
            ]
        );

        $adx = task(new get__pair__adx__signals__task,
            [
                $params['pair']->_()
            ]
        );

        $res->set('adx', $adx);

        return $res;
    }
];
