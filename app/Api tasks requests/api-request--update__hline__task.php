<?php
use App\Tasks\update__hline__task;

$params = [
    'id' => 'required|numeric',
    'value' => 'required|numeric',
    'is_fired' => 'required|numeric',
];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new update__hline__task,
            [
                (int)$params['id']->_(),
                (double)$params['value']->_(),
                (int)$params['is_fired']->_(),
            ]
        );

        return $res;
    }
];
