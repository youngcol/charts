<?php
use App\Tasks\create__adx__signal__task;

$params = [

    'period' => 'required|numeric',
    'pair' => 'required|string',
    'signal' => 'required|string',

];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new create__adx__signal__task,
        [
            $params['pair']->_(),
            (int)$params['period']->_(),
            $params['signal']->_(),
        ]
        );

        return $res;
    }
];
