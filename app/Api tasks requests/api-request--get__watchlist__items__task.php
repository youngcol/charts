<?php


use App\Facades\CU;
use App\Tasks\get__watchlist__items__task;
use App\Tasks\SocialEngine\GetPinterestAccountsBoardsTask;

$params = [

];

return [
// only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {

        $res = task(new get__watchlist__items__task,
            [

            ]
        );

        return $res;
    }
];
