<?php
use App\Tasks\Charts\get__trendlines__task;

$params = [
    'chart_tag' => 'required|string',
    'pair' => 'required|string',
    'period' => 'required|numeric',
];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new get__trendlines__task,
        [
            $params['chart_tag']->_(),
            $params['pair']->_(),
            (int) $params['period']->_(),
        ]
        );

        return $res;
    }
];
