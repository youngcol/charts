<?php
use App\Tasks\toggle__tline__is__trading__task;

$params = [
    'symbol' => 'required|string',
    'period' => 'required|numeric',
    'object_name' => 'required|string',
];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new toggle__tline__is__trading__task,
        [
            $params['symbol']->_(),
            (int)$params['period']->_(),
            $params['object_name']->_(),
        ]
        );

        if ($res->tline)
        {
            push_to_voice_queue($res->tline['is_trading'] ? " плюс торговая " : " плюс сигнальная ");
        }

        return $res;
    }
];
