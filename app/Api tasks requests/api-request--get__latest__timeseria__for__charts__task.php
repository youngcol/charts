<?php


use App\Facades\CU;
use App\Tasks\get__latest__timeseria__for__charts__task;
use App\Tasks\SocialEngine\GetPinterestAccountsBoardsTask;

$params = [
    'chartGroupIds' => 'required|ids.array',
    'fromDate' => 'required|datetime',
];

return [
// only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new get__latest__timeseria__for__charts__task,
            [
                CU::user(),
                $params['chartGroupIds'],
                $params['fromDate']
            ]
        );

        return $res;
    }
];
