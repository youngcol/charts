<?php
use App\Tasks\get__hline__task;

$params = [
    'id' => 'required|numeric',
];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new get__hline__task,
            [
                (int)$params['id']->_(),
            ]
        );

        return $res;
    }
];
