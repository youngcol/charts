<?php

use App\Tasks\get__market__pairs__task;

$params = [
    'symbols' => 'array',
    'periods' => 'array',
];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $symbols = $params['symbols']->toArray() ? $params['symbols']->toArray() : array_flatten(settings('pairs_list'));
        $res = task(new get__market__pairs__task,
        [
            $symbols,
            $params['periods']->toArray(),
            0
        ]
        );

        return $res;
    }
];
