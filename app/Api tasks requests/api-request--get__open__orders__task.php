<?php

use App\Tasks\ForexCore\Trading\get__open__orders__task;

$params = [

//    'accounts' => 'required|ids.array',

];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new get__open__orders__task,
        [

        ]
        );

        return $res;
    }
];
