<?php

use App\Tasks\ForexCore\get__tape__pairs__by__group__task;

$params = [
    'group' => 'required|string',
];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new get__tape__pairs__by__group__task,
            [
                $params['group']->_(),
            ]
        );

        return $res;
    }
];
