<?php
use App\Tasks\get__markup__request__data__task;

$params = [
    'period' => 'required|numeric',
    'indicator_name' => 'required|string',
    'symbol' => 'required|string',
];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new get__markup__request__data__task,
        [
            $params['indicator_name']->_(),
            $params['symbol']->_(),
            (int) $params['period']->_()
        ]
        );

        return $res;
    }
];
