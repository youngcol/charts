<?php


use App\Facades\CU;
use App\Tasks\Charts\clear__chart__from__all__levels__task;
use App\Tasks\SocialEngine\GetPinterestAccountsBoardsTask;

$params = [

    'chart_id' => 'required|numeric',

];

return [
// only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {

        $res = task(new clear__chart__from__all__levels__task,
            [
                CU::user(),
                $params['chart_id']->_()
            ]
        );

        return $res;
    }
];
