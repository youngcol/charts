<?php

$params = [
    'instrument' => 'required|string',
    'period1' => 'required|numeric',
    'period2' => 'required|numeric',
    'pic_view' => 'required|string',

];

return [
    // only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {
        $res = task(new remove_screenshot_drawning__task,
            [
                $params['instrument']->_(),
                (int)$params['period1']->_(),
                (int)$params['period2']->_(),
                $params['pic_view']->_(),
            ]
        );

        return $res;
    }
];