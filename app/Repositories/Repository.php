<?php
declare(strict_types=1);
namespace App\Repositories;

use App\Models\Model;
use Illuminate\Database\Query\Builder;

/**
 * Different repository methods with Eloquent model
 *
 * Class Repository
 * @package App\Repositories
 */
class Repository
{
    /**
     * @var string
     */
    public $querySql = [];

    /**
     * @var Model
     */
    protected $model;

    /**
     * @var Builder
     */
    protected $currentQuery = null;

    /**
     * Repository constructor.
     * @param string $modelClass
     */
    public function __construct(string $modelClass)
    {
        $this->model = new $modelClass;
    }

    /**
     * Resets current query
     */
    public function resetQuery()
    {
        $this->currentQuery = null;
    }

    /**
     * @return Builder|null
     */
    protected function getCurrentQuery()
    {
        if (eN($this->currentQuery))
        {
            $this->currentQuery = ($this->model)->newQueryWithoutScopes();
        }

        return $this->currentQuery;
    }

    /**
     * Model find method
     * @param $ids
     * @return mixed
     */
    public function find($ids)
    {
        if (is_array($ids)) {
            return $this->model->find($ids);
        } else {
            $found = $this->model->find([$ids]);
            return eN($found) ? NULL : $found->first();
        }
    }

    /**
     * Model all method
     * @return mixed
     */
    public function all()
    {
        return $this->model->all();
    }

    /**
     * Get first record
     * @param null $select
     * @return mixed
     */
    public function first($selectQuery=null)
    {
        $q = $this->getCurrentQuery();

        if ($selectQuery)
        {
            $s = explode(',', $selectQuery);
            $q->select($s);
        }

        $this->buildQueryLoq();

        $ret = $q->first();
        $this->resetQuery();

        return $ret;
    }

    /**
     * Get rows collection
     *
     * @param array $where
     * @param array $with
     * @param array $orderBy
     * @return mixed
     */
    public function get($selectQuery=null)
    {
        $q = $this->getCurrentQuery();

        if ($selectQuery)
        {
            $s = explode(',', $selectQuery);
            $q->select($s);
        }

        $this->buildQueryLoq();
        $ret = $q->get();
        $this->resetQuery();

        return $ret;
    }

    /**
     * @return mixed
     */
    public function toSql()
    {
        $this->buildQueryLoq();
        return $this->querySql;
    }

    /**
     * @return array
     */
    public function getSqlLog()
    {
        return $this->querySql;
    }

    /**
     * Pagination for select queries
     * @param int $page
     * @param int $limit
     * @return $this
     */
    public function page($page = 0, $perPage = 100)
    {
        $q = $this->getCurrentQuery();

        $q->limit($perPage);
        $q->offset($page * $perPage);

        return $this;
    }

    /**
     * @param $limit
     * @return $this
     */
    public function limit($limit)
    {
        $q = $this->getCurrentQuery();
        $q->limit($limit);

        return $this;
    }

    /**
     * @return bool
     */
    public function exists()
    {
        $q = $this->getCurrentQuery();

        $this->buildQueryLoq();
        $ret = $q->exists();
        $this->resetQuery();

        return $ret;
    }

    /**
     *
     * @param $where
     * @param null $opt
     *
     * examples
     *  where('key','val') `key = val`
     *  where('key<=','val') `key <= val`
     *  where(['key>=' => 'val']) `key>=val`
     * @return $this
     */
    public function where($where, $opt=null)
    {
        $q = $this->getCurrentQuery();

        // array case
        if (eN($opt))
        {
            foreach ($where as $key => $value)
            {
                $w = $this->getWhereOperators($key, $value);
                $q->where($w[0], $w[1], $w[2]);
            }
        }
        else
        {
            $w = $this->getWhereOperators($where, $opt);
            $q->where($w[0], $w[1], $w[2]);
        }

        return $this;
    }

    /**
     * @param $key
     * @param $value
     * @return array
     */
    private function getWhereOperators($key, $value)
    {
        $operator = '=';
        $l = strlen($key)-1;

        if ($key[$l] == '<')
        {
            $operator = '<';
        }
        elseif($key[$l] == '>')
        {
            $operator = '>';
        }
        elseif($key[$l-1] == '<' and $key[$l] == '=')
        {
            $operator = '<=';
        }
        elseif($key[$l-1] == '>' and $key[$l] == '=')
        {
            $operator = '>=';
        }

        return [trim($key, '<>='), $operator, $value];
    }

    /**
     * @param array $where
     * @param bool $isSubquery
     * @return $this
     */
    public function orWhere($where=[], $isSubquery=false)
    {
        $q = $this->getCurrentQuery();

        if ($isSubquery)
        {
            $q->orWhere(function ($query) use ($where) {
                foreach ($where as $key => $value)
                {
                    $w = $this->getWhereOperators($key, $value);
                    $query->where($w[0], $w[1], $w[2]);
                }
            });
        }
        else
        {
            foreach ($where as $key => $value) {
                $q->orWhere($key, '=', $value);
            }
        }


        return $this;
    }

    public function whereNull($field)
    {
        $q = $this->getCurrentQuery();
        $q->whereNull($field);
        return $this;
    }

    /**
     * @return $this
     */
    public function groupBy()
    {
        $groupBy = func_get_args();
        $q = $this->getCurrentQuery();

        foreach ($groupBy as $val) {
            $q->groupBy($val);
        }

        return $this;
    }

    /**
     * @param array $orderBy ['id' => 'asc']
     * @return $this
     */
    public function orderBy(array $orderBy=[])
    {
        $q = $this->getCurrentQuery();

        foreach ($orderBy as $field => $type) {
            $q->orderBy($field, $type);
        }

        return $this;
    }

    /**
     * With method of the model
     * with('other','some')
     * @param $with ('foo', 'bar')
     * @return $this
     */
    public function with()
    {
        $with = func_get_args();
        $q = $this->getCurrentQuery();

        foreach ($with as $item)
        {
            $q->with($item);
        }

        return $this;
    }

    /**
     * @param $name
     * @return $this
     */
    public function setTable1($name)
    {
        $this->model->setTable1($name);
        $this->resetQuery();

        return $this;
    }

    /**
     *
     */
    protected function buildQueryLoq()
    {
        $sql = $this->currentQuery->toSql();
        $bindings = $this->currentQuery->getBindings();

        $addSlashes = str_replace('?', "'?'", $sql);
        $fullSql = vsprintf(str_replace('?', '%s', $addSlashes), $bindings);

        $this->querySql[] = [$sql, $bindings, $fullSql];
    }

    /**
     * @return Builder|null
     */
    public function distinct()
    {
        $q = $this->getCurrentQuery();
        $q->distinct();
        return $q;
    }

    /**
     * @param $ids
     * @return int
     */
    public function delete($ids=[])
    {
        $q = $this->getCurrentQuery();

        if (count($ids))
        {
            $q->whereIn('id', $ids);
        }

        $this->buildQueryLoq();

        $ret = $q->delete($ids);
        $this->resetQuery();
        return $ret;
    }

    /**
     * @param string $field
     * @param array $values
     * @return $this
     */
    public function whereIn(string $field, array $values)
    {
        $q = $this->getCurrentQuery();
        $q->whereIn($field, $values);
        return $this;
    }

    /**
     * @param string $field
     * @param array $values
     * @return $this
     */
    public function whereNotIn(string $field, array $values)
    {
        $q = $this->getCurrentQuery();
        $q->whereNotIn($field, $values);
        return $this;
    }

    /**
     * @param $data [key => val]
     * @return int
     */
    public function update($data)
    {
        $q = $this->getCurrentQuery();
        $this->buildQueryLoq();

        $ret = $q->update($data);
        $this->resetQuery();
        return $ret;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function insert($data)
    {
        $q = $this->getCurrentQuery();
        $this->buildQueryLoq();

        $ret = $q->create($data);
        $this->resetQuery();
        return $ret;
    }

    /**
     * @param array $check
     * @param array $upd
     * @return mixed
     */
    public function updateOrCreate(array $check, array $upd)
    {
        $q = $this->getCurrentQuery();
        $this->buildQueryLoq();

        $ret = $q->updateOrCreate($check, $upd);
        $this->resetQuery();
        return $ret;
    }
}
