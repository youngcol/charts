<?php
declare(strict_types=1);
namespace App\Repositories;

use App\Models\Model;
use App\Models\Quote;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;

/**
 * Class UserRepository
 * @package App\Repositories
 */
class QuotesRepository extends Repository
{
    public function __construct()
    {
        parent::__construct(Quote::class);
    }

    /**
     * @param array $candles
     * @return array
     */
    public function storeCandles(array $candles)
    {
        $ret = [];

        foreach ($candles as $candle)
        {
            $date = Carbon::createFromFormat(
                'Y-m-d\TH:i:s',
                str_replace('.000000000Z', '', $candle['time'])
            );

            $ret[] = $this->updateOrCreate([
                'date' => date_mysql_format($date),
            ], [
                'o' => $candle['mid']['o'],
                'h' => $candle['mid']['h'],
                'l' => $candle['mid']['l'],
                'c' => $candle['mid']['c'],
            ]);
        }

        return $ret;
    }

    /**
     * @param Carbon $from
     * @param Carbon $to
     * @return mixed
     */
    public function getByDates(Carbon $from, Carbon $to)
    {
        $ret = $this
            ->where('date>=',  $from)
            ->where('date<=',  $to)
            ->orderBy(['date' => 'desc'])
            ->get()
            ->keyBy('date');

        return $ret;
    }
}
