<?php
// Middlewares
//$app->add($container->get('csrf'));
//$app->add(new App\Middlewares\Sender($container));

global $app;
$app->addBodyParsingMiddleware();
$app->addRoutingMiddleware();

$whoops = new \Whoops\Run;
$whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
$whoops->register();
