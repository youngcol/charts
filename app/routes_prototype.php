<?php
use Slim\Routing\RouteCollectorProxy;

/**
 * This files includes only when (APP_ENV=dev)
 * for development porpouse only
 */

//$app->group('/api_1', function () {
//
//
//});


$app->group('/proto', function (RouteCollectorProxy $group) {

    $group->get('', 'App\Controllers\Prototype\PrototypeController:index')->setName('proto.index');
    $group->get('/posts', 'App\Controllers\Prototype\PrototypeController:posts')->setName('proto.posts');
    $group->post('/post/send/test', 'App\Controllers\Prototype\PrototypeController:postStoreTest')->setName('api.post.send.test');

});
//->add(new App\Middlewares\Auth($container));
