package misc.support.jforex.indicators;

import com.dukascopy.api.IConsole;
import com.dukascopy.api.indicators.*;
import com.dukascopy.api.indicators.IIndicator;
import com.dukascopy.api.indicators.IIndicatorContext;
import com.dukascopy.api.indicators.IIndicatorsProvider;
import com.dukascopy.api.indicators.IndicatorInfo;
import com.dukascopy.api.indicators.IndicatorResult;
import com.dukascopy.api.indicators.InputParameterInfo;
import com.dukascopy.api.indicators.IntegerRangeDescription;
import com.dukascopy.api.indicators.OptInputParameterInfo;
import com.dukascopy.api.indicators.OutputParameterInfo;


import java.io.*;
import javax.imageio.ImageIO;
import com.dukascopy.api.*;
import com.dukascopy.api.util.*;
import com.dukascopy.api.feed.IFeedDescriptor;
import com.dukascopy.api.feed.util.*;
import java.net.*;
import java.lang.Exception.*;


public class TripleEMAIndicator implements IIndicator {
    private IndicatorInfo indicatorInfo;
    private InputParameterInfo[] inputParameterInfos;
    private OptInputParameterInfo[] optInputParameterInfos;
    private OutputParameterInfo[] outputParameterInfos;
    private double[][] inputs = new double[1][];
    private int[] timePeriod = new int[3];
    private double[][] outputs = new double[3][];
    private IIndicator ema;
    private String instrument = "";
    private IConsole console;
    private IIndicatorContext context;


    public void onStart(IIndicatorContext context) {
        IIndicatorsProvider indicatorsProvider = context.getIndicatorsProvider();
        this.console = context.getConsole();
        this.context = context;

        ema = indicatorsProvider.getIndicator("EMA");



        //this.instrument = (String) context.getInstrument().getName();


        indicatorInfo = new IndicatorInfo("THREEEMA", "Shows three different EMA indicators", "My indicators",
                true, false, true, 1, 3, 3);

        inputParameterInfos = new InputParameterInfo[] {
                new InputParameterInfo("Input data", InputParameterInfo.Type.DOUBLE)
        };

        optInputParameterInfos = new OptInputParameterInfo[] {
                new OptInputParameterInfo("Time period EMA1", OptInputParameterInfo.Type.OTHER,
                                          new IntegerRangeDescription(2, 2, 1000, 1)),
                new OptInputParameterInfo("Time period EMA2", OptInputParameterInfo.Type.OTHER,
                                          new IntegerRangeDescription(34, 2, 1000, 1)),
                new OptInputParameterInfo("Time period EMA3", OptInputParameterInfo.Type.OTHER,
                                          new IntegerRangeDescription(144, 2, 1000, 1)),
                 new OptInputParameterInfo("Period", OptInputParameterInfo.Type.OTHER,
                                          new IntegerListDescription(1, new int[] {1,2}, new String[] {"389 ticks", "6757 ticks"}))
            };

        outputParameterInfos = new OutputParameterInfo[] {
                new OutputParameterInfo("EMA1", OutputParameterInfo.Type.DOUBLE,
                                        OutputParameterInfo.DrawingStyle.LINE),
                new OutputParameterInfo("EMA2", OutputParameterInfo.Type.DOUBLE,
                                        OutputParameterInfo.DrawingStyle.LINE),
                new OutputParameterInfo("EMA3", OutputParameterInfo.Type.DOUBLE,
                                        OutputParameterInfo.DrawingStyle.LINE)};
    }

    public IndicatorResult calculate(int startIndex, int endIndex) {
        //calculating startIndex taking into account lookback value
        if (startIndex - getLookback() < 0) {
            startIndex -= startIndex - getLookback();
        }

        ema.setInputParameter(0, inputs[0]);

        //calculate first ema
        ema.setOptInputParameter(0, timePeriod[0]);
        ema.setOutputParameter(0, outputs[0]);
        ema.calculate(startIndex, endIndex);

        //calculate second ema
        ema.setOptInputParameter(0, timePeriod[1]);
        ema.setOutputParameter(0, outputs[1]);
        ema.calculate(startIndex, endIndex);

        //calculate third ema
        ema.setOptInputParameter(0, timePeriod[2]);
        ema.setOutputParameter(0, outputs[2]);
        IndicatorResult ir = ema.calculate(startIndex, endIndex);


        double[][] current = new double[3][1];
        double[][] prev = new double[3][1];

        current[0][0] = outputs[0][outputs[0].length-1]; //3
        current[1][0] = outputs[1][outputs[1].length-1];//34
        current[2][0] = outputs[2][outputs[2].length-1];//144


        prev[0][0] = outputs[0][outputs[0].length-2];
        prev[1][0] = outputs[1][outputs[1].length-2];
        prev[2][0] = outputs[2][outputs[2].length-2];


        // 3 ema
        if (prev[0][0] > current[1][0]  && current[0][0] < current[1][0])
        {
            console.getOut().println("3 ema SELL");
            this.sendGet(0, "3");
        }

        if (prev[0][0] < current[1][0]  && current[0][0] > current[1][0])
        {
            this.sendGet(1, "3");
            console.getOut().println("3 ema BUY");
        }

        // 34 ema
        if (prev[1][0] > current[2][0]  && current[1][0] < current[2][0])
        {
            this.sendGet(0, "34");
            console.getOut().println("34 ema SELL");
        }

        if (prev[1][0] < current[2][0]  && current[1][0] > current[2][0])
        {
            this.sendGet(1, "34");
            console.getOut().println("34 ema BUY");
        }

        //instrument = (String) this.context.getInstrument().getName();

        //console.getOut().println("1 ema " + outputs[0][outputs[0].length-1]); //3
        //console.getOut().println("2 ema " + outputs[1][outputs[1].length-1]); //34
        //console.getOut().println("3 ema " + outputs[2][outputs[2].length-1]); //144
        //console.getOut().println(outputs[1][0]);



        return ir;
    }

    public IndicatorInfo getIndicatorInfo() {
        return indicatorInfo;
    }

    public InputParameterInfo getInputParameterInfo(int index) {
        if (index <= inputParameterInfos.length) {
            return inputParameterInfos[index];
        }
        return null;
    }

    public int getLookback() {
        ema.setOptInputParameter(0, timePeriod[0]);
        int ema1Lookback = ema.getLookback();

        ema.setOptInputParameter(0, timePeriod[1]);
        int ema2Lookback = ema.getLookback();

        ema.setOptInputParameter(0, timePeriod[2]);
        int ema3Lookback = ema.getLookback();

        return Math.max(ema1Lookback, Math.max(ema2Lookback, ema3Lookback));
    }

    public int getLookforward() {
        return 0;
    }

    public OptInputParameterInfo getOptInputParameterInfo(int index) {
        if (index <= optInputParameterInfos.length) {
            return optInputParameterInfos[index];
        }
        return null;
    }

    public OutputParameterInfo getOutputParameterInfo(int index) {
        if (index <= outputParameterInfos.length) {
            return outputParameterInfos[index];
        }
        return null;
    }

    public void setInputParameter(int index, Object array) {
        inputs[index] = (double[]) array;
    }

    public void setOptInputParameter(int index, Object value) {
        timePeriod[index] = (Integer) value;
    }

    public void setOutputParameter(int index, Object array) {
        outputs[index] = (double[]) array;
    }

    public void sendGet(int direction, String emaName)
    {
        String instrument = (String) this.context.getInstrument().getName();

        try{
            String urlString = "http://2286498.td430236.web.hosting-test.net/ticks_signal?period=389&direction="+direction+"&instrument="+instrument+"&ema="+emaName;
            URL url = new URL(urlString);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");

            int responseCode = con.getResponseCode();
            console.getOut().println(urlString + " ++ " + responseCode);

        }
        catch(MalformedURLException e)
        {

        }
        catch(IOException e)
        {

        }
    }
}
