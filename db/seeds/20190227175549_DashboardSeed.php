<?php

use App\Facades\CU;
use App\Models\User;
use App\Tasks\create__chart__group__task;
use App\Tasks\create__chart__task;


class DashboardSeed
{

    public function run()
    {

        $instruments = explode(
            ',', 'eur_usd,gbp_usd,gbp_jpy,usd_chf,usd_jpy,usd_cad,aud_usd,nzd_usd,eur_jpy,aud_jpy,gbp_cad,gbp_chf,nzd_jpy'
        );

        $chartIds = [];
        $u = User::find(1);


        foreach ($instruments as $instrument)
        {
            $res = task(new create__chart__task,
                [
                    $u,
                    vo($instrument, 'instruments'),
                    vo('1', 'chart.timeframe'),
                    vo('M', 'chart.timeframe.type'),
                    vo('default', 'chart.view'),
                    vo(300, 'chart.scale'),
                    vo('', 'string'),
                ]
            );

            $chartIds[] = $res->chart->id;
        }


        $res = task(new create__chart__group__task,
            [
                $u,
                vo('main dashboard'),
                vo($chartIds, 'ids.array'),
                vo('1', 'chart.timeframe'),
                vo('M', 'chart.timeframe.type'),
                null,
                null
            ]
        );

    }
}
