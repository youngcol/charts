<?php

use App\Facades\CU;
use App\Models\Timeline;
use App\Models\User;
use App\Tasks\create__chart__group__task;
use App\Tasks\create__chart__task;
use App\VO\VoVal;


class ChartGroupsSeed
{

    public function run()
    {

//        $instruments = explode(',', settings('instruments'));
//        $iqoptions_instruments = explode(',', 'aud_jpy,gbp_cad,gbp_usd,usd_jpy,aud_usd,nzd_usd,eur_jpy');

        $shortTimeline = new Timeline();
        $shortTimeline->name = 'short timeline';
        $shortTimeline->save();

        $iqTimeline = new Timeline();
        $iqTimeline->name = 'iq option timeline';
        $iqTimeline->save();

        $u = User::find(1);

        $this->createTimelineCharts($u, $iqTimeline, settings('iqoptions_instruments'));
        $this->createTimelineCharts($u, $shortTimeline, settings('instruments'));
    }

    public function createTimelineCharts($u, $timeline, $instruments)
    {
        $timeframes = [15];

        foreach (explode(',', $instruments) as $instrument)
        {
            foreach ($timeframes as $timeframe)
            {
                $res = task(new create__chart__task,
                    [
                        $u,
                        vo($instrument, 'instruments'),
                        vo($timeframe, 'chart.timeframe'),
                        vo('M', 'chart.timeframe.type'),
                        vo('default', 'chart.view'),
                        vo(300, 'chart.scale'),
                        vo('', 'string'),
                    ]
                );

                $res = task(new create__chart__group__task,
                    [
                        $u,
                        vo($instrument),
                        vo([$res->chart->id], 'ids.array'),
                        vo($timeframe, 'chart.timeframe'),
                        vo('M', 'chart.timeframe.type'),
                        vo($timeline->id, 'numeric'),
                        vo(date_mysql_format(now_date()), 'datetime')
                    ]
                );
            }
        }
    }
}
