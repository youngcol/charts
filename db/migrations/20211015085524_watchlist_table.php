<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * https://laravel.com/docs/5.8/migrations#columns
 *
 * 20211015085524_watchlist_table.php
 */
class WatchlistTable
{
    /**
     * Do the migration
     */
    public function up()
    {
        //Capsule::schema()->table('watchlist_table', function($table) {
        //
        //});

        Capsule::schema()->create('watchlist', function($table) {
            $table->increments('id');

            $table->string('instrument');
            $table->integer('period');
            $table->string('direction');

            $table->timestamps();


            //$table->date('deleted_at')->nullable();
            //
            //$table->string('col', 255)->nullable();
            //
            //$table->unsignedInteger('votes');
            //$table->integer('integer')->nullable();
            //$table->float('amount', 8, 2);
            //$table->tinyInteger('votes');
            //
            //$table->dateTime('col');
            //$table->date('col');
            //$table->time('sunrise');
            //
            //$table->longText('longText');
            //$table->text('description');
            //$table->enum('level', ['easy', 'hard'])->default('hard');
            //
            //$table->integer('user_id')->unsigned()->nullable();
            //$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        Capsule::schema()->drop('watchlist');

        //Capsule::schema()->table('watchlist_table', function($table) {
        //    // $table->dropColumn('url');
        //});

    }
}
