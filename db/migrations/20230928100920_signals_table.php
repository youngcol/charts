<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * https://laravel.com/docs/5.8/migrations#columns
 *
 * 20230928100920_signals_table.php
 */
class SignalsTable
{
    /**
     * Do the migration
     */
    public function up()
    {

        Capsule::schema()->create('signals', function($table) {
            $table->increments('id');
            $table->dateTime('time');
            $table->string('name', 255);
            $table->string('instrument', 255);
            $table->integer('period');
            $table->float('price', 10, 5);
            $table->tinyInteger('direction');
            $table->text('params');

            $table->integer('event_id');
            $table->unique(['time', 'instrument', 'period', 'name']);
        });

        Capsule::schema()->table('events', function($table) {
            $table->float('price', 10, 5);
        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        Capsule::schema()->drop('signals');
    }
}
