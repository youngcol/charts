1<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * https://laravel.com/docs/5.8/migrations#columns
 *
 * 20200326204602_charts.php
 */
class Charts
{
    /**
     * Do the migration
     */
    public function up()
    {
        Capsule::schema()->create('chart_groups', function($table) {
            $table->increments('id');
            $table->timestamps();
            $table->date('deleted_at')->nullable();

            $table->string('name', 255)->nullable();
            $table->integer('timeframe');
            $table->string('timeframe_type', 10)->default('M');

            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

        });

        Capsule::schema()->create('charts', function($table) {
            $table->increments('id');
            $table->timestamps();
            $table->date('deleted_at')->nullable();

            $table->string('instrument');
            $table->enum('view', ['default', 'renko'])->default('default');

            $table->string('timeframe')->default(1);
            $table->string('timeframe_type', 10)->default('M');

            $table->unsignedInteger('scale')->default(100);
//
//            $table->integer('integer')->nullable();
//            $table->float('amount', 8, 2);
//            $table->tinyInteger('votes');
//
//            $table->dateTime('col');
//            $table->date('col');
//            $table->time('sunrise');
//
//            $table->longText('longText');
            $table->text('indicators')->nullable();
//

            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->integer('group_id')->unsigned()->nullable();
            $table->foreign('group_id')->references('id')->on('chart_groups')->onDelete('cascade');

        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        Capsule::schema()->drop('charts');

        Capsule::schema()->table('charts', function($table) {
            // $table->dropColumn('url');
        });

    }
}
