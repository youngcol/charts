<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * https://laravel.com/docs/5.8/migrations#columns
 *
 * 20230330183101_signals_tline_id.php
 */
class SignalsTlineId
{
    /**
     * Do the migration
     */
    public function up()
    {
        Capsule::schema()->table('events', function($table) {
            $table->integer('tline_id')->nullable();
        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        
    }
}
