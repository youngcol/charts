<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * https://laravel.com/docs/5.8/migrations#columns
 *
 * 20200328123405_chart_group_popup_time.php
 */
class ChartGroupPopupTime
{
    /**
     * Do the migration
     */
    public function up()
    {

        Capsule::schema()->table('chart_groups', function($table) {

            $table->datetime('next_popup_time')->nullable();

        });

//        Capsule::schema()->table('chart_group_popup_time', function($table) {
////            $table->increments('id');
////            $table->timestamps();
//
//            $table->date('next_popup_time')->nullable();
//
////            $table->string('col', 255)->nullable();
////
////            $table->unsignedInteger('votes');
////            $table->integer('integer')->nullable();
////            $table->float('amount', 8, 2);
////            $table->tinyInteger('votes');
////
////            $table->dateTime('col');
////            $table->date('col');
////            $table->time('sunrise');
////
////            $table->longText('longText');
////            $table->text('description');
////            $table->enum('level', ['easy', 'hard'])->default('hard');
////
////            $table->integer('user_id')->unsigned()->nullable();
////            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
//
//        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
//        Capsule::schema()->drop('chart_group_popup_time');

        Capsule::schema()->table('chart_group_popup_time', function($table) {

            $table->dropColumn('next_popup_time');

        });

    }
}
