<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * https://laravel.com/docs/5.8/migrations#columns
 *
 * 20230331102612_tline_is_trading.php
 */
class TlineIsTrading
{
    /**
     * Do the migration
     */
    public function up()
    {
        Capsule::schema()->table('chart_trendlines', function($table) {
            //$table->tinyInteger('is_trading')->nullable();
            //$table->string('params', 255)->nullable();
            $table->string('pair', 255);
        });


    }

    /**
     * Undo the migration
     */
    public function down()
    {


    }
}
