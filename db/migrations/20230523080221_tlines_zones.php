<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * https://laravel.com/docs/5.8/migrations#columns
 *
 * 20230523080221_tlines_zones.php
 */
class TlinesZones
{
    /**
     * Do the migration
     */
    public function up()
    {
        Capsule::schema()->table('chart_trendlines', function($table) {
            $table->integer('is_zone')->nullable();
            $table->integer('is_zone_buy')->nullable();
        });

    }

    /**
     * Undo the migration
     */
    public function down()
    {


    }
}
