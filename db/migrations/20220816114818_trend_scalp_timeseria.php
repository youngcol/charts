<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * https://laravel.com/docs/5.8/migrations#columns
 *
 * 20220816114818_trend_scalp_timeseria.php
 */
class TrendScalpTimeseria
{
    /**
     * Do the migration
     */
    public function up()
    {
        Capsule::schema()->table('timeseries', function($table) {


        });

        Capsule::schema()->create('timeseries', function($table) {
            $table->increments('id');

            $table->dateTime('time');
            $table->string('instrument', 255);
            $table->string('tag', 255);

            $table->integer('period');
            $table->float('value', 10, 5);
            $table->unique(['time', 'period', 'instrument', 'tag']);

//            $table->unsignedInteger('votes');
//            $table->tinyInteger('votes');
//            $table->dateTime('col');
//            $table->date('col');
//            $table->time('sunrise');
//
//            $table->longText('longText');
//            $table->text('description');
//            $table->enum('level', ['easy', 'hard'])->default('hard');
//
//            $table->integer('user_id')->unsigned()->nullable();
//            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        Capsule::schema()->drop('timeseries');

        Capsule::schema()->table('timeseries', function($table) {
            // $table->dropColumn('url');
        });

    }
}
