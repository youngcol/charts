<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * https://laravel.com/docs/5.8/migrations#columns
 *
 * 20220719194825_signals_history.php
 */
class SignalsHistory
{
    /**
     * Do the migration
     */
    public function up()
    {

        Capsule::schema()->create('events', function($table) {
            $table->increments('id');
            $table->timestamps();

            $table->datetime('generated_at')->nullable();
            $table->string('tag', 255)->nullable();
            $table->string('instrument', 255);
            $table->string('period', 255);
            $table->integer('direction');
            $table->text('params');

        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        Capsule::schema()->drop('events');

        Capsule::schema()->table('events', function($table) {
            // $table->dropColumn('url');
        });

    }
}
