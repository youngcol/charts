<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * https://laravel.com/docs/5.8/migrations#columns
 *
 * 20230416125421_tline_mockup_tag.php
 */
class TlineMockupTag
{
    /**
     * Do the migration
     */
    public function up()
    {
        Capsule::schema()->table('chart_trendlines', function($table) {
            $table->string('mockup_tag', 255);
        });
        
    }

    /**
     * Undo the migration
     */
    public function down()
    {

    }
}
