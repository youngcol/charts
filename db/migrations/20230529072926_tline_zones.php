<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * https://laravel.com/docs/5.8/migrations#columns
 *
 * 20230529072926_tline_zones.php
 */
class TlineZones
{
    /**
     * Do the migration
     */
    public function up()
    {
        Capsule::schema()->table('chart_trendlines', function($table) {
            //$table->unsignedInteger('is_zone')->nullable();
            //$table->unsignedInteger('is_zone_buy')->nullable();

        });

    }

    /**
     * Undo the migration
     */
    public function down()
    {
        Capsule::schema()->drop('tline_zones');

        Capsule::schema()->table('tline_zones', function($table) {
            // $table->dropColumn('url');
        });

    }
}
