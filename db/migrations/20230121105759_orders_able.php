<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * https://laravel.com/docs/5.8/migrations#columns
 *
 * 20230121105759_orders_able.php
 */
class OrdersAble
{
    /**
     * Do the migration
     */
    public function up()
    {
//        Capsule::schema()->table('orders_able', function($table) {
//
//        });

        Capsule::schema()->create('orders', function($table) {
            $table->increments('id');
            $table->timestamps();

            //$table->date('deleted_at')->nullable();

            $table->string('instrument', 255);
            $table->unsignedInteger('period');
            $table->integer('result')->nullable();


//            $table->float('amount', 8, 2);
//            $table->tinyInteger('votes');
//            $table->dateTime('col');
//            $table->date('col');
//            $table->time('sunrise');
//            $table->longText('longText');
//            $table->text('description');
//            $table->enum('level', ['easy', 'hard'])->default('hard');
//            $table->integer('user_id')->unsigned()->nullable();
//            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        Capsule::schema()->drop('orders_able');

        Capsule::schema()->table('orders_able', function($table) {
            // $table->dropColumn('url');
        });

    }
}
