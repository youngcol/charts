<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * https://laravel.com/docs/5.8/migrations#columns
 *
 * 20200407073933_timelines.php
 */
class Timelines
{
    /**
     * Do the migration
     */
    public function up()
    {
        Capsule::schema()->create('timelines', function($table) {
            $table->increments('id');
            $table->timestamps();
            $table->date('deleted_at')->nullable();
            $table->string('name');
        });

        Capsule::schema()->table('chart_groups', function($table) {
            $table->unsignedInteger('timeline_id')->nullable();;
//            $table->foreign('timeline_id')->references('id')->on('timelines')->onDelete('cascade');
        });

        Capsule::schema()->create('timeline_have_chart_group', function($table) {
            $table->unsignedInteger('timeline_id');
            $table->unsignedInteger('chart_group_id');

            $table->foreign('timeline_id')->references('id')->on('timelines')->onDelete('cascade');
            $table->foreign('chart_group_id')->references('id')->on('chart_groups')->onDelete('cascade');
        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        Capsule::schema()->drop('timelines');
        Capsule::schema()->drop('timeline_have_chart');

    }
}
