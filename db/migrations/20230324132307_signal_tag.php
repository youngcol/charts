<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * https://laravel.com/docs/5.8/migrations#columns
 *
 * 20230324132307_signal_tag.php
 */
class SignalTag
{
    /**
     * Do the migration
     */
    public function up()
    {
        Capsule::schema()->table('events', function($table) {
            $table->string('place_tag', 255)->nullable();
        });


    }

    /**
     * Undo the migration
     */
    public function down()
    {


    }
}
