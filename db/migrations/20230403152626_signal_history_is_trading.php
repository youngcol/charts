<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * https://laravel.com/docs/5.8/migrations#columns
 *
 * 20230403152626_signal_history_is_trading.php
 */
class SignalHistoryIsTrading
{
    /**
     * Do the migration
     */
    public function up()
    {
        Capsule::schema()->table('events', function($table) {
            $table->unsignedInteger('is_trading');
        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {

    }
}
