<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * https://laravel.com/docs/5.8/migrations#columns
 *
 * 20220904062958_forex_commands.php
 */
class ForexCommands
{
    /**
     * Do the migration
     */
    public function up()
    {
//        Capsule::schema()->table('forex_commands', function($table) {
//
//        });

        Capsule::schema()->create('forex_commands', function($table) {
            $table->increments('id');
            $table->integer('uniq_id');
            $table->timestamps();

            $table->string('name', 255)->nullable();
            $table->text('params');
//            $table->tinyInteger('deleted');
            $table->datetime('deleted_at')->nullable();

            // $table->unsignedInteger('votes');
            // $table->integer('integer')->nullable();
            // $table->float('amount', 8, 2);
            // $table->tinyInteger('votes');

            // $table->dateTime('col');
            // $table->date('col');
            // $table->time('sunrise');

            // $table->longText('longText');
            
            // $table->enum('level', ['easy', 'hard'])->default('hard');

            // $table->integer('user_id')->unsigned()->nullable();
            // $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        Capsule::schema()->drop('forex_commands');

        Capsule::schema()->table('forex_commands', function($table) {
            // $table->dropColumn('url');
        });

    }
}
