<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * https://laravel.com/docs/5.8/migrations#columns
 *
 * 20230518080745_tline_calc.php
 */
class TlineCalc
{
    /**
     * Do the migration
     */
    public function up()
    {
        Capsule::schema()->table('chart_trendlines', function($table) {
            $table->string('calc', 5000)->nullable();
        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        

    }
}
