<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * https://laravel.com/docs/5.8/migrations#columns
 *
 * 20230221165332_ohlc_table.php
 */
class OhlcTable
{
    /**
     * Do the migration
     */
    public function up()
    {

        Capsule::schema()->create('ohlc', function($table) {

            $table->dateTime('time');
            $table->integer('period');
            $table->string('instrument', 255);

            $table->float('o', 8, 5);
            $table->float('h', 8, 5);
            $table->float('l', 8, 5);
            $table->float('c', 8, 5);

            $table->unique(['time', 'period', 'instrument']);
            
        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        Capsule::schema()->drop('ohlc');

    }
}
