<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * https://laravel.com/docs/5.8/migrations#columns
 *
 * 20200424190129_chart_levels.php
 */
class ChartLevels
{
    /**
     * Do the migration
     */
    public function up()
    {

        Capsule::schema()->create('chart_levels', function($table) {
            $table->increments('id');
 
            $table->float('value', 8, 4);
            $table->float('fired_value', 8, 4)->nullable();
            $table->integer('is_fired')->nullable();
            $table->datetime('fired_at')->nullable();
            $table->enum('type', ['buy', 'sell', 'alert'])->nullable();
            $table->tinyInteger('is_trading');
            $table->string('params', 255)->nullable();
            $table->string('pair', 255);
            $table->unsignedInteger('period');

        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        Capsule::schema()->drop('chart_levels');

        Capsule::schema()->table('chart_levels', function($table) {
            // $table->dropColumn('url');
        });

    }
}
