<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * https://laravel.com/docs/5.8/migrations#columns
 *
 * 20230427102435_tline_is_trainer.php
 */
class TlineIsTrainer
{
    /**
     * Do the migration
     */
    public function up()
    {
        Capsule::schema()->table('chart_trendlines', function($table) {

            $table->tinyInteger('is_trainer')->nullable();
            
        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {

    }
}
