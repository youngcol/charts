<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * https://laravel.com/docs/5.8/migrations#columns
 *
 * 20230324105329_tline_period.php
 */
class TlinePeriod
{
    /**
     * Do the migration
     */
    public function up()
    {
        Capsule::schema()->table('chart_trendlines', function($table) {
            $table->unsignedInteger('period');
        });


    }

    /**
     * Undo the migration
     */
    public function down()
    {

    }
}
