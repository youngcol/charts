<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * https://laravel.com/docs/5.8/migrations#columns
 *
 * 20240130143927_add_events_is_shown.php
 */
class AddEventsIsShown
{
    /**
     * Do the migration
     */
    public function up()
    {
        Capsule::schema()->table('events', function($table) {
            $table->tinyInteger('is_shown');
            $table->tinyInteger('is_favorite');
        });


    }

    /**
     * Undo the migration
     */
    public function down()
    {
        Capsule::schema()->drop('add_events_is_shown');

        Capsule::schema()->table('add_events_is_shown', function($table) {
            // $table->dropColumn('url');
        });

    }
}
