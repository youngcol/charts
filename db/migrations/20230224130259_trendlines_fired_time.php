<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * https://laravel.com/docs/5.8/migrations#columns
 *
 * 20230224130259_trendlines_fired_time.php
 */
class TrendlinesFiredTime
{
    /**
     * Do the migration
     */
    public function up()
    {
        Capsule::schema()->table('chart_trendlines', function($table) {
            $table->datetime('fired_at')->nullable();
        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        Capsule::schema()->drop('trendlines_fired_time');

        Capsule::schema()->table('trendlines_fired_time', function($table) {
            // $table->dropColumn('url');
        });

    }
}
