<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * https://laravel.com/docs/5.8/migrations#columns
 *
 * 20230518110723_tline_fired_value.php
 */
class TlineFiredValue
{
    /**
     * Do the migration
     */
    public function up()
    {
        Capsule::schema()->table('chart_trendlines', function($table) {
            $table->float('tline_fired_value', 8, 4)->nullable();
        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {

    }
}
