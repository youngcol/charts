<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * https://laravel.com/docs/5.8/migrations#columns
 *
 * 20230426132823_ripper_events.php
 */
class RipperEvents
{
    /**
     * Do the migration
     */
    public function up()
    {
        Capsule::schema()->create('ripper_events', function($table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('event', 255);
            $table->string('params', 255);
        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {

    }
}
