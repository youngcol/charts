<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * https://laravel.com/docs/5.8/migrations#columns
 *
 * 20230324140253_trendline_fired_val.php
 */
class TrendlineFiredVal
{
    /**
     * Do the migration
     */
    public function up()
    {
        Capsule::schema()->table('chart_trendlines', function($table) {
            $table->float('fired_value', 8, 4)->nullable();
        });

    }

    /**
     * Undo the migration
     */
    public function down()
    {
        

    }
}
