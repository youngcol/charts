<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * https://laravel.com/docs/5.8/migrations#columns
 *
 * 20200509092804_timeline_sorting.php
 */
class TimelineSorting
{
    /**
     * Do the migration
     */
    public function up()
    {
        Capsule::schema()->table('timelines', function($table) {
            $table->string('chart_groups_sorting')->nullable();
        });
//
//        Capsule::schema()->create('timeline_sorting', function($table) {
//            $table->increments('id');
//            $table->timestamps();
//            $table->date('deleted_at')->nullable();
//
//            $table->string('col', 255)->nullable();
//
//            $table->unsignedInteger('votes');
//            $table->integer('integer')->nullable();
//            $table->float('amount', 8, 2);
//            $table->tinyInteger('votes');
//
//            $table->dateTime('col');
//            $table->date('col');
//            $table->time('sunrise');
//
//            $table->longText('longText');
//            $table->text('description');
//            $table->enum('level', ['easy', 'hard'])->default('hard');
//
//            $table->integer('user_id')->unsigned()->nullable();
//            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
//
//        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
//        Capsule::schema()->drop('timeline_sorting');
//
//        Capsule::schema()->table('timeline_sorting', function($table) {
//            // $table->dropColumn('url');
//        });

    }
}
