<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * https://laravel.com/docs/5.8/migrations#columns
 *
 * 20191006211417_task_logs.php
 */
class TaskLogs
{
    /**
     * Do the migration
     */
    public function up()
    {
        Capsule::schema()->table('task_logs', function($table) {

        });

        Capsule::schema()->create('task_logs', function($table) {
            $table->increments('id');
            $table->timestamps();
            $table->date('deleted_at')->nullable();

            $table->string('class_name', 255);
            $table->longText('backtrace');
            $table->longText('args');
            $table->string('duration');
            $table->string('status');

        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        Capsule::schema()->drop('task_logs');
    }
}
