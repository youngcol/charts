<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * https://laravel.com/docs/5.8/migrations#columns
 *
 * 20230420173915_trades.php
 */
class Trades
{
    /**
     * Do the migration
     */
    public function up()
    {
        Capsule::schema()->create('trades', function($table) {
            $table->increments('id');
            $table->timestamps();
//            $table->date('deleted_at')->nullable();

            $table->string('instrument', 255);
            $table->string('pic_view', 255);
            $table->integer('period');
            $table->integer('risk');
            $table->tinyInteger('type');
            
            $table->unsignedInteger('main_tline');
            $table->unsignedInteger('confirm_tline');
            
            
        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
//        Capsule::schema()->drop('trades');
//
//        Capsule::schema()->table('trades', function($table) {
//            // $table->dropColumn('url');
//        });

    }
}
