<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * https://laravel.com/docs/5.8/migrations#columns
 *
 * 20200503075201_chart_trendlines.php
 */
class ChartTrendlines
{
    /**
     * Do the migration
     */
    public function up()
    {

        Capsule::schema()->create('chart_trendlines', function($table) {
            $table->increments('id');

            $table->float('start_value', 8, 4);
            $table->float('end_value', 8, 4);
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->integer('is_fired');

            $table->string('uniq_id', 255);
            $table->index('uniq_id');

            $table->text('params');

//            $table->unsignedInteger('votes');
//            $table->integer('integer')->nullable();
//            $table->float('amount', 8, 2);
//            $table->tinyInteger('votes');
//            $table->date('col');
//            $table->time('sunrise');
//
//            $table->longText('longText');
//            $table->text('description');
//            $table->enum('level', ['easy', 'hard'])->default('hard');
//
//            $table->integer('user_id')->unsigned()->nullable();
//            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        Capsule::schema()->drop('chart_trendlines');

        Capsule::schema()->table('chart_trendlines', function($table) {
            // $table->dropColumn('url');
        });

    }
}
