<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * https://laravel.com/docs/5.8/migrations#columns
 *
 * 20220708100351_adx_signals.php
 */
class AdxSignals
{
    /**
     * Do the migration
     */
    public function up()
    {
//        Capsule::schema()->table('adx_signals', function($table) {
//
//        });

        Capsule::schema()->create('adx_signals', function($table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('instrument', 255);
            $table->string('period', 255);
            $table->integer('direction');

        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        Capsule::schema()->drop('adx_signals');

//        Capsule::schema()->table('adx_signals', function($table) {
//            // $table->dropColumn('url');
//        });

    }
}
