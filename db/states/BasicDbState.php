<?php

use App\Common\Res;
use App\Facades\db;
use App\Models\Pet;
use App\Tasks\SignUpUserTask;

/**
 * Class BasicDbState
 *
 */
class BasicDbState extends DbState
{

    public function __construct()
    {
        parent::__construct('basic:setup');
    }

    public function run()
    {
        $host= "mysql";
        $user= "root";

        $res = exec("mysql -h $host -u $user charts < " . DB_STATES_PATH . '/charts.sql');

        // mysql -u root -p classicmodels_backup < d:\db\classicmodels.sql

        $users = [];

        return new Res([
            'exec' => $res
        ]);
    }

}
