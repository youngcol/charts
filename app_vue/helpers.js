import axios from 'axios'

var def = {
    uniq_id: 0,

    api: {
        /**
         *
         * @param url
         * @param params
         * @returns {Promise<AxiosResponse<T>>}
         */
        async get(url, params={})
        {
            var props = {};
            props.params = params;

            return axios.get(url, props)
                .catch(error => {
                    error.response
                });
        },

        /**
         *
         * @param url
         * @param data
         * @returns {Promise<AxiosResponse<T>>}
         */
        async post(url, data={})
        {
            if (data instanceof FormData)
            {
                _.keys(csrf).map(function (key) {
                    data.append(key, csrf[key]);
                });
            }
            else
            {
                data = _.extend(data, csrf);
            }

            return axios
                .post(url, data)
                .catch(error => {
                    console.log(error);
                });
        },

        /**
         *
         * @param taskName
         * @param params
         * @returns {Promise<{resp: *, isOk: (function(): boolean), get: (function(*): *)}>}
         */
        async runTask(taskName, params)
        {
            var data = {
                task_name: taskName,
                task_params: params
            };

            data = _.extend(data, csrf);
            var url = sp(routes, 'api.run.task') + "?" + taskName + "____" + JSON.stringify(params);
            var ret = await axios.post(url, data);

            return taskResponse(ret.data.data['res__'+ taskName]);
        }
    },

    /**
     * Get route url by name
     *
     * @param name
     * @returns {*}
     */
    route(name)
    {
        var ret = sp(routes, name);
        if (ret === null)
        {
            throw Error(`No such route: ${name}`)
        }
        return ret;
    },

    get_uniq_id()
    {
        return this.uniq_id++;
    },

    log(str)
    {
        console.log(str);
    },

    make_id(length) {
        let result = '';
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        const charactersLength = characters.length;
        let counter = 0;
        while (counter < length) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
            counter += 1;
        }
        return result;
    },

    tline_trading_params(type, be_val, sl_val, risk, risk_type, pair, uniq_id) {
        return {
            type: type,
            sl: sl_val,
            be: be_val,
            tp_coef: 1,
            risk: risk,
            risk_type: risk_type,
            pair: pair,
            uniq_id: uniq_id,
            is_skip_be: 0,
        };
    },

    beep()
    {
        (new Audio("/sounds/beep2.wav")).play();
    }
};

var botUtterance = null;

def.speech = {

    init()
    {
        var U = new SpeechSynthesisUtterance();
        let voices = speechSynthesis.getVoices();
        const defaultVoice = voices.find(
            (voice) => voice.name === "Google русский"
        );

        U.onerror = function (err) {
            console.error(err);
        };
        U.voice = defaultVoice;
        U.lang = "ru-RU";
        U.volume = 1;
        U.rate = 1;
        U.pitch = 1;

        botUtterance = U;

        return U;
    },

    async say(text)
    {
        botUtterance.text = text;
        speechSynthesis.speak(botUtterance);
//        speechSynthesis.cancel()
    },
    cancel()
    {
        speechSynthesis.cancel();
    }
};

/**
 * Models repository object
 */
def.repo = {
    /**
     * Get cu models collection from server
     *
     * @param modelBuilder
     * @param params
     *      - models_list - build models from list
     *      - page -
     *      - limit -
     *      - order -
     *
     * @returns {Promise<Object|*>}
     */
    async getModels(modelObject, params, context)
    {
        var data = {
            model_name: modelObject.prototype.getModelName(),
            page: sp(params, 'page', 0),
            per_page: sp(params, 'perPage', 10),
            sort_by: sp(params, 'sort'),
            order: sp(params, 'order', 'asc'),
            relations: sp(params, 'relations', []),
            where: sp(params, 'where', []),

            with_total: sp(params, 'with_total', 0),
            with_is_next: sp(params, 'with_is_next', 0),
        };

        if (params.relations && params.relations.length)
        {
            data.relations = params.relations;
        }

        var respModels = await def.api.runTask('GetModelsCollectionTask', data);

        if (params.models_list)
        {
            var res = respModels.resp;
            var ret = _.cloneDeep(_.omit(res, ['list']));

            return _.extend(ret, {
                    list: _.map(respModels.resp.list, (item) => {
                        return modelObject(item);
                    })
                }
            )
        }
        else
        {
            return respModels.resp;
        }
    },


    /**
     * Update models on server
     * @param modelObject
     * @param modelsData
     * [{id: 1, param: 4343}, {id:2, title: 'change1'}]
     * @returns {Promise<void>}
     */
    async updateModels(modelObject, modelsData)
    {
        var ret = await def.api.runTask('UpdateModelsCollectionTask', {
                model_name: modelObject.prototype.getModelName(),
                update_data: modelsData
            }
        );

        return ret;
    },

    /**
     * Delete model on server
     *
     * @param modelName
     * @param ids [1,2,3]
     * @returns {Promise<void>}
     */
    async deleteModels(modelObject, ids=[])
    {
        var ret = await def.api.runTask('DeleteModelsCollectionTask', {
                model_name: modelObject.prototype.getModelName(),
                models_ids: ids
            }
        );

        return ret;
    },
};


export default def;
