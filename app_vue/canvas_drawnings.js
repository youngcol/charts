import h from './helpers';

export default {

   data() {

      return {
         is_drawning: false,

         canvas1: null,
         canvas2: null,
         canvas3: null,

         canvas1_result: null,
         canvas2_result: null,
         canvas3_result: null,

         draw_line_type: "fattlb",
         drawnings_types: [
            'fattlb',
            'circle',
         ],
         drawnings_types_locales: {
            'tlb34' : 'TLB34',
            'tlb144': 'TLB144',
            'fattlb': 'BET',
            'circle': 'BET',
            'nh': 'NH',
            'nl': 'NL',
            'alligator': 'ALI'
         },

         is_need_to_commit_drawning: false,
         is_alligator_drawning: false,
      }
   },

   computed: {


   },

   methods: {

      current_drawnings_color(y1, y2, draw_type)
      {
         var color = 'black'; //y2 > y1 ? "blue" : "red" ;//;

         if(draw_type == 'circle')
         {
            color = y2 > y1 ? "blue" : "red";
         }

         if(draw_type == 'fattlb')
         {
            color = 'red';
         }

         ///
         return color;
      },

      reset_drawning_instruments()
      {
         this.is_alligator_drawning = false;
      },

       isDrawning()
       {
           return this.is_drawning === true;
       },

      toggleDrawning(is_drawning)
      {
         this.is_drawning = is_drawning;
          this.canvas1_result = null;
          this.canvas1 = this.remove_canvas('image_holder');
          this.setup_canvas_functionality('first', 'image_holder');

         if (this.isDrawning())
         {

            // this.setup_canvas_functionality('second', this.second_chart_id);
            // this.setup_canvas_functionality('third', this.third_chart_id);

         } else {

            // this.canvas2 = this.remove_canvas(this.second_chart_id);
            // this.canvas3 = this.remove_canvas(this.third_chart_id);

            this.canvas1_result = null;
            // this.canvas2_result = null;
            // this.canvas3_result = null;
         }
      },

      setup_canvas_draw_line_color_and_stroke(canvas, start_y, y)
      {
         if(!canvas) return;

         var ctx = canvas.getContext('2d');
         ctx.lineWidth = 2;

         if (this.draw_line_type == 'circle')
         {
            ctx.lineWidth = 1;
         }

         ctx.strokeStyle = this.current_drawnings_color(start_y, y,this.draw_line_type);
      },

      remove_canvas(pic_holder)
      {
         var canvas = $('#' + pic_holder + " .canvas");
         canvas.remove();

         return null;
      },

      clear_canvas(canvas)
      {
         var ctx = canvas.getContext('2d');
         ctx.clearRect(0, 0, canvas.width, canvas.height);
      },

      async commitCanvasContent(canvasPlace, canvas)
      {
         this.upload_canvas_content(canvas, canvasPlace == 'first' ? this.first_chart_period : this.second_chart_period);
         this.clear_canvas(canvas);
      },

      toggleDrawLineType()
      {
         var self = this;
         var index = _.findIndex(this.drawnings_types, (e) => {
            return self.draw_line_type == e;
         });
         var next_index = array__next_index(index, this.drawnings_types);
         this.draw_line_type = this.drawnings_types[next_index];

         this.change_canvas_draw_line_type(this.draw_line_type);
      },

      commit_fresh_drawnings(period, pair)
      {
         var self = this;
         if(this.canvas1)
         {
             this.upload_canvas_content(this.canvas1_result, period, pair);

            //  self.commitCanvasContent('first', this.canvas1_result);
            // self.refresh_drawning_url('first');
            // self.clear_canvas(this.canvas1);
            // self.clear_canvas(this.canvas1_result);
         }

         // if(this.canvas2)
         // {
         //    self.commitCanvasContent('second', this.canvas2_result);
         //    self.refresh_drawning_url('second');
         //    self.clear_canvas(this.canvas2);
         //    self.clear_canvas(this.canvas2_result);
         // }
         // if(this.canvas3)
         // {
         //    self.commitCanvasContent('third', this.canvas3_result);
         //    self.refresh_drawning_url('third');
         //    self.clear_canvas(this.canvas3);
         //    self.clear_canvas(this.canvas3_result);
         // }

         this.reset_drawning_instruments();
         this.is_need_to_commit_drawning = false;
      },

      refresh_drawning_url(canvasPlace)
      {
         if (canvasPlace == 'first') {
            if(this.$refs.drawning1) {
               this.$refs.drawning1.src = this.drawnings_url(this.first_chart_period);
            }
         } else if (canvasPlace == 'second') {
            if (this.$refs.drawning2) {
               this.$refs.drawning2.src = this.drawnings_url(this.second_chart_period);
            }
         } else if (canvasPlace == 'third') {
            if (this.$refs.drawning3) {
               this.$refs.drawning3.src = this.drawnings_url(this.third_chart_period);
            }
         }
      },

      async onClearDrawnings(pair, period)
      {
         var rOhlc = await this.runTask('remove__screenshot__drawning__task', {
            instrument: pair,
            period: period,
            pic_view: this.current_pic_view
         });

         // this.refresh_drawning_url(canvasPlace);
         this.clear_result_canvas();
      },

      clear_result_canvas()
      {
         this.clear_canvas(this.canvas1_result);

         this.is_alligator_drawning = false;
         this.is_need_to_commit_drawning = false;
      },

      create_box_canvas(holderClass)
      {
         var $box = $('#' + holderClass);
         var self = this;
         var width = $box.width();
         var height = $box.height();
         var canvas_d = 'canv-' + h.get_uniq_id();
         $box.append("<canvas id="+canvas_d+" width=\""+width+"\" height=\""+height+"\" class=\"canvas\" style=\"\n" +
            "    position: absolute;\n" +
            "    left: 0px;\n" +
            "    top: 0px;\n" +
            "    z-index: 100;\n" +
            "\">\n" +
            "                                Your browser doesn't support canvas\n" +
            "                            </canvas>");

         return $('#'+canvas_d, $box);
      },


      setup_canvas_functionality(canvasPlace, holderClass)
      {
         var $box = $('#' + holderClass);
         var self = this;

         var width = $box.width();
         var height = $box.height();
         var $merge_canvas = this.create_box_canvas(holderClass);
         var $canvas = this.create_box_canvas(holderClass);
         var canvas = $canvas[0];
         var self = this;

         if (canvasPlace == 'first') {
            this.canvas1 = canvas;
            this.canvas1_result = $merge_canvas[0];
         } else if (canvasPlace == 'second') {
            this.canvas2 = canvas;
            this.canvas2_result = $merge_canvas[0];
         } else if (canvasPlace == 'third') {
            this.canvas3 = canvas;
            this.canvas3_result = $merge_canvas[0];
         }


         var ctx = canvas.getContext('2d');
         var startX, startY;

         var commitID = 0;
         // mouse events
         $canvas.mousedown(function(e) {
            startX = e.offsetX;
            startY= e.offsetY;

            //debugger;
            if (e.ctrlKey)
            {
               self.draw_line_type = 'circle';
            } else {
               self.draw_line_type = 'fattlb';
            }

            $(this).bind('mousemove', function(e){

               if(self.draw_line_type != 'circle')
               {
                  self.setup_canvas_draw_line_color_and_stroke(canvas, startY, e.offsetY);
                  if(self.draw_line_type == 'alligator')
                  {
                     if (!self.is_alligator_drawning) {
                        self.put_alligator_drawning(1, ctx, startX, startY, e.offsetX, e.offsetY, width, height);
                     } else {
                        self.put_alligator_drawning(2, ctx, startX, startY, e.offsetX, e.offsetY, width, height);
                     }

                  } else {
                     self.put_drawning(ctx, startX, startY, e.offsetX, e.offsetY, width, height);
                  }
               }

            });
         }).mouseup(function(e){
            $(this).unbind('mousemove');

            // circle
            if(self.draw_line_type == 'circle')
            {
               self.setup_canvas_draw_line_color_and_stroke(canvas, startY, e.offsetY);
               self.draw_circle(ctx, startX, startY, startY - e.offsetY, Math.abs(startY - e.offsetY)/2);
            }

            if(self.is_alligator_drawning)
            {
               self.is_alligator_drawning = false;
            } else {
               self.is_alligator_drawning = true;
            }

            var merge_ctx = $merge_canvas[0].getContext('2d');
            merge_ctx.drawImage(canvas, 0, 0);
            self.clear_canvas(canvas);

            // self.on_added_drawning_line(Math.abs(startY - e.offsetY))
            self.is_need_to_commit_drawning = true;
         }).dblclick(function (){
            ctx.clearRect (0, 0, width, height);
         });

         return canvas;
      },

      put_alligator_drawning(part, ctx, x, y, stopX, stopY, width, height)
      {
         var radius = 2;
         ctx.clearRect (0, 0, width, height);

         if (part == 1) {
//            ctx.fillText(this.drawnings_types_locales[this.draw_line_type], stopX + 5, stopY + 5);
            //ctx.stroke();

            //// eyes
            ctx.beginPath();

            var eyes_y = 0;
            if (y < stopY) {
               var eyes_y = +10;
            }

            ctx.arc(stopX - 5 + eyes_y,  stopY - 11, radius, 0, 2 * Math.PI, false);
            ctx.arc(stopX - 15 + eyes_y,  stopY - 11, radius, 0, 2 * Math.PI, false);
            ctx.lineWidth = 5;

            ctx.closePath();
            ctx.stroke();

            // teeth
            ctx.beginPath();
            ctx.lineWidth = 3;
            ctx.moveTo(x, y);
            ctx.lineTo(x, y+5 );
            ctx.closePath();
            ctx.stroke();
         } else if (part == 2) {

            ctx.lineWidth = 3;
            // teeth
            ctx.beginPath();
            ctx.moveTo(x, y);
            ctx.lineTo(x, y-5);
            ctx.closePath();
            ctx.stroke();
         }

         // line
            ctx.lineWidth = 2;
            ctx.beginPath();
            ctx.moveTo(x, y);
            ctx.lineTo(stopX, stopY);
            ctx.closePath();
            ctx.stroke();
         },

      put_drawning(ctx, x, y, stopX, stopY, width, height)
      {
         ctx.clearRect (0, 0, width, height);

         // #label
         ctx.fillStyle = this.current_drawnings_color(y, stopY); //<======= here
//         ctx.fillText(this.drawnings_types_locales[this.draw_line_type], x - 5, y + 10);

         // lineelder
         ctx.beginPath();
         ctx.moveTo(x, y);
         ctx.lineTo(stopX, stopY);
         ctx.closePath();
         ctx.stroke();
      },

      draw_circle(context, x, y, direction, radius)
      {
         context.fillText(this.drawnings_types_locales[this.draw_line_type], x -10, y + (radius + 10) * (direction < 0 ? 1 : -1) );

         context.beginPath();
         context.arc(x, y, radius, 0, 2 * Math.PI, false);
         context.stroke();
      },

      upload_canvas_content(canvas, chart_period, pair)
      {
         var canvasData = canvas.toDataURL("image/png");
         var self = this;
         var ajax = new XMLHttpRequest();
         ajax.onreadystatechange = function() {
           if (ajax.readyState === 4) {
              // update drawning
              var resp = JSON.parse(ajax.response).data;

//              if (!self.current_drawnings[resp.period][self.current_pic_view]) {
//                 self.current_drawnings[resp.period][self.current_pic_view] = resp.drawnings[resp.pair][resp.period][self.current_pic_view];
//              }
           }
         }

         let body = "img=" + canvasData + "&period=" + chart_period + "&pair=" + pair;
         ajax.open("POST", "/screenshot/image", false);
         ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
         ajax.send(body);
      },

      change_canvas_draw_line_type(type)
      {
         this.draw_line_type = type;
      },

   }
};
