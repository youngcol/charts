import h from './helpers';
import moment from "moment";

export default {
    methods: {
        /**
         * Return value from locales array
         *
         * @param route
         * @returns {*|string}
         */
        $L: function (route) {
            var parts = route.split('.');

            var ret = this.$root.i18n[parts.shift()];

            parts.map(function (i) {
                ret = ret[i];
            });

            // debugger;
            // return this.$root.i18n[route];

            return ret;
        },

        log(title, data)
        {
            console.log(title, data);
        },

        async runRipperCommand(command, params)
        {
            var ret = await this.runTask('put__command__to__the__sheet__task', {
                name: command,
                params: params
            });

            return ret.resp;
        },

        async runTask(name, params)
        {
            return await h.api.runTask(name, params);
        },

        async apiGetModels()
        {

        },

        successNotify(message='Something happened correctly!')
        {
            this.$buefy.notification.open({
                message: message,
                type: 'is-success'
            })
        },

        errorNotify(message='Error happend.')
        {
            this.$buefy.notification.open({
                message: message,
                type: 'is-danger'
            });
        },

        toastOk(message)
        {
            this.$buefy.toast.open({
                message: message,
                type: 'is-success',
                position: 'is-bottom-right'
            });
        },

        toastError(message)
        {
            this.$buefy.toast.open({
                message: message,
                type: 'is-danger',
                position: 'is-bottom-right'
            });
        },

        toastConfirm(assert, message, failMessage)
        {
            if (assert)
            {
                this.toastOk(message);
            }
            else
            {
                this.toastError(failMessage)
            }
        },

        emitTradeSignal(type, source, instrument, chart_group_id, unique_id)
        {
            this.$root.$emit('TRADE_SIGNAL', {
                type: type,
                source: source,
                time: moment().format('HH:mm:ss'),
                instrument: instrument,
                chart_group_id: chart_group_id,
                unique_id: unique_id
            });
        },

        open_symbol_mt4(symbol, is_open_m5)
        {
            this.runTask('put__command__to__the__sheet__task', {
                name: "OPEN_SYMBOL",
                params: {
                    symbol: symbol,
                    is_open_m5: is_open_m5 ? 1 : 0

                    // left: settings.charts.big_periods[0],
                    // center: settings.charts.big_periods[1],
                    // right: settings.charts.big_periods[2]
                }
            });
        }

    }
}
