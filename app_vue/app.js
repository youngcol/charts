import Vue from 'vue';
import _ from 'lodash'
import Buefy from 'buefy'

Vue.use(Buefy)

require('./bootstrap');


// debugger;
// if (process.env.MIX_APP_ENV == 'dev')
// {
//     debugger;
// }

// ----------- COMPONENTS ---------
import DataTable from './components_framework/DataTable.vue';
import DataList from './components_framework/DataList.vue';
import DraftsList from './components/drafts-list/DraftsList.vue';
import QueueList from './components/queue-list/QueueList.vue';
import SentList from './components/sent-list/SentList.vue';
import AccountSettingsGeneral from './components/account-settings/AccountSettingsGeneral.vue';
import AccountSettingsPassword from './components/account-settings/AccountSettingsPassword.vue';
import Chart from './components/Chart.vue';
import ChartsGroup from './components/ChartsGroup.vue';
import Macd from './components/Macd.vue';
import Rsi from './components/Rsi.vue';
import Timeline from './components/Timeline.vue';
import Screenshots from './components/Screenshots.vue';
import ScreenshotsTape from './components/ScreenshotsTape.vue';
import StatusPage from './components/StatusPage.vue';
import ScreenshotsOverview from './components/ScreenshotsOverview.vue';
import Pic from './components/pic/PicComponent.vue';
import TrendlineEditor from './components/TrendlineEditor.vue';
import ScreenshotsChart from './components/ScreenshotsChart.vue';
import TapeItem from './components/TapeItem.vue';
import OrdersPanel from './components/OrdersPanel.vue';
import HealthCheck from './components/HealthCheck.vue';
import ScreenshotsViewPair from './components/ScreenshotsViewPair.vue';
import ScreenshotsPush from './components/ScreenshotsPush.vue';
import ScreenshotsPanel from './components/ScreenshotsPanel.vue';
import SmallOhlcChart from './components/SmallOhlcChart.vue';
import PairOverview from './components/PairOverview.vue';
import InstrumentView from './components/InstrumentView.vue';
import InstrumentView2 from './components/InstrumentView2.vue';
import InstrumentView3 from './components/InstrumentView3.vue';
import AttackPanel from './components/AttackPanel.vue';
import SignalAttackPanel from './components/SignalAttackPanel.vue';
import TlinesPanel from './components/TlinesPanel.vue';
import TlineParams from './components/TlineParams.vue';
import DepoStatus from './components/DepoStatus.vue';
import DealsFeed from './components/DealsFeed.vue';
import ScreenerPage from './components/ScreenerPage.vue';
import GamePage from './components/GamePage.vue';
import ScreenerBadges from './components/ScreenerBadges.vue';
import MockupPanel from './components/MockupPanel.vue';
import Trainer from './components/Trainer.vue';
import PeriodFeed from './components/PeriodFeed.vue';
import SignalsTape from './components/SignalsTape.vue';
import SignalItem from './components/SignalItem.vue';
import WatchFeed from './components/WatchFeed.vue';
import Journal from './components/Journal.vue';
import MarkupPanel from './components/MarkupPanel.vue';
import SignalsFeed from './components/SignalsFeed.vue';
import SignalsFeedItem from './components/SignalsFeedItem.vue';
import SymbolOrdersList from './components/SymbolOrdersList.vue';
import MobilePage from './components/MobilePage.vue';
import PairEventView from './components/PairEventView.vue';
import Panel2 from './components/Panel2.vue';
import PairIndicatorView from './components/PairIndicatorView.vue';
import OhlcLinerView from './components/OhlcLinerView.vue';
import PairScreeshots from './components/PairScreeshots.vue';

// ----------- STORY COMPONENT ----------------
import Profile from './vue_stories/profile/Profile.vue';

// Framework components
Vue.component('data-table', DataTable);
Vue.component('data-list', DataList);

// App components
Vue.component('profile', Profile);
// Vue.component('create-post-window', CreatePostWindow);
// Vue.component('social-accounts', SocialAccounts);
// Vue.component('posts-list', PostsList);
Vue.component('drafts-list', DraftsList);
Vue.component('queue-list', QueueList);
Vue.component('sent-list', SentList);

Vue.component('account-settings-general', AccountSettingsGeneral);
Vue.component('account-settings-password', AccountSettingsPassword);
Vue.component('account-settings-password', AccountSettingsPassword);

Vue.component('chart', Chart);
Vue.component('macd', Macd);
Vue.component('pic', Pic);
Vue.component('rsi', Rsi);
//Vue.component('charts-group', ChartsGroup);
//Vue.component('timeline', Timeline);
//Vue.component('screenshots', Screenshots);
//Vue.component('screenshots-tape', ScreenshotsTape);
//Vue.component('screenshots-overview', ScreenshotsOverview);
Vue.component('status-page', StatusPage);
//Vue.component('trendline-editor', TrendlineEditor);
//Vue.component('screenshots-chart', ScreenshotsChart);
//Vue.component('tape-item', TapeItem);
Vue.component('orders-panel', OrdersPanel);
Vue.component('healt-check', HealthCheck);
Vue.component('screenshots-view-pair', ScreenshotsViewPair);
//Vue.component('screenshots-push', ScreenshotsPush);
Vue.component('screenshots-panel', ScreenshotsPanel);
Vue.component('small-ohlc-chart', SmallOhlcChart);
//Vue.component('pair-overview', PairOverview);
Vue.component('instrument-view', InstrumentView);
Vue.component('instrument-view2', InstrumentView2);
Vue.component('instrument-view3', InstrumentView3);
Vue.component('attack-panel', AttackPanel);
Vue.component('signal-attack-panel', SignalAttackPanel);
Vue.component('tlines-panel', TlinesPanel);
Vue.component('tline-params', TlineParams);

Vue.component('depo-status', DepoStatus);
Vue.component('deals-feed', DealsFeed);
Vue.component('screener', ScreenerPage);
Vue.component('game', GamePage);
Vue.component('screener-badges', ScreenerBadges);
Vue.component('mockup-panel', MockupPanel);
Vue.component('trainer', Trainer);
Vue.component('period-feed', PeriodFeed);
Vue.component('signals-tape', SignalsTape);
Vue.component('signal-item', SignalItem);
Vue.component('watch-feed', WatchFeed);
Vue.component('journal', Journal);
Vue.component('markup-panel', MarkupPanel);
Vue.component('symbol-orders-list', SymbolOrdersList);
Vue.component('signals-feed', SignalsFeed);
Vue.component('signals-feed-item', SignalsFeedItem);
Vue.component('mobile-page', MobilePage);
Vue.component('pair-event-view', PairEventView);
Vue.component('panel2', Panel2);
Vue.component('pair-indicator-view', PairIndicatorView);
Vue.component('ohlc-liner-view', OhlcLinerView);
Vue.component('pair-screenshots', PairScreeshots);

require('./actions_vue_register');



// ----------- END COMPONENTS ---------


// ----------- MODELS ---------
import h from './helpers';
import UserModel from './models/UserModel'
import TestModel from './models/TestModel'
// ----------- END MODELS ---------

import mixins from './components_mixins.js';

var currentUser = new UserModel(current_user);

let globalVue = new Vue({

    el: '#main-body',
    mixins: [mixins],

    data: _.extend(globalVueData, {

        currentUser: currentUser,

        // modal window example
        isCreatePostModalActive: false,
        createPostWindowProps: {
            email: 'evan@you.com',
            password: 'testing'
        },

        // create chart group
        newChartGroup: {}

    }),

    beforeMounted()
    {
        this.newChartGroup = this.defNewChartGroup();

    },

    mounted()
    {
        var vm = this;

        window.addEventListener('keydown', function(event) {
            if (event.key == ' ') {
                vm.$root.$emit('keyup_space');
                event.preventDefault();
            }
            if (event.key == 'Backspace') {
                vm.$root.$emit('keyup_backspace');
                event.preventDefault();
            }
            if (event.key == 'F1') {
                vm.$root.$emit('keydown_f1');
                event.preventDefault();
            }
            if (event.key == 'Control') {
                vm.$root.$emit('keydown_ctrl');
                event.preventDefault();
            }
            if (event.key == 'Shift') {
                vm.$root.$emit('keydown_shift');
                event.preventDefault();
            }

            if (event.key == 'CapsLock') {
                vm.$root.$emit('caps_lock');
                event.preventDefault();
            }

            if (event.key == 'Tab') {
                event.preventDefault();
                vm.$root.$emit('keydown_tab');
            }
        });

        window.addEventListener('keyup', function(event) {
            if (event.key == 'ArrowRight') {
                vm.$root.$emit('keyup_arrow_right');
            }
            if (event.key == 'ArrowLeft') {
                vm.$root.$emit('keyup_arrow_left');
            }
            if (event.key == 'ArrowUp') {
                vm.$root.$emit('keyup_arrow_up');
            }
            if (event.key == 'ArrowDown') {
                vm.$root.$emit('keyup_arrow_down');
            }
            if (event.key == 'Escape') {
                vm.$root.$emit('keyup_escape');
            }
            if (event.key == 'Enter') {
                vm.$root.$emit('keyup_enter');
            }
            if (event.key == '+') {
                vm.$root.$emit('keyup_plus');
            } else {
                vm.$root.$emit('keyup', event);
            }
        });

        h.speech.init();
    },

    methods: {



        // onKeyUp(e)
        // {
        //     if (e.key === 'ArrowRight') {
        //         e.preventDefault();
        //         debugger;
        //
        //         if (this.viewPic && this.viewPair.pics_by_index[this.viewPic.index+1]) {
        //             this.viewPic = this.viewPair.pics_by_index[this.viewPic.index+1];
        //         }
        //     } else if (e.key === 'ArrowLeft') {
        //         e.preventDefault();
        //         debugger;
        //
        //         if (this.viewPic && this.viewPair.pics_by_index[this.viewPic.index-1]) {
        //             this.viewPic = this.viewPair.pics_by_index[this.viewPic.index-1];
        //         }
        //     }
        // },

        defNewChartGroup()
        {
            return {
                instrument: 'eur_usd',
                name: '',
            }
        },


        handleScroll(e)
        {

        }
    }

});

