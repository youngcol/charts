import axios from "axios";
import moment from 'moment';

export default {

    data()
    {
        return {
            // delay in seconds
            INSTRUMENT_PRICE_DELAY: 5,
            chartGroupsByInstrument: {}
        };
    },

    methods: {

        async notifyChartLevelsBreakout()
        {
            this.connectPriceStream(true);

            // var resp = {
            //     type: "PRICE",
            //     time: "2020-05-01T17:08:28.014955850Z",
            //     closeoutBid: "1.0989",
            //     closeoutAsk: "1.0989",
            //     status: "tradeable",
            //     tradeable: true,
            //     instrument: "EUR_USD",
            // };
            //
            // this.gotNewPriceForInstrument(resp);
            // this.gotNewPriceForInstrument(resp);
        },

        /**
         * done only for minutes
         *
         * @param priceResp
         */
        updateChartGroupTimeseria(priceResp)
        {
            var groupInfo = this.chartGroupsByInstrument[priceResp.instrument];
            var chartGroup = _.find(this.resChartGroups, {id: groupInfo.group_id});
            var chart = chartGroup.charts[0];

            // this.mergePriceIntoTimeseria(chart.resChartTimeseries.timeseries.M1, 1, 'minutes', priceResp);
            this.mergePriceIntoTimeseria(chart.resChartTimeseries.timeseries.M15, 15, 'minutes', priceResp);
            // this.mergePriceIntoTimeseria(chart.resChartTimeseries.timeseries.S10, 10, 'seconds', priceResp);

            this.getChartGroupComponent(groupInfo.group_id)
                .redrawChartsCurrentChartSeries();

            if (sp(this.currentChartGroup, 'id') === toInt(groupInfo.group_id)) {
                this.$refs['current_chart_group'].redrawChartsCurrentChartSeries();
            }
        },

        mergePriceIntoTimeseria(timeseria, timeframe, diffType, priceResp)
        {
            var priceTime = moment.utc(priceResp.time);
            var priceValue = parseFloat(priceResp.closeoutAsk);
            var lastCandle = _.last(timeseria);
            var diffTime = priceTime.diff(moment.utc(lastCandle.date), diffType);

            if (diffTime >= timeframe)
            {
                timeseria.push({
                    id: lastCandle.id+1,
                    o: priceValue,
                    h: priceValue,
                    l: priceValue,
                    c: priceValue,
                    date: priceTime.format('YYYY-MM-DD HH:mm:ss')
                })
            }
            else
            {
                lastCandle.c = priceValue;
                if (lastCandle.c > lastCandle.h)
                {
                    lastCandle.h = lastCandle.c;
                }

                if (lastCandle.c < lastCandle.l)
                {
                    lastCandle.l = lastCandle.c;
                }
            }
        },


        gotNewPriceForInstrument(priceResp)
        {
            var self = this;
            // log(priceResp);

            var groupInfo = this.chartGroupsByInstrument[priceResp.instrument];
            var chartGroup = _.find(this.resChartGroups, {id: groupInfo.group_id});
            var chart = chartGroup.charts[0];

            groupInfo.chart_id
            chartGroup.id

            var priceTime = moment.utc(priceResp.time);

            chart.chart.levels.map(level => {
                if (!level.is_fired)
                {
                    var isLevelFired = false;
                    var breakDirection = '';
                    if (level.type === 'support' && (parseFloat(priceResp.closeoutAsk) < parseFloat(level.value)))
                    {
                        isLevelFired = true;
                        breakDirection = 'down';
                    }
                    else if(level.type === 'resistance' && (parseFloat(priceResp.closeoutAsk) > parseFloat(level.value)))
                    {
                        isLevelFired = true;
                        breakDirection = 'up';
                    }

                    if (isLevelFired)
                    {
                        level.is_fired = 1;

                        var resAdd = this.runTask('add__level__to__chart__task', {
                            chart_id: chart.chart.id,
                            value: level.value,
                            type: level.type,
                            is_fired: level.is_fired,
                            level_id: level.id
                        });

                        this.addNotifedEvent(
                            chartGroup.id,
                            priceResp.instrument,
                            'break ' + level.type + ' ' + level.value +' level Current price: ' + priceResp.closeoutAsk,
                            breakDirection
                        );

                        var uniqId = ['level', chart.chart.instrument, level.id].join('_');
                        this.emitTradeSignal(level.type === 'resistance' ? 'buy' : 'sell', 'level', chart.chart.instrument, chart.chart.group_id, uniqId);

                        var component = this.getChartGroupComponent(groupInfo.group_id);
                        if (component) {
                            component.setIsBorderActive(true);
                        }
                    }
                }
            });


            chart.chart.trendlines.map(trendline => {

                if (!trendline.is_fired)
                {
                    trendline.id

                    var endDate = moment.utc(trendline.end_date);
                    var startDate = moment.utc(trendline.start_date);

                    if (priceTime.isBetween(startDate, endDate))
                    {
                        var isBreakout = this.isBreakoutTrendline(trendline, parseFloat(priceResp.closeoutAsk), priceTime);

                        if (isBreakout)
                        {
                            trendline.is_fired = 1;

                            var resAdd = this.runTask('add__trendline__to__chart__task', {
                                chart_id: chart.chart.id,
                                id: trendline.id,
                                start_date: trendline.start_date,
                                end_date: trendline.end_date,
                                is_fired: 1,
                                start_value: trendline.start_value,
                                end_value: trendline.end_value,
                            });

                            var type = this.getTrendlineType(trendline);

                            this.addNotifedEvent(
                                chartGroup.id,
                                priceResp.instrument,
                                type + ' trendline breakout',
                                type === 'support' ? 'down' : 'up'
                            );

                            var uniqId = ['trendline', chart.chart.instrument, trendline.id].join('_');
                            this.emitTradeSignal(type === 'resistance' ? 'buy' : 'sell', 'trendline', chart.chart.instrument, chart.chart.group_id, uniqId);


                            var component = this.getChartGroupComponent(groupInfo.group_id);
                            if (component) {
                                component.setIsBorderActive(true);
                            }
                        }
                    }
                }
            });


            this.updateChartGroupTimeseria(priceResp);

            // var jsonResponse =
            // {
            //     type: "PRICE",
            //     time: "2020-05-01T16:32:48.010264534Z",
            //     closeoutAsk: "1.41033",
            //     closeoutAsk: "1.41055",
            //     status: "tradeable",
            //     tradeable: true,
            //     instrument: "USD_CAD",
            // };
        },

        isBreakoutTrendline(trendline, priceValue, priceTime)
        {
            var ret = false;
            var rangeDelta = trendline.end_value - trendline.start_value;
            var type = this.getTrendlineType(trendline);

            var diffMinutes = moment(trendline.end_date).diff(moment(trendline.start_date), 'minutes');
            var rangeStep = rangeDelta / diffMinutes;
            var currentPriceIndex = Math.abs(moment.utc(trendline.start_date).diff(priceTime, 'minutes'));

            var pointPrice = parseFloat(trendline.start_value) + rangeStep * currentPriceIndex;
            var testEndPrice = trendline.start_value + rangeStep * diffMinutes;

            if (type === 'support' && priceValue < pointPrice)
            {
                ret = true;
            }
            else if(type === 'resistance' && priceValue > pointPrice)
            {
                ret = true;
            }

            return ret;
        },

        getTrendlineType(trendline)
        {
            return trendline.start_value < trendline.end_value ? 'support' : 'resistance'
        },
        
        async connectPriceStream(isConnect)
        {
            var lastPriceTime = {};
            var self = this;

            var instruments = [];

            var lookup = [];
            this.resChartGroups.map((chartGroup) => {
                chartGroup.charts.map((o) => {

                    self.chartGroupsByInstrument[o.chart.instrument.toUpperCase()] = {
                        chart_id: o.chart.id,
                        group_id: chartGroup.id
                    };

                    instruments.push(o.chart.instrument.toUpperCase());

                    lookup.push({
                        instrument: o.chart.instrument.toUpperCase(),
                        group_id: chartGroup.id,
                        chart_id: o.chart.id,
                        levels: o.chart.levels
                    });
                })
            });

            if (isConnect)
            {
                instruments = instruments.join('%2C');
                var url = `https://stream-fxpractice.oanda.com/v3/accounts/${this.$root.oanda.account_id}/pricing/stream?instruments=${instruments}`;
                fetch(url, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${this.$root.oanda.api_key}`,
                    }
                })
                .then((response) => {

                    const reader = response.body.getReader();

                    reader.read().then(function processText({ done, value }) {
                        // Result objects contain two properties:
                        // done  - true if the stream has already given you all its data.
                        // value - some data. Always undefined when done is true.

                        if (done)
                        {
                            self.lastPriceStreamTime = "Stream completed";
                            return;
                        }

                        var stringResponse = new TextDecoder("utf-8").decode(value);

                        stringResponse.split("\n").map(o => {
                            if (o.length)
                            {
                                try {
                                    var jsonResponse = JSON.parse(o);

                                    if (jsonResponse.type === 'PRICE')
                                    {
                                        self.lastPriceStreamTime = jsonResponse.instrument + ' ' + jsonResponse.time;

                                        var respTime = moment(jsonResponse.time);
                                        var respTimestamp = respTime.format('X');
                                        var isProcessPrice = false;
                                        if (!lastPriceTime[jsonResponse.instrument])
                                        {
                                            lastPriceTime[jsonResponse.instrument] = {
                                                timestamp: respTimestamp,
                                                price: parseFloat(jsonResponse.closeoutAsk)
                                            };
                                        }
                                        else
                                        {
                                            if ((parseInt(respTimestamp) - parseInt(lastPriceTime[jsonResponse.instrument].timestamp)) > self.INSTRUMENT_PRICE_DELAY)
                                            {
                                                if (jsonResponse.closeoutAsk !== lastPriceTime[jsonResponse.instrument].closeoutAsk)
                                                {
                                                    lastPriceTime[jsonResponse.instrument] = {
                                                        timestamp: respTimestamp,
                                                        price: parseFloat(jsonResponse.closeoutAsk)
                                                    };

                                                    isProcessPrice = true;
                                                }
                                            }
                                        }

                                        if (isProcessPrice)
                                        {
                                            self.gotNewPriceForInstrument(jsonResponse)
                                        }
                                    }
                                }
                                catch(error) {
                                    // console.error(o);
                                }
                            }

                        });

                        // Read some more, and call this function again
                        return reader.read().then(processText);
                    });

                });
            }


            /**
            type: "PRICE"
            time: "2020-04-27T08:30:28.014955850Z"
            bids: [{…}]
            asks: [{…}]
            closeoutBid: "1.08425"
            closeoutAsk: "1.08437"
            status: "tradeable"
            tradeable: true
            instrument: "EUR_USD"
            **/
        },

    }
}
