import {ObjectModel} from "objectmodel"

const M = new ObjectModel({

    active: Number,
    is_admin: Number,
    language: String,
    timezone: String,
    created_at: String,
    permissions: String,
    email: String,
    name: String,
    id: Number,

}).defaultTo({

    getNameAndEmail() {

        return this.getModelName() + this.email + ": Dear " + this.name;
    },

});

M.prototype.getModelName = function () {
    return 'App\\Models\\User';
};


export default M
