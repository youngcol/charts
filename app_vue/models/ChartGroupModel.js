
import {ObjectModel} from "objectmodel"
import ChartModel from './ChartModel'

// instrument: "eur_usd"
// view: "default"
// timeframe: "5m"
// scale: 100
// indicators: null

const M = ObjectModel({
    id:         [Number],
    next_popup_time:       [null, String],    // public
    name: String,    // public


    // scale:      Number,    // public
    // timeframe:  Number,    // public
    // indicators: [String, null],    // public
    // _index: Number,    // private
    // UNIT: ["px","cm"], // constant
    // _ID: [Number],     // private and constant

}).defaultTo({
    _index: 0,

    getIndex(){ return this._index },
    setIndex(value){ this._index = value },

    /**
     *
     * @param fds
     * @returns {string}
     */
    linkSomethign(fds) {
        return 'sdfsdfsdf' + this._index;
    },
});


M.prototype.getFirstChart = function () {

    var ret = null;

    if (sp(this.charts, 0))
    {
        ret = new ChartModel(this.charts[0].chart);
    }

    return ret;
};


M.prototype.getModelName = function () {
    return 'App\\Models\\ChartGroup';
};

export default M

