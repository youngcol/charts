
import {ObjectModel} from "objectmodel"

// instrument: "eur_usd"
// view: "default"
// timeframe: "5m"
// scale: 100
// indicators: null

const M = ObjectModel({
    id:         [Number],
    scale:      Number,    // public
    timeframe:  Number,    // public
    timeframe_type: ['M', 'S'],    // public

    indicators: [String, null],    // public
    view:       String,    // public
    instrument: String,    // public

    levels: Array,
    trendlines: Array,
    _parsedIndicators: [Object, null],    // private
    // UNIT: ["px","cm"], // constant
    // _ID: [Number],     // private and constant

}).defaultTo({
    _index: 0,
    _parsedIndicators: null,

    macd_params: {
        fast: 1,
        slow: 15,
        signal: 9,
    },

    getIndex(){ return this._index },
    setIndex(value){ this._index = value },

    /**
     *
     * @param fds
     */
    linkSomethign(fds) {
        return 'sdfsdfsdf' + this._index;
    },
});

M.prototype.getModelName = function () {
    return 'App\\Models\\Chart';
};


M.prototype.MINUTES_TIMEFRAME = 'M';
M.prototype.SECONDS_TIMEFRAME = 'S';

M.prototype.getIndicators = function () {

    var ret = [];

    if (this._parsedIndicators) {
        return this._parsedIndicators;
    }

    if (this.indicators)
    {
        ret = this.indicators.split(';').map(function (item) {

            var parts = item.split(':');
            var par = [];

            if (parts[1])
            {
                par = parts[1].split(',');
            }

            return {
                name: parts[0],
                params: par
            }
        });
    }


    this._parsedIndicators = ret;

    return ret;
};

M.prototype.isIndicatorPresent = function (name)
{
    if (eN(this._parsedIndicators))
    {
        this.getIndicators();
    }

    var needle = _.find(this._parsedIndicators, {name: name});
    return !eU(needle);
};

M.prototype.getMacdParams = function()
{
    return this.macd_params;
};


export default M
