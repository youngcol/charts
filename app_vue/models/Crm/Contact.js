import {ObjectModel} from "objectmodel"


const M = new ObjectModel({

    created_at: String,
    id: Number,
    email: String,
    name: String,
    site_url: String,
    phone: String,
    address: String,
    facebook_link: String,
    youtube_link: String,
    linkedin_link: String,
    yelp_url: String,
    yelp_reviews_count: Number,
    yelp_reviews: String,

    category_id: Number,
    location_id: Number,

}).defaultTo({

    some_model_func() {

        return "Dear " + this.name;
    },

});

M.prototype.getModelName = function () {
    return "App\\Models\\Crm\\Contact";
};

export default M
