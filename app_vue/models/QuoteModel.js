
import {ObjectModel} from "objectmodel"
import ChartModel from './ChartModel'

// instrument: "eur_usd"
// view: "default"
// timeframe: "5m"
// scale: 100
// indicators: null

const M = ObjectModel({
    id:         [Number],

    date:       String,    // public
    o:  Number,    // public
    h:  Number,    // public
    l:  Number,    // public
    c:  Number,    // public


    // scale:      Number,    // public
    // timeframe:  Number,    // public
    // indicators: [String, null],    // public
    // _index: Number,    // private
    // UNIT: ["px","cm"], // constant
    // _ID: [Number],     // private and constant

}).defaultTo({
    _index: 0,

    getIndex(){ return this._index },
    setIndex(value){ this._index = value },

    /**
     *
     * @param fds
     * @returns {string}
     */
    linkSomethign(fds) {
        return 'sdfsdfsdf' + this._index;
    },
});


M.prototype.merge = function (quote) {

    this.l = _.min([this.l, quote.l]);
    this.h = _.max([this.h, quote.h]);
    this.c = _.clone(quote.c);

};


M.prototype.getModelName = function () {
    return 'App\\Models\\Quote';
};

export default M

