import {ObjectModel} from "objectmodel"

const M = new ObjectModel({

    'social_network_id': Number,
    'post_id': Number,
    'is_success': Number,
    'posted_time': Date

}).defaultTo({

    some_model_func() {

        return "Dear " + this.name;
    },

});

M.prototype.getModelName = function () {
    return 'App\\Models\\SocialEngine\\Post\\PostPublishLog';
};

export default M
