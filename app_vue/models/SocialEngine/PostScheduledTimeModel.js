import {ObjectModel} from "objectmodel"

const M = new ObjectModel({

    'user_id': Number,
    'post_id': Number,
    'scheduled_time': Date,
    'process_started_time': Date,
    'publish_params_id': Number,
    'process_status': String


    // active: Number,
    // is_admin: Number,
    // language: String,
    // timezone: String,
    // created_at: String,
    // permissions: String,
    // email: String,
    // name: String,
    // id: Number,

}).defaultTo({

    some_model_func() {

        return "Dear " + this.name;
    },

});

M.prototype.getModelName = function () {
    return 'App\\Models\\SocialEngine\\Post\\PostScheduledTime';
};

export default M
