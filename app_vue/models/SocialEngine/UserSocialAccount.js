import {ObjectModel} from "objectmodel"

const M = new ObjectModel({

    id: Number,
    // user_id: Number,
    social_network_id: Number,
    username: String,
    profile_image: String

    // '',
    // 'social_network_id',
    // 'access_token',
    // 'access_token_secret',
    // 'username',
    // 'social_account_id',
    //
    // active: Number,
    // is_admin: Number,
    // language: String,
    // timezone: String,
    // created_at: String,
    // permissions: String,
    // email: String,
    // name: String,
    // id: Number,

}).defaultTo({

    some_model_func() {

        return "Dear " + this.name;
    },

});

M.prototype.getModelName = function () {
    return 'App\\Models\\SocialEngine\\UserSocialAccount';
}



export default M
