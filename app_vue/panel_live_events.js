import h from './helpers';
import moment from "moment";

export default {

   data() {
      return {
            frontEventsID: null,
            SIGNALS_HISTORY_PING: 3000, // 1 sec

            is_voice_setuped: 1,
            last_signal_id: 0,
            signals_counter: 0,
            last_ripper_event_id: 0,
            signals_feed: [],
            ripper_events: [],
            events: {list: []},

            // FLAGS FOR LIVE EVENTS
            is_get_open_orders: 1,
            is_procces_pics: 1,
            is_voice_notify: true,
            is_ohlc_live_update: 1,

            unmapped_symbols: null,

            last_hour: null,
            last_15min: null,
            last_5_min: null,
            is_courousel: false,


            COUROUSEL_PING: 10000,
            MARKUP_PAN_VOICE_NOTIFY_PING: 1000 * 60 * 5, // 5 min,
            markup_timestamp: 0,
            markup_requests: {
                count: 0
            }
      }
   },

   computed: {


   },

   mounted()
   {
      var self = this;

   },

   watch:{

   },
   methods: {

      markup_requests_notify()
      {
          h.speech.say(" Нужна разметка " + this.markup_requests.count);

          setTimeout(() => {
              //h.speech.cancel();
          }, 3000);
      },

      toggleVoiceNotify()
      {
         this.is_voice_notify = !this.is_voice_notify;
      },
      toggleCourousel()
      {
         this.is_courousel = !this.is_courousel;
         var self = this;

         if(this.is_courousel)
         {
            this.courousel_index = 0;
            this.courouselID = setInterval(async function () {

               if(self.courousel_index > self.market.instruments_all.length-1)
               {
                  self.courousel_index = 0;
               }

               let symbol = self.market.instruments_all[self.courousel_index++];
               self.runTask('put__command__to__the__sheet__task', {
                  name: "OPEN_SYMBOL",
                  params: {
                     symbol: symbol,
                  }
               });

            }, this.COUROUSEL_PING);
         }
         else
         {
            clearInterval(this.courouselID);
         }
      },

      voiceStart()
      {
         h.speech.say('Голосовые уведомления включены');
         this.is_voice_setuped = true;
      },

      toggleFrontendEvents()
      {
         this.is_frontend_events = !this.is_frontend_events;
         if (this.is_frontend_events) {
            this.frontEventsID = this.setup_frontend_events();
         } else {
            clearInterval(this.frontEventsID);
         }
      },
      setup_frontend_events()
      {
         var self = this;
         var  is_processing = false;

         return setInterval(async function () {
               is_processing = true;

               var rEvents = await self.runTask('get__frontend__events__task', {
                  limit: 36,
                  from_signal_id: self.last_signal_id,
                  from_ripper_event_id: self.last_ripper_event_id,
               });

                //self.$refs.signals_tape.updateSignals(rEvents.resp.events);

                // pivots

                 if (self.$refs.instrument_view)
                 {
                     self.$refs.instrument_view.map( (view) => {
                         view.UpdatePivots(rEvents.resp.pivot_points[view._props.instrument]);
                     });
                 }
             // screener badges
               if (self.$refs.instrument_view)
               {
                  self.$refs.instrument_view.map( (view) => {
                     view.onUpdatedScreenerState(rEvents.resp.screener.state);
                  });
               }

               self.$root.$emit('updated__screener_state', rEvents.resp.screener);

               // pics
               // if (self.current_page == 'market')
               // {
               //    if (self.is_procces_pics)
               //    {
               //       self.process_pics_timestamps(rEvents.resp.pics_timestamps);
               //    }
               // }

               // self.$refs.depo_status.UpdateTime(rEvents.resp.timeseries_updated_time);

                // if(self.processBinaryUnmapped)
                // {
                //     self.processBinaryUnmapped(
                //         rEvents.resp.markup_requests.binary_unmapped,
                //         rEvents.resp.markup_requests.binary_mapped
                //     );
                // }

               if(self.is_voice_notify)
               {
                   self.events = rEvents.resp.events;

                   if (self.events.not_shown_counter)
                   {
                       self.events.not_shown_list.map((e) => {
                           // self.$refs.pair_screenshots.setLastEvent(e);
                           // self.$refs.pair_indicator_view.setLastEvent(e);
                       });
                   }


                   //self.process_voice_events(rEvents.resp.events.list);

                   if (rEvents.resp.voice_notify) {
                       h.speech.say(rEvents.resp.voice_notify);
                   }
               }

                if (rEvents.resp.beep_queue) {
                    self.process_beep_item(rEvents.resp.beep_queue);
                }

             if (rEvents.resp.rsi_change_queue) {
                 self.process_rsi_change(rEvents.resp.rsi_change_queue);
             }


               self.process_voice_ripper_events(rEvents.resp.ripper_events.list);

               // ohlc live update
               if (self.is_ohlc_live_update)
               {
                  self.process_ohlc_last_candels(rEvents.resp.ohlc_last, rEvents.resp.empty_bars);
               }

               if (rEvents.resp.markup_requests)
               {
                   self.markup_requests = rEvents.resp.markup_requests;

                   var delta = Date.now() - self.markup_timestamp;
                   if ((self.markup_requests.count > 0)
                       && (delta > self.MARKUP_PAN_VOICE_NOTIFY_PING))
                   {
                       if (self.is_voice_notify)
                       {
                           //self.markup_requests_notify();
                       }

                       self.markup_timestamp = Date.now();
                   }
               }

                 // if (rEvents.resp.signals)
                 // {
                 //     self.process_signals(rEvents.resp.events);
                 // }

               // NOTIFY COMPONENTS
               // open orders
               if (rEvents.resp.open_orders)
               {
                    self.open_orders = rEvents.resp.open_orders;
                    self.$root.$emit('event__open_orders', rEvents.resp.open_orders);
               }

               is_processing = false;
            }, this.SIGNALS_HISTORY_PING);
         },

         process_signals(signals)
         {
             if (this.$refs.instrument_view)
             {
                 this.$refs.instrument_view.map( (view) => {
                     if(view.UpdateSignals)
                     {
                         view.UpdateSignals(signals[view._props.instrument]);
                     }
                 });
             }
         },

         process_ohlc_last_candels(last, empty_bars)
         {
            var self = this;

            if (this.$refs.instrument_view)
            {
               this.$refs.instrument_view.map( (view) => {
                  if(view.UpdateOhlcSeria)
                  {
                     // view.UpdateOhlcSeria(
                     //     last.candels[view._props.instrument],
                     //     last.values[view._props.instrument],
                     //     empty_bars.ohlc[view._props.instrument]
                     // );
                  }
               });
            }

         },

         process_pics_timestamps(timestamps)
         {
               //                console.log([
               //                    timestamps.last_timestamp_delta,
               //                    this.PICS_TIMESTAMP_UPDATE,
               //                    !this.is_req_load_pics_sent
               //                ]);

            // if (timestamps.last_timestamp_delta > this.PICS_TIMESTAMP_UPDATE && !this.is_req_load_pics_sent)
            // {
            //    this.is_req_load_pics_sent = true;
            //    var status = this.runTask('put__command__to__the__sheet__task', {
            //       name: "GET_SCREENSHOTS",
            //       params: {
            //          pair: 'all',
            //          period: 'all'
            //       }
            //    });
            //    //                    console.log('GET_SCREENSHOTS');
            // }

            if (timestamps.last_timestamp_delta < this.PICS_TIMESTAMP_UPDATE
                 || timestamps.last_timestamp_delta > (1.5 * this.PICS_TIMESTAMP_UPDATE))
            {
               this.is_req_load_pics_sent = false;
            }

            // udpate instrument view pic urls
            //console.log('pics timestamp', timestamps);

            var self = this;
            if (this.last_pic_timestamps)
            {
               timestamps.pairs.map((pair) => {
                  if(timestamps[pair] != self.last_pic_timestamps[pair])
                  {
                     this.$root.$emit('instrument_view__server_pic_updated', {
                        pair: pair
                     });
                  }
               });
            }

            this.last_pic_timestamps = timestamps;
         },
         tline_fired_voice_notify(tline)
         {
            if (tline.is_trading) {
               var action = tline.trading_type == 'buy' ? " торговое тлб синее " : "торговое тлб красное";
               } else {
               var action = tline.trading_type == 'buy' ? "тлб синее" : " тлб красное";
            }
            //action += ' на графике ';

            var text  = this.pair__ru_name(tline.pair) + " " + this.period__voice_word(Number(tline.period)) + " "  + action;
            h.speech.say(text);
         },

         ripperEventVoiceNotify(e)
         {
            switch (e.event)
            {
               case "open_market_order":
                  var action = " открыт " + (e.params.type == "buy" ? "бай " : " селл ") + e.params.risk;
                  var text  = this.pair__ru_name(e.params.symbol) + action;
               break;

               case "order_set_auto_be":
                  var action = " отбойник выставлен на " + (e.params.type == "buy" ? "бай " : " селл ");
                  var text  = this.pair__ru_name(e.params.symbol) + action;
               break;

               default:
                  crit('no such tag');
            }

            h.speech.say(text);
         },

         signalVoiceNotify(signal)
         {
            clearTimeout(this.stopSpechID);
            var is_do = true;

            if (is_do)
            {
               switch (signal.tag)
               {
                   case "price_movement":
                   {
                       if (signal.instrument == 'XAUUSD')
                       {
                           var str_price = signal.price.toString();
                           var price = str_price[0] + str_price[1] + ' ' + str_price[2] + str_price[3];
                       }
                       else
                       {
                           var str_price = signal.price.toString().split('.');
                           var price = str_price[0] + ' ' +  str_price[0]+'0';
                       }
                       var action = price + ' ' +  (signal.direction == 1 ? " растет" : " падает ");
                       break;
                   }
                   case "indicator_tlb":
                   {
                       var action = signal.direction == 1 ? " открыты покупки " : " открыты продажи ";
                       break;
                   }

                   case "rsi_tlb":
                   {
                       var action = signal.direction == 1 ? " rsi тлб бай " : " rsi тлб селл ";
                       break;
                   }
                   case "rsi_nh":
                   {
                       var action =  " rsi нью хай ";
                       break;
                   }
                   case "rsi_nl":
                   {
                       var action = " rsi нью лоу ";
                       break;
                   }
                   case "tsi_tlb":
                   {
                       var action = signal.direction == 1 ? " tsi тлб бай " : " tsi тлб селл ";
                       break;
                   }
                   case "tsi_nh":
                   {
                       var action =  " tsi нью хай ";
                       break;
                   }
                   case "tsi_nl":
                   {
                       var action = " tsi нью лоу ";
                       break;
                   }
                  case "adx_tlb":
                  {
                      var action = signal.direction == 1 ? " adx тлб бай " : " adx тлб селл ";
                      break;
                  }
                  case "adx_nh":
                  {
                      var action =  " adx нью хай ";
                      break;
                  }
                   case "adx_nl":
                   {
                       var action = " adx нью лоу ";
                       break;
                   }
                  case "ohlc_tlb":

                     if (signal.is_trading)
                     {
                        var action = signal.direction == 1 ? " торговое тлб бай " : " торговое тлб селл ";
                     }
                     else
                     {
                        var action = signal.direction == 1 ? " тлб бай " : " тлб селл ";
                     }

                     break;

                  case "indicator_double_swing":
                     var action = signal.direction == 1 ? " дабл свинг вверх на индикаторе" : " дабл свинг вниз на индикаторе";
                     break;

                   case "ohlc_double_swing":
                       var action = signal.direction == 1 ? " дабл свинг вверх " : " дабл свинг вниз ";
                   break;

                   case "order_closed_by_sl":
                     var action = " cделка " + (signal.direction == 1 ? "бай" : "селл ") + " закрыта по стопу";
                     break;

                  case "order_closed_by_be":
                     var action = " cделка " + (signal.direction == 1 ? "бай" : "селл ") + " закрыта по отбойнику";
                     break;

                  case "order_closed_by_tp":
                     var action = " cделка " + (signal.direction == 1 ? "бай" : "селл ") + " закрыта по ТП";
                     break;

                  default:
                     crit('no such tag');
               }

               var period = Number(signal.period);

               var text  = this.pair__ru_name(signal.instrument) + " " + (period ? this.period__voice_word(period) : "") + " "  + action;
               h.speech.say(text);
               h.beep();

               this.stopSpechID = setTimeout(() => {
                  h.speech.cancel();
               }, 9000);
            }
         },


      period__voice_word(period)
      {
         switch (period)
         {
           case 1: return "минутки";
           case 5: return "5 мин";
           case 15: return "15 мин";
           case 30: return "30 мин";
           case 60: return "1 час";
           case 240: return "4 часа";
           case 1440: return "дни";
         }

         return period;
      },

      pair__ru_name(pair)
      {
         switch (pair.substr(0, 6))
         {
             case "US_DX": return "индекс";
             case "XAUUSD": return "золото";
             case "BTCUSD": return "биткоин";
             case "EURUSD": return "евро";
             case "EURJPY": return "евро ена";
             case "NZDJPY": return "новозеландия ена";
             case "USDJPY": return "ена";
             case "GBPUSD": return "фунт";
             case "GBPCHF": return "фунт франк";
             case "GBPJPY": return "джиджей";
             case "CADJPY": return "канада ена";
             case "AUDUSD": return "австралия";
             case "AUDJPY": return "австралия ена";
             case "USDCAD": return "канада";
             case "NZDUSD": return "новозеландия";
             case "USDNOK": return "доллар нок";

             default:
                return pair;
          }
      },
       process_rsi_change(item)
       {

           var text  = this.pair__ru_name(item.pair) + (item.direction === 0 ? ' падает ' : ' растет ');
           h.speech.say(text);
           h.beep();
       },

       process_beep_item2(item)
       {
           var period = Number(item.period);

           var text = this.pair__ru_name(item.pair) + " " + (period ? this.period__voice_word(period) : "") + " "  + (item.direction === 0 ? ' падает ' : ' растет ' ) + ' от пивота';
           h.speech.say(text);
           h.beep();
       },

       process_beep_item(item)
       {
           if (item.period == 1440 || item.period == 240)
           {
               var filename = 'beep3.wav';
           }
           else
           {
               var filename = item.direction == 1 ? 'new/percussion-hit-dough.wav' : 'new/percussion-hit-conflict.wav';
           }


           beep_signal(filename, item.beep);
           var period = Number(item.period);
           var text = this.pair__ru_name(item.pair) + " " + (period ? period : "") + " "  + (item.direction === 0 ? ' падает ' : ' растет ' );
           h.speech.say(text);
       },

      process_voice_ripper_events(events)
      {
         var self = this;
         var is_history_empty = self.ripper_events.length === 0;
         var is_pushed_new_item = false;

         events.map( (i) => {
            var found = _.find(self.ripper_events, {id: i.id});

            if(!found)
            {
//               console.log('Push new ripper_event', i.id, ' last_ripper_event_id ', self.last_ripper_event_id );
//               console.log(i.event, i.params.symbol, ' ' , i.params.type);

               self.ripper_events.unshift(i);
               if(i.id > self.last_ripper_event_id) {
                  self.last_ripper_event_id = i.id;
               }

               if (self.is_voice_setuped && !is_history_empty)
               {
                  self.ripperEventVoiceNotify(i);
               }
            }
         });
      },

      process_voice_events(signals)
      {
         var self = this;
         var is_history_empty = self.signals_feed.length === 0;
         var is_pushed_new_item = false;

         signals.map( (i) => {
            var found = _.find(self.signals_feed, (item) => {
               return item.id == i.id;
            });

            if(!found)
            {
//               console.log('Push new event', i.id, ' last_signal_id ', self.last_signal_id );
//               console.log(i.tag, (i.direction == 1 ? 'blue' : 'red'), i.instrument, ' period ' , i.period);
               self.signals_feed.unshift(i);

               if(i.id > self.last_signal_id)
               {
                  self.last_signal_id = i.id;
               }

               if(!is_history_empty) {
                  self.signals_counter++;
               }

               if (self.is_voice_setuped && !is_history_empty)
               {
                   self.signalVoiceNotify(i);

                   this.$root.$emit('signal__new', {
                       symbol: i.instrument
                   });
               }
            }
         });
      },

      beep_every_15_min()
      {
         var self = this;
         setInterval(async () => {
            let minutes = moment().minutes();
            let val = Math.ceil(minutes / 15);

            if(self.last_15min === null)
            {
               self.last_15min = val;
            } else {
               if(self.last_15min != val)
               {
                  self.last_15min = val;
                  if (this.is_voice_notify)
                  {
                     (new Audio("/sounds/beep2.wav")).play();
                  }
               }
            }
         }, 5000 );
      },

      beep_every_hour()
      {
         var self = this;
         setInterval(() => {
            let val = moment().hour();

            if(self.last_hour === null)
            {
               self.last_hour = val;
            } else {
               if(self.last_hour != val)
               {
                  self.last_hour = val;
                  if (this.is_voice_notify)
                  {
                     (new Audio("/sounds/beep.wav")).play();
                     fork(() => {
                        h.speech.say("Влад, времени почти не осталось, где деньги? Будь внимательным и осторожным, не спеши.");
                        }, 3000);
                  }
               }
            }
         }, 5000 );
      }
   }
};


