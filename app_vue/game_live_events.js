import h from './helpers';
import moment from "moment";

export default {

   data() {
      return {
            frontEventsID: null,
            EVENTS_PING: 1000,
            is_proccessing_price_movements: false,
            price_movements: []
      }
   },

   computed: {


   },

   mounted()
   {
      var self = this;

   },

   watch:{

   },
   methods: {

        setup_frontend_events()
        {
            var self = this;
            var is_processing = false;

            return setInterval(async function () {
                is_processing = true;
                var rEvents = await self.runTask('get__game__frontend__events__task', {

                });


                self.$root.$refs.game.UpdateOpenOrders(rEvents.resp.open_orders);
                self.$root.$refs.game.UpdatePrices(rEvents.resp.prices, rEvents.resp.updated_time);

                self.proccess_voice_notify_queue(rEvents.resp.voice_notify);

                is_processing = false;
            }, this.EVENTS_PING);
        },

        ripperEventVoiceNotify(e)
        {
            switch (e.event)
            {
                case "open_market_order":
                    var action = " открыт " + (e.params.type == "buy" ? "бай " : " селл ") + e.params.risk;
                    var text  = this.pair__ru_name(e.params.symbol) + action;
                break;

                case "order_set_auto_be":
                    var action = " отбойник выставлен на " + (e.params.type == "buy" ? "бай " : " селл ");
                    var text  = this.pair__ru_name(e.params.symbol) + action;
                break;

                default:
                  crit('no such tag');
            }

            h.speech.say(text);
        },


       async proccess_voice_notify_queue(voice_notify)
       {
           if (voice_notify)
           {
               //h.speech.cancel();
               h.speech.say(voice_notify);
           }
       },

        process_voice_ripper_events(events)
        {
            var self = this;
            var is_history_empty = self.ripper_events.length === 0;
            var is_pushed_new_item = false;

            events.map( (i) => {
                var found = _.find(self.ripper_events, {id: i.id});

                if(!found)
                {
                //               console.log('Push new ripper_event', i.id, ' last_ripper_event_id ', self.last_ripper_event_id );
                //               console.log(i.event, i.params.symbol, ' ' , i.params.type);

                    self.ripper_events.unshift(i);
                    if(i.id > self.last_ripper_event_id) {
                      self.last_ripper_event_id = i.id;
                    }

                    if (self.is_voice_setuped && !is_history_empty)
                    {
                      self.ripperEventVoiceNotify(i);
                    }
                }
            });
        },
   }
};


