#!/usr/bin/env bash

dest_folder='~/saas-pack-releases'
timestamp=`date +%s`
git_url='https://gitlab.com/youngcol/saas-pack.git'
env_folder='~/deploy'
#==================================

# ln -s ~/$dest_folder/$timestamp convelly.com

ssh uawenzyh@198.54.116.76 -p21098 << EOF

    cd $dest_folder
    echo "dest_folder:" $dest_folder/$timestamp
    #mkdir $timestamp
    git clone $git_url $timestamp
    cd  $dest_folder/$timestamp
    cp $env_folder/.env .env
    composer install
    ./scripts/install.sh
    php partisan migrate:up
    php partisan run:seed
    #php partisan compile:languages
    #npm i
    #npm run production
    #npm
    #php partisan prepare:production:build
    echo 'Build ready: ' + $timestamp
    cd ~ && rm convelly.com && ln -s $dest_folder/$timestamp/htdocs convelly.com
    echo 'Build in use: ' + $dest_folder/$timestamp
EOF

#"scripts": {
#		"post-create-project-cmd": "bash -c 'chmod 777 ./log && chmod 777 ./public/uploads && mv ./.env.dist ./.env'",
#		"post-update-cmd": "bash -c 'chmod 0755 ./version.sh; ./version.sh ${JOB_NAME} ${BUILD_NUMBER} ${GIT_BRANCH} > ./version.json'",
#		"post-install-cmd": "bash -c 'chmod 0755 ./version.sh; ./version.sh ${JOB_NAME} ${BUILD_NUMBER} ${GIT_BRANCH} > ./version.json'"
#	}
