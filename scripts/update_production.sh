#!/usr/bin/env bash

dest_folder='~/saas-pack-releases'
timestamp=`date +%s`
git_url='https://gitlab.com/youngcol/saas-pack.git'
env_folder='~/deploy'
#==================================

# ln -s ~/$dest_folder/$timestamp convelly.com

ssh uawenzyh@198.54.116.76 -p21098 << EOF

    cd convelly.com
    git pull
    php ./../partisan migrate:up
    php ./../partisan run:seed
    echo 'Build updated: '$timestamp

EOF

#"scripts": {
#		"post-create-project-cmd": "bash -c 'chmod 777 ./log && chmod 777 ./public/uploads && mv ./.env.dist ./.env'",
#		"post-update-cmd": "bash -c 'chmod 0755 ./version.sh; ./version.sh ${JOB_NAME} ${BUILD_NUMBER} ${GIT_BRANCH} > ./version.json'",
#		"post-install-cmd": "bash -c 'chmod 0755 ./version.sh; ./version.sh ${JOB_NAME} ${BUILD_NUMBER} ${GIT_BRANCH} > ./version.json'"
#	}
