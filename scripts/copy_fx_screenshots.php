<?php


//cd "/home/ttt/.wine/drive_c/Program Files (x86)/BDSwiss Global MetaTrader 4/MQL4/Files/SnapShotl"
//ls -la
//cp * "/home/ttt/apps/devilbox/data/www/charts/htdocs/screenshots" -r
//rm -r *
$folder = "/home/ttt/.wine/drive_c/Program Files (x86)/BDSwiss Global MetaTrader 4/MQL4/Files/SnapShotl";

//$UPLOAD_URL = "http://charts.loc/screenshot/upload";
$UPLOAD_URL = "http://2071212.td430236.web.hosting-test.net/screenshot/upload";



while(1)
{
    $seconds = date("s");

    if ($seconds > 10) {
        var_dump(date("r"));
        upload_files($folder);
    }

    sleep(10);
}

function upload_files($folder)
{
    $pairs = scandir($folder);

    foreach ($pairs as $pair) {
        if (strlen($pair) < 3) {
            continue;
        }

        var_dump($pair);

        $tags = scandir($folder.'/'.$pair);

        foreach ($tags as $tag) {
            if ($tag == '.' || $tag == '..') {
                continue;
            }

            $periods = scandir($folder . '/' . $pair . '/' . $tag);

            foreach ($periods as $period) {
                if ($period == '.' || $period == '..') {
                    continue;
                }

                $currentFolder = $folder . '/' . $pair . '/' . $tag. '/' . $period;
                $screenshots = scandir($currentFolder);
                $temp = [];
                array_shift($screenshots);
                array_shift($screenshots);
                sort($screenshots);
                $lastScreenshot = array_pop($screenshots);

                if ($lastScreenshot) {
                    var_dump([$tag, $period, $lastScreenshot]);
                    send_screenshot($tag, $period, $pair, $currentFolder . '/' . $lastScreenshot);

                    array_push($screenshots, $lastScreenshot);
                    remove_screenshots($currentFolder, $screenshots);
                }
            }
        }
    }

}


function send_screenshot($tag, $period, $instrument, $file)
{
    global $UPLOAD_URL;
    $curlFile = new CURLFile($file);

    $data = [
        'tag' => $tag,
        'period' => $period,
        'instrument' => $instrument,
        'file' => $curlFile
    ];

    $ch = curl_init();

    $curl = curl_init($UPLOAD_URL);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($curl);
    var_dump($response);

    curl_close($curl);
}


function remove_screenshots($folder, $screenshots)
{
    foreach ($screenshots as $item) {
        if (file_exists($folder . '/' . $item)) {
            unlink($folder . '/' . $item);
        }
    }
}
