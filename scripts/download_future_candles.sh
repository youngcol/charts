#!/usr/bin/env bash

echo "alias php='/usr/local/php72/bin/php'" >> ~/.bashrc && source ~/.bashrc
export PATH=/usr/local/php72/bin:$PATH

cd someshops.com.ua/charts/
php partisan download:future:candles:all:instruments
